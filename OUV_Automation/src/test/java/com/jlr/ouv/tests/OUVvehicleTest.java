package com.jlr.ouv.tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.ouv.constants.Constants;
import com.jlr.ouv.containers.OUVvehicleContainer;
import com.jlr.ouv.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class OUVvehicleTest extends TestBaseCC {

	public WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(OUVCalendarTest.class.getName());
	public OUVvehicleContainer OUVvehicleContainer = PageFactory.initElements(driver, OUVvehicleContainer.class);
	JavaScriptUtil javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	public void onStart() {
		setupTest("OUVTest");
		driver = getDriver();
		OUVvehicleContainer = PageFactory.initElements(driver, OUVvehicleContainer.class);
		javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	}

//User Logins to OUV Portal
	@Given("^Access to the OUV Portal with user \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void access_to_the_OUV_Portal_with_user_and_password_as(String UserName, String Password) throws Throwable {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		// User Logins to OUV Portal
		LOGGER.debug("Test Begin......");
		Reporter.addStepLog("User Access OUV Portal");
		onStart();
		driver.get(Constants.OUVURL);
		ActionHandler.wait(7);
		Reporter.addStepLog("User Logins to OUV Portal");

		ActionHandler.setText(OUVvehicleContainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVvehicleContainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(20);

		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.Verifycode)) {
			ActionHandler.wait(60);

			ActionHandler.click(OUVvehicleContainer.vCodeTextBox);
			ActionHandler.wait(5);
			Reporter.addStepLog("User have entered Verification Code");
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(OUVvehicleContainer.verifyBtn);
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			navigateToOUV();
		}
		ActionHandler.waitForElement(OUVvehicleContainer.welcome, 60);

		if (!VerifyHandler.verifyElementPresent(OUVvehicleContainer.welcome)) {
			ActionHandler.wait(4);
			driver.navigate().refresh();
			ActionHandler.waitForElement(OUVvehicleContainer.welcome, 60);
		}
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.welcome);
	}

//User navigate to SVO Portal
	public void navigateToOUV() throws Exception {
		String OUV = "OUV";
		ActionHandler.wait(3);
		Reporter.addStepLog("User tries to navigate to OUV Portal");
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.jlrText)) {
			ActionHandler.wait(2);
			ActionHandler.click(OUVvehicleContainer.menuBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(OUVvehicleContainer.searchBox, OUV);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.selectMenu(OUV))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

//Logout from OUV Portal
	@Then("^Logout from the OUV$")
	public void Logout_the_OUV() throws Exception {
		ActionHandler.wait(5);
		javascriptUtil.clickElementByJS(OUVvehicleContainer.homeTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// Logout from OUV Portal
		ActionHandler.click(OUVvehicleContainer.loginImg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.logout);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Logout from OUV Portal");
		driver.quit();
	}

	// User navigates to OUV vehicles
	@Then("^User navigate to OUV vehicles$")
	public void user_navigate_to_OUV_vehicles() throws Throwable {

		// User navigates to OUV vehicles
		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(OUVvehicleContainer.OUVvehicles);
		ActionHandler.wait(10);
		ActionHandler.waitForElement(OUVvehicleContainer.searchVehiclebox, 60);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV vehicles");
	}

	// Search for an vehicle
	@Then("^search for the vehicle with status \"([^\"]*)\"$")
	public void search_for_the_vehicle_with_status(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// Search for an vehicle
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVvehicleContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Vehicles");

		ActionHandler.click(OUVvehicleContainer.searchVehiclebox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVvehicleContainer.searchVehiclebox, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an vehicle");
		CommonFunctions.attachScreenshot();
	}

//search for the vehicle in the list with status 
	@Then("^search for the vehicle in the list with status \"([^\"]*)\"$")
	public void search_for_the_vehicle_in_the_list_with_status(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// Search for an vehicle
		ActionHandler.click(OUVvehicleContainer.searchVehiclebox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVvehicleContainer.searchVehiclebox, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an vehicle");
		CommonFunctions.attachScreenshot();
	}

	// Select vehicle
	@Then("^select the vehicle$")
	public void select_the_vehicle() throws Throwable {

		// Select vehicle
		ActionHandler.click(OUVvehicleContainer.VehicleSel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select vehicle");
		CommonFunctions.attachScreenshot();
	}

	// User edits master status
	@Then("^User edits master status to \"([^\"]*)\"$")
	public void user_edits_master_status_to(String status) throws Throwable {
		String Sea[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		// edit master dropdown
		ActionHandler.click(OUVvehicleContainer.editMasterstatus);
		ActionHandler.wait(7);
		Reporter.addStepLog("edit master dropdown");
		CommonFunctions.attachScreenshot();

		// Select master dropdown
		ActionHandler.click(OUVvehicleContainer.Masterstatus);
		ActionHandler.wait(7);
		Reporter.addStepLog("Select master dropdown");
		CommonFunctions.attachScreenshot();

		// Select dropdown
		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(status))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Select dropdown");
		CommonFunctions.attachScreenshot();
	}

	// User navigate to Keys transfer option and select
	@Then("^User navigate to Keys transfer option and select it$")
	public void user_navigate_to_Keys_transfer_option_and_select_it() throws Throwable {

		// Click on full vin
		ActionHandler.click(OUVvehicleContainer.Fullvin);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on full vin");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		// Transfered keys button is already selected
		if (VerifyHandler.verifyChkBoxRadioButtonSelected(OUVvehicleContainer.Transferedkeys)) {
			ActionHandler.wait(3);
			Reporter.addStepLog("Transfered keys button is already selected");
			CommonFunctions.attachScreenshot();
		}

		else {

			// Click on Transfered keys button
			ActionHandler.click(OUVvehicleContainer.Transferedkeys);
			ActionHandler.wait(3);
			Reporter.addStepLog("Click on Transfered keys button");
			CommonFunctions.attachScreenshot();
		}
	}

	// save the changes performed
	@Then("^save the changes performed$")
	public void save_the_changes_performed() throws Throwable {

		// Click on the save button
		ActionHandler.wait(5);
		ActionHandler.click(OUVvehicleContainer.save);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on the save button");
		CommonFunctions.attachScreenshot();
	}

	// Verify the displayed status error message
	@Then("^Verify the displayed status error message$")
	public void verify_the_displayed_status_error_message() throws Throwable {

		// Verify the error message
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.StatusError);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the error message");
		CommonFunctions.attachScreenshot();

		// The status has not updated
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("The status has not updated");
		CommonFunctions.attachScreenshot();

		// Click on the cancel button
		ActionHandler.click(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on the cancel button");
		CommonFunctions.attachScreenshot();
	}

	// Verify the displayed status error message for Primary Location
	@Then("^Verify the displayed status error message for Primary Location$")
	public void verify_the_displayed_status_error_message_for_Primary_Location() throws Throwable {

		// Verify the error message
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Primarylocationerror);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the error message");
		CommonFunctions.attachScreenshot();

		// The location has not been updated
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("The location has not been updated");
		CommonFunctions.attachScreenshot();

	}

	// User adds Justification
	@Then("^User adds Justification$")
	public void user_adds_Justification() throws Throwable {

		// User enters Justification
		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		// javascriptUtil.scrollIntoView(OUVvehicleContainer.JustificationHeader);
		ActionHandler.wait(2);
		ActionHandler.setText(OUVvehicleContainer.Justification, "text");
		ActionHandler.wait(3);
		Reporter.addStepLog("User enters Justification");
		CommonFunctions.attachScreenshot();

	}

	// User enters Primary Location
	@Then("^User enters Primary Location$")
	public void user_enters_Primary_Location() throws Throwable {

		// User enters Primary Location
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.Primarylocationerror);
		ActionHandler.wait(2);
		ActionHandler.setText(OUVvehicleContainer.Primarylocation, "Test123");
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Primary Location");

	}

	// User edits sub status
	@Then("^User edits sub status to \"([^\"]*)\"$")
	public void user_edits_sub_status_to(String status) throws Throwable {
		String Sea[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.wait(3);

		// Select sub status dropdown
		ActionHandler.click(OUVvehicleContainer.substatus);
		ActionHandler.wait(3);
		Reporter.addStepLog("Select sub status dropdown");
		CommonFunctions.attachScreenshot();

		// Select dropdown
		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(status))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Select dropdown");
		CommonFunctions.attachScreenshot();
	}

	// validate the status change to Handed In from Live
	@Then("^validate the status change to Handed In from Live$")
	public void validate_the_status_change_to_Handed_In_from_Live() throws Throwable {
		ActionHandler.pageScrollDown();

		// Verify the Proposed Hand In
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.ProposedHandIn);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Proposed Hand In");
		CommonFunctions.attachScreenshot();

		// Verify the Latest Planned Hand In
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.LatestPlannedHand);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Latest Planned Hand In");
		CommonFunctions.attachScreenshot();

		// Verify the Fore cast Actual Hand In
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.ForecastActual);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Fore cast Actual Hand In");
		CommonFunctions.attachScreenshot();

		// Verify the Default Last Fleet
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.DefaultLastFleet);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Default Last Fleet");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageUp();
		ActionHandler.wait(3);
		ActionHandler.pageUp();

		// Verify the OUV Reporting Level 1
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.DefaultreportingLv1);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the OUV Reporting Level 1");
		CommonFunctions.attachScreenshot();

		// Verify the OUV Reporting Level 2
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.DefaultreportingLv2);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the OUV Reporting Level 2");
		CommonFunctions.attachScreenshot();

		// Verify the OUV Reporting Level 3
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.DefaultreportingLv3);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the OUV Reporting Level 3");
		CommonFunctions.attachScreenshot();

		// Verify the Default Fleet
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.DefaultFleet);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Default Fleet");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageUp();
		ActionHandler.wait(3);

		// Verify the status change to Handed In
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.HandedIn);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the status change to Handed In");
		CommonFunctions.attachScreenshot();

		// Verify the Default Owner
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.DefaultOwner);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Default Owner");
		CommonFunctions.attachScreenshot();
	}

	// Verify the displayed substatus error message
	@Then("^Verify the displayed substatus error message$")
	public void verify_the_displayed_substatus_error_message() throws Throwable {
		// Verify the error message
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.SubStatusError);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the error message");
		CommonFunctions.attachScreenshot();

		// The status has not updated
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("The status has not updated");
		CommonFunctions.attachScreenshot();

		// Click on the save button
		ActionHandler.click(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on the save button");
		CommonFunctions.attachScreenshot();
	}

	// Verify the displayed Keys transfer error message
	@Then("^Verify the displayed Keys transfer error message$")
	public void verify_the_displayed_Keys_transfer_error_message() throws Throwable {

		// Verify the error message
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.KeysTransferred);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the error message");
		CommonFunctions.attachScreenshot();

		// The status has not updated
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("The status has not updated");
		CommonFunctions.attachScreenshot();

		// Click on the save button
		ActionHandler.click(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on the save button");
		CommonFunctions.attachScreenshot();
	}

	// User edits sub status to None
	@Then("^User edits sub status to None$")
	public void user_edits_sub_status_to_None() throws Throwable {
		ActionHandler.wait(3);

		// Select sub status dropdown
		ActionHandler.click(OUVvehicleContainer.substatus);
		ActionHandler.wait(3);
		Reporter.addStepLog("Select sub status dropdown");
		CommonFunctions.attachScreenshot();

		// Select dropdown as none
		ActionHandler.click(OUVvehicleContainer.subnone);
		ActionHandler.wait(3);
		Reporter.addStepLog("Select dropdown as none");
		CommonFunctions.attachScreenshot();
	}

	// select and edit the vehicle name
	@Then("^select and edit the vehicle	name$")
	public void select_and_edit_the_vehicle_name() throws Throwable {

		// User is able to edit the Vehicle Name
		ActionHandler.wait(5);
		Actions action = new Actions(driver);
		action.moveToElement(OUVvehicleContainer.EditVehicleName).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVvehicleContainer.EditVehicleNamefield);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to edit the Vehicle Name");
	}

	// Validate that user should be able to save the changes to the record
	@Then("^Validate that user should be able to save the changes to the record$")
	public void validate_that_user_should_be_able_to_save_the_changes_to_the_record() throws Throwable {

		// User is able to save the Vehicle Name
		ActionHandler.click(OUVvehicleContainer.SaveVehicleName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to save the Vehicle Name");

		// User verifies the error message should not be seen
		if (!VerifyHandler.verifyElementPresent(OUVvehicleContainer.VehicleNameError)) {
			Reporter.addStepLog("User verifies the error message should not be seen");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

	}

	// Validate that user should not be able to save the changes to the record
	@Then("^Validate that user should not be able to save the changes to the record$")
	public void validate_that_user_should_not_be_able_to_save_the_changes_to_the_record() throws Throwable {

		// User is not able to save the Vehicle Name
		ActionHandler.click(OUVvehicleContainer.SaveVehicleName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is not able to save the Vehicle Name");

		// User verifies the error message should be displayed
		if (!VerifyHandler.verifyElementPresent(OUVvehicleContainer.VehicleNameError)) {
			Reporter.addStepLog("User verifies the error message should be displayed");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

		// User is able to click on Cancel Button
		ActionHandler.click(OUVvehicleContainer.CancelSaveVehicleName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to click on Cancel Button");
	}

	// Enter the Vehicle Name as
	@Then("^Enter the Vehicle Name as \"([^\"]*)\"$")
	public void enter_the_Vehicle_Name_as(String Name) throws Throwable {

		// User enters the Vehicle Name
		String Na[] = Name.split(",");
		Name = CommonFunctions.readExcelMasterData(Na[0], Na[1], Na[2]);

		ActionHandler.clearAndSetText(OUVvehicleContainer.VehicleName, Name);
		ActionHandler.wait(2);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(2);
		Reporter.addStepLog("User enters the Vehicle Name");
		CommonFunctions.attachScreenshot();

	}

	// User searches for an OUV Vehicle on their fleet Search
	@Then("^User searches for an OUV Vehicle on their fleet Search \"([^\"]*)\"$")
	public void user_searches_for_an_OUV_Vehicle_on_their_fleet_Search(String Select) throws Throwable {

		String PR[] = Select.split(",");
		Select = CommonFunctions.readExcelMasterData(PR[0], PR[1], PR[2]);

		// User selects the OUVVehicles
		ActionHandler.click(OUVvehicleContainer.SelectOUVVehicles);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects the OUVVehicles ");
		CommonFunctions.attachScreenshot();

		// User selects the All
		ActionHandler.click(OUVvehicleContainer.SelectAll);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects the All");
		CommonFunctions.attachScreenshot();

		// User search the OUVVehicles
		ActionHandler.click(OUVvehicleContainer.SearchOUVVehicles);
		ActionHandler.wait(5);
		Reporter.addStepLog("User search the OUVVehicles ");
		CommonFunctions.attachScreenshot();

		// User search the OUVVehicles
		ActionHandler.setText(OUVvehicleContainer.SearchOUVVehicles, Select);
		ActionHandler.wait(5);
		Reporter.addStepLog("User search the OUVVehicles ");
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to search an OUV Vehicle on their fleet");
	}

	// User selects the vehicle to open the vehicle related page
	@Then("^User selects the vehicle to open the vehicle related page$")
	public void user_selects_the_vehicle_to_open_the_vehicle_related_page() throws Throwable {
		// User able to selects the OUV Vehicle
		ActionHandler.click(OUVvehicleContainer.FleetVehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User able to selects the OUV Vehicle");
	}

	// Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys
	// transferred are checked
	@Then("^Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked$")
	public void validate_that_the_fields_Transfer_No_of_Keys_Transferred_Transfer_Keys_transferred_are_checked()
			throws Throwable {

		// User able to selects Edit button
		ActionHandler.click(OUVvehicleContainer.SelectEdit1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to selects Edit button");
		CommonFunctions.attachScreenshot();

		// Transfered keys button is already selected
		if (VerifyHandler.verifyChkBoxRadioButtonSelected(OUVvehicleContainer.Transferedkeys)) {
			ActionHandler.wait(3);
			Reporter.addStepLog("Transfered keys button is already selected");
			CommonFunctions.attachScreenshot();
		}

		else {

			// Click on Transfered keys button
			ActionHandler.click(OUVvehicleContainer.Transferedkeys);
			ActionHandler.wait(3);
			Reporter.addStepLog("Click on Transfered keys button");
			CommonFunctions.attachScreenshot();
		}
		CommonFunctions.attachScreenshot();
		ActionHandler.pageUp();
		ActionHandler.wait(5);
		ActionHandler.pageUp();
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to Validate that the fields Transfer");

	}

	// Select any other fleet from the search Fleet
	@Then("^Select any other fleet from the search Fleet \"([^\"]*)\"$")
	public void select_any_other_fleet_from_the_search_Fleet(String ChangeFt) throws Throwable {

		String Ft[] = ChangeFt.split(",");
		ChangeFt = CommonFunctions.readExcelMasterData(Ft[0], Ft[1], Ft[2]);

		// User able to ClickCancel
		ActionHandler.click(OUVvehicleContainer.ClickCancel);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to ClickCancel");
		CommonFunctions.attachScreenshot();

		// User able to SearchFleet
		ActionHandler.click(OUVvehicleContainer.SearchFleet);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to SearchFleet");
		CommonFunctions.attachScreenshot();
		// User changes fleet
		ActionHandler.setText(OUVvehicleContainer.SearchFleet, ChangeFt);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to SearchFleet");
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to selects any other fleet from the search Fleet");
	}

	// Update OUV reporting Levels to categories that do not match the new fleet
	// LevelOne
	@Then("^Update OUV reporting Levels to categories that do not match the new fleet LevelOne \"([^\"]*)\" LevelTwo \"([^\"]*)\" LevelThree \"([^\"]*)\"$")
	public void update_OUV_reporting_Levels_to_categories_that_do_not_match_the_new_fleet_LevelOne_LevelTwo_LevelThree(
			String Level11, String Level22, String Level33) throws Throwable {

		String L1[] = Level11.split(",");
		Level11 = CommonFunctions.readExcelMasterData(L1[0], L1[1], L1[2]);

		String L2[] = Level22.split(",");
		Level22 = CommonFunctions.readExcelMasterData(L2[0], L2[1], L2[2]);

		String L3[] = Level33.split(",");
		Level33 = CommonFunctions.readExcelMasterData(L3[0], L3[1], L3[2]);

		// User able to select dependencies
		ActionHandler.click(OUVvehicleContainer.dependencies);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select dependencies");
		CommonFunctions.attachScreenshot();

		// User able to select Level1
		ActionHandler.click(OUVvehicleContainer.Level1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select Level1");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(Level11))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User able to select level2
		ActionHandler.click(OUVvehicleContainer.Level2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select level2");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(Level22))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User able to select level3
		ActionHandler.click(OUVvehicleContainer.Level3);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select level3");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(Level33))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.Apply);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Update OUV reporting Levels to categories that do not match the new fleet");

	}

	// User clicks on save button
	@Then("^User clicks on save button$")
	public void user_clicks_on_save_button() throws Throwable {

		// User able to clicks on save button
		ActionHandler.click(OUVvehicleContainer.Saved);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able click save button");
		CommonFunctions.attachScreenshot();

		// User able to verify the error
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Verifyerror);
		ActionHandler.wait(3);
		Reporter.addStepLog("User able to verify the error");
		CommonFunctions.attachScreenshot();

		// User able to select home tab
		javascriptUtil.clickElementByJS(OUVvehicleContainer.homeTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User able to select home tab");
		CommonFunctions.attachScreenshot();

		// User able to select discard changes
		ActionHandler.click(OUVvehicleContainer.DiscardChanges);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select discard changes");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to clicks on save button");
	}

	// Validate that the field Transfer - No of Keys Transferred & Transfer - Keys
	// transferred are checked
	@Then("^Validate that the field Transfer - No of Keys Transferred & Transfer - Keys transferred are checked$")
	public void validate_that_the_field_Transfer_No_of_Keys_Transferred_Transfer_Keys_transferred_are_checked()
			throws Throwable {
		ActionHandler.pageUp();
		ActionHandler.wait(5);
		ActionHandler.pageUp();
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(OUVvehicleContainer.EditCheck);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Validate that the field Transfer");
	}

	// User clicks on saved button
	@Then("^User clicks on saved button$")
	public void user_clicks_on_saved_button() throws Throwable {

		// User able to clicks on save button
		ActionHandler.click(OUVvehicleContainer.Saved);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to clicks on save button");
		CommonFunctions.attachScreenshot();

		// User able to verify the error
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.verifyerror1);
		ActionHandler.wait(3);
		Reporter.addStepLog("User able to verify the error");
		CommonFunctions.attachScreenshot();

		// User able to select home tab
		javascriptUtil.clickElementByJS(OUVvehicleContainer.homeTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User able to select home tab");
		CommonFunctions.attachScreenshot();

		// User able to select discard changes
		ActionHandler.click(OUVvehicleContainer.DiscardChanges);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select discard changes");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to clicks on save button");
	}

	// User selects the vehicle to open the vehicle related pages
	@Then("^User selects the vehicle to open the vehicle related pages$")
	public void user_selects_the_vehicle_to_open_the_vehicle_related_pages() throws Throwable {

		// User able to selects the vehicle to open the vehicle related pages
		ActionHandler.click(OUVvehicleContainer.FleetVehicle1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to selects the vehicle to open the vehicle related pages");
	}

	// Validate that New Requested Hand in Approval status should be approved
	@Then("^Validate that New Requested Hand in Approval status should be approved / New$")
	public void validate_that_New_Requested_Hand_in_Approval_status_should_be_approved_New() throws Throwable {

		// User able to Validate that New Requested Hand in Approval status
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.VerifyApproveStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Validate that New Requested Hand in Approval status");

	}

	// User tries to edit the New Requested Hand In Date on OUV Vehicle window
	// HandinDate
	@Then("^User tries to edit the New Requested Hand In Date on OUV Vehicle window HandinDate$")
	public void user_tries_to_edit_the_New_Requested_Hand_In_Date_on_OUV_Vehicle_window_HandinDate() throws Throwable {

		// User able to select edit hand date
		ActionHandler.click(OUVvehicleContainer.EditHandDate);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select edit hand date");
		CommonFunctions.attachScreenshot();

		// select future date
		String HandinDate;
		HandinDate = CommonFunctions.selectFutureDate();

		ActionHandler.clearAndSetText(OUVvehicleContainer.EnterHandDate, HandinDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to edit the New Requested Hand In Date on OUV Vehicle window HandinDate");

	}

	// Enter any free text in justification on OUV Vehicle window and click on Save
	// Justification
	@Then("^Enter any free text in justification on OUV Vehicle window and click on Save Justification \"([^\"]*)\"$")
	public void enter_any_free_text_in_justification_on_OUV_Vehicle_window_and_click_on_Save_Justification(
			String Justification) throws Throwable {

		String JS[] = Justification.split(",");
		Justification = CommonFunctions.readExcelMasterData(JS[0], JS[1], JS[2]);

		// User able to verify approve status
		ActionHandler.click(OUVvehicleContainer.VerifyApproveStatus);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to verify approve status");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		// User able to select enter justification
		ActionHandler.click(OUVvehicleContainer.EnterJustification);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select enter justification");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVvehicleContainer.EnterJustification, Justification);
		CommonFunctions.attachScreenshot();

		// User able to select save
		ActionHandler.click(OUVvehicleContainer.Save1);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select save");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Enter any free text in justification on OUV Vehicle window");
	}

	// Validate that New Requested Hand in Approval status is changed to Pending
	// Approval status
	@Then("^Validate that New Requested Hand in Approval status is changed to Pending Approval status$")
	public void validate_that_New_Requested_Hand_in_Approval_status_is_changed_to_Pending_Approval_status()
			throws Throwable {

		// User able to Validate that New Requested Hand in Approval status is changed
		// to Pending Approval status
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.VerifyPendingApproveStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User able to Validate that New Requested Hand in Approval status is changed to Pending Approval status");
	}

	// User selects the vehicle to open the vehicle related pagess
	@Then("^User selects the vehicle to open the vehicle related pagess$")
	public void user_selects_the_vehicle_to_open_the_vehicle_related_pagess() throws Throwable {

		// User able to select fleet vehicle
		ActionHandler.click(OUVvehicleContainer.FleetVehicle2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select fleet vehicle ");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to selects the vehicle to open the vehicle");
	}

	// Validate that New Requested Hand in Approval status should be pending
	// approval / New
	@Then("^Validate that New Requested Hand in Approval status should be pending approval / New$")
	public void validate_that_New_Requested_Hand_in_Approval_status_should_be_pending_approval_New() throws Throwable {

		// User able to Validate that New Requested Hand in Approval status should be
		// pending approval
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.VerifyPendingApproveStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User able to Validate that New Requested Hand in Approval status should be pending approval");

	}

	// Verify that User cannot edit New Requested Hand In date if the New Requested
	// Hand In Apporval status is Pending Approval
	@Then("^Verify that User cannot edit New Requested Hand In date if the New Requested Hand In Apporval status is Pending Approval$")
	public void verify_that_User_cannot_edit_New_Requested_Hand_In_date_if_the_New_Requested_Hand_In_Apporval_status_is_Pending_Approval()
			throws Throwable {

		// User able to select save
		ActionHandler.click(OUVvehicleContainer.Save1);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select save");
		CommonFunctions.attachScreenshot();

		// User able to select verify error
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.VerifyError);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select verify error");
		CommonFunctions.attachScreenshot();

		// User able to select home tab
		javascriptUtil.clickElementByJS(OUVvehicleContainer.homeTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User able to select home tab");
		CommonFunctions.attachScreenshot();

		// User able to select discard changes
		ActionHandler.click(OUVvehicleContainer.discardchanges);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select discard changes");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Verify that User cannot edit New Requested Hand In date");
	}

	// User selects the vehicle to open the vehicle related pagesss
	@Then("^User selects the vehicle to open the vehicle related pagesss$")
	public void user_selects_the_vehicle_to_open_the_vehicle_related_pagesss() throws Throwable {

		// User able to selects the vehicle to open the vehicle
		ActionHandler.click(OUVvehicleContainer.FleetVehicle3);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to selects the vehicle to open the vehicle");
	}

	// Change Master status to Dispose and Sub Status to Write-Off Master Status
	// \"([^\"]*)\" Subststus
	@Then("^Change Master status to Dispose and Sub Status to Write-Off Master Status \"([^\"]*)\" Subststus \"([^\"]*)\"$")
	public void change_Master_status_to_Dispose_and_Sub_Status_to_Write_Off_Master_Status_Subststus(String MasterStatus,
			String subststus) throws Throwable {

		String JS1[] = MasterStatus.split(",");
		MasterStatus = CommonFunctions.readExcelMasterData(JS1[0], JS1[1], JS1[2]);

		String JS2[] = subststus.split(",");
		subststus = CommonFunctions.readExcelMasterData(JS2[0], JS2[1], JS2[2]);

		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		// User able to select edit master status
		ActionHandler.click(OUVvehicleContainer.EditMasterstatus);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select edit master status");
		CommonFunctions.attachScreenshot();

		// User able to select click master status
		ActionHandler.click(OUVvehicleContainer.ClickMasterStatus);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select click master status");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(MasterStatus))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User able to select sub status
		ActionHandler.click(OUVvehicleContainer.SubStatus);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select sub status");
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(subststus))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.Ordernum);
		ActionHandler.wait(5);

		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		/* ActionHandler.pageScrollDown(); */
		/*
		 * ActionHandler.scrollDown(); ActionHandler.scrollDown();
		 * ActionHandler.wait(5);
		 */
		Reporter.addStepLog("User able to Change Master status to Dispose and Sub Status to Write-Off Master Status");
	}

	// Provide details on Justification window Justification
	@Then("^Provide details on Justification window Justification \"([^\"]*)\"$")
	public void provide_details_on_Justification_window_Justification(String Justification1) throws Throwable {

		String JS3[] = Justification1.split(",");
		Justification1 = CommonFunctions.readExcelMasterData(JS3[0], JS3[1], JS3[2]);

		// User able to Provide details on Justification window
		ActionHandler.click(OUVvehicleContainer.TypeJustification);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVvehicleContainer.TypeJustification, Justification1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Provide details on Justification window");
	}

	// Click on Save
	@Then("^Click on Save$")
	public void click_on_Save() throws Throwable {
		// User able to Click on Save
		ActionHandler.click(OUVvehicleContainer.SaveStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Click on Save");
	}

	// Update New Extended Hand In date in past HandIndate
	@Then("^Update New Extended Hand In date in past HandIndate$")
	public void update_New_Extended_Hand_In_date_in_past_HandIndate() throws Throwable {

		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		// selects the yesterday's date in calendar
		String Hdate;
		Hdate = CommonFunctions.selectYesterdayDate();

		// User able to select edit date
		ActionHandler.click(OUVvehicleContainer.Editdate);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select edit date");
		CommonFunctions.attachScreenshot();

		ActionHandler.clearAndSetText(OUVvehicleContainer.Editdate, Hdate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Update New Extended Hand In date in past HandIndate");

	}

	// Update sales channel, rate and date Sales channel
	@Then("^Update sales channel, rate and date Sales channel \"([^\"]*)\"  Sales Rate \"([^\"]*)\" Sales date \"([^\"]*)\"$")
	public void update_sales_channel_rate_and_date_Sales_channel_Sales_Rate_Sales_date(String SalesChannel,
			String SalesRate, String SalesDate) throws Throwable {

		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		String SC[] = SalesChannel.split(",");
		SalesChannel = CommonFunctions.readExcelMasterData(SC[0], SC[1], SC[2]);

		String SR[] = SalesRate.split(",");
		SalesRate = CommonFunctions.readExcelMasterData(SR[0], SR[1], SR[2]);

		String SD[] = SalesDate.split(",");
		SalesDate = CommonFunctions.readExcelMasterData(SD[0], SD[1], SD[2]);

		// User able to select press
		ActionHandler.click(OUVvehicleContainer.Press);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select press");
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		// User able to select sales channel
		ActionHandler.click(OUVvehicleContainer.Saleschannel);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select sales channel");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(SalesChannel))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User able to select sales price
		ActionHandler.click(OUVvehicleContainer.Salesprice);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select sales price");
		CommonFunctions.attachScreenshot();

		// ActionHandler.scrollDown();
		// ActionHandler.wait(5);

		// User able to select sales rate
		ActionHandler.setText(OUVvehicleContainer.Salesprice, SalesRate);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select sales rate");
		CommonFunctions.attachScreenshot();

		// User able to select sales date
		ActionHandler.click(OUVvehicleContainer.Salesdate);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select sales date");
		CommonFunctions.attachScreenshot();

		// String Dating = "30-11-2019";
		ActionHandler.setText(OUVvehicleContainer.Salesdate, SalesDate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Update sales channel, rate and date Sales channel");
	}

	// Update New Extended Hand In date in future HandIndate
	@Then("^Update New Extended Hand In date in future HandIndate$")
	public void update_New_Extended_Hand_In_date_in_future_HandIndate() throws Throwable {

		// User able to Update New Extended Hand In date in future HandIndate
		String HandinDate;
		HandinDate = CommonFunctions.selectFutureDate();

		// User able to select edit date
		ActionHandler.click(OUVvehicleContainer.Editdate);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select edit date");
		CommonFunctions.attachScreenshot();

		ActionHandler.clearAndSetText(OUVvehicleContainer.Editdate, HandinDate);
		CommonFunctions.attachScreenshot();

		// User able to select home tab
		javascriptUtil.clickElementByJS(OUVvehicleContainer.homeTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User able to select hame tab");
		CommonFunctions.attachScreenshot();

		// User able to select discard changes
		ActionHandler.click(OUVvehicleContainer.discardchanges);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select discard changes");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Update New Extended Hand In date in future HandIndate");
	}

	// User able to Update New Extended Hand In date in future HandIndate
	@Then("^User selects the vehicle to open the vehicle pagesss$")
	public void user_selects_the_vehicle_to_open_the_vehicle_pagesss() throws Throwable {

		ActionHandler.click(OUVvehicleContainer.FleetVehicle4);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to selects the vehicle to open the vehicle pages");
	}

	// Verify that New Requested Handed In Approval Status should be approved before
	// saving the record
	@Then("^Verify that New Requested Handed In Approval Status should be approved before saving the record$")
	public void verify_that_New_Requested_Handed_In_Approval_Status_should_be_approved_before_saving_the_record()
			throws Throwable {

		// User able to Verify that New Requested Handed In Approval Status should be
		// approved before saving the record
		if (!VerifyHandler.verifyElementPresent(OUVvehicleContainer.Verifypending)) {
			ActionHandler.wait(5);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog(
					"User able to Verify that New Requested Handed In Approval Status should be approved/new before saving the record");
		}

	}

	// User tries to change fleet for an OUV Vehicle without updating reporting
	// levels and without keys transferred selected

	// User select the vehicle to open the vehicle related page
	@Then("^User select the vehicle to open the vehicle related page$")
	public void user_select_the_vehicle_to_open_the_vehicle_related_page() throws Throwable {

		// User able to selects the OUV Vehicle
		ActionHandler.click(OUVvehicleContainer.FleetVehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to selects the OUV Vehicle");
	}

	// Select any other fleet from the Fleet
	@Then("^Select any other fleet from the Fleet \"([^\"]*)\"$")
	public void select_any_other_fleet_from_the_Fleet(String ChangeFt) throws Throwable {

		String Ft[] = ChangeFt.split(",");
		ChangeFt = CommonFunctions.readExcelMasterData(Ft[0], Ft[1], Ft[2]);

		// User able to select click date
		ActionHandler.click(OUVvehicleContainer.Clickedit);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select click date");
		CommonFunctions.attachScreenshot();

		// User able to select click channel
		ActionHandler.click(OUVvehicleContainer.ClickCancel);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select click channel");
		CommonFunctions.attachScreenshot();

		// User able to select search fleet
		ActionHandler.click(OUVvehicleContainer.SearchFleet);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select search fleet");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVvehicleContainer.SearchFleet, ChangeFt);
		ActionHandler.wait(10);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to selects any other fleet from the search Fleet");
	}

	// User click on save button
	@Then("^User click on save button$")
	public void user_click_on_save_button() throws Throwable {

		// User able to clicks on save button
		ActionHandler.click(OUVvehicleContainer.Saved);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select save");
		CommonFunctions.attachScreenshot();

		// User able to select verify error
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Verifyerror1);
		ActionHandler.wait(3);
		Reporter.addStepLog("User able to select verify error");
		CommonFunctions.attachScreenshot();

		// User able to select home tab
		javascriptUtil.clickElementByJS(OUVvehicleContainer.homeTab);
		ActionHandler.wait(1);
		Reporter.addStepLog("User able to select home tab");
		CommonFunctions.attachScreenshot();

		// User able to select discard changes
		ActionHandler.click(OUVvehicleContainer.DiscardChanges);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select discard changes");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to clicks on save button");
	}

	// clicks on save button
	@Then("^clicks on save button$")
	public void clicks_on_save_button() throws Throwable {

		// User able to clicks on save button
		ActionHandler.click(OUVvehicleContainer.Saved);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to clicks on save button");
	}

	// User edits the OUV Reporting Levelone
	@Then("^User edits the OUV Reporting Levelone$")
	public void User_edits_the_OUV_Reporting_Levelone() throws Throwable {

		// User able to edits the OUV Reporting Levelone
		ActionHandler.click(OUVvehicleContainer.Clicklevelone);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to edits the OUV Reporting Levelone");
	}

	// User click the save button
	@Then("^User click the save button$")
	public void user_click_the_save_button() throws Throwable {

		// User able to clicks on save button
		ActionHandler.click(OUVvehicleContainer.Saved);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select save");
		CommonFunctions.attachScreenshot();

		// User able to clicks on save button
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Verifyerror);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to clicks on save button");
	}

	// Open any live OUV Vehicle for which CheckIn & Close is completed
	@Then("^Open any live OUV Vehicle for which CheckIn & Close is completed$")
	public void open_any_live_OUV_Vehicle_for_which_CheckIn_Close_is_completed() throws Throwable {

		// User able to select Vehicle
		ActionHandler.click(OUVvehicleContainer.SelectVehicle);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select vehicle");
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollDown();
		ActionHandler.wait(5);

	}

	// Verify that the reservation is completed for which CheckIn & Close is
	// completed
	@Then("^Verify that the reservation is completed for which CheckIn & Close is completed$")
	public void verify_that_the_reservation_is_completed_for_which_CheckIn_Close_is_completed() throws Throwable {

		// User able to select gate house logs
		ActionHandler.click(OUVvehicleContainer.gatehouselogs);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select gate house logs");
		CommonFunctions.attachScreenshot();

		// User able to select reservation
		ActionHandler.click(OUVvehicleContainer.Selectreservation);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select reservation");
		CommonFunctions.attachScreenshot();

		// User able to select booking
		ActionHandler.click(OUVvehicleContainer.SelectBooking);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select booking");
		CommonFunctions.attachScreenshot();
	}

	// Verify that the OUV Booking status is closed which is associated with
	// reservation
	@Then("^Verify that the OUV Booking status is closed which is associated with reservation$")
	public void verify_that_the_OUV_Booking_status_is_closed_which_is_associated_with_reservation() throws Throwable {

		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Checkclosedstatus);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select ckeck closed status");
		CommonFunctions.attachScreenshot();
	}

	// Click CheckOut/CheckIn button
	@Then("^Click CheckOut/CheckIn button$")
	public void click_CheckOut_CheckIn_button() throws Throwable {

		ActionHandler.click(OUVvehicleContainer.Clickcheckinorout);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select click check in or out");
		CommonFunctions.attachScreenshot();
	}

	// Select CheckOut from the checkbox
	@Then("^Select CheckOut from the checkbox$")
	public void select_CheckOut_from_the_checkbox() throws Throwable {

		// User able to select click check out
		ActionHandler.click(OUVvehicleContainer.Clickcheckout);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select click check out");
		CommonFunctions.attachScreenshot();
	}

	// Click Continue button
	@Then("^Click Continue button$")
	public void click_Continue_button() throws Throwable {

		// User able to select click continue
		ActionHandler.click(OUVvehicleContainer.Clickcontinue);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select click continue");
		CommonFunctions.attachScreenshot();
	}
	// Verify that an error occurred when user tries to do CheckOut for Closed OUV
	// Booking

	@Then("^Verify that an error occurred when user tries to do CheckOut for Closed OUV Booking$")
	public void verify_that_an_error_occurred_when_user_tries_to_do_CheckOut_for_Closed_OUV_Booking() throws Throwable {

		// User able to select check error
		ActionHandler.click(OUVvehicleContainer.Checkerror1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select check error");
		CommonFunctions.attachScreenshot();
	}

//Verify that reservation is completed for which CheckIn & Close is completed
	@Then("^Verify that reservation is completed for which CheckIn & Close is completed$")
	public void verify_that_reservation_is_completed_for_which_CheckIn_Close_is_completed() throws Throwable {

		// User able to select gate house logs
		ActionHandler.click(OUVvehicleContainer.gatehouselogs);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select gate house logs");
		CommonFunctions.attachScreenshot();

		// User able to select reservation
		ActionHandler.click(OUVvehicleContainer.Selectreservation);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select reservation");
		CommonFunctions.attachScreenshot();
	}

	// Click on Complete button
	@Then("^Click on Complete button$")
	public void click_on_Complete_button() throws Throwable {

		// User able to select click complete
		ActionHandler.click(OUVvehicleContainer.Clickcomplete);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select  click complete");
		CommonFunctions.attachScreenshot();
	}

	// enter Keys Returned Date earlier than OUV Booking
	@Then("^Enter Keys Returned Date earlier than OUV Booking Start Date Start date$")
	public void enter_Keys_Returned_Date_earlier_than_OUV_Booking_Start_Date_Start_date() throws Throwable {

		// selects past date
		String Hdate;
		Hdate = CommonFunctions.selectYesterdayDate();

		// User able to select change to past date
		ActionHandler.click(OUVvehicleContainer.Changetopastdate);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select  change to past date");
		CommonFunctions.attachScreenshot();

		ActionHandler.clearAndSetText(OUVvehicleContainer.Changetopastdate, Hdate);
		CommonFunctions.attachScreenshot();

	}

//Fill No\\. Keys CheckedIn, Return Location, Mileage fields Keys CheckedIn
	@Then("^Fill No\\. Keys CheckedIn, Return Location, Mileage fields Keys CheckedIn \"([^\"]*)\" Return Location \"([^\"]*)\" Mileage \"([^\"]*)\"$")
	public void fill_No_Keys_CheckedIn_Return_Location_Mileage_fields_Keys_CheckedIn_Return_Location_Mileage(
			String KeyscheckedIn, String Location, String mileage) throws Throwable {

		String SC[] = KeyscheckedIn.split(",");
		KeyscheckedIn = CommonFunctions.readExcelMasterData(SC[0], SC[1], SC[2]);

		String SC1[] = Location.split(",");
		Location = CommonFunctions.readExcelMasterData(SC1[0], SC1[1], SC1[2]);

		String SC2[] = mileage.split(",");
		mileage = CommonFunctions.readExcelMasterData(SC2[0], SC2[1], SC2[2]);

		// User able to select key ckecked
		ActionHandler.click(OUVvehicleContainer.Keyschecked);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select key ckecked");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.Keyschecked(KeyscheckedIn))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User able to select click clear
		ActionHandler.click(OUVvehicleContainer.Clickclear);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select click clear");
		CommonFunctions.attachScreenshot();

		// User able to select location
		ActionHandler.click(OUVvehicleContainer.Selectlocation);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select location");
		CommonFunctions.attachScreenshot();

		// User able to select location
		ActionHandler.setText(OUVvehicleContainer.Selectlocation, Location);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select location ");
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User able to select click mile age
		ActionHandler.click(OUVvehicleContainer.Clickmileage);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select click mile age");
		CommonFunctions.attachScreenshot();

		// User able to select click mileage
		ActionHandler.setText(OUVvehicleContainer.Clickmileage, mileage);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select click mileage");
		CommonFunctions.attachScreenshot();

		// User able to select click save
		ActionHandler.click(OUVvehicleContainer.Clicksave);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to select click save");
		CommonFunctions.attachScreenshot();

	}

	// Verify that an error occurred when user tries to enter Keys Returned date in
	// past
	@Then("^Verify that an error occurred when user tries to enter Keys Returned date in past$")
	public void verify_that_an_error_occurred_when_user_tries_to_enter_Keys_Returned_date_in_past() throws Throwable {

		// User able to verifyingerror
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.verifyingerror);
		ActionHandler.wait(3);
		Reporter.addStepLog("User able to verifyingerror");
		CommonFunctions.attachScreenshot();
	}

	// Open any live OUV Vehicle for which CheckIn & complete is completed

	@Then("^Open any live OUV Vehicle for which CheckIn & complete is completed$")
	public void open_any_live_OUV_Vehicle_for_which_CheckIn_complete_is_completed() throws Throwable {

		// User able to SelectVehicle
		ActionHandler.click(OUVvehicleContainer.SelectVehicle1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to SelectVehicle");
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollDown();
		ActionHandler.wait(5);
	}

	// Verify that the reservation is completed for which CheckIn & complete is
	// completed
	@Then("^Verify that the reservation is completed for which CheckIn & complete is completed$")
	public void verify_that_the_reservation_is_completed_for_which_CheckIn_complete_is_completed() throws Throwable {

		// User able to go gatehouselogs
		ActionHandler.click(OUVvehicleContainer.gatehouselogs);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to go gatehouselogs");
		CommonFunctions.attachScreenshot();

		// User able to Selectreservation
		ActionHandler.click(OUVvehicleContainer.Selectreservation1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to Selectreservation");
		CommonFunctions.attachScreenshot();

		// User able to SelectBooking
		ActionHandler.click(OUVvehicleContainer.SelectBooking1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to SelectBooking");
		CommonFunctions.attachScreenshot();
	}

	// User enters New Requested Hand In Date and verifies that the Latest Planned
	// Hand In Date and Actual Hand In Date gets updated
	@Then("^User enters New Requested Hand In Date and verifies that the Latest Planned Hand In Date and Actual Hand In Date gets updated$")
	public void user_enters_New_Requested_Hand_In_Date_and_verifies_that_the_Latest_Planned_Hand_In_Date_and_Actual_Hand_In_Date_gets_updated()
			throws Throwable {
		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		// user selects future date
		String date = CommonFunctions.selectFutureYear();

		// User selects New Requested Hand In Date
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Newstatus);
		ActionHandler.wait(2);

		ActionHandler.click(OUVvehicleContainer.EditNewRequestedHandInDate);
		ActionHandler.wait(2);
		Reporter.addStepLog("User able to EditNewRequestedHandInDate");
		ActionHandler.clearAndSetText(OUVvehicleContainer.NewRequestedHandInDate, date);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects New Requested Hand In Date");

		// User able to Provide details on Justification window
		ActionHandler.pageDown();
		ActionHandler.click(OUVvehicleContainer.TypeJustification);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to TypeJustification");
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVvehicleContainer.TypeJustification, "test");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Provide details on Justification window");

		// Click on the save button
		ActionHandler.pageUp();
		ActionHandler.click(OUVvehicleContainer.save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Click on the save button");
		CommonFunctions.attachScreenshot();

		// User verifies Latest Planned Hand In Date is updated
		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVvehicleContainer.HandInDateValidation(date))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Latest Planned Hand In Date is updated");

		// User verifies Actual Hand In Date is updated
		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVvehicleContainer.ActualDateValidation(date))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Actual Hand In Date is updated");

	}

	// User click on CheckOut CheckIn button
	@When("^User click on CheckOut CheckIn button$")
	public void user_click_on_CheckOut_CheckIn_button() throws Throwable {

		// User click on CheckOut CheckIn button
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.CheckInCheckOutBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on CheckOut/CheckIn button");
	}

	// checkbox checkout and checkin at a time to proceed
	@Then("^checkbox checkout and checkin at a time to proceed$")
	public void checkbox_checkout_and_checkin_at_a_time_to_proceed() throws Throwable {

		// User checkbox the CheckIn
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.CheckInCheckBx);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User checkbox the CheckIn ");

		// User checkbox the CheckOut
		ActionHandler.click(OUVvehicleContainer.CheckOutCheckBx);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User checkbox the CheckOut");

		// user click on Continue button
		ActionHandler.click(OUVvehicleContainer.ContinueBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on Continue button");

	}

	// Verify the error message displayed to select one option
	@And("^Verify the error message displayed to select one option$")
	public void verify_the_error_message_displayed_to_select_one_option() throws Throwable {

		// Error message displayed to select one option
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.ErrorMsg)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message displayed to select one option");
		} else {

			// Error message dint displayed to select one option
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message dint displayed to select one option");
		}

		// click on cancel button
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.CancelSaveVehicleName);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on cancel button");
	}

	// User navigate to OUVGatehouseLogs tab
	@And("^User navigate to OUVGatehouseLogs tab$")
	public void user_navigate_to_OUVGatehouseLogs_tab() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		// user click on OUV Gate House log link
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.OUVGateHouseLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on OUV Gate House log link");

	}

	// Verify the ActionType which is in Checkin and close
	@Then("^Verify the ActionType which is in Checkin and close$")
	public void verify_the_ActionType_which_is_in_Checkin_and_close() throws Throwable {

		// User has completed the check in and close process
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.CheckInClose)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User has completed the check in and close process");
		} else {

			// User dint complete the check in and close process
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User dint complete the check in and close process");
		}
	}

	// Navigate back to selected OUV Vehicle
	@And("^Navigate back to selected OUV Vehicle$")
	public void navigate_back_to_selected_OUV_Vehicle() throws Throwable {

		// Navigate back to selected OUV Vehicle
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.VehicleLink);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Navigate back to selected OUV Vehicle");

	}

	// checkbox checkout and click on continue
	@Then("^checkbox checkout and click on continue$")
	public void checkbox_checkout_and_click_on_continue() throws Throwable {

		// User checkbox the CheckOut
		ActionHandler.click(OUVvehicleContainer.CheckOutCheckBx);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User checkbox the CheckOut");

		// user click on Continue button
		ActionHandler.click(OUVvehicleContainer.ContinueBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on Continue button");

	}

	// Verify the error message displayed
	@Then("^Verify the error message displayed$")
	public void verify_the_error_message_displayed() throws Throwable {

		// Error message caught
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.CheckOutError)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message caught");
		} else {

			// Error messgage dint displayed
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error messgage dint displayed");
		}
	}

	@Then("^click on drop down and select CheckIn and complete$")
	public void click_on_drop_down_and_select_CheckIn_and_complete() throws Throwable {
		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.ShowActions);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("Click on drop down icon");
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.CheckInComplete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on check in and complete");
	}

	@Then("^fill fields like driver logistics \"([^\"]*)\", milage at close booking with \"([^\"]*)\", return location \"([^\"]*)\" and vehicle condition \"([^\"]*)\"$")
	public void fill_fields_like_driver_logistics_milage_at_close_booking_with_return_location_and_vehicle_condition(
			String Driver, String Mileage, String Returnloc, String Vehiclecond) throws Throwable {

		String dr[] = Driver.split(",");
		Driver = CommonFunctions.readExcelMasterData(dr[0], dr[1], dr[2]);

		String mil[] = Mileage.split(",");
		Mileage = CommonFunctions.readExcelMasterData(mil[0], mil[1], mil[2]);

		String rl[] = Returnloc.split(",");
		Returnloc = CommonFunctions.readExcelMasterData(rl[0], rl[1], rl[2]);

		String vc[] = Vehiclecond.split(",");
		Vehiclecond = CommonFunctions.readExcelMasterData(vc[0], vc[1], vc[2]);

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.DriverLogistic);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on driver logistic");

		ActionHandler.wait(4);
		ActionHandler.setText(OUVvehicleContainer.DriverLogistic, Driver);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("driver name is given");

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.Mileage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on mileage");

		ActionHandler.wait(2);
		ActionHandler.setText(OUVvehicleContainer.Mileage, Mileage);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("mileage should be given");

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.ReturnLoc);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("mileage should be given");

		ActionHandler.wait(2);
		// ActionHandler.setText(OUVvehicleContainer.ReturnLoc, Returnloc);
		ActionHandler.wait(4);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.ReturnLoc);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("mileage should be given");

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.ReturnLoc);
		ActionHandler.wait(3);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(1);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.vehiclecondition);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on vehicle condition");

		ActionHandler.wait(2);
		ActionHandler.setText(OUVvehicleContainer.vehiclecondition, Vehiclecond);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User should give vehicle condition");

	}

	@Then("^Click on save$")
	public void click_on_save() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.saveButton);
		ActionHandler.wait(3);

		// Error message dint displayed
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Error messgage dint displayed");

	}

	// validate the status change to Live from Pre-Live
	@Then("^validate the status change to Live from Pre-Live$")
	public void validate_the_status_change_to_Live_from_Pre_Live() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.pageDown();
		ActionHandler.pageDown();

		String endDate = selectTodayDate();

		// Verify the Actual Add to Fleet Date
		VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVvehicleContainer.validationpoint(endDate))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Proposed Hand In");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the Proposed Hand In");

		// Verify the Proposed Hand In
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.ProposedHandIn);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Proposed Hand In");
		CommonFunctions.attachScreenshot();

		// Verify the Latest Planned Hand In
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.LatestPlannedHand);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Latest Planned Hand In");
		CommonFunctions.attachScreenshot();

		// Verify the Fore cast Actual Hand In
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.ForecastActual);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Fore cast Actual Hand In");
		CommonFunctions.attachScreenshot();

		// Verify the Default Last Fleet
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.LiveFleet);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Default Last Fleet");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageUp();
		ActionHandler.wait(3);
		ActionHandler.pageUp();

		// Verify the OUV Reporting Level 1
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.livereportingLv1);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the OUV Reporting Level 1");
		CommonFunctions.attachScreenshot();

		// Verify the OUV Reporting Level 2
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.livereportingLv2);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the OUV Reporting Level 2");
		CommonFunctions.attachScreenshot();

		// Verify the OUV Reporting Level 3
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.livereportingLv3);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the OUV Reporting Level 3");
		CommonFunctions.attachScreenshot();

		// Verify the Default Fleet
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.LiveFleet);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Default Fleet");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageUp();
		ActionHandler.wait(3);

		// Verify the status change to Handed In
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.HandedIn);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the status change to Handed In");
		CommonFunctions.attachScreenshot();

		// Verify the Owner
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.fleetOwner);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the Owner");
		CommonFunctions.attachScreenshot();
	}

	public String selectTodayDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 0);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-M-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	// User searches for OUV Vehicle on their fleet Search
	@Then("^User searches for OUV Vehicle on their fleet Search \"([^\"]*)\"$")
	public void user_searches_for_OUV_Vehicle_on_their_fleet_Search(String Select) throws Throwable {

		String PR[] = Select.split(",");
		Select = CommonFunctions.readExcelMasterData(PR[0], PR[1], PR[2]);

		ActionHandler.click(OUVvehicleContainer.SearchOUVVehicles);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVvehicleContainer.SearchOUVVehicles, Select);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);

		Reporter.addStepLog("User able to search an OUV Vehicle on their fleet");
	}

// User tries to edit new requested hand in date
	@Then("^User edits the New Requested Hand In Date on OUV Vehicle window$")
	public void user_edits_the_New_Requested_Hand_In_Date_on_OUV_Vehicle_window() throws Throwable {

		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVvehicleContainer.NewHandinDate);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.NewHandinDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on edit icon");

		String Hdate;
		Hdate = CommonFunctions.selectYesterdayDate();

		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(OUVvehicleContainer.NewHandinDate, Hdate);
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.SaveEdit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		CommonFunctions.attachScreenshot();

	}

// Verify the error message for date
	@Then("^Verify the error message for date$")
	public void verify_the_error_message_for_date() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.SnagMsg)) {
			Reporter.addStepLog("Error message displayed");
		} else {
			Reporter.addStepLog("Error message not displayed");
		}
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.Cancelchanges);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on cancel button");

	}

	// navigate to fleet section
	@Then("^navigate to fleet section$")
	public void navigate_to_fleet_section() throws Throwable {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		Reporter.addStepLog("Navigate to fleet section");
		CommonFunctions.attachScreenshot();

		// User able to select verify fleet is available
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Clickedit);
		ActionHandler.wait(3);
		Reporter.addStepLog("User able to select verify fleet is available ");
		CommonFunctions.attachScreenshot();
	}

	// Verify the displayed fleet error message
	@Then("^Verify the displayed fleet error message$")
	public void verify_the_displayed_fleet_error_message() throws Throwable {

		// User able to select verify the displayed error
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.verifyerror1);
		ActionHandler.wait(3);
		Reporter.addStepLog("User able to select verify the displayed error");
		CommonFunctions.attachScreenshot();

		// The fleet has not updated
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("The fleet has not updated");
		CommonFunctions.attachScreenshot();

		// Click on the cancel button
		ActionHandler.click(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on the cancel button");
		CommonFunctions.attachScreenshot();
	}

	// navigate to Approval History quick link and verify pending status
	@Then("^navigate to Approval History quick link and verify pending status$")
	public void navigate_to_Approval_History_quick_link_and_verify_pending_status() throws Throwable {

		ActionHandler.pageUp();
		ActionHandler.wait(3);

		ActionHandler.pageUp();
		ActionHandler.wait(3);

		// User clicks on show all
		ActionHandler.click(OUVvehicleContainer.ShowAll);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on show all");

		// User clicks on Approval History
		ActionHandler.click(OUVvehicleContainer.ApprovalHistory);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Approval History");

		// User able to select verify the pending status
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Pending);
		ActionHandler.wait(3);
		Reporter.addStepLog("User able to select verify the pending status");
		CommonFunctions.attachScreenshot();

	}

	// Verify the owner name of vehicle
	@Then("^Verify the owner name of vehicle as \"([^\"]*)\"$")
	public void verify_the_owner_name_of_vehicle_as(String owner) throws Throwable {

		String SC2[] = owner.split(",");
		owner = CommonFunctions.readExcelMasterData(SC2[0], SC2[1], SC2[2]);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);

		// User able to verify the Owner information
		VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelectiontext(owner))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User able to verify the Owner information");
		CommonFunctions.attachScreenshot();

	}

	// click on the fleet and verify the fleet manager
	@Then("^click on the fleet and verify the fleet manager as \"([^\"]*)\"$")
	public void click_on_the_fleet_and_verify_the_fleet_manager_as(String manager) throws Throwable {

		String SC2[] = manager.split(",");
		manager = CommonFunctions.readExcelMasterData(SC2[0], SC2[1], SC2[2]);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		Reporter.addStepLog("Navigate to fleet section");
		CommonFunctions.attachScreenshot();

		// User clicks on Fleet link
		ActionHandler.click(OUVvehicleContainer.fleet);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Fleet link");

		// User able to verify the Manager information
		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelectiontext(manager))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User able to verify the Manager information");
		CommonFunctions.attachScreenshot();
	}

	// validate the Registration Approval Status is New
	@Then("^validate the Registration Approval Status is New$")
	public void validate_the_Registration_Approval_Status_is_New() throws Throwable {
		ActionHandler.wait(4);
		javascriptUtil.scrollIntoView(OUVvehicleContainer.Adminstrationtitle);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		String n = "New";
		String text = (OUVvehicleContainer.RegistrationStatus).getText();
		if (text.equals(n)) {
			System.out.println(text);

		} else {
			System.out.println("Not equals");
		}
	}

	// click on edit icon for Registration Required and select the checkbox
	@Then("^click on edit icon for Registration Required and select the checkbox$")
	public void click_on_edit_icon_for_Registration_Required_and_select_the_checkbox() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RegistrationRequirededit);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on edit button");

		ActionHandler.click(OUVvehicleContainer.RegCheckbox);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects checkbox");
	}

	// clone the list with name
	@Then("^clone the list with name \"([^\"]*)\"$")
	public void clone_the_list_with_name(String name) throws Throwable {

		String SC2[] = name.split(",");
		name = CommonFunctions.readExcelMasterData(SC2[0], SC2[1], SC2[2]);

		// Search for an vehicle
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVvehicleContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User views all the listed OUV Vehicles");

		ActionHandler.click(OUVvehicleContainer.ListView);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on the list view");

		ActionHandler.click(OUVvehicleContainer.clonelist);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on the clone list");

		ActionHandler.clearAndSetText(OUVvehicleContainer.clonelisttitle, name);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters the clone title");

		ActionHandler.click(OUVvehicleContainer.clonesave);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on the save button");

	}

	// add the filter with fleet as
	@Then("^add the filter with fleet as \"([^\"]*)\"$")
	public void add_the_filter_with_fleet_as(String fleet) throws Throwable {

		String SC2[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(SC2[0], SC2[1], SC2[2]);

		ActionHandler.click(OUVvehicleContainer.addFilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on add Filter button");

		ActionHandler.click(OUVvehicleContainer.SelectOption);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Select Option button");

		javascriptUtil.clickElementByJS(OUVvehicleContainer.Fleetselection);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Fleet selection button");

		ActionHandler.clearAndSetText(OUVvehicleContainer.valuefleet, fleet);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User enters the fleet info");

		ActionHandler.click(OUVvehicleContainer.Donefilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Done filter button");

		ActionHandler.click(OUVvehicleContainer.Savefilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Save filter button");

		ActionHandler.click(OUVvehicleContainer.Closefilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Close filter button");

	}

	// delete the created list view
	@Then("^delete the created list view \"([^\"]*)\"$")
	public void delete_the_created_list_view(String list) throws Throwable {

		String SC2[] = list.split(",");
		list = CommonFunctions.readExcelMasterData(SC2[0], SC2[1], SC2[2]);

		// User navigates to OUV vehicles
		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(OUVvehicleContainer.OUVvehicles);
		ActionHandler.wait(10);
		ActionHandler.waitForElement(OUVvehicleContainer.searchVehiclebox, 60);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV vehicles");

		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelectiontext(list))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on list button");

		ActionHandler.click(OUVvehicleContainer.ListView);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on the list view");

		ActionHandler.click(OUVvehicleContainer.deleteclonelist);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on delete clone list");

		ActionHandler.click(OUVvehicleContainer.deleteclonelistconfor);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on delete clone list confirm");
	}

	// click on transfer and recieved keys
	@Then("^click on transfer and recieved keys$")
	public void click_on_transfer_and_recieved_keys() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);

		if (VerifyHandler.verifyChkBoxRadioButtonSelected(OUVvehicleContainer.Transferedkeys)) {

			ActionHandler.wait(5);
			Reporter.addStepLog("Transfered keys already selected");
			CommonFunctions.attachScreenshot();

		} else {
			ActionHandler.click(OUVvehicleContainer.Transferedkeys);
			ActionHandler.wait(5);
			Reporter.addStepLog("User able to select Transfered keys");
			CommonFunctions.attachScreenshot();
		}

		if (VerifyHandler.verifyChkBoxRadioButtonSelected(OUVvehicleContainer.KeysReceived)) {

			ActionHandler.wait(5);
			Reporter.addStepLog("Keys Received already selected");
			CommonFunctions.attachScreenshot();

		} else {

			ActionHandler.click(OUVvehicleContainer.KeysReceived);
			ActionHandler.wait(5);
			Reporter.addStepLog("User able to select Keys Received");
			CommonFunctions.attachScreenshot();
		}

	}

	// validate that the Registration Approval Status is set as New
	@Then("^validate that the Registration Approval Status is set as New$")
	public void validate_that_the_Registration_Approval_Status_is_set_as_New() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);

		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.RegistrationStatus)) {

			ActionHandler.wait(5);
			Reporter.addStepLog("Registration Approval Status is set as New");
			CommonFunctions.attachScreenshot();

		} else {

			ActionHandler.wait(5);
			Reporter.addStepLog("Registration Approval Status is not set as New");
			CommonFunctions.attachScreenshot();
		}

	}

	// add the filter with Registration Approval Status as New
	@Then("^add the filter with Registration Approval Status as New$")
	public void add_the_filter_with_Registration_Approval_Status_as_New() throws Throwable {

		ActionHandler.click(OUVvehicleContainer.addFilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on add Filter button");

		ActionHandler.click(OUVvehicleContainer.SelectOption);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Select Option button");

		javascriptUtil.clickElementByJS(OUVvehicleContainer.RegistrationApprovalselection);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Fleet selection button");

		ActionHandler.click(OUVvehicleContainer.OptionSelects);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on add Option Selects button");

		ActionHandler.click(OUVvehicleContainer.OptionSelectsNew);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Select new button");

		ActionHandler.click(OUVvehicleContainer.Donefilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Done filter button");

	}

	// User changes the fleet of an vehicle
	@Then("^User change the fleet to \"([^\"]*)\"$")
	public void user_change_the_fleet_to(String arg1) throws Throwable {

		String rl[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(rl[0], rl[1], rl[2]);

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(5);

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		// Click on Edit fleet icon
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.EditFleet);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Edit fleet icon");

		// Click on clear fleet icon
		ActionHandler.click(OUVvehicleContainer.ClearFleet);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on clear fleet icon");

		// User selects new fleet
		ActionHandler.setText(OUVvehicleContainer.Fleet, arg1);
		ActionHandler.wait(3);
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects new fleet");

		// User click on save button
		ActionHandler.click(OUVvehicleContainer.save);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on save button");

	}

	@Then("^validate the Registration Approval Status is Pending Approval$")
	public void validate_the_Registration_Approval_Status_is_Pending_Approval() throws Throwable {
		ActionHandler.wait(4);

		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		String n = "Pending Approval";
		String text = (OUVvehicleContainer.PendingApproval).getText();
		if (text.equals(n)) {
			System.out.println(text);

		} else {
			System.out.println("Not equals");
		}
	}

	// search for the vehicle in the Editable list with status
	@Then("^search for the vehicle in the Editable list with status \"([^\"]*)\"$")
	public void search_for_the_vehicle_in_the_Editable_list_with_status(String Search) throws Throwable {

		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// Search for an vehicle
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVvehicleContainer.EditableLiveHandin);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views Editable Live Handin the listed OUV Vehicles");

		ActionHandler.click(OUVvehicleContainer.searchVehiclebox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVvehicleContainer.searchVehiclebox, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an vehicle");
		CommonFunctions.attachScreenshot();
	}

	// Select the bulk vehicles
	@Then("^Select the bulk vehicles$")
	public void select_the_bulk_vehicles() throws Throwable {

		ActionHandler.click(OUVvehicleContainer.Select1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Select 1 button");

		ActionHandler.click(OUVvehicleContainer.Select2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Select 2 button");

	}

	// click on change Master status and select master status
	@Then("^click on change Master status and select master status as \"([^\"]*)\" and sub as \"([^\"]*)\"$")
	public void click_on_change_Master_status_and_select_master_status_as_and_sub_as(String master, String sub)
			throws Throwable {

		String Sea[] = master.split(",");
		master = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		String subs[] = sub.split(",");
		sub = CommonFunctions.readExcelMasterData(subs[0], subs[1], subs[2]);

		ActionHandler.click(OUVvehicleContainer.ChangeMasterStatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Change Master Status button");

		ActionHandler.click(OUVvehicleContainer.MasterStat);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Master Status button");

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.Keyschecked(master))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Master Status button");

		ActionHandler.click(OUVvehicleContainer.subMaster);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Master Status button");

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.Keyschecked(sub))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on sub Master Status button");

	}

	// click on Keys transferred check box
	@Then("^click on Keys transferred checkbox$")
	public void click_on_Keys_transferred_checkbox() throws Throwable {

		if (VerifyHandler.verifyChkBoxRadioButtonSelected(OUVvehicleContainer.Keystransfer)) {
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			Reporter.addStepLog("User already selected transfered keys");
		}

		else {

			ActionHandler.click(OUVvehicleContainer.Keystransfer);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(5);
			Reporter.addStepLog("User selected transfered keys");
		}

	}

	// click on save for bulk status change
	@Then("^click on save for bulk status change$")
	public void click_on_save_for_bulk_status_change() throws Throwable {

		ActionHandler.click(OUVvehicleContainer.Savebulk);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects Save button");

		ActionHandler.click(OUVvehicleContainer.Okaybutton);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		Reporter.addStepLog("User selects Okay button");
	}

	// user verifies the error message and click on dismiss
	@Then("^Verify the error message for bulk status change and click on dismiss$")
	public void verify_the_error_message_for_bulk_status_change_and_click_on_dismiss() throws Throwable {
		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.toastmsg)) {
			Reporter.addStepLog("Error message was found");
		} else {
			Reporter.addStepLog("Error message was not found");
		}

		ActionHandler.click(OUVvehicleContainer.check1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects checkboxes button");

		ActionHandler.click(OUVvehicleContainer.dismissbutton);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

	}

	// search for UAT editable vehicles
	@Then("^search for the vehicle in the UAT Editable list with status \"([^\"]*)\"$")
	public void search_for_the_vehicle_in_the_UAT_Editable_list_with_status(String Search) throws Throwable {

		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// Search for an vehicle
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVvehicleContainer.UATEditable);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views Editable Live Handin the listed OUV Vehicles");

		ActionHandler.click(OUVvehicleContainer.searchVehiclebox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVvehicleContainer.searchVehiclebox, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an vehicle");
		CommonFunctions.attachScreenshot();
	}

	// User verify the OUV Reporting level one of the fleet matches to
	@When("^User verify the OUV Reporting level one of the fleet matches to \"([^\"]*)\"$")
	public void user_verify_the_OUV_Reporting_level_one_of_the_fleet_matches_to(String status) throws Throwable {
		String Sea[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(OUVvehicleContainer.OUVRLone);
		ActionHandler.wait(3);
		Reporter.addStepLog("OUV Reporting level one of the fleet matches to fleet");
		CommonFunctions.attachScreenshot();

	}

	// User click on Pre sold Admin icon
	@Then("^User click on Pre sold Admin icon$")
	public void user_click_on_Pre_sold_Admin_icon() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.PreSoldAdmin);
		ActionHandler.wait(3);
		Reporter.addStepLog("User click on Pre sold Admin icon");
		CommonFunctions.attachScreenshot();

	}

	// Verify the error message then click on cancel
	@Then("^Verify the error message then click on cancel$")
	public void verify_the_error_message_then_click_on_cancel() throws Throwable {

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.ErrorPreSold);
		ActionHandler.wait(3);
		Reporter.addStepLog("Error message caught");
		CommonFunctions.attachScreenshot();

		// user click on cancel button
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.CancelSold);
		ActionHandler.wait(3);
		Reporter.addStepLog("user click on cancel button");
		CommonFunctions.attachScreenshot();

	}

	@Then("^verify the core information$")
	public void verify_the_core_information() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.VehicleUser)) {
			Reporter.addStepLog("Vehicle User field is present");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("Vehicle User field is not present");
		}
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.VehicleUserEmail)) {
			Reporter.addStepLog("Vehicle User email field is present");
		} else {
			Reporter.addStepLog("Vehicle User email field is not present");
		}

	}

	@And("^Verify the other detials$")
	public void verify_the_other_details() throws Throwable {
		ActionHandler.wait(4);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.UserCity)) {
			Reporter.addStepLog("User city field is present");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("User city field is not present");
		}
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.UserState)) {
			Reporter.addStepLog("User State field is present");
		} else {
			Reporter.addStepLog("User State field is not present");
		}
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.UserCountry)) {
			Reporter.addStepLog("User country field is present");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("User Country field is not present");
		}

		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.UserZipCode)) {
			Reporter.addStepLog("User ZIP code field is present");
		} else {
			Reporter.addStepLog("User ZIP code field is not present");
		}

	}

	// User view all the fields for Financial section in an OUV Vehicle
	@Then("^User view all the fields for Financial section in an OUV Vehicle$")
	public void user_view_all_the_fields_for_Financial_section_in_an_OUV_Vehicle() throws Throwable {
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);

		// javascriptUtil.scrollIntoView(OUVvehicleContainer.FinancialsHeader);
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.FinanceClassification);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.InventoryStatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Currency);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.TotalInitialRetailPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.RetailPrice);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.ResidualValue);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.LossOnResale);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies all the fields for Financial section are present in an OUV Vehicle");

	}

	// User view only the selected fields for Financial section in an OUV Vehicle
	@Then("^User view only the selected fields for Financial section in an OUV Vehicle$")
	public void user_view_only_the_selected_fields_for_Financial_section_in_an_OUV_Vehicle() throws Throwable {
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		ActionHandler.wait(3);

		// javascriptUtil.scrollIntoView(OUVvehicleContainer.FinancialsHeader);
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Currency);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog(
				"User verifies only the currency fields for Financial section is present in an OUV Vehicle");

	}

	// SelectingTomorrowDate
	public String selectTomorrowDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	// Selecting FutureDate
	public static String selectFutureDate() {
		final DateFormat dateFormat = new SimpleDateFormat("d-M-yyyy");
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DATE, 2);
		Date currentDatePlusOne = c.getTime();
		String FutureDate = dateFormat.format(currentDatePlusOne);
		System.out.println("Future Date is = " + FutureDate);

		return FutureDate;

	}

	// User edits the Registration Date and DeRegistration Date
	@Then("^User edits the Registration Date and DeRegistration Date$")
	public void user_edits_the_Registration_Date_and_DeRegistration_Date() throws Throwable {

		String RegistrationDate = selectTomorrowDate();
		String DeRegistrationDate = selectFutureDate();

		ActionHandler.click(OUVvehicleContainer.EditButton);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on edit button");

		javascriptUtil.scrollIntoView(OUVvehicleContainer.Admininstration);
		ActionHandler.wait(3);

		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.clearAndSetText(OUVvehicleContainer.RegistrationDate, RegistrationDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User enters Registration Date");

		ActionHandler.clearAndSetText(OUVvehicleContainer.RegistrationDate, DeRegistrationDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User enters De-Registration Date");
	}

	// Select Check-In checkbox and click on continue
	@Then("^Select Check-In checkbox and click on continue$")
	public void select_Check_In_checkbox_and_click_on_continue() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.CheckInCheckBx);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVvehicleContainer.ContinueBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select Check-In checkbox and click on continue");
	}

	// Verify the error message displayed for Check-In
	@Then("^Verify the error message displayed for Check-In$")
	public void verify_the_error_message_displayed_for_Check_In() throws Throwable {

		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.SnagMsg)) {

			Reporter.addStepLog("error message window displayed");
		} else {
			Reporter.addStepLog("error message window displayed");
		}
	}

	// Verify the ActionType which is in Checkin
	@Then("^Verify the ActionType which is in Checkin$")
	public void Verify_the_ActionType_which_is_in_Checkin() throws Throwable {

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.checkin);
		ActionHandler.wait(3);
		Reporter.addStepLog("Action Type is set to Check in");
		CommonFunctions.attachScreenshot();

	}

	@Then("^Update OUV reporting Levels to LevelOne \"([^\"]*)\" LevelTwo \"([^\"]*)\" LevelThree \"([^\"]*)\"$")
	public void update_OUV_reporting_Levels_to_LevelOne_LevelTwo_LevelThree(String Level11, String Level22,
			String Level33) throws Throwable {

		String L1[] = Level11.split(",");
		Level11 = CommonFunctions.readExcelMasterData(L1[0], L1[1], L1[2]);

		String L2[] = Level22.split(",");
		Level22 = CommonFunctions.readExcelMasterData(L2[0], L2[1], L2[2]);

		String L3[] = Level33.split(",");
		Level33 = CommonFunctions.readExcelMasterData(L3[0], L3[1], L3[2]);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		// javascriptUtil.scrollIntoView(OUVvehicleContainer.RepoLev1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RepoLev1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.ViewDependencies);
		ActionHandler.wait(5);

		ActionHandler.click(OUVvehicleContainer.Level1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(Level11))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.Level2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(Level22))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.Level3);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(Level33))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.Apply);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Update OUV reporting Levels to categories that do not match the new fleet");

	}

	@And("^Verify the displayed error message$")
	public void verify_the_displayed_error_message() throws Exception {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.SnagMsg)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message displayed");
		} else {
			Reporter.addStepLog("Error message not displayed");
		}
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.Cancelchanges);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on cancel button");

	}

	@Then("^Click on Fleet field$")
	public void click_on_Fleet_field() throws Throwable {

		ActionHandler.wait(2);

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.Fleet);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on fleet");
		ActionHandler.wait(3);

	}

	@Then("^Validate that user cannot edit the OUV Reporting Category field$")
	public void validate_that_user_cannot_edit_the_OUV_Reporting_Category_field() throws Throwable {

		ActionHandler.wait(10);

		if (!(VerifyHandler.verifyElementPresent(OUVvehicleContainer.RepoCategory1))) {
			Reporter.addStepLog("user is not able to click on the edit button");
		} else {
			Reporter.addStepLog("user is able to click on the edit button");
		}
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user verifies the button");
		ActionHandler.wait(2);

	}

	@Then("^Click on pre-sold admin button and validate the error message$")
	public void click_on_pre_sold_admin_button_and_validate_the_error_message() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.PresoldAdmin);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user clicks the button");
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.errorMsg)) {
			Reporter.addStepLog("Error message was found");
		} else {
			Reporter.addStepLog("Error message was not found");
		}
	}

	@Then("^click on cancel$")
	public void click_on_cancel() throws Throwable {

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.cancelbtn);
		ActionHandler.wait(3);
	}

	@And("^Validate that new requested hand in approval status as \"([^\"]*)\"$")
	public void validate_that_new_requested_hand_in_approval_status_as(String NewStatus) throws Throwable {

		String ns[] = NewStatus.split(",");
		NewStatus = CommonFunctions.readExcelMasterData(ns[0], ns[1], ns[2]);
		ActionHandler.wait(4);

		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.NewHandinEdit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.NewHandinclick);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(NewStatus))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	@Then("^enter the new requested hand in date and enter free text in justification as \"([^\"]*)\"$")
	public void enter_the_new_requested_hand_in_date_and_enter_free_text_in_justification_as(String Justification)
			throws Throwable {

		String jus[] = Justification.split(",");
		Justification = CommonFunctions.readExcelMasterData(jus[0], jus[1], jus[2]);

		ActionHandler.wait(5);
		ActionHandler.click(OUVvehicleContainer.Handinfiled);
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.Today);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);

		ActionHandler.click(OUVvehicleContainer.Justification);
		ActionHandler.wait(4);
		ActionHandler.setText(OUVvehicleContainer.Justification, Justification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

	}

	@Then("^Click on save and validate that new requested hand in status as approved$")
	public void click_on_save_and_validate_that_new_requested_hand_in_status_as_approved() throws Throwable {

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.SaveEdit);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Status should be changed as approved");

	}

	// Verify the change in status to handed In
	@Then("^Verify the change in status to handed In$")
	public void verify_the_change_in_status_to_handed_In() throws Throwable {

		// Verify the the change 1
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.HandedIn1);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the the change 1");
		CommonFunctions.attachScreenshot();

		// Verify the the change 2
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.HandedIn2);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the the change 2");
		CommonFunctions.attachScreenshot();
	}

	// change list to recently viewed
	@Then("^change list to recently viewed$")
	public void change_list_to_recently_viewed() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.Dismiss)) {

			// click on select all button
			ActionHandler.click(OUVvehicleContainer.BulkerrorAll);
			ActionHandler.wait(3);
			Reporter.addStepLog("click on select all button");
			CommonFunctions.attachScreenshot();

			// click on Dismiss button
			ActionHandler.click(OUVvehicleContainer.Dismissandclose);
			ActionHandler.wait(3);
			Reporter.addStepLog("click on Dismiss button");
			CommonFunctions.attachScreenshot();

		}

		ActionHandler.wait(5);
		ActionHandler.click(OUVvehicleContainer.EditableLiveHandin);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views Editable Live Handin the listed OUV Vehicles");

		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
	}

	// verify the error message for bulk status update
	@Then("^verify the error message for bulk status update$")
	public void verify_the_error_message_for_bulk_status_update() throws Throwable {

		// Verify the the change 1 error
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Bulkerror1);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the the change 1 error");
		CommonFunctions.attachScreenshot();

		// Verify the the change 2 error
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Bulkerror2);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the the change 2 error");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(OUVvehicleContainer.Dismiss)) {

			// click on select all button
			ActionHandler.click(OUVvehicleContainer.BulkerrorAll);
			ActionHandler.wait(3);
			Reporter.addStepLog("click on select all button");
			CommonFunctions.attachScreenshot();

			// click on Dismiss button
			ActionHandler.click(OUVvehicleContainer.Dismissandclose);
			ActionHandler.wait(3);
			Reporter.addStepLog("click on Dismiss button");
			CommonFunctions.attachScreenshot();
		}

	}

	@Then("^Provide no details on Justification window$")
	public void provide_no_details_on_Justification_window_and_click_on_save() throws Exception {

		// ActionHandler.pageDown();
		// ActionHandler.pageDown();
		// ActionHandler.pageDown();
		// ActionHandler.pageDown();
		ActionHandler.wait(5);
		ActionHandler.scrollToView(OUVvehicleContainer.Justification);

		ActionHandler.wait(4);
		ActionHandler.click(OUVvehicleContainer.Justification);
		ActionHandler.wait(3);
		ActionHandler.clearText(OUVvehicleContainer.Justification);
		ActionHandler.wait(10);
		Reporter.addStepLog("User enters Justification");
		CommonFunctions.attachScreenshot();
	}

	// user fills the re-marketing details
	@Then("^Enters details for Remarketing Section with vehicle condition \"([^\"]*)\", CustomerIdentifier \"([^\"]*)\",SalesChannel \"([^\"]*)\",FinalPrice \"([^\"]*)\"$")
	public void enters_details_for_Remarketing_Section_with_vehicle_condition_CustomerIdentifier_SalesChannel_FinalPrice(
			String VehicleCond, String CustomerId, String SalesChannel, String FinalPrice) throws Exception {

		String vc[] = VehicleCond.split(",");
		VehicleCond = CommonFunctions.readExcelMasterData(vc[0], vc[1], vc[2]);

		String ci[] = CustomerId.split(",");
		CustomerId = CommonFunctions.readExcelMasterData(ci[0], ci[1], ci[2]);

		String sc[] = SalesChannel.split(",");
		SalesChannel = CommonFunctions.readExcelMasterData(sc[0], sc[1], sc[2]);

		String fp[] = FinalPrice.split(",");
		FinalPrice = CommonFunctions.readExcelMasterData(fp[0], fp[1], fp[2]);

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		// javascriptUtil.scrollIntoView(OUVvehicleContainer.Invoicelabel);

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.Vehiclecond);
		ActionHandler.wait(2);
		ActionHandler.setText(OUVvehicleContainer.Vehiclecond, VehicleCond);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters text for vehicle condition");

		ActionHandler.click(OUVvehicleContainer.Cid);
		ActionHandler.wait(2);
		ActionHandler.setText(OUVvehicleContainer.Cid, CustomerId);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.SalesChannel);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(SalesChannel))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.Finalprice);
		ActionHandler.wait(2);
		ActionHandler.setText(OUVvehicleContainer.Finalprice, FinalPrice);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.DateofSale);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVvehicleContainer.Today);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

	}

	@And("^User validates that extended Handed In status is not approved$")
	public void user_validates_that_extended_Handed_In_status_is_not_approved() throws Exception {
		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVvehicleContainer.HandedInHeader);
		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(OUVvehicleContainer.ApprovedHandedIn)) {
			Reporter.addStepLog("User validates that extended Handed In status is not approved");
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.pageCompleteScrollUp();

	}

	@Then("^Verify the displayed error message	for Handed In status$")
	public void verify_the_displayed_error_message_for_Handed_In_status() throws Throwable {
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Errormsg);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the error message");
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("The status has not updated");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on the save button");
		CommonFunctions.attachScreenshot();
	}

	// Select vehicle
	@Then("^select vehicle$")
	public void select_vehicle() throws Throwable {

		// Select vehicle
		ActionHandler.click(OUVvehicleContainer.VehicleSel7);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select vehicle");
		CommonFunctions.attachScreenshot();
	}

	// Click on Pre-Sold Admin button
	@Then("^Click on Pre-Sold Admin button$")
	public void click_on_Pre_Sold_Admin_button() throws Throwable {

		// Click on Pre-Sold Admin button
		ActionHandler.click(OUVvehicleContainer.PreSoldAdmin);
		ActionHandler.wait(15);
		Reporter.addStepLog("Click on Pre-Sold Admin button");
		CommonFunctions.attachScreenshot();

		// Click on Save button
		ActionHandler.click(OUVvehicleContainer.Save);
		ActionHandler.wait(15);
		Reporter.addStepLog("Click on Save button");
		CommonFunctions.attachScreenshot();

	}

	@Then("^Navigate to Vehicle Pre-sold check box in Re-marketing section and validate that check box is ticked$")
	public void navigate_to_Vehicle_Pre_sold_check_box_in_Re_marketing_section_and_validate_that_check_box_is_ticked()
			throws Throwable {

		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);

		// presold button is already selected
		if (VerifyHandler.verifyChkBoxRadioButtonSelected(OUVvehicleContainer.VerifyCheckBox)) {
			ActionHandler.wait(3);
			Reporter.addStepLog("presold button is already selected");
			CommonFunctions.attachScreenshot();
		}

		else {

			// Click on Pre-Sold Admin button
			ActionHandler.click(OUVvehicleContainer.PreSoldAdmin);
			ActionHandler.wait(15);
			Reporter.addStepLog("Click on Pre-Sold Admin button");
			CommonFunctions.attachScreenshot();

			// Click on Save button
			ActionHandler.click(OUVvehicleContainer.Save);
			ActionHandler.wait(15);
			Reporter.addStepLog("Click on Save button");
			CommonFunctions.attachScreenshot();
		}

	}

	@Then("^Logout of OUV Portal$")
	public void logout_of_OUV_Portal() throws Throwable {

		// Logout of OUV Portal
		ActionHandler.click(OUVvehicleContainer.Logoutimage);
		ActionHandler.wait(15);
		Reporter.addStepLog("Logout of OUV Portal");
		CommonFunctions.attachScreenshot();

		// User able to click Addusername
		ActionHandler.click(OUVvehicleContainer.Addusername);
		ActionHandler.wait(15);
		Reporter.addStepLog("User able to click Addusername");
		CommonFunctions.attachScreenshot();
	}

	// Select available vehicle
	@Then("^select the available vehicle$")
	public void select_the_available_vehicle() throws Throwable {

		// Select available vehicle
		ActionHandler.click(OUVvehicleContainer.SelectSameVehicle);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select vehicle");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Edit the New Requested Hand In Date$")
	public void edit_the_New_Requested_Hand_In_Date() throws Throwable {

		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);

		// User able to click EditHandinDate button
		ActionHandler.click(OUVvehicleContainer.EditHandinDate);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to click EditHandinDate button");
		CommonFunctions.attachScreenshot();

	}

	@Then("^Select any future date$")
	public void select_any_future_date() throws Throwable {

		// User able to enter HandinDate
		ActionHandler.click(OUVvehicleContainer.EnterHandinDate);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to enter HandinDate");
		CommonFunctions.attachScreenshot();

		// select future date
		String HandinDate;
		HandinDate = CommonFunctions.selectFutureDate();

		ActionHandler.clearAndSetText(OUVvehicleContainer.EnterHandinDate, HandinDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to edit the New Requested Hand In Date on OUV Vehicle window HandinDate");

	}

	@Then("^Enter Justification and click on Save \"([^\"]*)\"$")
	public void enter_Justification_and_click_on_Save(String Justifiaction) throws Throwable {

		String Sea[] = Justifiaction.split(",");
		Justifiaction = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		// User able to enter justification
		ActionHandler.click(OUVvehicleContainer.justification);
		ActionHandler.wait(5);
		ActionHandler.setText(OUVvehicleContainer.justification, Justifiaction);
		ActionHandler.wait(10);
		Reporter.addStepLog("User able to enter justification");
		CommonFunctions.attachScreenshot();

		// Click on Save button
		ActionHandler.click(OUVvehicleContainer.Save);
		ActionHandler.wait(15);
		Reporter.addStepLog("Click on Save button");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Validate that an error message is displayed while saving$")
	public void validate_that_an_error_message_is_displayed_while_saving() throws Throwable {

		// Validate Error messgare displayed
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.ClickHandindateError);
		ActionHandler.wait(3);
		Reporter.addStepLog("Validate Error messgare displayed");
		CommonFunctions.attachScreenshot();

		// User able to Click error message
		ActionHandler.click(OUVvehicleContainer.ClickHandindateError);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to Click error message");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Validate that a warning message is displayed for New Requested Hand In Date$")
	public void validate_that_a_warning_message_is_displayed_for_New_Requested_Hand_In_Date() throws Throwable {

		// Validate Error messgare displayed
		ActionHandler.scrollToView(OUVvehicleContainer.VerifyHandindateError);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.VerifyHandindateError);
		ActionHandler.wait(3);
		Reporter.addStepLog("Validate Error messgare displayed");
		CommonFunctions.attachScreenshot();

		// User able to Click cancel
		ActionHandler.click(OUVvehicleContainer.Clickcancel);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to Click cancel");
		CommonFunctions.attachScreenshot();

	}

	@Then("^select the edit button for the OUV Vehicle$")
	public void select_the_edit_button_for_the_OUV_Vehicle() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVvehicleContainer.EditButton);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on edit button");

		driver.navigate().refresh();
		ActionHandler.wait(7);

	}

	// User edits master status
	@Then("^User updates master status to \"([^\"]*)\"$")
	public void user_updates_master_status_to(String status) throws Throwable {
		String Sea[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(OUVvehicleContainer.MasterStatusselect);
		ActionHandler.wait(3);

		// Select master dropdown
		ActionHandler.click(OUVvehicleContainer.Masterstatus1);
		ActionHandler.wait(7);
		Reporter.addStepLog("Select master dropdown");
		CommonFunctions.attachScreenshot();

		// Select dropdown
		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(status))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Select dropdown");
		CommonFunctions.attachScreenshot();
	}

	// User edits sub status
	@Then("^User updates sub status to \"([^\"]*)\"$")
	public void user_updates_sub_status_to(String status) throws Throwable {
		String Sea[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.wait(3);

		// Select sub status dropdown
		ActionHandler.click(OUVvehicleContainer.substatus1);
		ActionHandler.wait(3);
		Reporter.addStepLog("Select sub status dropdown");
		CommonFunctions.attachScreenshot();

		// Select dropdown
		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.ValueSelection(status))));
		ActionHandler.wait(3);
		Reporter.addStepLog("Select dropdown");
		CommonFunctions.attachScreenshot();
	}

	// Search for an vehicle
	@Then("^search the vehicle with status \"([^\"]*)\"$")
	public void search_the_vehicle_with_status(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// Search for an vehicle
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVvehicleContainer.UATEditable);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Vehicles");

		ActionHandler.click(OUVvehicleContainer.searchVehiclebox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVvehicleContainer.searchVehiclebox, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an vehicle");
		CommonFunctions.attachScreenshot();
	}

	// Select multiple checkbox for OUV Vehicle with fleet as PR NL in OUV Vehicle
	// search list window
	@Then("^Select multiple checkbox for OUV Vehicle with fleet as PR NL in OUV Vehicle search list window$")
	public void select_multiple_checkbox_for_OUV_Vehicle_with_fleet_as_PR_NL_in_OUV_Vehicle_search_list_window()
			throws Throwable {

		// Select multiple Checkbox

		/*
		 * ActionHandler.click(OUVvehicleContainer.MultipleCheck);
		 * CommonFunctions.attachScreenshot(); ActionHandler.wait(2);
		 * Reporter.addStepLog("Select multiple Checkbox");
		 */

		ActionHandler.click(OUVvehicleContainer.MultipleCheck1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.MultipleCheck2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.MultipleCheck3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("Select multiple Checkbox");

	}

	// Click on Change Master Status button
	@Then("^Click on Change Master Status button$")
	public void click_on_Change_Master_Status_button() throws Throwable {

		// Click on Change Master Status button
		ActionHandler.click(OUVvehicleContainer.Changemasterstatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("Click on Change Master Status button");
	}

	@Then("^Add Mandatory details Master Status \"([^\"]*)\" Sub Status \"([^\"]*)\" Location \"([^\"]*)\" No of Keys \"([^\"]*)\"$")
	public void add_Mandatory_details_Master_Status_Sub_Status_Location_No_of_Keys(String Masterstatus,
			String subststus, String location, String Keys) throws Throwable {

		String Sea[] = Masterstatus.split(",");
		Masterstatus = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		String Sea1[] = subststus.split(",");
		subststus = CommonFunctions.readExcelMasterData(Sea1[0], Sea1[1], Sea1[2]);

		String Sea2[] = location.split(",");
		location = CommonFunctions.readExcelMasterData(Sea2[0], Sea2[1], Sea2[2]);

		String Sea3[] = Keys.split(",");
		Keys = CommonFunctions.readExcelMasterData(Sea3[0], Sea3[1], Sea3[2]);

		// User able to select click master status
		ActionHandler.click(OUVvehicleContainer.masterstatus);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select click master status");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.masterstatus(Masterstatus))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User able to select click sub status
		ActionHandler.click(OUVvehicleContainer.Substatus);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select click sub status");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.Substatus(subststus))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User able to select Primary Location
		ActionHandler.click(OUVvehicleContainer.selectlocation);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select Primary Location");
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVvehicleContainer.selectlocation, location);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(org.openqa.selenium.Keys.ARROW_DOWN).build().perform();
		act.sendKeys(org.openqa.selenium.Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User able to select Transfer Keys
		ActionHandler.click(OUVvehicleContainer.TransferCheck);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select Transfer Keys");
		CommonFunctions.attachScreenshot();

		// User able to select No of Keys Received
		ActionHandler.click(OUVvehicleContainer.Keysreceived);
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to select No of Keys Received");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVvehicleContainer.Keysreceived(Keys))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// User click on save button
		ActionHandler.click(OUVvehicleContainer.saved);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on save button");
		CommonFunctions.attachScreenshot();

		// User click on Ok button
		ActionHandler.click(OUVvehicleContainer.ok);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on ok button");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Click on the List View dropdown and select All \"([^\"]*)\"$")
	public void click_on_the_List_View_dropdown_and_select_All(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// Search for an vehicle
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVvehicleContainer.RecentlyViews);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Vehicles");

	}

	// Validate the Master status for all the vehicles changed to Live
	@Then("^Validate the Master status for all the vehicles changed to Live$")
	public void validate_the_Master_status_for_all_the_vehicles_changed_to_Live() throws Throwable {

		// Validate the Master status for all the vehicles changed to Live
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.validateliveststus);
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.validateliveststus1);
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.validateliveststus2);
		ActionHandler.wait(3);
		Reporter.addStepLog("Validate the Master status for all the vehicles changed to Live");
		CommonFunctions.attachScreenshot();
	}

	// Search for an vehicle
	@Then("^search vehicle with status \"([^\"]*)\"$")
	public void search_vehicle_with_status(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// Search for an vehicle
		ActionHandler.wait(2);
		ActionHandler.click(OUVvehicleContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVvehicleContainer.Editable);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Vehicles");

		ActionHandler.click(OUVvehicleContainer.searchVehiclebox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVvehicleContainer.searchVehiclebox, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an vehicle");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Validate error message is displayed$")
	public void validate_error_message_is_displayed() throws Throwable {

		// Validate error message is displayed
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.verifyerrormsg);
		ActionHandler.wait(3);
		Reporter.addStepLog("Validate error message is displayed");
		CommonFunctions.attachScreenshot();
	}

	// Select the checkboxes, and click on dismiss
	@Then("^Select the checkboxes, and click on dismiss$")
	public void select_the_checkboxes_and_click_on_dismiss() throws Throwable {

		ActionHandler.click(OUVvehicleContainer.Dismiss);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVvehicleContainer.Dismissandclose);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("Select the checkboxes, and click on dismiss");
	}

	// Verify the displayed error message for insufficient permissions
	@Then("^Verify the displayed error message for insufficient permissions$")
	public void verify_the_displayed_error_message_for_insufficient_permissions() throws Throwable {

		ActionHandler.wait(3);
		// Verify the error message
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.PermissionsError);
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the error message");
		CommonFunctions.attachScreenshot();

		// The status has not updated
		VerifyHandler.verifyElementPresent(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("The status has not updated");
		CommonFunctions.attachScreenshot();

		// Click on the cancel button
		ActionHandler.click(OUVvehicleContainer.Cancel);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on the cancel button");
		CommonFunctions.attachScreenshot();
	}

}
