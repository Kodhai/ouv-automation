package com.jlr.ouv.tests;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.ouv.constants.Constants;
import com.jlr.ouv.containers.OUVCalendarContainer;
import com.jlr.ouv.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class OUVCalendarTest extends TestBaseCC {
	public WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(OUVCalendarTest.class.getName());
	public OUVCalendarContainer OUVCalendarContainer = PageFactory.initElements(driver, OUVCalendarContainer.class);
	JavaScriptUtil javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	String ID = "Booking";
	String driverName = "New";

	public void onStart() {
		setupTest("OUVTest");
		driver = getDriver();
		OUVCalendarContainer = PageFactory.initElements(driver, OUVCalendarContainer.class);
		javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	}

	// User saves an OUV Booking
	public void User_Saves_OUV_Booking(String bookingQuickRef, String bookingJustification, String bookingReqType,
			String passoutType, String journeyType) throws Exception {

		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";

		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String driver1 = "Test";

		String otherLocation = "Other Location";

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// Selecting vehicles from Reservation Calendar
	public void selectVehicleCalendar() throws Exception {
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.click(OUVCalendarContainer.dayBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.TodayCalendar);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.rightButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.SelectVehicleFromDay);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle from day tab");
	}

	// Selecting Vehicles Calendar
	public void selectVehiclesCalendar() throws Exception {
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.click(OUVCalendarContainer.dayBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.TodayCalendar);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.rightButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

	}

	@Given("^Access the OUV Portal with user \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void access_the_OUV_Portal_with_user_and_password_as(String UserName, String Password) throws Throwable {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Test Begin......");
		Reporter.addStepLog("User Access OUV Portal");
		onStart();
		driver.get(Constants.OUVURL);
		ActionHandler.wait(7);
		Reporter.addStepLog("User Logins to OUV Portal");

		ActionHandler.setText(OUVCalendarContainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(7);

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.Verifycode)) {
			ActionHandler.wait(60);

			ActionHandler.click(OUVCalendarContainer.vCodeTextBox);
			ActionHandler.wait(5);
			Reporter.addStepLog("User have entered Verification Code");
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(OUVCalendarContainer.verifyBtn);
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			navigateToOUV();

		}
		ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);

		if (!VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome)) {
			ActionHandler.wait(4);
			driver.navigate().refresh();
			ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);
		}
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome);
	}

	// Navigate to OUV Portal
	public void navigateToOUV() throws Exception {
		String OUV = "OUV";
		ActionHandler.wait(3);
		Reporter.addStepLog("User tries to navigate to OUV Portal");
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.jlrText)) {
			ActionHandler.wait(2);
			ActionHandler.click(OUVCalendarContainer.menuBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(OUVCalendarContainer.searchBox, OUV);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.selectMenu(OUV))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

	// Below code is used to Search Vehicle by giving Market and Fleet
	@Then("^Search for Vehicle by giving Market as \"([^\"]*)\" and Fleet as \"([^\"]*)\"$")
	public void search_Vehicle_by_giving_Market_as_and_Fleet_as(String market, String fleet) throws Throwable {
		String fleetValue[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fleetValue[0], fleetValue[1], fleetValue[2]);
		System.out.println("Fetched Fleet value is = " + fleet);

		String MarketValue[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(MarketValue[0], MarketValue[1], MarketValue[2]);
		System.out.println("Fetched Fleet value is = " + market);

		ActionHandler.wait(5);

		Select marDD = new Select(OUVCalendarContainer.marketDD);
		marDD.selectByVisibleText(market);
		ActionHandler.wait(1);
		Reporter.addStepLog("Market selection has been done");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		Select fleetDD = new Select(OUVCalendarContainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");
	}

	// Selecting day tab in calendar
	@Then("^select the day tab in calendar and verify its format$")
	public void select_the_day_tab_in_calendar_and_verify_its_format() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the reservarion calendar has Days, Weeks and Month");

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.dayBtn);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.weekBtn);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.monthBtn);
		System.out.println("Days, Week and Month button is available");

		ActionHandler.click(OUVCalendarContainer.dayBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on day tab");

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.dayformat);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the format");
	}

	// Selecting Week Tab in Calendar
	@Then("^select the Week tab in calendar and verify its format$")
	public void select_the_Week_tab_in_calendar_and_verify_its_format() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the reservarion calendar has Days, Weeks and Month");

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.dayBtn);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.weekBtn);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.monthBtn);
		System.out.println("Days, Week and Month button is available");

		ActionHandler.click(OUVCalendarContainer.weekBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on Week tab");

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.weekformat);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the format");
	}

	// Selecting Month Tab in Calendar
	@Then("^select the Month tab in calendar and verify its format$")
	public void select_the_Month_tab_in_calendar_and_verify_its_format() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the reservarion calendar has Days, Weeks and Month");

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.dayBtn);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.weekBtn);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.monthBtn);
		System.out.println("Days, Week and Month button is available");

		ActionHandler.click(OUVCalendarContainer.monthBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on month tab");

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.monthformat);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the format");
	}

	// verify the View list of searched vehicles in the Reservation Calendar
	@Then("^verify the View list of searched vehicles in the Reservation Calendar$")
	public void verify_the_list_searched_vehicles_Reservation_Calendar() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the reservarion calendar has Days, Weeks and Month");

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.dayBtn);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.weekBtn);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.monthBtn);
		System.out.println("Days, Week and Month button is available");

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.VehicleList);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that searched vehicles are present");
	}

	// Select an available Booking Period
	@When("^selected an available booking period$")
	public void selected_an_available_booking_period() throws Throwable {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.VehicleList);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that searched vehicles are present");

		ActionHandler.click(OUVCalendarContainer.weekBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.calendarSelection);
		ActionHandler.wait(1);
		System.out.println("Week has been selected");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle and Week selection is done");

		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.reservation);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("OUV Booking Pop Up is displayed");
	}

	// Close the OUV Booking Pop up
	@Then("^Close the OUV Booking Pop Up$")
	public void close_the_OUV_Booking_Pop_Up() throws Throwable {
		ActionHandler.wait(1);
		ActionHandler.click(OUVCalendarContainer.cancel);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("close reservation window");
	}

	// Naviagte to OUV Calendar
	@And("^User navigate to OUV Calendar$")
	public void User_Navigate_OUV_Calendar() throws Exception {
		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.OUVCalendar);
		ActionHandler.wait(10);
		ActionHandler.waitForElement(OUVCalendarContainer.searchVehicleText, 60);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV Calendar");
	}

	// User searches Vehicle by selecting Market and Fleet
	@Then("^User searches Vehicle by selecting Market \"([^\"]*)\" and Fleet \"([^\"]*)\"$")
	public void User_Searches_vehicle_market_fleet(String market, String fleet) throws Exception {
		String m[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String f[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(f[0], f[1], f[2]);

		ActionHandler.wait(5);
		Select marDD = new Select(OUVCalendarContainer.marketDD);
		marDD.selectByVisibleText(market);
		ActionHandler.wait(3);
		Reporter.addStepLog("Market selection has been done");
		CommonFunctions.attachScreenshot();

		Select fleetDD = new Select(OUVCalendarContainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
	}

	// View all searched Vehicle and select available vehicle
	@And("^View all searched Vehicle and select available vehicle$")
	public void View_Add_Searched_vehicle_Select_Available_vehicle() throws IOException {
		ActionHandler.wait(3);
		if (!VerifyHandler.verifyElementPresent(OUVCalendarContainer.calendarSelection)) {
			ActionHandler.wait(2);
			javascriptUtil.scrollIntoView(OUVCalendarContainer.calendarSelection);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
		}
		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.calendarSelection);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User can select Vehicle for reservation");

	}

	// Verify that user can select Reservation from the dropdown on OUV Booking
	// window
	@Then("^Verify that user can select Reservation from the dropdown on OUV Booking window$")
	public void Verify_User_Select_Reservation_Drop_Down() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.reservation);
		System.out.println("User opens drop down on OUV Booking window");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.reservationSelection);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User selects Reservation[Passout] option from drop down");
	}

	// Logout from OUV Portal
	@Then("^Logout from OUV$")
	public void Logout_OUV() throws Exception {
		driver = getDriver();

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.loginImg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.logout);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Logout from OUV Portal");
		driver.quit();

	}

	// User edits start date and start time
	@And("^User edit Start Date \"([^\"]*)\" and Start Time \"([^\"]*)\" on OUV Booking window$")
	public void User_Edit_Start_Date_Start_Time(String startDate, String startTime) throws Exception {
		String sd[] = startDate.split(",");
		startDate = CommonFunctions.readExcelMasterData(sd[0], sd[1], sd[2]);

		String st[] = startTime.split(",");
		startTime = CommonFunctions.readExcelMasterData(st[0], st[1], st[2]);

		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.startDate, startDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.startTime);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.startTime, startTime);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		Reporter.addStepLog("User edits start date and start time on OUV Booking window");
	}

	// User edit End Date and End Time
	@And("^User edit End Date \"([^\"]*)\" and End Time \"([^\"]*)\" on OUV Booking window$")
	public void User_Edits_End_Date_End_Time(String endDate, String endTime) throws Exception {
		String ed[] = endDate.split(",");
		endDate = CommonFunctions.readExcelMasterData(ed[0], ed[1], ed[2]);

		String et[] = endTime.split(",");
		endTime = CommonFunctions.readExcelMasterData(et[0], et[1], et[2]);

		ActionHandler.click(OUVCalendarContainer.endDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVCalendarContainer.endDate, endDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.endTime);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVCalendarContainer.endTime, endTime);
		ActionHandler.wait(1);
		OUVCalendarContainer.endTime.sendKeys(Keys.TAB);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits End Date and End Time on OUV Booking window");
	}

	// User saves an OUV Booking with Start Date
	@And("^User saves an OUV Booking with Booking Quick Reference \"([^\"]*)\",Start Date \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Saves_OUV_Booking_With_StartDate(String bookingQuickRef, String startDate,
			String bookingJustification, String bookingReqType, String passoutType, String journeyType)
			throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String driver1 = "Test123";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String sd[] = startDate.split(",");
		startDate = CommonFunctions.readExcelMasterData(sd[0], sd[1], sd[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		ActionHandler.clearAndSetText(OUVCalendarContainer.startDate, startDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// User saves an OUV Booking with End Date
	@And("^User saves an OUV Booking with Booking Quick Reference \"([^\"]*)\",End Date \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Saves_OUV_Booking_With_EndDate(String bookingQuickRef, String endDate, String bookingJustification,
			String bookingReqType, String passoutType, String journeyType) throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String driver1 = "testdriver";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String ed[] = endDate.split(",");
		endDate = CommonFunctions.readExcelMasterData(ed[0], ed[1], ed[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		ActionHandler.clearAndSetText(OUVCalendarContainer.endDate, endDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// Validates an error message when user saves an OUV Booking with prior Start
	// Date
	@Then("^An error occurred when user saves an OUV Booking with prior Start Date$")
	public void Error_Occurred_User_Saves_OUV_Booking_Prior_start_date() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.error);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("An error occurred when user saves an OUV booking in past start date");
	}

	// Validates an error message when user saves an OUV Booking with prior End Date
	@Then("^An error occurred when user saves an OUV Booking with prior End Date$")
	public void Error_Occurred_User_Saves_OUV_Booking_prior_End_date() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.error);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("An error occurred when user tries to save an OUV booking in past end date");
	}

	// Validates an error message when user selects prior booking end date
	@Then("^Verify the error message displayed on selecting prior booking end date$")
	public void verify_the_error_message_displayed_on_selecting_prior_booking_end_date() throws Throwable {
		ActionHandler.wait(5);
		Reporter.addStepLog("Validate the prior end date error message");
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.errorPriorEnd);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
	}

	// Select mandatory fields start date, end date and search vehicles
	@Then("^Select mandatory fields start date, end date and search vehicles$")
	public void select_mandatory_fields_start_date_end_date_and_search_vehicles() throws Throwable {
		ActionHandler.wait(5);
		Reporter.addStepLog("Verify that the default start date is selected as current date");

		ActionHandler.wait(5);
		String EDate = "1-Jan-2030";
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(OUVCalendarContainer.endDate, EDate);
		Reporter.addStepLog("End date selected");

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the vehicles in the selected range are dsplayed in the page");
	}

	// Selecting market, fleet, start date, end date and search vehicles
	@Then("^Select market, fleet, start date, end date and search vehicles$")
	public void select_market_fleet_start_date_end_date_and_search_vehicles() throws Throwable {
		String market = "Netherlands";
		ActionHandler.wait(5);
		Select marDD = new Select(OUVCalendarContainer.marketDD);
		marDD.selectByVisibleText(market);
		ActionHandler.wait(5);
		Reporter.addStepLog("Market selection has been done");
		CommonFunctions.attachScreenshot();

		String fleet = "Netherlands - PR NL";
		Select fleetDD = new Select(OUVCalendarContainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		ActionHandler.wait(5);
		Reporter.addStepLog("Verify that the default start date is selected as current date");

		ActionHandler.wait(5);
		String EDate = "1-Jan-2030";
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(OUVCalendarContainer.endDate, EDate);
		Reporter.addStepLog("End date selected");

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the vehicles in the selected range are dsplayed in the page");
	}

	// Verifying that unavailable vehicles are greyed out
	@Then("^Verify that unavailable vehicles are greyed out$")
	public void verify_that_unavailable_vehicles_are_greyed_out() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.scrollToView(OUVCalendarContainer.monthView);
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.monthView);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.unavailableVehcReser);
		ActionHandler.wait(5);
		// Need to remove click on cancel after defect is resolved
		ActionHandler.click(OUVCalendarContainer.cancelReser);
		ActionHandler.wait(5);
		Reporter.addStepLog(
				"Unavailable vehicles are greyed out and user should not be able to make reservations for the same");

	}

	// Verifying that user is able to select an available Vehicle
	@Then("^Verify that user is able to select an available Vehicle$")
	public void verify_that_user_is_able_to_select_an_available_Vehicle() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.scrollToView(OUVCalendarContainer.monthView);
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.monthView);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.availableVehcReser);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.cancelReser);
		ActionHandler.wait(5);
		Reporter.addStepLog("User is able to select an available Vehicle");
		ActionHandler.wait(1);
	}

	// Selecting Day, Week and Month Tab in the Reservation Calendar
	@Then("^Select Day, Week and Month Tab in the Reservation Calendar$")
	public void select_Day_Week_and_Month_Tab_in_the_Reservation_Calendar() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.dayView);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.weekView);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.monthView);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to select Day, Week and Month Tab in the Reservation Calendar");
	}

	// Verifying the error message displayed for No records found
	@Then("^Verify the error message displayed for No records found$")
	public void verify_the_error_message_displayed_for_No_records_found() throws Throwable {
		ActionHandler.wait(5);
		Reporter.addStepLog("Validate the error message for no records found");
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.NoRecordError);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
	}

	// User searches for Vehicles by giving Market and Fleet
	@Then("^Search Vehicles by giving Market as \"([^\"]*)\" and Fleet as \"([^\"]*)\"$")
	public void search_Vehicles_by_giving_Market_as_and_Fleet_as(String market, String fleet) throws Throwable {
		String fleetValue[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fleetValue[0], fleetValue[1], fleetValue[2]);
		System.out.println("Fetched Fleet value is = " + fleet);

		String MarketValue[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(MarketValue[0], MarketValue[1], MarketValue[2]);
		System.out.println("Fetched Fleet value is = " + market);

		ActionHandler.wait(3);
		Select marDD = new Select(OUVCalendarContainer.marketDD);
		marDD.selectByVisibleText(market);
		ActionHandler.wait(3);
		Reporter.addStepLog("Market selection has been done");
		CommonFunctions.attachScreenshot();

		Select fleetDD = new Select(OUVCalendarContainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");
	}

	// User searches for Vehicles
	@Then("^Search for Vehicles$")
	public void search_for_Vehicles() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");
	}

	// User Searches Vehicles for Brand
	@Then("^Search Vehicles for Brand \"([^\"]*)\"$")
	public void search_Vehicles_for_Brand(String Brand) throws Throwable {

		ActionHandler.click(OUVCalendarContainer.BrandDD);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.SelectBrand(Brand))));
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects a Brand");
		CommonFunctions.attachScreenshot();

	}

	// Selecting Future Date
	public String selectFutureDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 3);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	// Selecting FutureEnd Date
	public String selectFutureEndDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 4);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	// SelectingTomorrowDate
	public String selectTomorrowDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	// User searches for vehicles by selecting Future Booking Start Date and End
	@Given("^Search Vehicles by selecting Future Booking Start Date and End Date$")
	public void search_Vehicles_by_selecting_Future_Booking_Start_Date_and_End_Date() throws Throwable {
		Date date = new Date();
		System.out.println("Todays date is = " + date);
		System.out.println("Todays date is = " + date);

		String startDate = selectTomorrowDate();
		String endDate = selectFutureDate();

		ActionHandler.click(OUVCalendarContainer.BookingStartdate);
		ActionHandler.clearAndSetText(OUVCalendarContainer.BookingStartdate, startDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(OUVCalendarContainer.BookingEnddate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVCalendarContainer.BookingEnddate, endDate);
		CommonFunctions.attachScreenshot();
	}

	// Select Yesterday Date
	public String selectYesterdayDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, -1);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	// User searches for Vehicles by selecting Prior Booking End Date
	@Given("^Search Vehicles by selecting Prior Booking End Date$")
	public void Search_Vehicles_by_selecting_Prior_Booking_End_Date() throws Throwable {
		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String startDate = sdt.format(date);
		System.out.println("Todays date is = " + date);
		System.out.println("Todays date is = " + date);

		String endDate = selectYesterdayDate();

		ActionHandler.click(OUVCalendarContainer.BookingStartdate);
		ActionHandler.clearAndSetText(OUVCalendarContainer.BookingStartdate, startDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(OUVCalendarContainer.BookingEnddate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVCalendarContainer.BookingEnddate, endDate);
		CommonFunctions.attachScreenshot();

	}

	// Search Vehicles by selecting Prior Booking Start Date
	@Given("^Search Vehicles by selecting Prior Booking Start Date$")
	public void Search_Vehicles_by_selecting_Prior_Booking_Start_Date() throws Throwable {
		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String endDate = sdt.format(date);
		System.out.println("Todays date is = " + date);
		System.out.println("Todays date is = " + date);

		String startDate = selectYesterdayDate();

		ActionHandler.click(OUVCalendarContainer.BookingStartdate);
		ActionHandler.clearAndSetText(OUVCalendarContainer.BookingStartdate, startDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.click(OUVCalendarContainer.BookingEnddate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVCalendarContainer.BookingEnddate, endDate);
		CommonFunctions.attachScreenshot();

	}

	// User verifies the displayed Booking Start Date error message
	@Then("^Verify the displayed Booking Start Date error message$")
	public void verify_the_displayed_Booking_Start_Date_error_message() throws Throwable {
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Validate Booking start date error");
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.errorStartdate);
	}

	// Select market, fleet, prior booking end date and search vehicles
	@Then("^Select market, fleet, prior booking end date and search vehicles$")
	public void select_market_fleet_prior_booking_end_date_and_search_vehicles() throws Throwable {
		String market = "Netherlands";
		ActionHandler.wait(5);
		Select marDD = new Select(OUVCalendarContainer.marketDD);
		marDD.selectByVisibleText(market);
		ActionHandler.wait(5);
		Reporter.addStepLog("Market selection has been done");
		CommonFunctions.attachScreenshot();

		String fleet = "Netherlands - PR NL";
		Select fleetDD = new Select(OUVCalendarContainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		String EDate = "1-Jan-2020";
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(OUVCalendarContainer.endDate, EDate);
		Reporter.addStepLog("Prior end date selected");

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// User edits details of OUV Booking
	@Then("^User edits details of OUV Booking$")
	public void User_Edits_Details_OUV_Booking() throws Exception {
		String updatedBookingRef = "Test Booking Edited";
		String bookingReqType = "Multiple Vehicle booking";

		ActionHandler.click(OUVCalendarContainer.editRes);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.bookingQuickRef, updatedBookingRef);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits Booking Quick Reference and Booking Request Type");

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		ActionHandler.wait(7);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(updatedBookingRef))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// User opens any existing OUV Booking which is not yet approved from OUV
	// Calendar
	@And("^User opens any existing OUV Booking which is not yet approved from OUV Calendar$")
	public void User_Opens_Existing_OUV_Booking_not_Approved() throws Exception {
		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.firstOUVBooking)) {
			ActionHandler.click(OUVCalendarContainer.firstOUVBooking);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User should be able to open any existing OUV Booking");
		} else {
			Reporter.addStepLog("No any OUV Booking found. User needs to create a new OUV Booking");
			String bookingQuickRef = "Test Booking Edit";
			String bookingJustification = "Event/Sales activity";
			String bookingReqType = "Single Vehicle Booking";
			String passoutType = "Overnight";
			String journeyType = "Return";

			ActionHandler.wait(3);
			Reporter.addStepLog("User selects any vehicle for Booking");
			View_Add_Searched_vehicle_Select_an_Available_vehicle();
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User makes reservation for selected Vehicle");
			User_Saves_OUV_Booking(bookingQuickRef, bookingJustification, bookingReqType, passoutType, journeyType);
			ActionHandler.wait(3);

			ActionHandler.wait(7);
			ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
		}

	}

	// User deletes created OUV Booking
	@Then("^User deletes created OUV Booking$")
	public void User_Deletes_Created_OUV_Booking() throws Exception {
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Delete button to delete OUV Booking");
		ActionHandler.click(OUVCalendarContainer.deleteOUVBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.ConfirmDeleteOUVBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// Verify that User cannot delete OUV Booking
	@And("^Verify that User cannot delete OUV Booking$")
	public void Verify_User_Cannot_Delete_OUV_Booking() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.deleteErrorMessage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.closeDeleteErrorMessage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.closeReservationWindow);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("An error occurred when user deletes OUV Booking");
	}

	// User clicks on View In Salesforce button and verify all details are displayed
	@Then("^User clicks on View In Salesforce button and verify all details are displayed$")
	public void User_Clicks_View_In_Salesforce_Button() throws Exception {
		ActionHandler.wait(3);
		String resWindow = driver.getWindowHandle();
		System.out.println("Reservation Window id = " + resWindow);

		ActionHandler.click(OUVCalendarContainer.viewInSalesForce);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Set<String> bookingWindow = driver.getWindowHandles();
		Iterator<String> i = bookingWindow.iterator();
		while (i.hasNext()) {
			String window = i.next();
			if (!resWindow.equalsIgnoreCase(window)) {
				driver.switchTo().window(window);
				ActionHandler.wait(3);

				if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.reservationText)) {
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.pageScrollDown();
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					ActionHandler.pageScrollDown();
					ActionHandler.wait(3);
					CommonFunctions.attachScreenshot();

					Reporter.addStepLog("User can view details of OUV Booking");
				}
				driver.close();
				driver.switchTo().window(resWindow);
			}
		}
		Reporter.addStepLog("User clicks on View in Salesforce button");
	}

	// User cancels any changes made in an existing OUV Booking
	@Then("^User cancels any changes made in an existing OUV Booking$")
	public void User_Cancels_Any_Changes_made_OUV_Booking() throws Exception {
		ActionHandler.wait(3);
		String updatedBookingRef = "Test Booking Edited";
		String bookingReqType = "Multiple Vehicle booking";

		ActionHandler.click(OUVCalendarContainer.editRes);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.bookingQuickRef, updatedBookingRef);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits Booking Quick Reference and Booking Request Type");

		ActionHandler.wait(1);
		ActionHandler.click(OUVCalendarContainer.cancelEditOUVBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cancels any changes made in an existing OUV Booking");
	}

	// User saves an OUV Booking without driver and other location
	@And("^User saves an OUV Booking with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Saves_OUV_Booking_witout_driver_other_location(String bookingQuickRef, String bookingJustification,
			String bookingReqType, String passoutType, String journeyType) throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// User is able to verify all the details are auto-populated for the Booking
	@Then("^User is able to verify all the details are auto-populated with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void user_is_able_to_verify_all_the_details_are_auto_populated_with(String bookingQuickRef,
			String bookingJustification, String bookingReqType, String passoutType, String journeyType)
			throws Throwable {

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(
				driver.findElement(By.xpath(OUVCalendarContainer.Fieldvalidation(bookingQuickRef))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(
				driver.findElement(By.xpath(OUVCalendarContainer.Fieldvalidation(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(
				driver.findElement(By.xpath(OUVCalendarContainer.Fieldvalidation2(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.Fieldvalidation(passoutType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.Fieldvalidation(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

	}

	// User saves an OUV Booking including other location
	@And("^User saves an OUV Booking including other location with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Saves_OUV_Booking_including_other_location(String bookingQuickRef, String bookingJustification,
			String bookingReqType, String passoutType, String journeyType) throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.c";
		String driver1 = "DHL";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.scrollToView(OUVCalendarContainer.Details);
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// User validates the details in the Reservation Calendar
	@Then("^Validate the details in the Reservation Calender$")
	public void validate_the_details_in_the_Reservation_Calender() throws Exception {
		ActionHandler.wait(15);
		Actions action = new Actions(driver);
		action.moveToElement(OUVCalendarContainer.Calendarselection).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to validate the details in the Reservation Calender");
	}

	// User verifies the error message while saving the booking
	@Then("^An error occurred when user saves an OUV Booking$")
	public void Error_Occurred_User_Saves_OUV_Booking() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.error);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("An error occurred when user saves an OUV booking");

	}

	// User View all searched Vehicle and select an available vehicle
	@And("^View all searched Vehicle and select an available vehicle$")
	public void View_Add_Searched_vehicle_Select_an_Available_vehicle() throws Exception {
		selectVehicleCalendar();
		ActionHandler.wait(2);
		Reporter.addStepLog("User can select Vehicle for reservation");

	}

	// User saves an OUV Booking including Preparation/Transportation
	@Then("^User saves an OUV Booking including Preparation/Transportation field with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void user_saves_an_OUV_Booking_including_Preparation_Transportation_field_with_Booking_Quick_Reference_Booking_Justification_Booking_Request_Type_Passout_Type_Journey_Type(
			String bookingQuickRef, String bookingJustification, String bookingReqType, String passoutType,
			String journeyType) throws Throwable {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		ActionHandler.click(OUVCalendarContainer.PrepTrans);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.SelectStartDate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		String start = selectTomorrowDate();
		ActionHandler.clearAndSetText(OUVCalendarContainer.SelectStartDate, start);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select start date");

		ActionHandler.click(OUVCalendarContainer.SelectEndDate);
		String endDate = selectFutureEndDate();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVCalendarContainer.SelectEndDate, endDate);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.SelectPrepDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		String EndDate = selectFutureDate();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVCalendarContainer.SelectPrepDate, EndDate);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// Verify that Booking Start Date and Booking End date is pre-populated
	@And("^Verify that Booking Start Date and Booking End date is pre-populated$")
	public void Verify_Booking_Start_Date_Booking_End_Date_Auto_Populated() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.BookingStartdate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.BookingEnddate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User should be able to view prepopulated fields");
	}

	// Verify that User can see Start date and Start Time are Pre-populated
	@And("^Verify that User can see Start date and Start Time are Pre-populated$")
	public void Verify_User_View_Start_Date_Start_Time_Auto_Populated() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.startDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.startTime);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User should be able to view auto populated fields");
	}

	// Verify that User can see End date and End Time are Pre-populated
	@And("^Verify that User can see End date and End Time are Pre-populated$")
	public void Verify_User_View_End_date_End_Time_Auto_Populated() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.endDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.endTime);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User should be able to view auto populated fields");
	}

	// User saves an OUV Booking without entering Preparation/Transportation End
	// date
	@And("^User saves an OUV Booking without entering Preparation/Transportation End date Field with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Saves_An_OUV_booking_Without_Preparation_Transportation_End_date(String bookingQuickRef,
			String bookingJusti, String bookingReqType, String passoutType, String journeyType) throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJusti.split(",");
		bookingJusti = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		ActionHandler.click(OUVCalendarContainer.PrepTrans);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJusti))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// An error occurred while saving new OUV Booking without entering
	// Preparation/Trasportation End Date
	@Then("^An error occurred while saving new OUV Booking without entering Preparation/Trasportation End Date$")
	public void An_Error_occurred_Saving_New_OUV_Booking_Without_Entering_PreparationTransportatiom_End_Date()
			throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.deleteErrorMessage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"An error occurred when user tries to save OUV Booking without entering Preparation/Transportation field");

		ActionHandler.click(OUVCalendarContainer.closeDeleteErrorMessage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// User validate that OUV Vehicle is pre-populated in the OUV Booking window
	@Then("^User validate that OUV Vehicle is pre-populated in the OUV Booking window$")
	public void user_validate_that_OUV_Vehicle_is_pre_populated_in_the_OUV_Booking_window() throws Throwable {
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Vehicle);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("OUV Vehicle is pre-populated");
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.Close);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("OUV Booking window is closed");

	}

	// User validates that an approved OUV Booking is highlighted in green color
	@Then("^Validate that an approved OUV Booking is highlighted in green color$")
	public void validate_that_an_approved_OUV_Booking_is_highlighted_in_green_color() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.monthBtn);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.Previousmonth);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User should be navigated to the previous month");
		System.out.println("Validate Color");

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.firstOUVBooking)) {
			String bgcolor = driver.findElement(By.xpath("(//div[@class='scheduler_default_event_inner'])[1]"))
					.getCssValue("background-color");
			ActionHandler.wait(2);
			String[] hexValue = bgcolor.replace("rgba(", "").replace(")", "").split(",");

			System.out.println(hexValue);
			int hexValue1 = Integer.parseInt(hexValue[0]);
			hexValue[1] = hexValue[1].trim();
			int hexValue2 = Integer.parseInt(hexValue[1]);
			hexValue[2] = hexValue[2].trim();
			int hexValue3 = Integer.parseInt(hexValue[2]);

			String actualColor = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
			System.out.println(actualColor);
			Assert.assertEquals("#229951", actualColor);

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User should be able to validate the green color");
		} else {
			Reporter.addStepLog("No any OUV Booking found. User needs to create a new OUV Booking");
			String bookingQuickRef = "Test Booking Edit";
			String bookingJustification = "Event/Sales activity";
			String bookingReqType = "Single Vehicle Booking";
			String passoutType = "Overnight";
			String journeyType = "Return";

			ActionHandler.wait(3);
			Reporter.addStepLog("User selects any vehicle for Booking");
			View_Add_Searched_vehicle_Select_Available_vehicle();
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User makes reservation for selected Vehicle");
			User_Saves_OUV_Booking(bookingQuickRef, bookingJustification, bookingReqType, passoutType, journeyType);
			ActionHandler.wait(3);

			ActionHandler.wait(1);
			ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
		}
	}

	// User Validates that a Non Approved OUV Booking is highlighted in blue color
	@Then("^Validate that a Non Approved OUV Booking is highlighted in blue color$")
	public void validate_that_a_Non_Approved_OUV_Booking_is_highlighted_in_blue_color() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.monthBtn);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.Previousmonth);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User should be navigated to the previous month");
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.secondOUVBooking)) {
			String bgcolor = driver.findElement(By.xpath("(//div[@class='scheduler_default_event_inner'])[25]"))
					.getCssValue("background-color");
			ActionHandler.wait(2);
			String[] hexValue = bgcolor.replace("rgba(", "").replace(")", "").split(",");

			System.out.println(hexValue);
			int hexValue1 = Integer.parseInt(hexValue[0]);
			hexValue[1] = hexValue[1].trim();
			int hexValue2 = Integer.parseInt(hexValue[1]);
			hexValue[2] = hexValue[2].trim();
			int hexValue3 = Integer.parseInt(hexValue[2]);

			String actualColor = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
			System.out.println(actualColor);
			Assert.assertEquals("#85c1e9", actualColor);

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User should be able to validate the blue color");
		} else {
			Reporter.addStepLog("No any OUV Booking found. User needs to create a new OUV Booking");
			String bookingQuickRef = "Test Booking Edit";
			String bookingJustification = "Event/Sales activity";
			String bookingReqType = "Single Vehicle Booking";
			String passoutType = "Overnight";
			String journeyType = "Return";

			ActionHandler.wait(3);
			Reporter.addStepLog("User selects any vehicle for Booking");
			View_Add_Searched_vehicle_Select_Available_vehicle();
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User makes reservation for selected Vehicle");
			User_Saves_OUV_Booking(bookingQuickRef, bookingJustification, bookingReqType, passoutType, journeyType);
			ActionHandler.wait(3);

			/*
			 * ActionHandler.wait(1);
			 * ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.
			 * bookingTable(bookingQuickRef)))); ActionHandler.wait(3);
			 * CommonFunctions.attachScreenshot();
			 */
		}
	}

	// User navigates to OUV Booking ID
	@Then("^User navigates to OUV Booking ID$")
	public void user_navigates_to_OUV_Booking_ID() throws Throwable {
		ActionHandler.wait(15);

		ActionHandler.wait(20);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Passout);
		ActionHandler.wait(3);
		ActionHandler.waitForElement(OUVCalendarContainer.BookingID, 60);
		ActionHandler.click(OUVCalendarContainer.BookingID);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to the booking ID");
	}

	// Validate that user is able to see the Booking ID generated
	@Then("^Validate that user is able to see the Booking ID generated$")
	public void validate_that_user_is_able_to_see_the_Booking_ID_generated() throws Throwable {
		ActionHandler.wait(15);
		ActionHandler.click(OUVCalendarContainer.Calendarselection1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the newly created reservation");

		ActionHandler.wait(20);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Passout);
		ActionHandler.wait(3);
		ActionHandler.waitForElement(OUVCalendarContainer.BookingID, 60);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.BookingID);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates the Booking ID is generated");

	}

	// Validate that user sees a confirmation message for Reservation successfully
	// saved
	@Then("^Validate that user sees a confirmation message for Reservation successfully saved$")
	public void validate_that_user_sees_a_confirmation_message_for_Reservation_successfully_saved() throws Throwable {
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Confirmationmsg);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is able to validate the confirmation message");

	}

	// User selects Auto Generate VLA
	@Then("^User selects Auto Generate VLA$")
	public void user_selects_Auto_Generate_VLA() throws Throwable {
		ActionHandler.wait(10);

		javascriptUtil.scrollIntoView(OUVCalendarContainer.VehicleLoan);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Vehicle Loan Agreement");
		ActionHandler.click(OUVCalendarContainer.EditVLA);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.AutoVLA);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

	}

	// User selects Auto Generate VLA and enters details for Loan Agreement and
	// saves the record
	@Then("^User selects Auto Generate VLA and enters details for Loan Agreement and saves the record$")
	public void user_selects_Auto_Generate_VLA_and_enters_details_for_Loan_Agreement_and_saves_the_record()
			throws Throwable {

		ActionHandler.wait(10);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.VehicleLoan);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Vehicle Loan Agreement");
		ActionHandler.click(OUVCalendarContainer.EditVLA);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.AutoVLA);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.ViewdependTemplate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.VLATemplate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Jaguarwithcost);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.VLALanguage);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.English);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.ApplyVLA);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Savechanges);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the Vehicle Loan Agreement");

		ActionHandler.pageCompleteScrollUp();
	}

	// User saves the record and validate that an error message is displayed
	@Then("^User saves the record and validate that an error message is displayed$")
	public void user_saves_the_record_and_validate_that_an_error_message_is_displayed() throws Throwable {
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Savechanges);
		ActionHandler.wait(2);
		Reporter.addStepLog("User saves the Vehicle Loan Agreement");
		CommonFunctions.attachScreenshot();
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.VLAerror);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates the error message displayed");
		ActionHandler.click(OUVCalendarContainer.Cancelchanges);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// User adds an OUV Vehicle Loan Agreement
	@Then("^User adds an OUV Vehicle Loan Agreement$")
	public void user_adds_an_OUV_Vehicle_Loan_Agreement() throws Throwable {
		ActionHandler.wait(10);

		String Frequentdriver = "Jane Austin Test";

		javascriptUtil.scrollIntoView(OUVCalendarContainer.VehicleLoan);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Vehicle Loan Agreement");
		ActionHandler.click(OUVCalendarContainer.EditVLA);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.AutoVLA);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.ViewdependTemplate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.VLATemplate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Jaguarwithcost);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.VLALanguage);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.English);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.ApplyVLA);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Savechanges);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the Vehicle Loan Agreement");

		javascriptUtil.scrollIntoView(OUVCalendarContainer.Relatedlist);
		ActionHandler.wait(8);
		ActionHandler.click(OUVCalendarContainer.OpenVLAAgreement);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV Vehicle Loan Agreement from Related List Quick Links");
		ActionHandler.wait(7);
		ActionHandler.click(OUVCalendarContainer.NewVLA);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.SearchDrivers);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.SearchDrivers, Frequentdriver);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.ShowAllResults);
		ActionHandler.wait(14);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.Driverselection(Frequentdriver))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.EditTypeofDriver);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Individual);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the newly created Vehicle Loan Agreement");

		ActionHandler.click(OUVCalendarContainer.Backtobooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User is navigated back to the Booking ID Page");

	}

	// User Navigate to OUV Vehicle Loan Agreements
	@And("^User Navigate to OUV Vehicle Loan Agreements$")
	public void User_Navigate_OUV_vehicle_Loan_Agreement() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.VLAAgreement);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to VLA Agreement page");
	}

	// Verify that User cannot see Frequent Drivers in New OUV Vehicle Loan
	// Agreement window
	@Then("^Verify that User cannot see Frequent Drivers in New OUV Vehicle Loan Agreement window$")
	public void Verify_User_Cannot_See_Frequent_Drivers() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.NewVLA);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cannot see Frequent Driver field for Remarketing Manager");
		ActionHandler.click(OUVCalendarContainer.CloseVLA);
		ActionHandler.wait(3);
	}

	// User enters only Frequent Driver and verify the error message
	@And("^User enters only Frequent Driver \"([^\"]*)\" and verify the error upon saves the Agreement$")
	public void User_Enters_Frequent_Driver_Verify_Error_Message(String fDriver) throws Exception {
		String fd[] = fDriver.split(",");
		fDriver = CommonFunctions.readExcelMasterData(fd[0], fd[1], fd[2]);

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.frequentDriverLabel);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User finds Frequent Driver");
	}

	// User enters only Frequent Driver and Type of Driver
	@And("^User enters only Frequent Driver \"([^\"]*)\" and Type of Driver \"([^\"]*)\" and saves VLA$")
	public void User_Enters_Frequent_Driver_Type_Driver(String fDriver, String typeOfDriver) throws Exception {
		String FirstName = "Test";
		String LastName = "Driver";
		String EmailId = "test@c.cc";

		String fd[] = fDriver.split(",");
		fDriver = CommonFunctions.readExcelMasterData(fd[0], fd[1], fd[2]);

		String tDriver[] = typeOfDriver.split(",");
		typeOfDriver = CommonFunctions.readExcelMasterData(tDriver[0], tDriver[1], tDriver[2]);

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.NewVLA);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.frequentDriverLabel);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User finds Frequent Driver");

		ActionHandler.click(OUVCalendarContainer.EditTypeofDriver);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(typeOfDriver))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Type of Driver");

		if (typeOfDriver.equals("Individual")) {
			ActionHandler.setText(OUVCalendarContainer.VLAFirstName, FirstName);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(OUVCalendarContainer.VLALastName, LastName);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(OUVCalendarContainer.VLAEmailID, EmailId);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

		}

		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(9);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User creates a new VLA");

		ActionHandler.click(OUVCalendarContainer.backToBookingVLA);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// User submits the Booking for Approval
	@Then("^User submits the booking for Approval$")
	public void user_submits_the_booking_for_Approval() throws Exception {
		ActionHandler.click(OUVCalendarContainer.Submit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User submits the booking for approval");
	}

	// User opens any created OUV Booking
	@And("^User opens any created OUV Booking$")
	public void User_Opens_Created_OUV_Booking() throws Exception {
		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.firstOUVBooking)) {
			ActionHandler.click(OUVCalendarContainer.firstOUVBooking);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User should be able to open any existing OUV Booking");
		} else {
			Reporter.addStepLog("No any OUV Booking found. User needs to create a new OUV Booking");
			String bookingQuickRef = "Test Booking Edit";
			String bookingJustification = "Event/Sales activity";
			String bookingReqType = "Single Vehicle Booking";
			String passoutType = "Overnight";
			String journeyType = "Return";

			ActionHandler.wait(3);
			Reporter.addStepLog("User selects any vehicle for Booking");
			View_Add_Searched_vehicle_Select_Available_vehicle();
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User makes reservation for selected Vehicle");
			User_Saves_OUV_Booking(bookingQuickRef, bookingJustification, bookingReqType, passoutType, journeyType);
			ActionHandler.wait(3);

			ActionHandler.wait(1);
			ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
		}
	}

	// Verify the status of OUV Booking for Event/Sales Activity
	@And("^Verify that the status of OUV Booking based on Booking Justification \"([^\"]*)\"$")
	public void Verify_the_status_OUV_Booking(String bookingJustification) throws Exception {
		String bookingJusti[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bookingJusti[0], bookingJusti[1], bookingJusti[2]);

		ActionHandler.wait(2);
		if (bookingJustification.equals("Event/Sales activity")) {
			ActionHandler.wait(2);
			Reporter.addStepLog("Booking Justification is Event Sales, so Booking direclt goes to Pending Approval");
			VerifyHandler.verifyElementPresent(OUVCalendarContainer.BookingPendingApproLabel);
			CommonFunctions.attachScreenshot();
		} else if (bookingJustification.equals("External Loan  (VLA)")) {
			// User need to create VLA and submit booking for an approval
		}
	}

	// User navigates to Approval History
	@And("^User navigates to Approval History$")
	public void User_navigates_approval_history() throws Exception {
		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.ApprovalHistory);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User checks the approval history");
		ActionHandler.wait(2);

		driver.navigate().back();
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
	}

	// User verify that the OUV Booking status moves to New from Booked Pending
	// Approval
	@Then("^User verify that the OUV Booking status moves to New from Booked Pending Approval$")
	public void User_Verify_OUV_Booking_New() throws Exception {
		ActionHandler.wait(4);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.OUVBookingNewStatus);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies that OUV Booking status moves to New from Booked Pending Approval");
	}

	// Verify that OUV Booking status is Approved for Event/Sales Activity
	@Then("^Verify that OUV Booking status is Approved if Booking Justification \"([^\"]*)\"$")
	public void Verify_OUV_Booking_Status(String bookingJustification) throws Exception {
		String bookingJusti[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bookingJusti[0], bookingJusti[1], bookingJusti[2]);

		if (bookingJustification.equals("Event/Sales activity")) {
			ActionHandler.wait(2);
			VerifyHandler.verifyElementPresent(OUVCalendarContainer.OUVBookingApproved);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("OUV booking status is approved if Booking Justification is Event/Sales Activity");
		}

	}

	// Verify that OUV Booking status is Rejected for Event/Sales Activity
	@Then("^Verify that OUV Booking status is Rejected if Booking Justification \"([^\"]*)\"$")
	public void Verify_OUV_Booking_Status_Rejected(String bookingJustification) throws Exception {
		String bookingJusti[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bookingJusti[0], bookingJusti[1], bookingJusti[2]);

		if (bookingJustification.equals("Event/Sales activity")) {
			ActionHandler.wait(2);
			VerifyHandler.verifyElementPresent(OUVCalendarContainer.OUVBookingRejected);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("OUV booking status is Rejected if Booking Justification is Event/Sales Activity");
		}

	}

	// User verifies First level of approval is Fleet Manager from Booking Approvers
	// List
	@Then("^User verifies First level of approval is Fleet Manager from Booking Approvers List$")
	public void User_Verifies_First_Approval_Fleet_manager() throws Exception {
		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.BookingApprover);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User should be able to view Booking Approvers");
	}

	// User submit an OUV Booking for an Approval after recalling
	@And("^User submit an OUV Booking for an Approval after recalling$")
	public void User_Submit_For_Approval() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Submit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.BookingPendingApproLabel);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User submits an OUV Booking for an Approval");
	}

	// User rejects the Booking
	@Then("^User rejects the Booking$")
	public void User_Rejects_OUV_Booking() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RejectOUVBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RejectOUVBookingBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User rejects OUV Booking");
	}

	// Open recently rejected OUV Booking
	@And("^Open recently rejected OUV Booking$")
	public void Open_Recently_Rejected_OUV_Booking() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.All);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User opens list of all OUV Booking");

		ActionHandler.click(OUVCalendarContainer.firstOUVBookingFrmAll);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User can open recently approved OUV Booking");

	}

	// User edits the details in the OUV Booking ID Page
	@And("^User edits the details in the OUV Booking ID Page$")
	public void User_Edits_Details_OUV_Booking_ID() throws Exception {
		String bookingQuickRef = "Test Booking-Edited";
		String passoutType = "Weekend";
		String journeyType = "Return";

		ActionHandler.wait(3);
		Reporter.addStepLog("User wants to edit some fields on OUV Booking ID Page");

		ActionHandler.click(OUVCalendarContainer.editBookingQuickRef);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRefField, bookingQuickRef);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.passOutTypeDD);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.JourneyTypeDD);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User saves the changes made on OUV Booking");

	}

	@And("^Verify that the document in S-Sign section is available for E-Sign$")
	public void Verify_Document_S_Sign_Section_Available_For_E_Sign() throws Exception {
		ActionHandler.wait(2);
		driver.navigate().back();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		User_Navigate_OUV_vehicle_Loan_Agreement();
		Reporter.addStepLog("User navigates to Vehicle Loan Agreement section");

		ActionHandler.wait(2);
		// User_view_details_newly_created_VLA();
		Reporter.addStepLog("User opens details of created VLA");

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.S_SignVLA);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to S-Sign envelop");

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.FirstS_Sign);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first S-Sign Envelop");

	}

	@Then("^User view details of newly created OUV Vehicle Loan Agreement$")
	public void user_view_details_newly_created_OUV_VLA() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.VLAFirst);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User can view details of available VLA");
	}

	@Then("^User sends the Sign Request$")
	public void User_sends_Sign_Request() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.sendSignBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on send S-Sign Request Button");
	}

	@And("^User verifies that Encrypted Company & Encrypted Full Name field are auto populated$")
	public void User_verifies_encrypted_company_encrypted_full_name_auto_populated() throws Exception {
		ActionHandler.wait(2);

		driver.navigate().back();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		driver.navigate().back();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		driver.navigate().back();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.backToBooking);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate back to Booking");

		ActionHandler.wait(2);
		ActionHandler.scrollToView(OUVCalendarContainer.driverInfo);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.encryptedFullName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verify that Encrypted full name is available");

		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.encryptedDriver1);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verify that Encrypted Driver information is available");

	}

	// User Validates that first approval is assigned to the Fleet Manager
	@Then("^Validate that first approval is assigned to the Fleet Manager$")
	public void validate_that_first_approval_is_assigned_to_the_Fleet_Manager() throws Throwable {

		ActionHandler.wait(5);
		String ID = OUVCalendarContainer.BookingHeader.getText();
		ActionHandler.wait(5);
		System.out.println("Booking ID is = " + ID);
	}

	// User recalls an Approval
	@Then("^User recalls an Approval$")
	public void user_recalls_an_Approval() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.ApprovalHistory);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Approval History");
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Recall);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.RecallButton);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User recalls an approval");

	}

	// User validates that the Approval status changes to Recalled
	@Then("^User validates that the Approval status changes to Recalled$")
	public void user_validates_that_the_Approval_status_changes_to_Recalled() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Recalled);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates that the status is changed to recalled");
	}

	// Login with Fleet Manager
	@Given("^Login with Fleet Manager as \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void login_with_Fleet_Manager_as_and_password_as(String UserName, String Password) throws Throwable {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		javascriptUtil.clickElementByJS(OUVCalendarContainer.homeTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginImg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.addUserName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(7);
		ActionHandler.setText(OUVCalendarContainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(20);

	}

	// Login with OUV Booking Owner
	@Then("^Login with OUV Booking Owner with user \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void login_with_OUV_Booking_Owner_as_and_password_as(String UserName, String Password) throws Exception {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		javascriptUtil.clickElementByJS(OUVCalendarContainer.homeTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginImg);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.addUserName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(7);
		ActionHandler.setText(OUVCalendarContainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(20);
	}

	// User selects the booking from Notifications Menu
	@Then("^Select the Booking from the Notifications Menu$")
	public void select_the_Booking_from_the_Notifications_Menu() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.Notification);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Notification Menu");
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.Booking(ID))));
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Booking ID");
	}

	// User approves the Booking
	@Then("^User approves the Booking$")
	public void user_approves_the_Booking() throws Throwable {
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Approve);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.ApproveButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User approves the booking");

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.ApprovedStatus);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// User navigates to OUV Booking Menu
	@And("^User navigates to OUV Booking menu$")
	public void user_navigates_OUV_Booking() throws Exception {
		ActionHandler.wait(5);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.OUVBookingWindow);
		// ActionHandler.click(OUVCalendarContainer.OUVBookingWindow);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV Booking Window");
	}

	// User verifies OUV Booking status is Recalled
	@And("^User verifies OUV Booking status is Recalled$")
	public void User_Verifies_OUV_Booking_Recalled() throws Exception {
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Recalled);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies OUV Booking status is recalled");
	}

	// Open recently approved OUV Booking
	@And("^Open recently approved OUV Booking$")
	public void Open_Recently_Approved_OUV_Booking() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.All);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User opens list of all OUV Booking");

		ActionHandler.click(OUVCalendarContainer.firstOUVBookingFrmAll);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User can open recently approved OUV Booking");
	}

	// User navigate to OUV Vehicles
	@And("^User navigate to OUV Vehicles$")
	public void User_Navigate_OUV_Vehicles() throws Exception {
		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.OUVVehicles);
		ActionHandler.wait(10);
		ActionHandler.waitForElement(OUVCalendarContainer.OUVVehicleHeader, 60);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV Vehicle");
	}

	// User searches for an OUV Vehicle
	@Then("^User searches for an OUV Vehicle$")
	public void user_searches_for_an_OUV_Vehicle() throws Throwable {

		String fleet = "Fl";
		String fleetname = "PR NL";
		String masterstatus = "ms";
		String statusmaster = "Pre-live";

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.CopyofAll)) {
			ActionHandler.click(OUVCalendarContainer.CopyofAll);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
		} else {
			ActionHandler.click(OUVCalendarContainer.All);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			Reporter.addStepLog("User views all the listed OUV Vehicles");

			ActionHandler.click(OUVCalendarContainer.ListViewControls);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
			ActionHandler.click(OUVCalendarContainer.Clone);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(3);
			ActionHandler.click(OUVCalendarContainer.SaveClone);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(4);
		}

		ActionHandler.click(OUVCalendarContainer.Showfilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.AddFilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User select Filter Button");

		String select = (Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
		driver.findElement(By.xpath("(//input[@placeholder='Select an Option'])[1]")).sendKeys(select);
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(OUVCalendarContainer.Field, fleet);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects Field");
		ActionHandler.setText(OUVCalendarContainer.EnterValue, fleetname);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects Value");
		ActionHandler.click(OUVCalendarContainer.Done);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.AddFilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User select Filter Button");

		ActionHandler.clearAndSetText(OUVCalendarContainer.Field, masterstatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User selects Field");
		ActionHandler.setText(OUVCalendarContainer.EnterValue, statusmaster);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User selects Value");
		ActionHandler.click(OUVCalendarContainer.Done);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.SaveFilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User saves the filter");
		ActionHandler.click(OUVCalendarContainer.CloseFilter);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User closes the filter");

		ActionHandler.click(OUVCalendarContainer.SelectVehicle);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects an OUV Vehicle");

	}

	// User validates that an error message is displayed
	@Then("^User validates that an error message is displayed$")
	public void user_validates_that_an_error_message_is_displayed() throws Throwable {

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Error);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User validates that an error message is displayed");

	}

	// User verifies the owner name
	@And("^User verifies the owner name \"([^\"]*)\" from OUV Booking ID Window$")
	public void User_Verifies_The_Owner_name(String ownerName) throws Exception {
		String owner[] = ownerName.split(",");
		ownerName = CommonFunctions.readExcelMasterData(owner[0], owner[1], owner[2]);

		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.Booking(ownerName))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies Owner Name");
	}

	// view the searched vehicles and select a vehicle which is available
	@Then("^view the searched vehicles and select a vehicle which is available$")
	public void view_the_searched_vehicles_and_select_a_vehicle_which_is_available() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.wait(2);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.click(OUVCalendarContainer.dayBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.TodayCalendar);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.rightButton2);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.SelectVehicleFromDay);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle from day tab");
		ActionHandler.wait(2);
		Reporter.addStepLog("User can select Vehicle for reservation");
	}

	// verify the error message for no VLA record
	@Then("^verify the error message for no VLA record$")
	public void verify_the_error_message_for_no_VLA_record() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.VLArecerror)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("Verify the displayed error message");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("No error message displayed");
			CommonFunctions.attachScreenshot();
		}

	}

	// User reassign the approval request
	@Then("^User reassign the approval request to \"([^\"]*)\"$")
	public void user_reassign_the_approval_request_to(String Search) throws Throwable {

		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.click(OUVCalendarContainer.Notification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Notification Menu");

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.Booking2(ID))));
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Booking ID");

		ActionHandler.click(OUVCalendarContainer.Reassign);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the reassign button");

		ActionHandler.clearAndSetText(OUVCalendarContainer.SearchUsers, Search);
		ActionHandler.wait(5);
		Reporter.addStepLog("Enter the assignee information");
		CommonFunctions.attachScreenshot();

		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("select owner");

		ActionHandler.click(OUVCalendarContainer.ReassignButton);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select the change assignee tab");
		CommonFunctions.attachScreenshot();

		WebElement assignee = driver.findElement(By.xpath(OUVCalendarContainer.TextSelection(Search)));

		if (VerifyHandler.verifyElementPresent(assignee)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("Actual Approver name changes to the Reassigned user");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("Actual Approver name did not changes to the Reassigned user");
			CommonFunctions.attachScreenshot();
		}

	}

	// User verify that VLA Market field is Auto populated by verifying Market
	@And("^User verify that VLA Market field is Auto populated by verifying Market \"([^\"]*)\"$")
	public void User_Verify_VLA_market_Auto_Populated(String VLA) throws Exception {
		String vla[] = VLA.split(",");
		VLA = CommonFunctions.readExcelMasterData(vla[0], vla[1], vla[2]);

		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.VehicleLoanAgreementLabel);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Vehicle Loan Agreement section");

		VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.VLAFieldvalidation(VLA))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// User enters an Invalid JLR Contact Email
	@And("^User enters an Invalid JLR Contact Email$")
	public void User_Enters_Invalid_JLR_Contact_Email() throws Exception {
		String internalJLR = "test";

		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.internalJLRCntLabel);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.EditinternalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLRCntTxtBox, internalJLR);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.internalJLRCntError);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User enters an invalid JLR contact and it gives an error");
	}

	// User searches for an OUV Booking with status
	@Then("^User searches for an OUV Booking with status \"([^\"]*)\"$")
	public void user_searches_for_an_OUV_Booking_with_status(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Bookings");

		ActionHandler.click(OUVCalendarContainer.OUVBookingsearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVCalendarContainer.OUVBookingsearch, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an OUV Booking");
		CommonFunctions.attachScreenshot();
	}

	// User navigate to OUV Bookings
	@And("^User navigate to OUV Bookings$")
	public void user_navigate_to_OUV_Bookings() throws Throwable {
		ActionHandler.wait(7);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.OUVBookings);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User naviagte to OUV bookings");
	}

	// User validates that document template section is blank
	@Then("^User validates that document template section is blank$")
	public void user_validates_that_document_template_section_is_blank() throws Throwable {
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.pageUp();
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.EditTemplate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.NoTemplate);
		Reporter.addStepLog("User verifies no template is present");
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.Cancelchanges);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

	}

	// User clicks on generate document
	@Then("^User clicks on generate document$")
	public void user_clicks_on_generate_document() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.GenerateDocument);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Generate Document");
		ActionHandler.click(OUVCalendarContainer.SaveDocument);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Save Button");
		ActionHandler.wait(2);

	}

	// Validate that an error message is displayed
	@Then("^Validate that an error message is displayed$")
	public void validate_that_an_error_message_is_displayed() throws Throwable {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.ErrorTemplate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.CancelDocument);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on cancel Button");
	}

	// User clicks on Cancel Booking
	@Then("^User clicks on Cancel Booking$")
	public void user_clicks_on_Cancel_Booking() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.EditDropdown);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Dropdown button");
		ActionHandler.click(OUVCalendarContainer.CancelBooking);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on cancel booking");
		ActionHandler.click(OUVCalendarContainer.Savecancelbooking);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

	}

	// Validate that the Booking status is moved to cancelled
	@Then("^Validate that the Booking status is moved to cancelled$")
	public void validate_that_the_Booking_status_is_moved_to_cancelled() throws Throwable {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Cancelled);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User verifies the status moved to cancelled");

	}

	// select the OUV Booking
	@Then("^select the OUV Booking$")
	public void select_the_Request() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.OUVSel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select OUV Booking");
		CommonFunctions.attachScreenshot();

	}

	// User clicks on Edit Option
	@Then("^User clicks on Edit Option$")
	public void user_clicks_on_Edit_Option() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.EditButton);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Edit button");
	}

	// User tries to change the Booking Request Type
	@Then("^User tries to change the Booking Request Type$")
	public void user_tries_to_change_the_Booking_Request_Type() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.SelectBookingRequest);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User tries to edit Booking Request Type");
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("select Booking Request Type");

		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

	}

	// Validate that user is not able to save the record
	@Then("^Validate that user is not able to save the record$")
	public void validate_that_user_is_not_able_to_save_the_record() throws Throwable {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Errormsg);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User validates the error message displayed");
		ActionHandler.click(OUVCalendarContainer.CloseButton);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

	}

	// Verify the VLA NUmber should be auto-populated in OUV booking ID page
	@Then("^Verify the VLA NUmber should be auto-populated in OUV booking ID page$")
	public void verify_the_VLA_NUmber_should_be_auto_populated_in_OUV_booking_ID_page() throws Throwable {
		ActionHandler.wait(5);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.VehicleLoan);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Vehicle Loan Agreement");
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.VLANumber)) {
			Reporter.addStepLog("VLA number was auto-populated");
		} else {
			Reporter.addStepLog("VLA number was not auto populated");
		}
		System.out.println("VLA Number is" + OUVCalendarContainer.VLANumber);

	}

	// Login to OUV Portal with an Approver
	@Then("^Login with Approver as \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void login_with_Approver_as_and_password_as(String UserName, String Password) throws Throwable {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		javascriptUtil.clickElementByJS(OUVCalendarContainer.homeTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginImg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.addUserName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(7);
		ActionHandler.setText(OUVCalendarContainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(20);

	}

	// Verify the VLA status should be updated as created
	@Then("^Verify the VLA status should be updated as created$")
	public void verify_the_VLA_status_should_be_updated_as_created() throws Throwable {
		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.click(OUVCalendarContainer.BookingId1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		javascriptUtil.scrollIntoView(OUVCalendarContainer.VehicleLoan);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.VLAstatus)) {
			Reporter.addStepLog("VLA status was updated");
		} else {
			Reporter.addStepLog("VLA status was not updated");
		}
		System.out.println("VLA Number is" + OUVCalendarContainer.VLAstatus);

	}

	// Select the first booking from the notification menu
	@And("^select the first booking from the notification menu$")
	public void select_the_first_booking_from_the_notification_menu() throws Throwable {
		ActionHandler.wait(6);
		ActionHandler.click(OUVCalendarContainer.Notification);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Notification Menu");
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.firstNotification);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

	}

	// user navigates to reservations in related list quick links and selects
	// reservation
	@Then("^User navigate to reservations and select a reservation$")
	public void user_navigate_to_reservations_and_select_a_reservation() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.Reseravtions);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to reservations");
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.ReservationName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens reservation");

	}

	// User navigates to permanent accessories
	@Then("^User navigate to Permanent Accessories and click on New$")
	public void user_navigate_to_Permanent_Accessories_and_click_on_New() throws Throwable {
		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.PermanentAccessories);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to permanant accessories");
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(OUVCalendarContainer.NewBtn);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on new button");

	}

	// Fill Mandatory fields like Accessory and click on Save
	@Then("^Fill Mandatory fields like Accessory with \"([^\"]*)\" and click on save$")
	public void fill_Mandatory_fields_like_Accessory_with_and_click_on_save(String Accessories) throws Throwable {
		ActionHandler.wait(2);
		String acc[] = Accessories.split(",");
		Accessories = CommonFunctions.readExcelMasterData(acc[0], acc[1], acc[2]);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.Accessory);
		ActionHandler.wait(4);
		ActionHandler.setText(OUVCalendarContainer.Accessory, Accessories);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// Verify the accessory created
	@Then("^Verify the accessory created$")
	public void verify_the_accessory_created() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.verifyAccessory)) {
			Reporter.addStepLog("permanent Accessory was created");
		} else {
			Reporter.addStepLog("permanent Accessory was not created");
		}
		System.out.println("accessory id is " + OUVCalendarContainer.VLAstatus);

	}

	// user deletes the accessory
	@Then("^Click on drop down and select delete option$")
	public void click_on_drop_down_and_select_delete_option() throws Throwable {
		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.click(OUVCalendarContainer.ShowActions2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		ActionHandler.click(OUVCalendarContainer.Delete);
		ActionHandler.wait(4);
		Reporter.addStepLog("User selects delete option");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.DeleteBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User deletes accessory");
		Reporter.addStepLog("User creates a new VLA");
	}

	// User enters all the Insurance details
	@Then("^User enters all the Insurance details like Insurance Company \"([^\"]*)\" Insurance Policy Number \"([^\"]*)\" Vehicle Storage \"([^\"]*)\" and Driving Experience \"([^\"]*)\"$")
	public void user_enters_all_the_Insurance_details_like_Insurance_Company_Insurance_Policy_Number_Vehicle_Storage_and_Driving_Experience(
			String InsuranceC, String InsuranceP, String VehicleS, String DExperience) throws Throwable {
		String IC[] = InsuranceC.split(",");
		InsuranceC = CommonFunctions.readExcelMasterData(IC[0], IC[1], IC[2]);

		String IP[] = InsuranceP.split(",");
		InsuranceP = CommonFunctions.readExcelMasterData(IP[0], IP[1], IP[2]);

		String VS[] = VehicleS.split(",");
		VehicleS = CommonFunctions.readExcelMasterData(VS[0], VS[1], VS[2]);

		String DE[] = DExperience.split(",");
		DExperience = CommonFunctions.readExcelMasterData(DE[0], DE[1], DE[2]);

		javascriptUtil.scrollIntoView(OUVCalendarContainer.InsuranceDetails);
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.Insuredby);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("select Insuredby details");

		ActionHandler.setText(OUVCalendarContainer.InsuranceCompany, InsuranceC);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.InsurancePolicyNumber, InsuranceP);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.VehicleStorage, VehicleS);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.DrivingExperience, DExperience);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User enters all the fields for Insurance Details");
	}

	// Verify all the details are autopopulated in the VLA Details Page
	@Then("^Verify all the details are autopopulated in the VLA Details Page with Insurance Company \"([^\"]*)\" Insurance Policy Number \"([^\"]*)\" Vehicle Storage \"([^\"]*)\" and Driving Experience \"([^\"]*)\"$")
	public void verify_all_the_details_are_autopopulated_in_the_VLA_Details_Page_with_Insurance_Company_Insurance_Policy_Number_Vehicle_Storage_and_Driving_Experience(
			String InsuranceC, String InsuranceP, String VehicleS, String DExperience) throws Throwable {
		String IC[] = InsuranceC.split(",");
		InsuranceC = CommonFunctions.readExcelMasterData(IC[0], IC[1], IC[2]);

		String IP[] = InsuranceP.split(",");
		InsuranceP = CommonFunctions.readExcelMasterData(IP[0], IP[1], IP[2]);

		String VS[] = VehicleS.split(",");
		VehicleS = CommonFunctions.readExcelMasterData(VS[0], VS[1], VS[2]);

		String DE[] = DExperience.split(",");
		DExperience = CommonFunctions.readExcelMasterData(DE[0], DE[1], DE[2]);

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.VLAname);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects the VLA");
		CommonFunctions.attachScreenshot();

		javascriptUtil.scrollIntoView(OUVCalendarContainer.InsuranceDetails);
		ActionHandler.wait(5);

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(InsuranceC))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates Insurance Company");

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(InsuranceP))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates Insurance Policy Number");

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(VehicleS))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates Vehicle Storage");

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(DExperience))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates Driving Experience");

	}

	// User tries to change the owner
	@Then("^User tries to change the owner as \"([^\"]*)\"$")
	public void user_tries_to_change_the_owner_as(String owner) throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Changeowner);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Change Owner option");
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.searchusers, owner);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("select an owner");

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.changeownerbutton);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Change Owner option");
		CommonFunctions.attachScreenshot();

	}

	// Validate that user is not able to cancel an OUV Booking
	@Then("^Validate that user is not able to cancel an OUV Booking$")
	public void validate_that_user_is_not_able_to_cancel_an_OUV_Booking() throws Throwable {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.CancelError);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User verifies the error message");

	}

	// Navigate to Reservation
	@Then("^Navigate to Reservation$")
	public void navigate_to_Reservation() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.Reservations);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Reservation");
		ActionHandler.wait(2);
	}

	// Select the Reservation
	@Then("^Select the Reservation$")
	public void select_the_Reservation() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.SelectReservation);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Reservation");
		ActionHandler.wait(3);
	}

	// User tries to edit Reservation in Approved Booking
	@Then("^User tries to edit Reservation in Approved Booking$")
	public void user_tries_to_edit_Reservation_in_Approved_Booking() throws Throwable {
		javascriptUtil.scrollIntoView(OUVCalendarContainer.ReservationAdministration);
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.EditKeyscheckbox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits the keys checkdout button");
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.selectKeyscheckbox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the keys checkdout button");
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save Button");
		ActionHandler.wait(2);

	}

	// Validate that an error message is displayed while editing Reservation
	@Then("^Validate that an error message is displayed while editing Reservation$")
	public void validate_that_an_error_message_is_displayed_while_editing_Reservation() throws Throwable {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Errormsg);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User verifies the error message");
		ActionHandler.click(OUVCalendarContainer.Cancelchanges);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Cancel Button");
		ActionHandler.wait(2);

	}

	// User tries to delete the Reservation
	@Then("^User tries to delete the Reservation$")
	public void user_tries_to_delete_the_Reservation() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.click(OUVCalendarContainer.ShowActions);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.DeleteResevation);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on delete button");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.Confirmdelete);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on delete button");
		CommonFunctions.attachScreenshot();

	}

	// Validate that an error message is displayed while deleting Reservation
	@Then("^Validate that an error message is displayed while deleting Reservation$")
	public void validate_that_an_error_message_is_displayed_while_deleting_Reservation() throws Throwable {

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.DeleteReservationError);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User verifies the error message");
		ActionHandler.click(OUVCalendarContainer.Closewindow);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Close Button");
		ActionHandler.wait(2);
	}

	// select Reservation quick link
	@Then("^select Reservation quick link$")
	public void select_Reservation_quick_link() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Reservations);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reservation quick link");

	}

	// select the Reservation name
	@Then("^select the Reservation name$")
	public void select_the_Reservation_name() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.ReservationsName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reservation Name");

	}

	// click on the Issue keys and enter number of keys
	@Then("^click on the Issue keys and enter number of keys as \"([^\"]*)\"$")
	public void click_on_the_Issue_keys_and_enter_number_of_keys_as(String Keys) throws Throwable {

		String Key[] = Keys.split(",");
		Keys = CommonFunctions.readExcelMasterData(Key[0], Key[1], Key[2]);

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.IssueKeys);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Issue Keys");

		ActionHandler.click(OUVCalendarContainer.numberofKeys);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks number of Keys");

		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.Number(Keys))));
		Reporter.addStepLog("User clicks number of Keys to select");
		CommonFunctions.attachScreenshot();
	}

	// click on the save button
	@When("^click on the save button$")
	public void click_on_the_save_button() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.Save);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save button");
	}

	// verify the displayed Reservation error message
	@When("^verify the displayed Reservation error message$")
	public void verify_the_displayes_Reservation_error_message() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.Reviewerrors)) {
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User validates error message");
		} else {

			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User validates that no error message displayed");
		}

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Cancel2);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel button");
	}

	// click on the complete and enter number of keys
	@Then("^click on the complete and enter number of keys as \"([^\"]*)\"$")
	public void click_on_the_complete_and_enter_number_of_keys_as(String Keys) throws Throwable {

		String Key[] = Keys.split(",");
		Keys = CommonFunctions.readExcelMasterData(Key[0], Key[1], Key[2]);

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.completeReservation);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on complete Reservation");

		ActionHandler.click(OUVCalendarContainer.numberofKeys);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks number of Keys");

		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.Number(Keys))));
		Reporter.addStepLog("User clicks number of Keys to select");
		CommonFunctions.attachScreenshot();
	}

	// select Temporary Accessories quick link
	@Then("^select Temporary Accessories quick link$")
	public void select_Temporary_Accessories_quick_link() throws Throwable {

		ActionHandler.wait(7);
		ActionHandler.click(OUVCalendarContainer.TemporaryAccessories);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Temporary Accessories quick link");
	}

	// click on New and fill the mandatory details like accessory
	@Then("^click on New and fill the mandatory details like accessory \"([^\"]*)\"$")
	public void click_on_New_and_fill_the_mandatory_details_like_accessory(String accessory) throws Throwable {

		String access[] = accessory.split(",");
		accessory = CommonFunctions.readExcelMasterData(access[0], access[1], access[2]);

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.New);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button");

		ActionHandler.setText(OUVCalendarContainer.SearchAccessories, accessory);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User enter the accessory information");
	}

	// save the new Temporary Accessories
	@When("^save the new Temporary Accessories$")
	public void save_the_new_Temporary_Accessories() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save button");
	}

	// verify the displayed Temporary Accessories error message
	@When("^verify the displayed Temporary Accessories error message$")
	public void verify_the_displayes_Temporary_Accessories_error_message() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.ErrorAccessories)) {
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User validates error message");
		} else {

			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User validates that no error message displayed");
		}

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.Cancelchanges);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Cancel button");
	}

	@Then("^select Permanent Accessories quick link$")
	public void select_Permanent_Accessories_quick_link() throws Throwable {

		ActionHandler.wait(7);
		ActionHandler.click(OUVCalendarContainer.PermanentAccessories);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Permanent Accessories quick link");
	}

	// User searches for an OUV Booking with ID
	@Then("^User searches for an OUV Booking with ID$")
	public void user_searches_for_an_OUV_Booking_with_ID() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Bookings");

		ActionHandler.click(OUVCalendarContainer.OUVBookingsearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVCalendarContainer.OUVBookingsearch, ID);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an OUV Booking");
		CommonFunctions.attachScreenshot();
	}

	// select OUV vehicle loan Agreement quick link
	@Then("^select OUV vehicle loan Agreement quick link$")
	public void select_OUV_vehicle_loan_Agreement_quick_link() throws Throwable {

		javascriptUtil.scrollIntoView(OUVCalendarContainer.Relatedlist);
		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.OpenVLAAgreement);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV Vehicle Loan Agreement from Related List Quick Links");
		ActionHandler.wait(3);
	}

	// delete the OUV vehicle loan Agreement
	@Then("^delete the OUV vehicle loan Agreement$")
	public void delete_the_OUV_vehicle_loan_Agreement() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.click(OUVCalendarContainer.ShowActions2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.Delete);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Delete");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.DeleteItems);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on confirm Delete");
		CommonFunctions.attachScreenshot();
	}

	// verify that user is able to delete vehicle loan Agreement
	@Then("^verify that user is able to delete vehicle loan Agreement$")
	public void verify_that_user_is_able_to_delete_vehicle_loan_Agreement() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.vehicleloanAgreement)) {
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to delete vehicle loan Agreement");
		} else {

			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to delete vehicle loan Agreement");
		}
	}

	// verify that user is able to add new Temporary Accessories
	@Then("^verify that user is able to add new Temporary Accessories$")
	public void verify_that_user_is_able_to_add_new_Temporary_Accessories() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.AccessoryReservation)) {
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to add new Temporary Accessories");
		} else {

			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to add new Temporary Accessories");
		}
	}

	// navigate to the reservation and select Temporary Accessories quick link
	@Then("^navigate to the reservation and select Temporary Accessories quick link$")
	public void navigate_to_the_reservation_and_select_Temporary_Accessories_quick_link() throws Throwable {

		ActionHandler.wait(20);
		ID = OUVCalendarContainer.BookingHeader.getText();
		ActionHandler.wait(2);
		System.out.println("Booking ID is = " + ID);

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Reservations2);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reservation quick link");

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.ReservationsName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reservation Name");

		ActionHandler.wait(7);
		ActionHandler.click(OUVCalendarContainer.TemporaryAccessories2);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Temporary Accessories quick link");

	}

	// click on the Temporary Accessories and clone it
	@Then("^click on the Temporary Accessories and clone it$")
	public void click_on_the_Temporary_Accessories_and_clone_it() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.AccessoryReservation);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Accessory Reservation");

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Clone3);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on clone button");

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button");
	}

	// click on New button and fill the mandatory details like accessory
	@Then("^click on New button and fill the mandatory details like accessory \"([^\"]*)\"$")
	public void click_on_New_button_and_fill_the_mandatory_details_like_accessory(String accessory) throws Throwable {

		String access[] = accessory.split(",");
		accessory = CommonFunctions.readExcelMasterData(access[0], access[1], access[2]);

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Newone);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on New button");

		ActionHandler.setText(OUVCalendarContainer.SearchAccessories, accessory);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User enter the accessory information");
	}

	// navigate to the Temporary Accessories
	@Then("^navigate to the Temporary Accessories$")
	public void navigate_to_the_Temporary_Accessories() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.ReservationsName2);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reservations Name");

		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.TemporaryAccessories2);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Temporary Accessories");
	}

	// navigate to the reservation and clone it
	@Then("^navigate to the reservation and clone it$")
	public void navigate_to_the_reservation_and_clone_it() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Reservations2);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reservation quick link");

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.ReservationsName);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Reservation Name");

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Clone2);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Clone");

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on save button");

	}

	// Verify the reservation clone error
	@Then("^Verify the reservation clone error$")
	public void verify_the_reservation_clone_error() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.ReservationCloneerror)) {
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to add new reservation error displayed");
		} else {

			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to add new reservation");
		}

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Cancelchanges);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on cancel");
	}

	//// User navigates to OUV Calendar and searches for Vehicle
	@Then("^User navigates to OUV Calendar and searches for Vehicle$")
	public void user_navigates_to_OUV_Calendar_and_searches_for_Vehicle() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.OUVCalendardetails);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on OUV Calendar");
		ActionHandler.wait(3);

		ActionHandler.pageDown();
		ActionHandler.click(OUVCalendarContainer.ReservedVehicles);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");
	}

	// view the searched vehicles and select another vehicle which is available
	@Then("^view the searched vehicles and select another vehicle which is available$")
	public void view_the_searched_vehicles_and_select_another_vehicle_which_is_available() throws Throwable {
		ActionHandler.pageDown();
		ActionHandler.wait(2);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.click(OUVCalendarContainer.dayBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.TodayCalendar);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.rightButton2);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.SelectsecondVehicle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle from day tab");
		ActionHandler.wait(2);
		Reporter.addStepLog("User can select Vehicle for reservation");

	}

	// User saves the booking for Multiple reservations
	@Then("^User saves the booking for Multiple reservations$")
	public void user_saves_the_booking_for_Multiple_reservations() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

	}

	// User saves a Repair Type OUV Booking including other location
	@And("^User saves a Repair Type OUV Booking including other location with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Saves_a_Repair_Type_OUV_Booking_including_other_location(String bookingQuickRef,
			String bookingJustification, String bookingReqType, String passoutType, String journeyType)
			throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String driver1 = "DHL";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		ActionHandler.click(OUVCalendarContainer.ReservationPassoutdropdown);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.RepairType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the OUV Booking as Repair Type");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.scrollToView(OUVCalendarContainer.Details);
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Navigate back to the Booking ID
	@Then("^Navigate back to the Booking ID$")
	public void navigate_back_to_the_Booking_ID() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.backtoBookingID);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User should be navigated to the bookingID");

	}

	// Validate that an approved repaired OUV Booking is highlighted in yellow color
	@Then("^Validate that an approved repaired OUV Booking is highlighted in yellow color$")
	public void validate_that_an_approved_repaired_OUV_Booking_is_highlighted_in_yellow_color() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.monthBtn);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.Previousmonth);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User should be navigated to the previous month");
		System.out.println("Validate Color");

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.SelectVehicleFromDay)) {
			String bgcolor = driver.findElement(By.xpath("(//div[@class='scheduler_default_event_inner'])[1]"))
					.getCssValue("background-color");
			ActionHandler.wait(2);
			String[] hexValue = bgcolor.replace("rgba(", "").replace(")", "").split(",");

			System.out.println(hexValue);
			int hexValue1 = Integer.parseInt(hexValue[0]);
			hexValue[1] = hexValue[1].trim();
			int hexValue2 = Integer.parseInt(hexValue[1]);
			hexValue[2] = hexValue[2].trim();
			int hexValue3 = Integer.parseInt(hexValue[2]);

			String actualColor = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
			System.out.println(actualColor);
			Assert.assertEquals("#FFA500", actualColor);

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User should be able to validate the yellow color");
		}
	}

	// User selects submit for approval button
	@Then("^User selects submit for approval button$")
	public void user_selects_submit_for_approval_button() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.EditDropdown);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Dropdown button");
		ActionHandler.click(OUVCalendarContainer.SubmitforApproval);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Submit for Approval");

	}

	// Validate that user is not able to submit the booking for approval
	@Then("^Validate that user is not able to submit the booking for approval$")
	public void validate_that_user_is_not_able_to_submit_the_booking_for_approval() throws Throwable {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.SubmitError);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User validates the error message displayed");
		ActionHandler.wait(3);

	}

	// User edits the details for an OUV Vehicle Loan Agreement
	@Then("^User edits the details for an OUV Vehicle Loan Agreement$")
	public void user_edits_the_details_for_an_OUV_Vehicle_Loan_Agreement() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.VLAname);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects the VLA");
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.EditLoanAgreement);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Edit button");
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.SelectDrivertype);
		ActionHandler.wait(3);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("select Driver type");
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on Save button");
		CommonFunctions.attachScreenshot();

	}

	// User enter all the driver information
	@Then("^User enter all the driver information like Type of driver \"([^\"]*)\" First Name \"([^\"]*)\" Last Name \"([^\"]*)\" and Email \"([^\"]*)\"$")
	public void user_enter_all_the_driver_information_like_Type_of_driver_First_Name_Last_Name_and_Email(
			String typeOfDriver, String FirstName, String LastName, String Email) throws Throwable {
		String fd[] = FirstName.split(",");
		FirstName = CommonFunctions.readExcelMasterData(fd[0], fd[1], fd[2]);

		String ld[] = LastName.split(",");
		LastName = CommonFunctions.readExcelMasterData(ld[0], ld[1], ld[2]);

		String ed[] = Email.split(",");
		Email = CommonFunctions.readExcelMasterData(ed[0], ed[1], ed[2]);

		String tDriver[] = typeOfDriver.split(",");
		typeOfDriver = CommonFunctions.readExcelMasterData(tDriver[0], tDriver[1], tDriver[2]);

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.NewVLA);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.EditTypeofDriver);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(typeOfDriver))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Type of Driver");

		if (typeOfDriver.equals("Individual")) {
			ActionHandler.setText(OUVCalendarContainer.VLAFirstName, FirstName);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(OUVCalendarContainer.VLALastName, LastName);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.pageDown();
			ActionHandler.wait(4);
			ActionHandler.setText(OUVCalendarContainer.VLAEmailID, Email);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

	}

	// Verify all the details are autopopulated in the VLA Details Page
	@Then("^Verify all the details are autopopulated in the VLA Details Page with Type of driver \"([^\"]*)\" First Name \"([^\"]*)\" Last Name \"([^\"]*)\" and Email \"([^\"]*)\"$")
	public void verify_all_the_details_are_autopopulated_in_the_VLA_Details_Page_with_Type_of_driver_First_Name_Last_Name_and_Email_and_saves_the_VLA(
			String typeOfDriver, String FirstName, String LastName, String email) throws Throwable {

		String fd[] = FirstName.split(",");
		FirstName = CommonFunctions.readExcelMasterData(fd[0], fd[1], fd[2]);

		String ld[] = LastName.split(",");
		LastName = CommonFunctions.readExcelMasterData(ld[0], ld[1], ld[2]);

		String ed[] = email.split(",");
		email = CommonFunctions.readExcelMasterData(ed[0], ed[1], ed[2]);

		String tDriver[] = typeOfDriver.split(",");
		typeOfDriver = CommonFunctions.readExcelMasterData(tDriver[0], tDriver[1], tDriver[2]);

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.VLAname);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects the VLA");
		CommonFunctions.attachScreenshot();

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(typeOfDriver))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates Type of Driver");

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(FirstName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates First Name");

		javascriptUtil.scrollIntoView(OUVCalendarContainer.FirstNameheader);
		ActionHandler.wait(5);

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(LastName))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates Last Name");

		VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.EmailValidation(email))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User validates Email");

	}

	// User saves the VLA
	@Then("^User saves the VLA$")
	public void user_saves_the_VLA() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(8);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User creates a new VLA");
	}

	// Select the time slot next to previous booking
	@And("^Select the time slot next to previous booking$")
	public void select_the_time_slot_next_to_previous_booking() throws Throwable {
		ActionHandler.wait(8);
		ActionHandler.click(OUVCalendarContainer.SelectVehicleFromDay1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle from day tab");

		ActionHandler.wait(2);
		Reporter.addStepLog("User can select Vehicle for reservation");
	}

	// User saves an OUV Booking with Booking start time
	@Then("^User saves an OUV Booking including other location with Booking start time Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void user_saves_an_OUV_Booking_including_other_location_with_Booking_start_time_Quick_Reference_Booking_Justification_Booking_Request_Type_Passout_Type_Journey_Type(
			String bookingQuickRef, String bookingJustification, String bookingReqType, String passoutType,
			String journeyType) throws Throwable {

		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String driver1 = "test";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		// change start time

		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.TimeLable);
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.timeslot1);

		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_UP).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();

		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.scrollToView(OUVCalendarContainer.Details);
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		/*
		 * ActionHandler.wait(1);
		 * ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.
		 * bookingTable(bookingQuickRef)))); ActionHandler.wait(5);
		 * CommonFunctions.attachScreenshot();
		 */

	}

	// Verify the error message displayed and click on close and cancel
	@Then("^Verify the error message displayed and click on close and cancel$")
	public void verify_the_error_message_displayed_and_click_on_close_and_cancel() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.ConflictMsg)) {
			ActionHandler.wait(2);
			Reporter.addStepLog("Error Message was found");
		} else {

			Reporter.addStepLog("Error Message was not found");
		}

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.CloseErrorWindow);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.cancleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cancels the reservation");

	}

	// User searches for an OUV Vehicles with fleet
	@And("^User searches for an OUV Vehicles with fleet \"([^\"]*)\"$")
	public void user_searches_for_an_OUV_Vehicles_with_fleet(String Fleet) throws Throwable {

		String fle[] = Fleet.split(",");
		Fleet = CommonFunctions.readExcelMasterData(fle[0], fle[1], fle[2]);

		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Vehicles");

		ActionHandler.click(OUVCalendarContainer.OUVvehicleSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.OUVvehicleSearch, Fleet);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an OUV Booking");
		CommonFunctions.attachScreenshot();
	}

	// select the OUV Vehicles
	@And("^select the OUV Vehicles$")
	public void select_the_OUV_Vehicles() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.OUVSel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select OUV Vehicle");
		CommonFunctions.attachScreenshot();

	}

	// User tries to edit new requested hand in date
	@Then("^User edits the New Requested Hand In Date on OUV Vehicle window \"([^\"]*)\"$")
	public void user_edits_the_New_Requested_Hand_In_Date_on_OUV_Vehicle_window(String Hdate) throws Throwable {

		String hd[] = Hdate.split(",");
		Hdate = CommonFunctions.readExcelMasterData(hd[0], hd[1], hd[2]);

		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.NewHandinDate);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.NewHandinDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on edit icon");

		ActionHandler.wait(5);
		ActionHandler.clearAndSetText(OUVCalendarContainer.NewHandinDate, Hdate);
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		CommonFunctions.attachScreenshot();

	}

	// click on edit associate booking
	@Then("^click on edit associate booking$")
	public void click_on_edit_associate_booking() throws Throwable {

		ActionHandler.pageScrollDown();

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Associated Booking");

		ActionHandler.click(OUVCalendarContainer.AssociatedBooking);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Associated Booking icon");

	}

	// User clicks on Save and New VLA
	@Then("^User clicks on Save and New VLA$")
	public void user_clicks_on_Save_and_New_VLA() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.SaveandNew);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User tries to create a new VLA");
	}

	// Validate the error message displayed for creating a New VLA
	@Then("^Validate the error message displayed for creating a New VLA$")
	public void validate_the_error_message_displayed_for_creating_a_New_VLA() throws Throwable {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.VLAError);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the error message");
		ActionHandler.click(OUVCalendarContainer.Cancelchanges);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// User adds another VLA by entering all the driver information
	@Then("^User adds another VLA by entering all the driver information like Type of driver \"([^\"]*)\" First Name \"([^\"]*)\" Last Name \"([^\"]*)\" and Email \"([^\"]*)\"$")
	public void user_adds_another_VLA_by_entering_all_the_driver_information_like_Type_of_driver_First_Name_Last_Name_and_Email(
			String typeOfDriver, String FirstName, String LastName, String Email) throws Throwable {
		String fd[] = FirstName.split(",");
		FirstName = CommonFunctions.readExcelMasterData(fd[0], fd[1], fd[2]);

		String ld[] = LastName.split(",");
		LastName = CommonFunctions.readExcelMasterData(ld[0], ld[1], ld[2]);

		String ed[] = Email.split(",");
		Email = CommonFunctions.readExcelMasterData(ed[0], ed[1], ed[2]);

		String tDriver[] = typeOfDriver.split(",");
		typeOfDriver = CommonFunctions.readExcelMasterData(tDriver[0], tDriver[1], tDriver[2]);

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.EditTypeofDriver);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(typeOfDriver))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Type of Driver");

		if (typeOfDriver.equals("Individual")) {
			ActionHandler.setText(OUVCalendarContainer.VLAFirstName, FirstName);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(OUVCalendarContainer.VLALastName, LastName);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();

			ActionHandler.pageDown();
			ActionHandler.wait(4);
			ActionHandler.setText(OUVCalendarContainer.VLAEmailID, Email);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}

	}

	// User tries to Clone an OUV Booking
	@Then("^User tries to Clone an OUV Booking$")
	public void user_tries_to_Clone_an_OUV_Booking() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.EditDropdown);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on the dropdown");

		ActionHandler.click(OUVCalendarContainer.Clone);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Clone option");

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.NextButton)) {
			ActionHandler.click(OUVCalendarContainer.NextButton);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Next Button");
		}

		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks the Save Button");

	}

	// Validate that no Reservation is created for the cloned Booking
	@Then("^Validate that no Reservation is created for the cloned Booking$")
	public void validate_that_no_Reservation_is_created_for_the_cloned_Booking() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(OUVCalendarContainer.SelectReservation)) {
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User verifies no Reservation is created");
		}

	}

	// User edit the details of the cloned Booking
	@Then("^User edit the details of the cloned Booking$")
	public void user_edit_the_details_of_the_cloned_Booking() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.Editbutton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks the Edit Button");

		ActionHandler.clearAndSetText(OUVCalendarContainer.bookingQuickRefField, "Cloned Booking");
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits the Booking Quick Reference Field");

	}

	// User Save the changes
	@Then("^User Save the changes$")
	public void user_Save_the_changes() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks the Save Button");

	}

	// User tries to submit the booking for Approval
	@Then("^User tries to submit the booking for Approval$")
	public void user_tries_to_submit_the_booking_for_Approval() throws Exception {
		ActionHandler.click(OUVCalendarContainer.Submit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Submit Button");

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.SubmitConfirmation)) {
			ActionHandler.click(OUVCalendarContainer.Yes);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User clicks on Confirmation Button");
		}

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.ReservationError);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verifies the error message");
	}

	// User clicks on VLA and verify details are autopopulated
	@Then("^User clicks on VLA and verify details are autopopulated like Driver \"([^\"]*)\" First Name \"([^\"]*)\" Last Name \"([^\"]*)\" Phone \"([^\"]*)\"$")
	public void user_clicks_on_VLA_and_verify_details_are_autopopulated_like_Driver_First_Name_Last_Name_Phone(
			String Driver, String First, String Last, String Phone) throws Throwable {

		String Dri[] = Driver.split(",");
		Driver = CommonFunctions.readExcelMasterData(Dri[0], Dri[1], Dri[2]);

		String Fir[] = First.split(",");
		First = CommonFunctions.readExcelMasterData(Fir[0], Fir[1], Fir[2]);

		String Las[] = Last.split(",");
		Last = CommonFunctions.readExcelMasterData(Las[0], Las[1], Las[2]);

		String Pho[] = Phone.split(",");
		Phone = CommonFunctions.readExcelMasterData(Pho[0], Pho[1], Pho[2]);

		ActionHandler.wait(7);
		ActionHandler.click(OUVCalendarContainer.OpenVLAAgreement);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV Vehicle Loan Agreement from Related List Quick Links");
		ActionHandler.wait(7);

		ActionHandler.click(OUVCalendarContainer.vehicleloanAgreement);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on VLA");

		WebElement Driver1 = driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(Driver)));
		WebElement First1 = driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(First)));
		WebElement Last1 = driver.findElement(By.xpath(OUVCalendarContainer.ValueValidation(Last)));

		VerifyHandler.verifyElementPresent(Driver1);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Driver information present");

		VerifyHandler.verifyElementPresent(First1);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("First name information present");

		VerifyHandler.verifyElementPresent(Last1);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Last name information present");

		ActionHandler.pageDown();
		ActionHandler.wait(4);

		WebElement Phone1 = driver.findElement(By.xpath(OUVCalendarContainer.manager(Phone)));

		VerifyHandler.verifyElementPresent(Phone1);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Phone information present");

	}

	// User is able to Add a new Booking with repair type
	@Then("^Select Repair option as booking type from drop down Booking Type \"([^\"]*)\"$")
	public void select_Repair_option_as_booking_type_from_drop_down_Booking_Type(String Repair) throws Throwable {

		String rp[] = Repair.split(",");
		Repair = CommonFunctions.readExcelMasterData(rp[0], rp[1], rp[2]);

		ActionHandler.click(OUVCalendarContainer.ClickReserve);

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Associated Booking");

		ActionHandler.click(OUVCalendarContainer.AssociatedBooking);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Associated Booking icon");

	}

	// Select a Vehicle which is having an OUV booking and click on View in
	// Salesforce
	@Then("^Select a Vehicle which is having an OUV booking and click on View in Salesforce$")
	public void select_a_Vehicle_which_is_having_an_OUV_booking_and_click_on_View_in_Salesforce() throws Throwable {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.SelectVehicle1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.SalesForce);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Select a Vehicle which is having an OUV booking");

		String NewWindow = driver.getWindowHandle();
		ActionHandler.wait(30);
		Set<String> attchFileWindow = driver.getWindowHandles();
		Iterator<String> i = attchFileWindow.iterator();
		while (i.hasNext()) {
			String attachWin = i.next();
			if (!NewWindow.equalsIgnoreCase(attachWin)) {
				driver.switchTo().window(attachWin);
				verify_the_Vehicle_should_be_in_Live_stage();
				click_on_Check_out_Check_in_button();
				select_Checkbox_for_Check_out_and_click_on_Continue_button();
				driver.switchTo().window(NewWindow);

			}
		}

	}

	// Verify the Vehicle should be in Live stage
	@Then("^Verify the Vehicle should be in Live stage$")
	public void verify_the_Vehicle_should_be_in_Live_stage() throws Throwable {
		ActionHandler.wait(4);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.VerifyLiverStage);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Verify the Vehicle should be in Live stage");
	}

	// Click on Check-out/Check-in button
	@Then("^Click on Check-out/Check-in button$")
	public void click_on_Check_out_Check_in_button() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.Checkinorout);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Click on Check-out/Check-in button");
	}

	// Select Checkbox for Check-out and click on Continue button
	@Then("^Select Checkbox for Check-out and click on Continue button$")
	public void select_Checkbox_for_Check_out_and_click_on_Continue_button() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.ClickChechout);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Continue);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Select Checkbox for Check-out and click on Continue button");
	}

	// Select the Booking from the Notification Menu
	@Then("^Select the Booking from the Notification Menu$")
	public void select_the_Booking_from_the_Notification_Menu() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.Notification);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Notification Menu");
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.clicknotification);
		// ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.Booking(ID))));
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Booking ID");
	}

	// Click on Clone button
	@Then("^Click on Clone button$")
	public void click_on_Clone_button() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.Clickclone);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Click on Clone button ");
	}

	// Enter mandatory fields in the window open driver one
	@Then("^Enter mandatory fields in the window open driver one \"([^\"]*)\"$")
	public void enter_mandatory_fields_in_the_window_open_driver_one(String Driver1) throws Throwable {

		String Dri[] = Driver1.split(",");
		Driver1 = CommonFunctions.readExcelMasterData(Dri[0], Dri[1], Dri[2]);

		ActionHandler.click(OUVCalendarContainer.selectdriver);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.selectdriver, Driver1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.saved);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Enter mandatory fields in the window ");
	}

	// navigate to calender
	@Then("^navigate to calender$")
	public void navigate_to_calender() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.clickcalender);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.Search);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to navigate to calender ");
	}

	// save booking
	@Then("^save booking$")
	public void save_booking() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.save1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to save booking ");

	}

	// Click on Submit for Approval button
	@Then("^Click on Submit for Approval button$")
	public void click_on_Submit_for_Approval_Button() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.Submitforapproval);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Click on Submit for Approval button");
	}

	// Verify that an error occurred when user submit cloned OUV Booking for an
	// approval
	@Then("^Verify that an error occurred when user submit cloned OUV Booking for an approval$")
	public void verify_that_an_error_occurred_when_user_submit_cloned_OUV_Booking_for_an_approval() throws Throwable {

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.ErrorChecked);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog(
				"User able to Verify that an error occurred when user submit cloned OUV Booking for an approval");
	}

	// View that Booking Approval Status is set to New
	@Then("^View that Booking Approval Status is set to New$")
	public void View_that_Booking_Approval_Status_is_set_to_New() throws Throwable {

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.checknewstatus);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.clickeditbtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to View that Booking Approval Status is set to New");
	}

	// Edit the Fields under Booking Request section
	@Then("^Edit the Fields under Booking Request section on OUV Booking ID window and click on save Quickreff \"([^\"]*)\" bookin reason \"([^\"]*)\" booking req type \"([^\"]*)\" journey type \"([^\"]*)\"$")
	public void edit_the_Fields_under_Booking_Request_section_on_OUV_Booking_ID_window_and_click_on_save_Quickreff_bookin_reason_booking_req_type_journey_type(
			String bookingQuickRef, String reason, String bookingReqType, String journeyType) throws Throwable {

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String pt[] = reason.split(",");
		reason = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		ActionHandler.click(OUVCalendarContainer.quickref1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.quickref1, bookingQuickRef);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingreason);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.bookingreason, reason);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingreq1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeytyp);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.saving);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User able to Edit the Fields under Booking Request section on OUV Booking ID window and click on save");

	}

	// selectVehicleCalendar
	public void selectVehicleCalendar2() throws Exception {
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.click(OUVCalendarContainer.dayBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.TodayCalendar);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.SelectVehicleFromDaynext);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle from day tab");

	}

	// User edit the Booking Quick reference
	@When("^User edit the Booking Quick reference \"([^\"]*)\" and save it$")
	public void user_edit_the_Booking_Quick_reference_and_save_it(String arg1) throws Throwable {

		String BR[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(BR[0], BR[1], BR[2]);

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User click on Booking Quick reference pencil icon
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.BookingQuickRefPencilIcon);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Booking Quick reference pencil icon");

		// set text in booking quick reference
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.BookingQuickRefTxtBx, arg1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("set text in booking quick reference");

		// click on Save button
		ActionHandler.click(OUVCalendarContainer.SaveEdits);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Save button");
	}

	// Verify the error message displayed to edit
	@Then("^Verify the error message displayed to edit$")
	public void verify_the_error_message_displayed_to_edit() throws Throwable {
		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.snagMsg)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message caught");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message dint displayed");
		}

		// user click on cancel button
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.CancelEdits);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on cancel button");
	}

	// User searches for live vehicle
	@And("^User searches for live vehicle \"([^\"]*)\"$")
	public void User_searches_for_live_vehicle(String Vehicle) throws Exception {

		String f[] = Vehicle.split(",");
		Vehicle = CommonFunctions.readExcelMasterData(f[0], f[1], f[2]);

		ActionHandler.wait(3);
		Select VehicleDD = new Select(OUVCalendarContainer.VehicleSelection);
		VehicleDD.selectByVisibleText(Vehicle);
		ActionHandler.wait(3);
		Reporter.addStepLog("Vehcile with live stage selection has been done");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");
	}

	// select an availble slot for selected vehicle
	@And("^select an availble slot for selected vehicle$")
	public void select_an_availble_slot_for_selected_vehicle() throws Exception {
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.click(OUVCalendarContainer.dayBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on day button");

		ActionHandler.click(OUVCalendarContainer.TodayCalendar);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on today button");

		ActionHandler.click(OUVCalendarContainer.SelectVehicleFromDay);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle from day tab");
	}

	// Navigate to OUV Gatehouse Logs tab
	@When("^Navigate to OUV Gatehouse Logs tab$")
	public void Navigate_to_OUV_Gatehouse_Logs_tab() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		// user click on OUV Gate House log link
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.OUVGateHouseLink);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on OUV Gate House log link");

	}

	// User selects driver or logistics and save it
	@When("^User selects driver or logistics and save it$")
	public void User_selects_driver_or_logistics_and_save_it() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.driverLogistics);
		ActionHandler.wait(2);
		Actions act1 = new Actions(driver);
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();

		// user click on save button
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.SaveFilter);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects driver or logistics and save it");
	}

	// User saves an OUV Booking including other location
	@And("^User saves an OUV Booking including other location and Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void user_saves_an_OUV_Booking_including_other_location_and_Booking_Quick_Reference(String bookingQuickRef,
			String bookingJustification, String bookingReqType, String passoutType, String journeyType)
			throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String driver1 = "DHL";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

	}

	// User selects checkbox checkin and close then click on continue
	@Then("^checkbox checkin and close then click on continue$")
	public void checkbox_checkin_and_close_then_click_on_continue() throws Throwable {

		// User checkbox the CheckIn
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.CheckIncloseCheckBx);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User checkbox the CheckIn & close");

		// user click on Continue button
		ActionHandler.click(OUVCalendarContainer.ContinueBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on Continue button");

	}

	// Verify that status of Reservation is set to confirmed
	@Then("^Verify that status of Reservation is set to confirmed$")
	public void Verify_that_status_of_Reservation_is_set_to_confirmed() throws Throwable {
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.StatusConfirmed)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("status of Reservation is set to confirmed");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("status of Reservation is not set to confirmed");
		}
	}

	// Select Repair option as booking type from drop down Booking Types
	@Then("^Select Repair option as booking type from drop down Booking Types \"([^\"]*)\"$")
	public void select_Repair_option_as_booking_type_from_drop_down_Booking_Types(String Repair) throws Throwable {

		String rp[] = Repair.split(",");
		Repair = CommonFunctions.readExcelMasterData(rp[0], rp[1], rp[2]);

		ActionHandler.click(OUVCalendarContainer.ClickReserve);

		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Associated Booking");

		ActionHandler.click(OUVCalendarContainer.AssociatedBooking);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User click on Associated Booking icon");

	}

	// booking information is entered click save
	@When("^booking information is entered click save$")
	public void booking_information_is_entered_click_save() throws Throwable {

		String Email = "test@c.cc";
		String Driver1 = "DHL";
		String Driver2 = "Test";

		ActionHandler.wait(5);
		String Booking = OUVCalendarContainer.BookingHeader.getText();
		ActionHandler.wait(5);

		ActionHandler.clearAndSetText(OUVCalendarContainer.AssociatedBookinginput, Booking);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters booking info");

		Actions act1 = new Actions(driver);
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();

		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Save");

		ActionHandler.click(OUVCalendarContainer.JLRContactMail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.JLRContactMail, Email);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.Driver1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.Driver1, Driver1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.Driver2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.Driver2, Driver2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

	}

	// verify the error message for associate booking
	@When("^verify the error message for associate booking$")
	public void verify_the_error_message() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.AssociatedBookinginput)) {
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that error message is displayed");
		}

		else {
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that error message not displayed");
		}

		ActionHandler.click(OUVCalendarContainer.Cancelchanges);
		ActionHandler.wait(4);

		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User click on Cancel");

	}

	// Click on newly created OUV Booking record in the Reservation Calendar
	@Then("^Click on newly created OUV Booking record in the Reservation Calendar for the pop up to appear again$")
	public void click_on_newly_created_OUV_Booking_record_in_the_Reservation_Calendar_for_the_pop_up_to_appear_again()
			throws Throwable {

		ActionHandler.click(OUVCalendarContainer.NewBookingRecord);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.OuvBooking);
		ActionHandler.wait(3);

	}

	// Click on the OUV Booking ID hyperlink in the booking window
	@Then("^Click on the OUV Booking ID hyperlink in the booking window$")
	public void click_on_the_OUV_Booking_ID_hyperlink_in_the_booking_window() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.RecordId);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Select a booking Slot in Calendar
	@Then("^Select a booking Slot in Calendar$")
	public void select_a_booking_Slot_in_Calendar() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.wait(5);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.click(OUVCalendarContainer.monthBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.TodayCalendar);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		/*
		 * ActionHandler.click(OUVCalendarContainer.rightButton2);
		 * ActionHandler.wait(3); CommonFunctions.attachScreenshot();
		 */

		ActionHandler.click(OUVCalendarContainer.NewBookingRecord1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Select a booking Slot in Calendar");

	}

	// Open recently approved OUV Bookings
	@And("^Open recently approved OUV Bookings$")
	public void Open_Recently_Approved_OUV_Bookings() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.All);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User opens list of all OUV Booking");

		ActionHandler.click(OUVCalendarContainer.secondOUVBookingFrmAll);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User can open recently approved OUV Booking");
	}

	// Navigate to Reservations from Related Quick Links
	@Then("^Navigate to Reservations from Related Quick Links$")
	public void navigate_to_Reservations_from_Related_Quick_Links() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.clickreservations);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Navigate to Reservations from Related Quick Links");
	}

	// Click on OUV Vehicle by clicking on Blue link
	@Then("^Click on OUV Vehicle by clicking on Blue link$")
	public void click_on_OUV_Vehicle_by_clicking_on_Blue_link() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.Clickouvvehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Click on OUV Vehicle by clicking on Blue link");
	}

	// Verify that an error occurred when user tries to do CheckOut again
	@Then("^Verify that an error occurred when user tries to do CheckOut again for (\\d+)nd Booking$")
	public void verify_that_an_error_occurred_when_user_tries_to_do_CheckOut_again_for_nd_Booking(int arg1)
			throws Throwable {
		ActionHandler.click(OUVCalendarContainer.Clickouvvehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able toVerify that an error occurred when user tries to do CheckOut again");
	}

	// User save an OUV Booking including other location
	@And("^User save an OUV Booking including other location with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Save_OUV_Booking_including_other_location(String bookingQuickRef, String bookingJustification,
			String bookingReqType, String passoutType, String journeyType) throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String driver1 = "test";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.scrollToView(OUVCalendarContainer.Details);
		ActionHandler.wait(3);

		/*
		 * ActionHandler.click(OUVCalendarContainer.saveReservation);
		 * ActionHandler.wait(7); CommonFunctions.attachScreenshot();
		 * Reporter.addStepLog("Reservation has been saved");
		 */

		/*
		 * ActionHandler.wait(1);
		 * ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.
		 * bookingTable(bookingQuickRef)))); ActionHandler.wait(5);
		 * CommonFunctions.attachScreenshot();
		 */

	}

	// User set local system date and time for the reservation
	@Then("^User set local system date and time for the reservation \"([^\"]*)\" Booking \"([^\"]*)\"$")
	public void User_set_local_system_date_and_time_for_the_reservation_Booking(String EndTime, String bookingQuickRef)
			throws Exception {

		String f1[] = EndTime.split(",");
		EndTime = CommonFunctions.readExcelMasterData(f1[0], f1[1], f1[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.wait(5);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.startTime);
		ActionHandler.wait(3);

		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 2);
		c.add(Calendar.MINUTE, 3);
		dt = c.getTime();

		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stime = new SimpleDateFormat("HH:mm");
		String newDate = sdt.format(dt);
		String newTime = stime.format(dt);

		System.out.println("New Date is = " + newDate);

		// User set the End time and date for booking reservation
		ActionHandler.clearAndSetText(OUVCalendarContainer.Enddate, newDate);
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.EndTime, EndTime);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the End time and date for booking reservation");

		// User set the start time as local system time
		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(OUVCalendarContainer.starttime, newTime);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the start time as local system time");

		// Reservation has been saved
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Verify the check-out record in OUV Gatehouse Logs
	@Then("^Verify the check-out record in OUV Gatehouse Logs$")
	public void verify_the_check_out_record_in_OUV_Gatehouse_Logs() throws Throwable {

		// ActionHandler.wait(60);

		ActionHandler.click(OUVCalendarContainer.Clickreservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		driver.navigate().refresh();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.clickouvvehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Verify the check-out record in OUV Gatehouse Logs");
	}

	// Check-in Window should be displayed and Select driver
	@Then("^Check-in Window should be displayed and Select driver$")
	public void check_in_Window_should_be_displayed_and_Select_driver() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.clickdriver1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.selectdrive);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.Clicksave);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Check-in Window should be displayed and Select driver");
	}

	// Select Checkbox for Check-in and click on Continue button
	@Then("^Select Checkbox for Check-in and click on Continue button$")
	public void select_Checkbox_for_Check_in_and_click_on_Continue_button() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.ClickChechin);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Continue);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Select Checkbox for Check-in and click on Continue button");
	}

	// Verify the check-in record in OUV Gatehouse Logs
	@Then("^Verify the check-in record in OUV Gatehouse Logs$")
	public void verify_the_check_in_record_in_OUV_Gatehouse_Logs() throws Throwable {

		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.gatehouselogs);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Verify the check-in record in OUV Gatehouse Logs");
	}

	// User clicks on Save Button
	@Then("^Click on save button$")
	public void click_on_save_button() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.ClickSave);
		ActionHandler.wait(5);
	}

	// Click on Drop down at top right corner
	@Then("^Click on Drop down at top right corner$")
	public void click_on_Drop_down_at_top_right_corner() throws Throwable {

		/*
		 * driver.navigate().refresh(); ActionHandler.wait(5);
		 */

		ActionHandler.click(OUVCalendarContainer.clickdropdown);
		ActionHandler.wait(10);

		CommonFunctions.attachScreenshot();
	}

	// User verifies the error message
	@Then("^Verifying error message$")
	public void verifying_error_message() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.VerifyErrorMsg)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("error message is visible");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("error message is not visible");
		}
	}

	// Select Check-in & Complete
	@Then("^Select Check-in & Complete$")
	public void select_Check_in_Complete() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.checkinandcomplete);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Click on Drop down at top right corner");
	}

	// Fill out Mandatory fields like Driver,Mileage,keys checked
	@Then("^Fill out Mandatory fields Driver \"([^\"]*)\" mileage \"([^\"]*)\" keys checked \"([^\"]*)\" Return Location \"([^\"]*)\" Vehicle Condition \"([^\"]*)\"$")
	public void fill_out_Mandatory_fields_Driver_mileage_keys_checked_Return_Location_Vehicle_Condition(String Driver,
			String mileage, String Keyschecked, String Location, String Condition) throws Throwable {
		String bqref[] = Driver.split(",");
		Driver = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String pt[] = mileage.split(",");
		mileage = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String br[] = Keyschecked.split(",");
		Keyschecked = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String jt[] = Location.split(",");
		Location = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		String ld[] = Condition.split(",");
		Condition = CommonFunctions.readExcelMasterData(ld[0], ld[1], ld[2]);

		ActionHandler.click(OUVCalendarContainer.driverfield);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driverfield, Driver);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.mileagefield);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.mileagefield, mileage);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.selectkey);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.selectkey(Keyschecked))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.location1);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.location1, Location);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.condition);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.condition, Condition);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		/*
		 * ActionHandler.click(OUVCalendarContainer.condition); ActionHandler.wait(10);
		 * CommonFunctions.attachScreenshot();
		 */

		ActionHandler.click(OUVCalendarContainer.save);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Fill out Mandatory fields");
	}

	// Verify the check-in & complete record in OUV Gatehouse Logs
	@Then("^Verify the check-in & complete record in OUV Gatehouse Logs$")
	public void verify_the_check_in_complete_record_in_OUV_Gatehouse_Logs() throws Throwable {
		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.gatehouselogs);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Verify the check-in & complete record in OUV Gatehouse Logs");
	}

	// Verify that CheckOut entry is presented in the OUV Gatehouse Logs
	@Then("^Verify that CheckOut entry is presented in the OUV Gatehouse Logs$")
	public void verify_that_CheckOut_entry_is_presented_in_the_OUV_Gatehouse_Logs() throws Throwable {
		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.gatehouselogs);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Verify that CheckOut entry is presented in the OUV Gatehouse Logs");
	}

	// Navigate to OUV Vehicle Utilisation
	@Then("^Navigate to OUV Vehicle Utilisation$")
	public void navigate_to_OUV_Vehicle_Utilisation() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.utilizationlogs);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Navigate to OUV Vehicle Utilisation");
	}

	// Verify that User can be able to view utilisation logs after doing CheckOut &
	// CheckIn
	@Then("^Verify that User can be able to view utilisation logs after doing CheckOut & CheckIn$")
	public void verify_that_User_can_be_able_to_view_utilisation_logs_after_doing_CheckOut_CheckIn() throws Throwable {
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User able to Verify that User can be able to view utilisation logs after doing CheckOut & CheckIn");
	}

	// Verify that start date and time is before end date and time
	@Then("^Verify that start date and time is before end date and time$")
	public void Verify_that_start_date_and_time_is_before_end_date_and_time() throws Throwable {

		String StartDate = (OUVCalendarContainer.StartDate).getText();
		String EndDate = (OUVCalendarContainer.EndDate).getText();

		System.out.println("start date and time" + StartDate + "," + EndDate);
		ActionHandler.wait(3);

		String s[] = StartDate.split(" ");
		String t[] = EndDate.split(" ");

		if (s[0].contentEquals(t[0])) {
			Reporter.addStepLog("Start date is same as end date");
			CommonFunctions.attachScreenshot();
			if (s[1].contentEquals(t[1])) {
				Reporter.addStepLog("Start time is same as end time");
				CommonFunctions.attachScreenshot();
			} else {
				Reporter.addStepLog("Start time before the end time");
				CommonFunctions.attachScreenshot();
			}
		} else {
			Reporter.addStepLog("Start date and time is before end date and time");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.CancelBtn);
		ActionHandler.wait(3);
		Reporter.addStepLog("click on cancel button");
		CommonFunctions.attachScreenshot();
	}

	// Navigate to Reservations Tab
	@And("^Navigate to Reservations Tab$")
	public void navigate_to_Reservations_Tab() throws Throwable {

		// Navigate to Reservations Tab
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.ReservationLink);
		ActionHandler.wait(5);
		Reporter.addStepLog("Navigate to Reservations Tab");
		CommonFunctions.attachScreenshot();
	}

	// User select the vehicle
	@Then("^User select the vehicle$")
	public void user_select_the_vehicle() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User select the vehicle
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.VehcileLink);
		ActionHandler.wait(5);
		Reporter.addStepLog("Navigate to OUV Vehicle Tab");
		CommonFunctions.attachScreenshot();
	}

	// User click on CheckOut CheckIn
	@When("^User click on CheckOut CheckIn$")
	public void user_click_on_CheckOut_CheckIn() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User click on CheckOut CheckIn button
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.CheckInCheckOutBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on CheckOut/CheckIn button");
	}

	// checkbox the checkout and click on continue
	@Then("^checkbox the checkout and click on continue$")
	public void checkbox_the_checkout_and_click_on_continue() throws Throwable {

		// User checkbox the CheckOut
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.CheckOutCheckBx);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User checkbox the CheckOut");

		// user click on Continue button
		ActionHandler.click(OUVCalendarContainer.ContinueBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on Continue button");

	}

	// checkbox the checkIn and click on continue
	@Then("^checkbox the checkIn and click on continue$")
	public void checkbox_the_checkIn_and_click_on_continue() throws Throwable {

		// User checkbox the CheckOut
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.CheckInCheckBx);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User checkbox the CheckIn");

		// user click on Continue button
		ActionHandler.click(OUVCalendarContainer.ContinueBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on Continue button");

	}

	// Verify the error message displayed to checkout
	@And("^Verify the error message displayed to checkout$")
	public void verify_the_error_message_displayed_to_checkout() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.ErrorCheckOut)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message displayed to check out");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message dint displayed to check out");
		}
		ActionHandler.wait(6);
		// user click on cancel button
		ActionHandler.click(OUVCalendarContainer.CancelPopup);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on cancel button");
		ActionHandler.wait(5);
	}

	// User navigate back to booking number
	@Then("^User navigate back to booking number$")
	public void User_navigate_back_to_booking_number() throws Throwable {

		// User navigate back to booking number
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.click(OUVCalendarContainer.BookingNumber);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to the booking Number");
	}

	// verify that Approval status is set to Booking Pending Approval
	@Then("^Verify that Approval status is set to Booking Pending Approval$")
	public void verify_that_Approval_status_is_set_to_Booking_Pending_Approval() throws Throwable {

		ActionHandler.wait(7);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.PendingStatus)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Approval status is set to Booking Pending Approval");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Approval status is not set to Booking Pending Approval");
		}
	}

	// Click on Check box under includes Preparation/Transportation
	@Then("^Click on Check box under includes Preparation/Transportation$")
	public void click_on_Check_box_under_includes_Preparation_Transportation() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.ClickCheckbox);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Select date which is greater than Reservation end date
	@Then("^Select date which is greater than Reservation end date for OUV booking End Date \"([^\"]*)\"$")
	public void select_date_which_is_greater_than_Reservation_end_date_for_OUV_booking_End_Date(String EndDate)
			throws Throwable {

		String Edate[] = EndDate.split(",");
		EndDate = CommonFunctions.readExcelMasterData(Edate[0], Edate[1], Edate[2]);

		ActionHandler.click(OUVCalendarContainer.PrepEndDate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Today);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// Save a booking
	@Then("^Fill out all Mandatory fields in Booking Window Booking Quick Reference \"([^\"]*)\" Internal JLR \"([^\"]*)\" Other Location \"([^\"]*)\" Email Id \"([^\"]*)\" Driverone \"([^\"]*)\" Drivertwo \"([^\"]*)\"$")
	public void fill_out_all_Mandatory_fields_in_Booking_Window_Booking_Quick_Reference_Internal_JLR_Other_Location_Email_Id_Driverone_Drivertwo(
			String Qreff, String Internal, String Other, String Email, String Driver1, String Driver2)
			throws Throwable {

		String qr[] = Qreff.split(",");
		Qreff = CommonFunctions.readExcelMasterData(qr[0], qr[1], qr[2]);

		String IJLR[] = Internal.split(",");
		Internal = CommonFunctions.readExcelMasterData(IJLR[0], IJLR[1], IJLR[2]);

		String ol[] = Other.split(",");
		Other = CommonFunctions.readExcelMasterData(ol[0], ol[1], ol[2]);

		String EId[] = Email.split(",");
		Email = CommonFunctions.readExcelMasterData(EId[0], EId[1], EId[2]);

		String D1[] = Driver1.split(",");
		Driver1 = CommonFunctions.readExcelMasterData(D1[0], D1[1], D1[2]);

		String D2[] = Driver2.split(",");
		Driver2 = CommonFunctions.readExcelMasterData(D2[0], D2[1], D2[2]);

		ActionHandler.click(OUVCalendarContainer.Quick);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.Quick, Qreff);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.JLRContact);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.JLRContact, Internal);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.OtherLocation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.OtherLocation, Other);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.JLRContactMail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.JLRContactMail, Email);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.Driver1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.Driver1, Driver1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.Driver2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.Driver2, Driver2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		Reporter.addStepLog("User able to Fill out all Mandatory fields in Booking Window");

	}

	// Click on Save, and user will be navigated back to Calendar
	@Then("^Click on Save, and user will be navigated back to Calendar$")
	public void click_on_Save_and_user_will_be_navigated_back_to_Calendar() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.Save2);
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.Cancel);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User able to Click on Save, and user will be navigated back to Calendar");

	}

	// save the new Permanent Accessories
	@When("^save the new Permanent Accessories$")
	public void save_the_new_Permanent_Accessories() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Save button");
	}

	// verify the displayed Permanent Accessories error message
	@When("^verify the displayed Permanent Accessories error message$")
	public void verify_the_displayes_Permanent_Accessories_error_message() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.ErrorAccessories)) {
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User validates error message");
		} else {

			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User validates that no error message displayed");
		}
	}

	// User saves an OUV Booking including other location and time
	@And("^User saves an OUV Booking including other location and time with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Saves_OUV_Booking_including_other_location_and_time(String bookingQuickRef,
			String bookingJustification, String bookingReqType, String passoutType, String journeyType)
			throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.com";
		String driver1 = "DHL";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.scrollToView(OUVCalendarContainer.Dh);
		ActionHandler.wait(4);

		ActionHandler.click(OUVCalendarContainer.StartDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(4);

		ActionHandler.click(OUVCalendarContainer.today);
		ActionHandler.wait(4);

		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		c.add(Calendar.MINUTE, 2);
		dt = c.getTime();

		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stime = new SimpleDateFormat("HH:mm");
		String newDate = sdt.format(dt);
		String newTime = stime.format(dt);

		System.out.println("New Date is = " + newDate);

		// User set the start time as local system time
		ActionHandler.clearAndSetText(OUVCalendarContainer.startTime, newTime);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the start time as local system time");

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// Search for an OUV Booking with Approved status Search List
	@Then("^Search for an OUV Booking with Approved status Search List with \"([^\"]*)\"$")
	public void search_for_an_OUV_Booking_with_Approved_status_Search_List_with(String searchBooking) throws Throwable {
		String Search[] = searchBooking.split(",");
		searchBooking = CommonFunctions.readExcelMasterData(Search[0], Search[1], Search[2]);

		ActionHandler.wait(10);
		ActionHandler.click(OUVCalendarContainer.SelectBookings);
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.SelectAll1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.SearchBookings);
		ActionHandler.wait(5);
		ActionHandler.setText(OUVCalendarContainer.SearchBookings, searchBooking);
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Search for an OUV Booking with Approved status Search List");

	}

	// select the driver from the list and click on save
	@Then("^select the driver from the list and click on save$")
	public void select_the_driver_from_the_list_and_click_on_save() throws Throwable {

		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.selectdriver);
		ActionHandler.wait(3);

		Actions act1 = new Actions(driver);
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.SaveFilter);
		ActionHandler.wait(3);
		Reporter.addStepLog("user clicks on Save button");

	}

	// Login with Fleet Admin
	@Given("^Login with Fleet Admin as \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void login_with_Fleet_Admin_as_and_password_as(String UserName, String Password) throws Throwable {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		javascriptUtil.clickElementByJS(OUVCalendarContainer.homeTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginImg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.addUserName);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(7);
		ActionHandler.setText(OUVCalendarContainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(20);

	}

	// click on vehicle
	@Then("^click on vehicle$")
	public void click_on_vehicle() throws Throwable {

		ActionHandler.wait(4);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.click(OUVCalendarContainer.Rvehicle);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// User clicks on check out button
	@When("^click on checkout button and select checkout option$")
	public void click_on_checkout_button_and_select_checkout_option() throws Throwable {

		// User click on CheckOut CheckIn button
		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.CheckInCheckOutBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on CheckOut/CheckIn button");

		// User checkbox the CheckOut
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.CheckOutCheckBx);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User checkbox the CheckOut");

		// user click on Continue button
		ActionHandler.click(OUVCalendarContainer.ContinueBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user click on Continue button");

	}

	// User set local systems date and time for the reservation
	@Then("^User set local systems date and time for the reservation \"([^\"]*)\" Booking \"([^\"]*)\"$")
	public void User_set_local_systems_date_and_time_for_the_reservation_Booking(String EndTime, String bookingQuickRef)
			throws Exception {

		String f1[] = EndTime.split(",");
		EndTime = CommonFunctions.readExcelMasterData(f1[0], f1[1], f1[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.wait(5);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.startTime);
		ActionHandler.wait(3);

		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		c.add(Calendar.MINUTE, 30);
		dt = c.getTime();

		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stime = new SimpleDateFormat("HH:mm");
		String newDate = sdt.format(dt);
		String newTime = stime.format(dt);

		System.out.println("New Date is = " + newDate);

		// User set the End time and date for booking reservation
		ActionHandler.clearAndSetText(OUVCalendarContainer.Enddate, newDate);
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.EndTime, EndTime);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the End time and date for booking reservation");

		// User set the start time as local system time
		ActionHandler.clearAndSetText(OUVCalendarContainer.starttime, newTime);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the start time as local system time");

		// Reservation has been saved
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		ActionHandler.wait(1);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Verify the Day Pass Approver information
	@Then("^Verify that the Day Pass Approver information as \"([^\"]*)\"$")
	public void verify_that_the_Day_Pass_Approver_information_as(String UserName) throws Throwable {

		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		ActionHandler.pageCompleteScrollDown();

		VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.textsearch(UserName))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the Day Pass Approver information");
	}

	// verify that the Pending Approval is assigned to the Day Pass Approver
	@Then("^verify that the Pending Approval is assigned to the Day Pass Approver \"([^\"]*)\"$")
	public void verify_that_the_Pending_Approval_is_assigned_to_the_Day_Pass_Approver(String UserName)
			throws Throwable {

		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.click(OUVCalendarContainer.backtoBookingID);
		Reporter.addStepLog("Back to booking ID");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.ApprovalHistory);
		Reporter.addStepLog("Click on Approval History");
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.textsearch(UserName))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify the Day Pass Approver information");
	}

	// View all searched Vehicles
	@And("^View all searched Vehicles$")
	public void view_all_searched_vehicles() throws Throwable {

		selectVehiclesCalendar();
		ActionHandler.wait(2);
		Reporter.addStepLog("User can select Vehicle for reservation");
	}

	// verify the error message
	@And("^verify the error message$")
	public void verify_the_error_message1() throws Throwable {
		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.errormail)) {
			Reporter.addStepLog("Error message found");
		} else {
			Reporter.addStepLog("Error message not found");
		}

	}

	@And("^User Collects Vehicle User Information$")
	public void User_Collects_vehicle_User_Information() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.collectDriverInfo);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	@And("^User enters all vehicle user information like Company \"([^\"]*)\" Phone \"([^\"]*)\" Date of Birth \"([^\"]*)\" Occupation \"([^\"]*)\" Address \"([^\"]*)\" Postcode \"([^\"]*)\" "
			+ "Passport Number \"([^\"]*)\" Driver License Number \"([^\"]*)\" Country of issue \"([^\"]*)\" License Start Date \"([^\"]*)\" "
			+ "License End Date \"([^\"]*)\" Insured By \"([^\"]*)\" Insurance company \"([^\"]*)\"$")
	public void User_enters_All_Vehicle_User_Information(String company, String phone, String DOB, String occupation,
			String address, String postCode, String passportNumber, String drivingLicenseNumber, String country,
			String licenseStartDate, String licenseEndDate, String insuredBy, String insuredCompany) throws Exception {
		ActionHandler.wait(2);

		String com[] = company.split(",");
		company = CommonFunctions.readExcelMasterData(com[0], com[1], com[2]);

		String ph[] = phone.split(",");
		phone = CommonFunctions.readExcelMasterData(ph[0], ph[1], ph[2]);

		String dob[] = DOB.split(",");
		DOB = CommonFunctions.readExcelMasterData(dob[0], dob[1], dob[2]);

		String occu[] = occupation.split(",");
		occupation = CommonFunctions.readExcelMasterData(occu[0], occu[1], occu[2]);

		String add[] = address.split(",");
		address = CommonFunctions.readExcelMasterData(add[0], add[1], add[2]);

		String passCo[] = postCode.split(",");
		postCode = CommonFunctions.readExcelMasterData(passCo[0], passCo[1], passCo[2]);

		String passNum[] = passportNumber.split(",");
		passportNumber = CommonFunctions.readExcelMasterData(passNum[0], passNum[1], passNum[2]);

		String driLicNum[] = drivingLicenseNumber.split(",");
		drivingLicenseNumber = CommonFunctions.readExcelMasterData(driLicNum[0], driLicNum[1], driLicNum[2]);

		String cou[] = country.split(",");
		country = CommonFunctions.readExcelMasterData(cou[0], cou[1], cou[2]);

		String LicStartDate[] = licenseStartDate.split(",");
		licenseStartDate = CommonFunctions.readExcelMasterData(LicStartDate[0], LicStartDate[1], LicStartDate[2]);

		String LicEndDate[] = licenseEndDate.split(",");
		licenseEndDate = CommonFunctions.readExcelMasterData(LicEndDate[0], LicEndDate[1], LicEndDate[2]);

		String insBy[] = insuredBy.split(",");
		insuredBy = CommonFunctions.readExcelMasterData(insBy[0], insBy[1], insBy[2]);

		String insCom[] = insuredCompany.split(",");
		insuredCompany = CommonFunctions.readExcelMasterData(insCom[0], insCom[1], insCom[2]);

		Reporter.addStepLog("User start filling Vehicle User Information");
		// ActionHandler.click(OUVCalendarContainer.EditCompany);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLACompany, company);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAPhone, phone);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLADOB, DOB);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAOccupation, occupation);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAAddress1, address);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAAddress2, address);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAAddress3, address);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAAddress4, address);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAPostcode, postCode);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAAddress1, address);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAPassportNumber, passportNumber);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLALicenceNumber, drivingLicenseNumber);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLACountry, country);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLALicenceNumber, drivingLicenseNumber);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLALicStartDate, licenseStartDate);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLALicExpiryDate, licenseEndDate);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.VLAInsuredBy);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(insuredBy))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.VLAInsCompany, insuredCompany);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		driver.navigate().back();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// User saves an OUV Booking including other location and invalid email
	@And("^User saves an OUV Booking including other location and invalid emial with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Saves_OUV_Booking_including_other_location_and_invalid_email(String bookingQuickRef,
			String bookingJustification, String bookingReqType, String passoutType, String journeyType)
			throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Test Booking";

		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.cc";
		String driver1 = "DHL";

		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.scrollToView(OUVCalendarContainer.Details);
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		ActionHandler.wait(10);

	}

	@Then("^User Includes Preparation or transportation$")
	public void user_Includes_Preparation_or_transportation() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.PrepTrans);
		ActionHandler.wait(3);
		Reporter.addStepLog("User Includes Preparation or transportations");
		CommonFunctions.attachScreenshot();

		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		c.add(Calendar.MINUTE, 3);
		dt = c.getTime();

		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stime = new SimpleDateFormat("HH:mm");
		String newDate = sdt.format(dt);

		System.out.println("New Date is = " + newDate);

		// User set the End time and date for booking reservation
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.TransDate, newDate);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the Transportation date for booking reservation");

	}

	// approved status should be verified
	@And("^verify the approved status$")
	public void verify_the_approved_status() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.ApprovedSts)) {
			Reporter.addStepLog("Approved status verified");
		} else {
			Reporter.addStepLog("Approved status not verified");
		}
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// User navigates to permanent accessories
	@Then("^User navigate to Permanent Accessories$")
	public void user_navigate_to_Permanent_Accessories() throws Throwable {
		ActionHandler.wait(4);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(OUVCalendarContainer.PermanentAccessories);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to permanant accessories");
		ActionHandler.wait(5);

	}

	// User adds permanent accessories
	@And("^fill permanent accessories with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void fill_permanent_accessories_with_and(String permanentacc, String permanentaccessories) throws Throwable {

		String pa[] = permanentacc.split(",");
		permanentacc = CommonFunctions.readExcelMasterData(pa[0], pa[1], pa[2]);

		String pac[] = permanentaccessories.split(",");
		permanentaccessories = CommonFunctions.readExcelMasterData(pac[0], pac[1], pac[2]);

		ActionHandler.wait(4);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.PermAcc);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.PermAccessories);
		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(OUVCalendarContainer.PermAccessories, permanentacc);
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.PermAcc1);
		ActionHandler.wait(3);
		Reporter.addStepLog("selects accessories");

		ActionHandler.clearAndSetText(OUVCalendarContainer.PermAccessories, permanentaccessories);
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.PermAcc1);
		ActionHandler.wait(3);
		Reporter.addStepLog("selects accessories");

	}

	@Then("^Navigate to booking window$")
	public void navigate_to_booking_window() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(10);
		ActionHandler.click(OUVCalendarContainer.BookingId1);
		ActionHandler.wait(3);
	}

	// select an available slot for selected vehicle
	@And("^select an availble slot of week for selected vehicle$")
	public void select_an_availble_slot_of_week_for_selected_vehicle() throws Exception {
		ActionHandler.wait(6);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.click(OUVCalendarContainer.SelectVehicleFromDay);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle from day tab");
	}

	// select an available slot for selected vehicle
	@And("^select a slot of week for selected vehicle$")
	public void select_a_slot_of_week_for_selected_vehicle() throws Exception {
		ActionHandler.wait(6);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.SelectVehicleFromDayB);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle from day tab");
	}

	@Then("^User select two temporary Accessories \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_select_two_temporary_Accessories_and(String TemAcc1, String TemAcc2) throws Throwable {

		String pt[] = TemAcc1.split(",");
		TemAcc1 = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = TemAcc2.split(",");
		TemAcc2 = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.TemporaryAccessory);
		ActionHandler.wait(2);

		// ActionHandler.pageScrollDown();
		ActionHandler.clearAndSetText(OUVCalendarContainer.TemporaryAccTextBx, TemAcc1);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.AccSelection(TemAcc1))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.clearAndSetText(OUVCalendarContainer.TemporaryAccTextBx, TemAcc2);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.AccSelection(TemAcc2))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

	}

	@Then("^User select three Service Reservations \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_select_three_Service_Reservations_and_and(String ServRes1, String ServRes2, String ServRes3)
			throws Throwable {

		String pt[] = ServRes1.split(",");
		ServRes1 = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = ServRes2.split(",");
		ServRes2 = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		String sr[] = ServRes3.split(",");
		ServRes3 = CommonFunctions.readExcelMasterData(sr[0], sr[1], sr[2]);

		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.ServiceReservation);
		ActionHandler.wait(2);

		// ActionHandler.pageScrollDown();
		ActionHandler.clearAndSetText(OUVCalendarContainer.ServResTextBx, ServRes1);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.AccSelection(ServRes1))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.clearAndSetText(OUVCalendarContainer.ServResTextBx, ServRes2);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.AccSelection(ServRes2))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.clearAndSetText(OUVCalendarContainer.ServResTextBx, ServRes3);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.AccSelection(ServRes3))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	@Then("^User select two Permanent accessories \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_select_two_Permanent_accessories_and(String PerAcc1, String PerAcc2) throws Throwable {

		String jt[] = PerAcc1.split(",");
		PerAcc1 = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		String sr[] = PerAcc2.split(",");
		PerAcc2 = CommonFunctions.readExcelMasterData(sr[0], sr[1], sr[2]);

		ActionHandler.wait(3);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.PermanentAccessory);
		ActionHandler.wait(2);

		// ActionHandler.pageScrollDown();
		ActionHandler.clearAndSetText(OUVCalendarContainer.PermAccTxtBx, PerAcc1);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.AccSelection(PerAcc1))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.clearAndSetText(OUVCalendarContainer.PermAccTxtBx, PerAcc2);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.AccSelection(PerAcc2))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	@Then("^User saves an OUV Booking including other location and Booking Quick Reference \"([^\"]*)\",ReasonOfBooking \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",internalJLRCnt \"([^\"]*)\",internalJLREmail \"([^\"]*)\",driver \"([^\"]*)\", Other Location \"([^\"]*)\", Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void user_saves_an_OUV_Booking_including_other_location_and_Booking_Quick_Reference_ReasonOfBooking_Booking_Justification_Booking_Request_Type_internalJLRCnt_internalJLREmail_driver_Other_Location_Passout_Type_Journey_Type(
			String bookingQuickRef, String reasonOfBooking, String bookingJustification, String bookingReqType,
			String internalJLRCnt, String internalJLREmail, String driver1, String otherLocation, String passoutType,
			String journeyType) throws Throwable {
		Reporter.addStepLog("Start OUV Booking");

		String Re[] = reasonOfBooking.split(",");
		reasonOfBooking = CommonFunctions.readExcelMasterData(Re[0], Re[1], Re[2]);

		String JLR[] = internalJLRCnt.split(",");
		internalJLRCnt = CommonFunctions.readExcelMasterData(JLR[0], JLR[1], JLR[2]);

		String email[] = internalJLREmail.split(",");
		internalJLREmail = CommonFunctions.readExcelMasterData(email[0], email[1], email[2]);

		String driv[] = driver1.split(",");
		driver1 = CommonFunctions.readExcelMasterData(driv[0], driv[1], driv[2]);

		String OthLo[] = otherLocation.split(",");
		otherLocation = CommonFunctions.readExcelMasterData(OthLo[0], OthLo[1], OthLo[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

	}

	// navigates to calendar

	@When("^User Navigate to calender tab$")
	public void user_Navigate_to_calender_tab() throws Throwable {
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.click(OUVCalendarContainer.CalenderTab);
		ActionHandler.wait(3);
		Reporter.addStepLog("User Navigate to calender tab");
		CommonFunctions.attachScreenshot();

	}

	// User set local system date and time for the reservation
	@Then("^User set local system date and time for the reservation to calendar \"([^\"]*)\" Booking \"([^\"]*)\"$")
	public void User_set_local_system_date_and_time_for_the_reservation_to_calendar(String EndTime,
			String bookingQuickRef) throws Exception {

		String f1[] = EndTime.split(",");
		EndTime = CommonFunctions.readExcelMasterData(f1[0], f1[1], f1[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.wait(5);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.startTime);
		ActionHandler.wait(3);

		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 2);
		c.add(Calendar.MINUTE, 3);
		dt = c.getTime();

		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stime = new SimpleDateFormat("HH:mm");
		String newDate = sdt.format(dt);
		String newTime = stime.format(dt);

		System.out.println("New Date is = " + newDate);

		// User set the End time and date for booking reservation
		ActionHandler.clearAndSetText(OUVCalendarContainer.Enddate, newDate);
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.EndTime, EndTime);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the End time and date for booking reservation");

		// User set the start time as local system time
		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(OUVCalendarContainer.starttime, newTime);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the start time as local system time");

		// Reservation has been saved
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

	}

	@And("^User navigates to OUV Frequent Driver tab$")
	public void User_Navigates_OUV_Frequent_driver() throws Exception {
		String value = "OUV Frequent Drivers";

		ActionHandler.wait(4);
		ActionHandler.click(OUVCalendarContainer.menuBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.menuSearchBar, value);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.selectMenu(value))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User navigates to OUV Frequent Drivers");
	}

	@Then("^Verify that User can view All OUV Frequent drivers$")
	public void Verify_User_can_view_all_OUV_Frequent_driver() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.All);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User can view all OUV Frequent Drivers");

	}

	@And("^User opens any Frequent Driver from the list$")
	public void User_opens_frequent_driver_from_list() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.firstFreqDriver);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User can open any frequent driver from the list");
	}

	@Then("^Verify that User is able to view OUV Frequent Driver of \"([^\"]*)\" Market Team$")
	public void Verify_User_able_view_OUV_Frequent_driver_Benelux_Market(String market) throws Exception {
		String m[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		ActionHandler.click(OUVCalendarContainer.driverOwner);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Driver Owner's information");

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.userDetailBtn);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User view User details");

		ActionHandler.wait(4);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.userRole);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User verified User role of Driver Owner");
	}

	@And("^User searches \"([^\"]*)\" Frequent Driver from the list$")
	public void User_searches_Frequent_Driver(String driver) throws Exception {
		String d[] = driver.split(",");
		driver = CommonFunctions.readExcelMasterData(d[0], d[1], d[2]);

		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.searchBarDriver, driver);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searches frequent driver from the list");
	}

	@Then("^Verify that No result found for searched Frequent Driver$")
	public void Verify_that_No_Result_Found_Searched_Driver() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.driverNotFound);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("No Result found for searched Frequent Driver");
	}

	@And("^Verify that User can view OUV Frequent drivers Owner$")
	public void Verify_User_can_view_Frequent_drivers_owner() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.freqDriOwner);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User can view Frequent Driver Owner");
	}

	@Then("^User verifies the Owner Name from the list$")
	public void User_Verifies_Owner_Name_list() throws Exception {
		ActionHandler.wait(2);
		driverName = OUVCalendarContainer.freqDriOwnerFirstName.getText();
		System.out.println("Driver's First name is = " + driverName);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Driver's First name is = " + driverName);
	}

	@And("^User Navigates to Reports$")
	public void User_navigates_to_Reports() throws Exception {
		String text = "Reports";

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.moreBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		javascriptUtil.clickElementByJS(driver.findElement(By.xpath(OUVCalendarContainer.Booking(text))));
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Reports");
	}

	@Then("^User opens OUV Benelux Reports folder$")
	public void User_Opens_OUV_Benelux_Reports() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.allFolders);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to All folders");

		ActionHandler.wait(2);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.OUVBeneluxReport);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens OUV Benelux Reports folder");
	}

	@And("^User opens Benelux Users Report$")
	public void User_opens_Benelux_Users_Report() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.UATTestFolder);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens UAT Test Folder");

		ActionHandler.wait(2);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.beneluxUserReport);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens Benelux User Report");
	}

	@Then("^Verify Owner Name from Benelux Users Report$")
	public void Verify_owner_name_from_benelux_user_report() throws Exception {
		ActionHandler.wait(2);
		System.out.println("Driver Owner name is = " + driverName);
		ActionHandler.wait(3);

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.EmailValidation(driverName))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User verifies the owner name from Benelux User Report");
	}

	// User Navigate to OUV Vehicle
	@Then("^User Navigate to OUV Vehicle$")
	public void user_Navigate_to_OUV_Vehicle() throws Throwable {

		// User navigates to OUV vehicles
		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.OUVvehicle);
		ActionHandler.wait(10);
		ActionHandler.waitForElement(OUVCalendarContainer.searchVehicleB, 60);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV vehicles");

	}

	@And("^User search the Vehicle \"([^\"]*)\"$")
	public void User_search_the_Vehicle(String Search) throws Throwable {

		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// Search for an vehicle
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Vehicles");

		ActionHandler.click(OUVCalendarContainer.searchVehicleB);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVCalendarContainer.searchVehicleB, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an vehicle");
		CommonFunctions.attachScreenshot();

	}

	// User selects vehicle
	@When("^User selects vehicle$")
	public void user_selects_vehicle() throws Throwable {
		// Select vehicle
		ActionHandler.click(OUVCalendarContainer.VehicleSel2);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select vehicle");
		CommonFunctions.attachScreenshot();
	}

	// User checkbox the Registration Required
	@Then("^User checkbox the Registration Required$")
	public void user_checkbox_the_Registration_Required() throws Throwable {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		for (int i = 0; i < 8; i++) {
			ActionHandler.scrollDown();
		}

		// User click on Edit Registration Required Pencil icon"
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.EditRegistrationRequired);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Edit Registration Required Pencil icon");

		// User Check box the Registration Required
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.RegReqCheckBox);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Check box the Registration Required");

		// User click on save button
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.SaveEdits);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on save button");

	}

	@Then("^Verify the Registration Approval Status is changed to Pending Approval$")
	public void verify_the_Registration_Approval_Status_is_changed_to_Pending_Approval() throws Throwable {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		for (int i = 0; i < 10; i++) {
			ActionHandler.scrollDown();
		}

		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.PendingApproval);
		ActionHandler.wait(3);
		Reporter.addStepLog("Registration Approval Status is changed to Pending Approval");
		CommonFunctions.attachScreenshot();

	}

	@Then("^Verify the Registration Approval Status is changed to Approved$")
	public void verify_the_Registration_Approval_Status_is_changed_to_Approved() throws Throwable {
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		for (int i = 0; i < 5; i++) {
			ActionHandler.scrollDown();
		}

		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Approved);
		ActionHandler.wait(3);
		Reporter.addStepLog("Registration Approval Status is changed to Approved");
		CommonFunctions.attachScreenshot();

	}

	// Verify that two Reservations are created on the same vehicle
	@Then("^Verify that two Reservations are created on the same vehicle$")
	public void verify_that_two_Reservations_are_created_on_the_same_vehicle() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.FirstReservation);
		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.FirstReservation);
		ActionHandler.wait(3);
		Reporter.addStepLog("two Reservations are created on the same vehicle");
		CommonFunctions.attachScreenshot();

	}

	// Verify that status is Confirmed for both the Reservations
	@Then("^Verify that status  is Confirmed for both the Reservations$")
	public void verify_that_status_is_Confirmed_for_both_the_Reservations() throws Throwable {

		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.ConfirmedStatus1);
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.ConfirmedStatus2);
		ActionHandler.wait(3);
		Reporter.addStepLog("status  is Confirmed for both the Reservations");
		CommonFunctions.attachScreenshot();

	}

	// Verify that two Reservations are created in the Calendar for the booking
	@Then("^Verify that two Reservations are created in the Calendar for the booking \"([^\"]*)\"$")
	public void verify_that_two_Reservations_are_created_in_the_Calendar_for_the_booking(String bookingQuickRef)
			throws Throwable {

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.TodayCalendar);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Reservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("two Reservations are created in the Calendar for the booking");

	}

	// Navigate to first reservation
	@Then("^Navigate to first reservation$")
	public void navigate_to_first_reservation() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User select the vehicle
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.FirstReservation);
		ActionHandler.wait(5);
		Reporter.addStepLog("Navigate to First reservation");
		CommonFunctions.attachScreenshot();

	}

	// verify the start time of booking and end time of transportation
	@Then("^verify the start time of booking and end time of transportation$")
	public void verify_the_start_time_of_booking_and_end_time_of_transportation() throws Throwable {

		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.startDate);
		ActionHandler.wait(3);
		Reporter.addStepLog(
				" start time (start time of the booking) and end time (end time of the Transportation time) displayed");
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.startDate);
		Reporter.addStepLog(
				"the start time(end time of the Transportation time) and end time (end time of the booking) displayed");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		driver.navigate().back();
		ActionHandler.wait(5);

	}

	// Navigate to Second reservation
	@Then("^Navigate to Second reservation$")
	public void navigate_to_Second_reservation() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User select the vehicle
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.SecondReservation);
		ActionHandler.wait(5);
		Reporter.addStepLog("Navigate to Second reservation");
		CommonFunctions.attachScreenshot();

	}

	// Search vehicles after Check boxing the Reserved Vehicles
	@Then("^Search vehicles after Check boxing the Reserved Vehicles$")
	public void search_vehicles_after_Check_boxing_the_Reserved_Vehicles() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.ResevationCheckbx);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");

		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");
	}

	// ^User save the Reservation
	@Then("^User save the Reservation$")
	public void user_save_the_Reservation() throws Throwable {

		// Reservation has been saved
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// Select the first Reservation
	@Then("^Select the first Reservation$")
	public void select_the_first_Reservation() throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.FirstReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first reservation");
	}

	// Select the second Reservation
	@Then("^Select the second Reservation$")
	public void select_the_second_Reservation() throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.SecondReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens second reservation");
	}

	// User navigate to Temporary Accessories
	@Then("^User navigate to Temporary Accessories$")
	public void user_navigate_to_Temporary_Accessories() throws Throwable {
		ActionHandler.wait(4);
		driver.navigate().refresh();
		ActionHandler.wait(7);

		ActionHandler.click(OUVCalendarContainer.TempAccessories);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to temporary accessories");
		ActionHandler.wait(5);

	}

	// User navigate to Service Reservations
	@Then("^User navigate to Service Reservations$")
	public void user_navigate_to_Service_Reservations() throws Throwable {
		ActionHandler.wait(4);
		driver.navigate().refresh();
		ActionHandler.wait(7);

		ActionHandler.click(OUVCalendarContainer.ServiceReservations);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Service reservations");
		ActionHandler.wait(5);

	}

	// ^Navigate back to Booking
	@Then("^Navigate back to Booking$")
	public void navigate_back_to_Booking() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(7);
		ActionHandler.click(OUVCalendarContainer.BookingId);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates back to booking ID");
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(7);

	}

	// Verify no accessory is created
	@Then("^Verify no accessory is created$")
	public void verify_no_accessory_is_created() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.Noaccessories)) {
			Reporter.addStepLog("No accessories is listed");
		} else {
			Reporter.addStepLog("Accessories is listed");
		}
		System.out.println("no accessory id is " + OUVCalendarContainer.VLAstatus);

		ActionHandler.click(OUVCalendarContainer.BacktoReservations);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

	}

	// User navigate to Annual Market Summary
	@Given("^User navigate to Annual Market Summary$")
	public void user_navigate_to_Annual_Market_Summary() throws Throwable {
		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.AnnualMarketSummary);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Annual Market Summary");
	}

	// User selects an AMS with Fiscal Year
	@Then("^User selects an AMS with Fiscal Year \"([^\"]*)\"$")
	public void user_selects_an_AMS_with_Fiscal_Year(String Year) throws Throwable {
		String fyear[] = Year.split(",");
		Year = CommonFunctions.readExcelMasterData(fyear[0], fyear[1], fyear[2]);

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed AMS");

		ActionHandler.setText(OUVCalendarContainer.SearchAMS, Year);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		Reporter.addStepLog("User searches for an AMS");

		ActionHandler.click(OUVCalendarContainer.OUVSel1);
		ActionHandler.wait(3);
		Reporter.addStepLog("Select an AMS");
		CommonFunctions.attachScreenshot();

	}

	@Then("^Navigate to OUV Forecast Snapshot$")
	public void navigate_to_OUV_Forecast_Snapshot() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(7);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.Relatedlistquicklinks);
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.OUVForecastSnapshots);
		ActionHandler.wait(3);
		Reporter.addStepLog("Navigate to OUV Forecast Snapshot");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Navigate to OUV Forecast Snapshots$")
	public void navigate_to_OUV_Forecast_Snapshots() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(7);
		ActionHandler.pageScrollDown();
		// javascriptUtil.scrollIntoView(OUVCalendarContainer.Relatedlistquicklinks);
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.OUVForecastSnapshot);
		ActionHandler.wait(3);
		Reporter.addStepLog("Navigate to OUV Forecast Snapshot");
		CommonFunctions.attachScreenshot();
	}

	@Then("^User selects an OUV Forecast Snapshot with status \"([^\"]*)\"$")
	public void user_selects_an_OUV_Forecast_Snapshot_with_status(String status) throws Throwable {
		String Status[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(Status[0], Status[1], Status[2]);

		driver.navigate().refresh();
		ActionHandler.wait(7);

		ActionHandler.click(OUVCalendarContainer.Showquickfilters);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.status(status))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects the status");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.ApplyVLA);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.CloseFilter);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.OUVSel1);
		ActionHandler.wait(6);
		Reporter.addStepLog("Select the AMS");
		CommonFunctions.attachScreenshot();

	}

	// Verify the accessory created
	@Then("^Verify the accessories created$")
	public void verify_the_accessories_created() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.verifyAccessories)) {
			Reporter.addStepLog("permanent Accessory was created");
		} else {
			Reporter.addStepLog("permanent Accessory was not created");
		}
		System.out.println("accessory id is " + OUVCalendarContainer.VLAstatus);
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.BacktoReservations);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
	}

	// User update the status and saves the record
	@Then("^User update the status to \"([^\"]*)\" and saves the record$")
	public void user_update_the_status_to_and_saves_the_record(String status) throws Throwable {
		String Status[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(Status[0], Status[1], Status[2]);

		ActionHandler.click(OUVCalendarContainer.Editstatusbutton);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on edit status button");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.Statusdropdown);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on status dropdown");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.statusupdate(status))));
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects the status");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(3);
		Reporter.addStepLog("User saves the record");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(10);
	}

	// Verify that user cannot edit the status again
	@Then("^Verify that user cannot edit the status again$")
	public void verify_that_user_cannot_edit_the_status_again() throws Throwable {
		if (!VerifyHandler.verifyElementPresent(OUVCalendarContainer.Editstatusbutton)) {
			Reporter.addStepLog("user cannot edit the status again");
		} else {
			Reporter.addStepLog("user can edit the status again");
		}
	}

	// Click on OUV app navigation item
	@Then("^User clicks on OUV App navigation items$")
	public void User_clicks_on_OUV_App_navigation_items() throws Exception {
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.PersonalizeNav);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on OUV App navigation items");
	}

	// search for OUV frequent drivers
	@Then("^User clicks on Add more items and searches for \"([^\"]*)\" under All section$")
	public void User_clicks_on_Add_more_items_and_searches_for_under_All_section(String Item) throws Exception {
		ActionHandler.click(OUVCalendarContainer.AddItems);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.AllItems);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVCalendarContainer.SearchItems, Item);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searches for OUV Frequent drivers");
	}

	// Verify OUV Frequent Drivers is not available in OUV App navigation items
	@Then("^Verify the OUV Frequent Drivers is not available in OUV App navigation items$")
	public void Verify_the_OUV_Frequent_Drivers_is_not_available_in_OUV_App_navigation_items() throws Exception {
		ActionHandler.click(OUVCalendarContainer.CancelSearch);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.CancelAddItems);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("OUV Frequent Drivers is not available in OUV App navigation items");
	}

	// navigate to annual market summary
	@And("^User navigates to Annual Market Summary$")
	public void User_Navigates_to_Annual_Market_Summary() throws Exception {
		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.AnnualMarket);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Annual Market Summary");
	}

	// User opens list of all Annual market summary
	@And("^User selects All from list view and verify AMS for markets Netherands and Belgium$")
	public void User_selects_All_from_list_view_and_verify_AMS_for_markets_Netherands_and_Belgium() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.All);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User opens list of all Annual market summary and verify AMS for markets Netherands and Belgium only");
	}

	// select benelux from list view
	@And("^User selects Benelux from list view$")
	public void User_selects_Benelux_from_list_view() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.Benelux);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens list of Benelux Annual market summary");
	}

	// select AMS for Benelux for fiscal year 2020-2021
	@And("^Select an AMS for fiscal year \"([^\"]*)\"$")
	public void Select_an_AMS_for_fiscal_year(String Year) throws Exception {
		ActionHandler.click(OUVCalendarContainer.AnnualMarketSearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.AnnualMarketSearch, Year);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.FirstAnnualMarket);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(10);
		Reporter.addStepLog("select AMS for Benelux for fiscal year 2020-2021");
		CommonFunctions.attachScreenshot();
	}

	// edit annual budget threshold field
	@And("^Edit the field for Annual budget threshold and save$")
	public void Edit_the_field_for_Annual_budget_threshold_and_save() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.ThresholdButton);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		String NewThreshold = "10";
		ActionHandler.clearAndSetText(OUVCalendarContainer.ThresholdInput, NewThreshold);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.ThresholdButton);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		String OldThreshold = "50";
		ActionHandler.clearAndSetText(OUVCalendarContainer.ThresholdInput, OldThreshold);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens list of Benelux Annual market summary");
	}

	// user cannot edit the Annual Budget Threshold field
	@And("^Verify user cannot edit the Annual Budget Threshold field$")
	public void Verify_user_cannot_edit_the_Annual_Budget_Threshold_field() throws Exception {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.ThresholdElement);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("user cannot edit the Annual Budget Threshold field");
	}

	// Annual Budget Threshold field is not present under Details section
	@And("^Verify Annual Budget Threshold field is not present under Details section$")
	public void Verify_Annual_Budget_Threshold_field_is_not_present_under_Details_section() throws Exception {
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.DetailsElement);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Annual Budget Threshold field is not present under Details section");
	}

	// User searches Vehicle by selecting Market and Fleet
	@Then("^User searches Vehicle by selecting Market \"([^\"]*)\" and Fleet \"([^\"]*)\" and Brand \"([^\"]*)\"$")
	public void User_Searches_vehicle_market_fleet_brand(String market, String fleet, String brand) throws Exception {
		String m[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String f[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(f[0], f[1], f[2]);

		String b[] = brand.split(",");
		brand = CommonFunctions.readExcelMasterData(b[0], b[1], b[2]);

		ActionHandler.wait(5);
		Select marDD = new Select(OUVCalendarContainer.marketDD);
		marDD.selectByVisibleText(market);
		ActionHandler.wait(3);
		Reporter.addStepLog("Market selection has been done");
		CommonFunctions.attachScreenshot();

		Select fleetDD = new Select(OUVCalendarContainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		// selects the brand

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.Brand);
		ActionHandler.wait(3);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);

	}

	@And("^select buyback checkbox$")
	public void select_buyback_checkbox() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.buyback);
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");

		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
	}

	// User set local system date and time for the reservation
	@Then("^User set local system date and time for the reservation to calendars \"([^\"]*)\" Booking \"([^\"]*)\"$")
	public void User_set_local_system_date_and_time_for_the_reservation_to_calendars(String EndTime,
			String bookingQuickRef) throws Exception {

		String f1[] = EndTime.split(",");
		EndTime = CommonFunctions.readExcelMasterData(f1[0], f1[1], f1[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.wait(5);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.startTime);
		ActionHandler.wait(3);

		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 2);
		c.add(Calendar.MINUTE, 3);
		dt = c.getTime();

		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stime = new SimpleDateFormat("HH:mm");
		String newDate = sdt.format(dt);
		String newTime = stime.format(dt);

		Date dy = new Date();
		Calendar c1 = Calendar.getInstance();
		c1.setTime(dy);
		c1.add(Calendar.DATE, 1);
		dy = c1.getTime();

		System.out.println(dy);

		SimpleDateFormat sdy = new SimpleDateFormat("dd-MMM-yyyy");

		String startdate = sdy.format(dy);

		System.out.println("New Date is = " + newDate);

		// User set the End time and date for booking reservation
		ActionHandler.clearAndSetText(OUVCalendarContainer.Enddate, newDate);
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.EndTime, EndTime);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the End time and date for booking reservation");

		ActionHandler.clearAndSetText(OUVCalendarContainer.startDates, startdate);
		// User set the start time as local system time
		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(OUVCalendarContainer.starttime, newTime);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the start time as local system time");

		// Reservation has been saved
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

	}

	// User set local system date and time for the reservation
	@Then("^User set local system date and time for the reservation to calendarss \"([^\"]*)\" Booking \"([^\"]*)\"$")
	public void User_set_local_system_date_and_time_for_the_reservation_to_calendarss(String EndTime,
			String bookingQuickRef) throws Exception {

		String f1[] = EndTime.split(",");
		EndTime = CommonFunctions.readExcelMasterData(f1[0], f1[1], f1[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.wait(5);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.startTime);
		ActionHandler.wait(3);

		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 4);
		c.add(Calendar.MINUTE, 3);
		dt = c.getTime();

		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stime = new SimpleDateFormat("HH:mm");
		String newDate = sdt.format(dt);
		String newTime = stime.format(dt);

		Date dy = new Date();
		Calendar c1 = Calendar.getInstance();
		c1.setTime(dy);
		c1.add(Calendar.DATE, 3);
		dy = c1.getTime();

		System.out.println(dy);

		SimpleDateFormat sdy = new SimpleDateFormat("dd-MMM-yyyy");

		String startdate = sdy.format(dy);

		System.out.println("New Date is = " + newDate);

		// User set the End time and date for booking reservation
		ActionHandler.clearAndSetText(OUVCalendarContainer.Enddate, newDate);
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.EndTime, EndTime);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the End time and date for booking reservation");

		// User set the start time as local system time

		ActionHandler.clearAndSetText(OUVCalendarContainer.startDates, startdate);

		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(OUVCalendarContainer.starttime, newTime);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the start time as local system time");

		// Reservation has been saved
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

	}

	// User set local system date and time for the reservation
	@Then("^User set local system date and time for the reservation to calendarsss \"([^\"]*)\" Booking \"([^\"]*)\"$")
	public void User_set_local_system_date_and_time_for_the_reservation_to_calendarsss(String EndTime,
			String bookingQuickRef) throws Exception {

		String f1[] = EndTime.split(",");
		EndTime = CommonFunctions.readExcelMasterData(f1[0], f1[1], f1[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.wait(5);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.startTime);
		ActionHandler.wait(3);

		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 5);
		c.add(Calendar.MINUTE, 3);
		dt = c.getTime();

		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stime = new SimpleDateFormat("HH:mm");
		String newDate = sdt.format(dt);
		String newTime = stime.format(dt);

		Date dy = new Date();
		Calendar c1 = Calendar.getInstance();
		c1.setTime(dy);
		c1.add(Calendar.DATE, 4);
		dy = c1.getTime();

		System.out.println(dy);

		SimpleDateFormat sdy = new SimpleDateFormat("dd-MMM-yyyy");

		String startdate = sdy.format(dy);

		System.out.println("New Date is = " + newDate);

		// User set the End time and date for booking reservation
		ActionHandler.clearAndSetText(OUVCalendarContainer.Enddate, newDate);
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.EndTime, EndTime);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the End time and date for booking reservation");

		ActionHandler.clearAndSetText(OUVCalendarContainer.startDates, startdate);
		// User set the start time as local system time
		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(OUVCalendarContainer.starttime, newTime);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the start time as local system time");

		// Reservation has been saved
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

	}

	// User set local system date and time for the reservation
	@Then("^User set local system date and time for the reservation to calendarssss \"([^\"]*)\" Booking \"([^\"]*)\"$")
	public void User_set_local_system_date_and_time_for_the_reservation_to_calendarssss(String EndTime,
			String bookingQuickRef) throws Exception {

		String f1[] = EndTime.split(",");
		EndTime = CommonFunctions.readExcelMasterData(f1[0], f1[1], f1[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.wait(5);
		javascriptUtil.scrollIntoView(OUVCalendarContainer.startTime);
		ActionHandler.wait(3);

		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 6);
		c.add(Calendar.MINUTE, 3);
		dt = c.getTime();

		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stime = new SimpleDateFormat("HH:mm");
		String newDate = sdt.format(dt);
		String newTime = stime.format(dt);

		Date dy = new Date();
		Calendar c1 = Calendar.getInstance();
		c1.setTime(dy);
		c1.add(Calendar.DATE, 5);
		dy = c1.getTime();

		System.out.println(dt);

		SimpleDateFormat sdy = new SimpleDateFormat("dd-MMM-yyyy");

		String startdate = sdy.format(dy);

		System.out.println("New Date is = " + newDate);

		// User set the End time and date for booking reservation
		ActionHandler.clearAndSetText(OUVCalendarContainer.Enddate, newDate);
		ActionHandler.wait(3);
		ActionHandler.clearAndSetText(OUVCalendarContainer.EndTime, EndTime);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the End time and date for booking reservation");

		ActionHandler.clearAndSetText(OUVCalendarContainer.startDates, startdate);
		// User set the start time as local system time
		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(OUVCalendarContainer.starttime, newTime);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User set the start time as local system time");

		// Reservation has been saved
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

	}

	// User navigates to OUV Fleet
	@Then("^User navigates to OUV Fleets$")
	public void user_navigates_to_OUV_Fleets() throws Throwable {

		// Click on menu
		ActionHandler.wait(10);
		ActionHandler.click(OUVCalendarContainer.Menuopen);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on menu");

		// Enter OUV fleet

		String OUVFleet = "OUV Fleets";
		ActionHandler.clearAndSetText(OUVCalendarContainer.appsearch, OUVFleet);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter OUV fleet");

		// Click on OUV Fleet
		ActionHandler.click(OUVCalendarContainer.OUVFleet);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on OUV Fleet");

	}

	// User changes the fleet name
	@When("^User changes fleet name to \"([^\"]*)\"$")
	public void user_changes_fleet_name(String name) throws Throwable {

		String nam[] = name.split(",");
		name = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		// Enter OUV fleet name
		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(OUVCalendarContainer.EditFleetName);
		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(OUVCalendarContainer.fleetname, name);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter OUV fleet name");

		// Click on save button
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();

	}

	@Then("^search for the Fleet with \"([^\"]*)\"$")
	public void search_for_the_Fleet_with(String Search) throws Throwable {

		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// click on Recently viewed list
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		// selects All from the filter list
		ActionHandler.click(OUVCalendarContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Fleet");

		// Search for an Fleet
		ActionHandler.click(OUVCalendarContainer.SearchFleet);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVCalendarContainer.SearchFleet, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an Fleet");
		CommonFunctions.attachScreenshot();

	}

	// Select the fleet
	@Then("^Select the fleets$")
	public void select_the_fleets() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.ATOSel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select Fleet");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Navigates to Reports \"([^\"]*)\"$")
	public void navigates_to_Reports(String Reports) throws Throwable {

		String rp[] = Reports.split(",");
		Reports = CommonFunctions.readExcelMasterData(rp[0], rp[1], rp[2]);

		// Click on menu
		ActionHandler.wait(5);
		ActionHandler.click(OUVCalendarContainer.Menuopen);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on menu");

		// Enter OUV fleet
		ActionHandler.clearAndSetText(OUVCalendarContainer.appsearch, Reports);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Reports");

		// Click on OUV Fleet
		ActionHandler.click(OUVCalendarContainer.Reports);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Reports");
	}

	// Select All folders under folders section
	@Then("^Selects All folders under folders section$")
	public void selects_All_folders_under_folders_section() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.AllFolders);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select All folders under folders section");
	}

	@Then("^Select the folders \"([^\"]*)\"$")
	public void select_the_folders(String OUVBeneluxReport) throws Throwable {

		String nam[] = OUVBeneluxReport.split(",");
		OUVBeneluxReport = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(3);
		javascriptUtil
				.clickElementByJS(driver.findElement(By.xpath(OUVCalendarContainer.FolderSelection(OUVBeneluxReport))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Selects Report");
	}

	@Then("^Navigates to the folder \"([^\"]*)\"$")
	public void navigates_to_the_folder(String UATtest) throws Throwable {
		String nam[] = UATtest.split(",");
		UATtest = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollDown();

		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(driver.findElement(By.xpath(OUVCalendarContainer.FolderSelection(UATtest))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Selects Report");

	}

	@Then("^Selects Report for check \"([^\"]*)\"$")
	public void selects_Report_for_check(String ReportCheck) throws Throwable {
		String nam[] = ReportCheck.split(",");
		ReportCheck = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(5);
		javascriptUtil
				.clickElementByJS(driver.findElement(By.xpath(OUVCalendarContainer.FolderSelection(ReportCheck))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Selects Report for check");

	}

	@Then("^Verifys that Fleet name is updated to \"([^\"]*)\"$")
	public void verifys_that_Fleet_name_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(20);
		driver.switchTo().frame(0);
		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.DetailsCheck(arg1))))) {
			String FleetName = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVCalendarContainer.DetailsCheck(arg1))));
			if (FleetName.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Fleet Name is updated to: " + FleetName);
			} else {
				Reporter.addStepLog("Fleet Name is not updated");
			}
		}
	}

	@Then("^Verifys that Fleet Administrator is updated to \"([^\"]*)\"$")
	public void verifys_that_Fleet_Administrator_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.DetailsCheck(arg1))))) {
			String Approver = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVCalendarContainer.DetailsCheck(arg1))));
			if (Approver.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("ll5 Approver details updated to : " + Approver);
			} else {
				Reporter.addStepLog("ll5 Approver details not updated");
			}
		}
	}

	@Then("^Verifys that approver is updated to \"([^\"]*)\"$")
	public void verifys_that_approver_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVCalendarContainer.DetailsCheck(arg1))))) {
			String FleetAdministrator = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVCalendarContainer.DetailsCheck(arg1))));
			if (FleetAdministrator.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Fleet Administrator details updated to : " + FleetAdministrator);
			} else {
				Reporter.addStepLog("Fleet Administrator details not updated");
			}
		}
	}

	@Then("^User changes the llfive approver to \"([^\"]*)\"$")
	public void user_changes_the_llfive_approver_to(String Approver) throws Throwable {
		// Select edit name
		String nam[] = Approver.split(",");
		Approver = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(OUVCalendarContainer.OVRapprover);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.clearselect);
		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.ovrplace, Approver);
		ActionHandler.wait(2);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		Reporter.addStepLog("Enter ll5 approver Manager");
		ActionHandler.wait(3);

		// Click on save button
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();
	}

	// user clicks on edit fleet administrator
	@When("^User edit fleet administrators and enter \"([^\"]*)\"$")
	public void user_edit_fleet_administrators_and_enter(String name) throws Throwable {

		// Select edit name
		String nam[] = name.split(",");
		name = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.click(OUVCalendarContainer.fleetAdminstrator);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.clearsel);
		ActionHandler.wait(3);
		ActionHandler.setText(OUVCalendarContainer.fleetadmin, name);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);

		Reporter.addStepLog("Select edit name");
		CommonFunctions.attachScreenshot();

		// Click on save button
		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();
	}

	// enter frequent driver and type of driver
	@And("^User enters Frequent Driver as \"([^\"]*)\" and Type of Driver as \"([^\"]*)\" and saves VLA$")
	public void User_Enters_Frequent_Driver_as_and_Type_Driver_as(String fDriver, String typeOfDriver)
			throws Exception {

		String fd[] = fDriver.split(",");
		fDriver = CommonFunctions.readExcelMasterData(fd[0], fd[1], fd[2]);

		String tDriver[] = typeOfDriver.split(",");
		typeOfDriver = CommonFunctions.readExcelMasterData(tDriver[0], tDriver[1], tDriver[2]);

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.NewVLA);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.frequentDriverLabel);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User finds Frequent Driver");

		ActionHandler.setText(OUVCalendarContainer.FreqDriver, fDriver);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.EditTypeofDriver);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(typeOfDriver))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Type of Driver");

		ActionHandler.click(OUVCalendarContainer.SaveEdit);
		ActionHandler.wait(9);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User creates a new VLA");

		ActionHandler.click(OUVCalendarContainer.backToBookingVLA);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// validate the requested approval
	@And("^Validate that the Requested Approval will be listed on the Home Page under Items to Approve Tab$")
	public void Validate_that_the_Requested_Approval_will_be_listed_on_the_Home_Page_under_Items_to_Approve_Tab()
			throws Exception {
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Requested Approval is listed on the Home Page under Items to Approve Tab");
	}

	// Verify that the status of OUV Booking based on Booking Justification
	@Then("^Verify that the status of OUV Booking based on Booking Justification \"([^\"]*)\" FrequentDriver \"([^\"]*)\" DriverType \"([^\"]*)\" EmailId \"([^\"]*)\"$")
	public void Verify_that_the_status_of_OUV_Booking_based_on_Booking_Justification(String BookJus, String FreqDr,
			String DriverTyep, String emailId) throws Exception {

		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.bookingJust);
		Reporter.addStepLog("all details were displayed");
	}

	// Verify that Second Level of Approval is Pass Manager for PassoutType
	@Then("^Verify that Second Level of Approval is Pass Manager for PassoutType \"([^\"]*)\"$")
	public void Verify_that_Second_Level_of_Approval_is_Pass_Manager_for_PassoutType(String arg1) throws Throwable {

		ActionHandler.wait(5);
		String ID = OUVCalendarContainer.BookingHeader.getText();
		ActionHandler.wait(5);
		System.out.println("Booking ID is = " + ID);

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.passmanager);
		Reporter.addStepLog("all details were displayed");

	}

	// user verifies booking status as pending approval

	@And("^Verify OUV Booking status is Booked Pending Approval$")
	public void verify_OUV_Booking_status_is_booked_pending_approval() throws Throwable {
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.bookedpending)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Status was updtaed");
		} else {
			Reporter.addStepLog("Status was not updtaed");
		}
	}

	// user verifies booking status

	@Then("^Verify that status of OUV Booking as submitted$")
	public void verify_that_status_of_OUV_booking_as_submitted() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.submitted)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Status was updtaed");
		} else {
			Reporter.addStepLog("Status was not updtaed");
		}
	}

	@And("^View all search Vehicle and select an available vehicles$")
	public void View_Add_Search_vehicle_Select_an_Available_vehicles() throws Exception {
		selectVehicleCalendar2();
		ActionHandler.wait(2);
		Reporter.addStepLog("User can select Vehicle for reservation");

	}

	@Then("^View all searched Vehicle and select an available vehicle \"([^\"]*)\"$")
	public void view_all_searched_Vehicle_and_select_an_available_vehicle(String fDriver) throws Throwable {

		String fd[] = fDriver.split(",");
		fDriver = CommonFunctions.readExcelMasterData(fd[0], fd[1], fd[2]);

		ActionHandler.wait(3);
		Reporter.addStepLog("Verify that user moves to OUV Calendar");

		ActionHandler.click(OUVCalendarContainer.dayBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.TodayCalendar);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.rightButton);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(fDriver)));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects vehicle from day tab");

		ActionHandler.wait(2);
		Reporter.addStepLog("User can select Vehicle for reservation");
	}

	// Select date which is greater than Reservation end date
	@Then("^Select date which is greater than Reservation end date for OUV booking End Date$")
	public void select_date_which_is_greater_than_Reservation_end_date_for_OUV_booking_End_Date() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.PrepEndDate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.Today);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// User selects booking from Notifications Menu.
	@Then("^Select Booking from the Notifications Menu$")
	public void select_Booking_from_the_Notifications_Menu() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.Notification);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Notification Menu");
		ActionHandler.wait(5);

		ActionHandler.click(OUVCalendarContainer.notificationApprove);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Booking ID");
	}

	// User saves an OUV Booking including other location
	@And("^User saves OUV Booking including other location with Booking Quick Reference \"([^\"]*)\",Booking Justification \"([^\"]*)\",Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\"$")
	public void User_Saves_OUV_Booking_including_other_location1(String bookingQuickRef, String bookingJustification,
			String bookingReqType, String passoutType, String journeyType) throws Exception {
		Reporter.addStepLog("Start OUV Booking");
		String reasonOfBooking = "Single vehicle Booking with Services";
		String internalJLRCnt = "test";
		String internalJLREmail = "test@c.c";
		String driver1 = "DHL";
		String otherLocation = "Other Location";

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

	}

	// Enter three Services from the list of Service
	@Then("^Enter three Services from the list of Services Serviceone \"([^\"]*)\" Servicetwo \"([^\"]*)\" Servicethree \"([^\"]*)\" Booking Quick Reference \"([^\"]*)\"$")
	public void enter_three_Services_from_the_list_of_Services_Serviceone_Servicetwo_Servicethree_Booking_Quick_Reference(
			String Service1, String Service2, String Service3, String bookingQuickRef) throws Throwable {

		String service[] = Service1.split(",");
		Service1 = CommonFunctions.readExcelMasterData(service[0], service[1], service[2]);

		String service1[] = Service2.split(",");
		Service2 = CommonFunctions.readExcelMasterData(service1[0], service1[1], service1[2]);

		String service2[] = Service3.split(",");
		Service3 = CommonFunctions.readExcelMasterData(service2[0], service2[1], service2[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.scrollToView(OUVCalendarContainer.services);
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.services);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.services(Service1))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.services);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.services(Service2))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.services);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.services(Service3))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		ActionHandler.wait(10);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter three Services from the list of Services");
	}

	// Click on the OUV Booking hyperlink
	@Then("^Click on the OUV Booking hyperlink$")
	public void Click_on_the_OUV_Booking_hyperlink() throws Throwable {

		ActionHandler.scrollToView(OUVCalendarContainer.Bookingid);
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.Bookingid);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on the OUV Booking hyperlink");
	}

	// Navigate to Reservations under Related list quick links
	@Then("^Navigate to Reservations under Related list quick links$")
	public void Navigate_to_Reservations_under_Related_list_quick_links() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.selectreservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.reservationnum);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Navigate to Reservations under Related list quick links");
	}

	// Navigate to Service Reservations under Related list quick links
	@Then("^Navigate to Service Reservations under Related list quick links$")
	public void navigate_to_Service_Reservations_under_Related_list_quick_links() throws Throwable {

		ActionHandler.click(OUVCalendarContainer.Servicereservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Navigate to Service Reservations under Related list quick links");
	}

	// Verify the three Services created and listed for a reservation
	@Then("^Verify the three Services created and listed for a reservation$")
	public void verify_the_three_Services_created_and_listed_for_a_reservation() throws Throwable {

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.services1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.services2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.services3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		Reporter.addStepLog("Verify the three Services created and listed for a reservation");
	}

	// Enter three Services from the list of Services
	@Then("^Enter two Permanent Accessories from the list of Permanent Accessories Accessoriesone \"([^\"]*)\" Accessoriestwo \"([^\"]*)\" Accessoriesthree \"([^\"]*)\" Booking Quick Reference \"([^\"]*)\"$")
	public void Enter_two_Permanent_Accessories_from_the_list_of_Permanent_Accessories_Accessoriesone_Accessoriestwo_Accessoriesthree_Booking_Quick_Reference(
			String Service1, String Service2, String Service3, String bookingQuickRef) throws Throwable {

		String service[] = Service1.split(",");
		Service1 = CommonFunctions.readExcelMasterData(service[0], service[1], service[2]);

		String service1[] = Service2.split(",");
		Service2 = CommonFunctions.readExcelMasterData(service1[0], service1[1], service1[2]);

		String service2[] = Service3.split(",");
		Service3 = CommonFunctions.readExcelMasterData(service2[0], service2[1], service2[2]);

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.scrollToView(OUVCalendarContainer.services);
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.Accessories);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.services(Service1))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.Accessories);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.services(Service2))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.Accessories);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.services(Service3))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		ActionHandler.wait(10);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter three Services from the list of Services");

	}

	// click on save booking option
	@Then("^click on save booking option$")
	public void click_on_save_booking_option() throws Throwable {

		ActionHandler.scrollToView(OUVCalendarContainer.Details);
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// enter the Temporary Accessories
	@Then("^enter the Temporary Accessories as \"([^\"]*)\"  in OUV Booking$")
	public void enter_the_Temporary_Accessories_as_in_OUV_Booking(String Accessories) throws Throwable {

		String pt[] = Accessories.split(",");
		Accessories = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		ActionHandler.scrollToView(OUVCalendarContainer.TempAccessories);
		ActionHandler.wait(3);

		ActionHandler.click(OUVCalendarContainer.Accessory);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Accessory to be entered");

		ActionHandler.clearAndSetText(OUVCalendarContainer.Accessory, Accessories);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter in the Accessories information");

		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.textdriverclick(Accessories))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Accessory is selected");
	}

	// Verify that Transportation reservation is highlighted in purple color
	@When("^Verify that Transportation reservation is highlighted in purple color$")
	public void verify_that_Transportation_reservation_is_highlighted_in_purple_color() throws Throwable {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.firstReservation)) {
			String bgcolor = driver.findElement(By.xpath("(//div[@class='month_default_event'])[1]"))
					.getCssValue("background-color");
			ActionHandler.wait(2);
			String[] hexValue = bgcolor.replace("rgba(", "").replace(")", "").split(",");

			System.out.println(hexValue);
			int hexValue1 = Integer.parseInt(hexValue[0]);
			hexValue[1] = hexValue[1].trim();
			int hexValue2 = Integer.parseInt(hexValue[1]);
			hexValue[2] = hexValue[2].trim();
			int hexValue3 = Integer.parseInt(hexValue[2]);

			String actualColor = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
			System.out.println(actualColor);
			VerifyHandler.verifyElementPresent(OUVCalendarContainer.firstReservationcolour);

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User should be able to validate the purple color");
		}
	}

	// the second reservation is highlighted in green color
	@When("^the second reservation is highlighted in green color$")
	public void the_second_reservation_is_highlighted_in_green_color() throws Throwable {

		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.SecondReservation)) {
			String bgcolor = driver.findElement(By.xpath("(//div[@class='month_default_event'])[2]"))
					.getCssValue("background-color");
			ActionHandler.wait(2);
			String[] hexValue = bgcolor.replace("rgba(", "").replace(")", "").split(",");

			System.out.println(hexValue);
			int hexValue1 = Integer.parseInt(hexValue[0]);
			hexValue[1] = hexValue[1].trim();
			int hexValue2 = Integer.parseInt(hexValue[1]);
			hexValue[2] = hexValue[2].trim();
			int hexValue3 = Integer.parseInt(hexValue[2]);

			String actualColor = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
			System.out.println(actualColor);
			VerifyHandler.verifyElementPresent(OUVCalendarContainer.SecondReservationcolour);

			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User should be able to validate the green color");
		}
	}

	// User saves an OUV Booking including other location with Booking Quick
	// Reference
	@Then("^User saves an OUV Booking including other location with Booking Quick Reference \"([^\"]*)\" reason Of Booking \"([^\"]*)\" Booking Justification \"([^\"]*)\" Booking Request Type \"([^\"]*)\",Passout Type \"([^\"]*)\",Journey Type \"([^\"]*)\" Internal JLR Contact \"([^\"]*)\" email \"([^\"]*)\" Location \"([^\"]*)\" and Driver \"([^\"]*)\"$")
	public void user_saves_an_OUV_Booking_including_other_location_with_Booking_Quick_Reference_reason_Of_Booking_Booking_Justification_Booking_Request_Type_Passout_Type_Journey_Type_Internal_JLR_Contact_email_Location_and_Driver(
			String bookingQuickRef, String reasonOfBooking, String bookingJustification, String bookingReqType,
			String passoutType, String journeyType, String internalJLRCnt, String internalJLREmail,
			String otherLocation, String driver1) throws Throwable {

		Reporter.addStepLog("Start OUV Booking");

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		String rqref[] = reasonOfBooking.split(",");
		reasonOfBooking = CommonFunctions.readExcelMasterData(rqref[0], rqref[1], rqref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingReqType.split(",");
		bookingReqType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		String jtc[] = internalJLRCnt.split(",");
		internalJLRCnt = CommonFunctions.readExcelMasterData(jtc[0], jtc[1], jtc[2]);

		String itc[] = internalJLREmail.split(",");
		internalJLREmail = CommonFunctions.readExcelMasterData(itc[0], itc[1], itc[2]);

		String loc[] = otherLocation.split(",");
		otherLocation = CommonFunctions.readExcelMasterData(loc[0], loc[1], loc[2]);

		String dri[] = driver1.split(",");
		driver1 = CommonFunctions.readExcelMasterData(dri[0], dri[1], dri[2]);

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		ActionHandler.setText(OUVCalendarContainer.bookingQuickRef, bookingQuickRef);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.reasonBooking, reasonOfBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.click(OUVCalendarContainer.bookingJustification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingJustification))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.bookingRefType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(bookingReqType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollToView(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.passoutType);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(passoutType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.journeyType);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(journeyType))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLR, internalJLRCnt);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.otherLocation, otherLocation);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.driver1, driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("All Required details have been entered");

		ActionHandler.scrollToView(OUVCalendarContainer.Details);
		ActionHandler.wait(3);
	}

	// verify that user is able to add two new Temporary Accessories
	@Then("^verify that user is able to add two new Temporary Accessories$")
	public void verify_that_user_is_able_to_add_two_new_Temporary_Accessories() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.AccessoryReservation)) {
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to add new Temporary Accessories");
		} else {

			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to add new Temporary Accessories");
		}

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.AccessoryReservation2)) {
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is able to add 2nd new Temporary Accessories");
		} else {

			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User is not able to add 2nd new Temporary Accessories");
		}
	}

	// User clicks on Booking Quick Reference
	@Then("^User clicks on Booking Quick Reference \"([^\"]*)\" and navigates to OUV Booking ID$")
	public void user_clicks_on_Booking_Quick_Reference_and_navigates_to_OUV_Booking_ID(String bookingQuickRef)
			throws Throwable {

		String bqref[] = bookingQuickRef.split(",");
		bookingQuickRef = CommonFunctions.readExcelMasterData(bqref[0], bqref[1], bqref[2]);

		ActionHandler.wait(20);
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.bookingTable(bookingQuickRef))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(20);
		ActionHandler.scrollToView(OUVCalendarContainer.Details);
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.Passout);
		ActionHandler.wait(3);
		ActionHandler.waitForElement(OUVCalendarContainer.BookingID, 60);
		ActionHandler.click(OUVCalendarContainer.BookingID);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to the booking ID");
	}

}
