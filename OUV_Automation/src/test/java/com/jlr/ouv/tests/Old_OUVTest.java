package com.jlr.ouv.tests;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.base.TestBaseCC;
import com.jlr.ouv.constants.Constants;
import com.jlr.ouv.containers.OUVTestContainer;
import com.jlr.ouv.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Old_OUVTest extends TestBaseCC {
	private WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(Old_OUVTest.class.getName());
	OUVTestContainer OUVtestcontainer = PageFactory.initElements(driver, OUVTestContainer.class);
	JavaScriptUtil javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);

	public void onStart() {
		setupTest("OUVTest");
		driver = getDriver();
		OUVtestcontainer = PageFactory.initElements(driver, OUVTestContainer.class);
		javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	}

	// Access OUV Portal
	@Given("^Access OUV Portal$")
	public void Access_OUV_Portal() throws Exception {
		LOGGER.debug("Test Begin......");
		Reporter.addStepLog("User Access OUV Portal");
		onStart();
		driver.get(Constants.OUVURL);
		ActionHandler.wait(7);
		Reporter.addStepLog("User Logins to OUV Portal");

		String UserName = Config.getPropertyValue("OUV_UserName");
		String Password = Config.getPropertyValue("OUV_Password");

		ActionHandler.setText(OUVtestcontainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVtestcontainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVtestcontainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(120);
	}

	@Then("^Navigate to OUV Calendar$")
	public void Navigate_OUV_Calendar() throws Exception {
		ActionHandler.wait(2);
		Reporter.addStepLog("User Navigate to OUV Calendar");
		Actions calendar = new Actions(driver);
		calendar.moveToElement(OUVtestcontainer.OUVCalendar);
		ActionHandler.wait(1);
		System.out.println("Element found");

		javascriptUtil.clickElementByJS(OUVtestcontainer.OUVCalendar);
		System.out.println("OUV Calendar clicked");
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
	}

	public void Select_Market() throws Exception {
		String market = "Austria";
		Select marDD = new Select(OUVtestcontainer.marketDD);
		marDD.selectByVisibleText(market);
		ActionHandler.wait(1);
		Reporter.addStepLog("Market selection has been done");
		CommonFunctions.attachScreenshot();
	}

	@And("^Search vehicle for fleet$")
	public void Search_vehicle_Fleet() throws Exception {
		Select_Market();
		ActionHandler.wait(1);
		String Fleet = "Austria - buybacks";
		ActionHandler.wait(1);
		Select fleetDD = new Select(OUVtestcontainer.fleetDD);
		fleetDD.selectByVisibleText(Fleet);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		ActionHandler.wait(1);
		ActionHandler.click(OUVtestcontainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Validate No Record Found error");
		VerifyHandler.verifyElementPresent(OUVtestcontainer.errorReservation);

	}

	@And("^Search Vehicle by giving Fleet \"([^\"]*)\"$")
	public void Search_Vehicle_giving_Fleet(String fleet) throws Exception {
		String fleetValue[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fleetValue[0], fleetValue[1], fleetValue[2]);
		System.out.println("Fetched Fleet value is = " + fleet);

		javascriptUtil.clickElementByJS(OUVtestcontainer.OUVCalendar);
		ActionHandler.wait(5);
		Select_Market();
		ActionHandler.wait(1);
		Select fleetDD = new Select(OUVtestcontainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		ActionHandler.click(OUVtestcontainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");
	}

	@And("^The entry shows in Reservation Calendar in Days,Weeks and Months$")
	public void The_Entry_Shows_Reservation_Calendar() throws Exception {
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verify that the reservarion calendar has Days, Weeks and Month");

		VerifyHandler.verifyElementPresent(OUVtestcontainer.VINCheck);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVtestcontainer.dayBtn);
		VerifyHandler.verifyElementPresent(OUVtestcontainer.weekBtn);
		VerifyHandler.verifyElementPresent(OUVtestcontainer.monthBtn);
		System.out.println("Days, Week and Month button is available");

		ActionHandler.click(OUVtestcontainer.dayBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVtestcontainer.weekBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVtestcontainer.monthBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
	}

	@When("^Select vehicle and week for Reservation$")
	public void Select_Vehicle_And_week_for_reservation() throws Exception {
		ActionHandler.click(OUVtestcontainer.weekBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		/*
		 * Actions action = new Actions(driver);
		 * action.dragAndDrop(OUVtestcontainer.SourceWeek,
		 * OUVtestcontainer.DestinationWeek).build().perform(); ActionHandler.wait(3);
		 */

		ActionHandler.click(OUVtestcontainer.calendarSelection);
		ActionHandler.wait(1);
		System.out.println("Week has been selected");
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle and Week selection is done");
	}

	@Then("^Make Reservation with required details like Start Date \"([^\"]*)\" End Date \"([^\"]*)\" Booking Quick Reference \"([^\"]*)\", Booking Justification \"([^\"]*)\", Booking Request Type \"([^\"]*)\", Passout Type \"([^\"]*)\", Journey Type \"([^\"]*)\", Location \"([^\"]*)\"$")
	public void Make_Reservation(String startDate, String endDate, String bookingQuickReference,
			String bookingJustification, String bookingRequestType, String passoutType, String journeyType,
			String location) throws Exception {
		Reporter.addStepLog("Start making reservation");

		String sd[] = startDate.split(",");
		startDate = CommonFunctions.readExcelMasterData(sd[0], sd[1], sd[2]);

		String ed[] = endDate.split(",");
		endDate = CommonFunctions.readExcelMasterData(ed[0], ed[1], ed[2]);

		String bref[] = bookingQuickReference.split(",");
		bookingQuickReference = CommonFunctions.readExcelMasterData(bref[0], bref[1], bref[2]);

		String bj[] = bookingJustification.split(",");
		bookingJustification = CommonFunctions.readExcelMasterData(bj[0], bj[1], bj[2]);

		String br[] = bookingRequestType.split(",");
		bookingRequestType = CommonFunctions.readExcelMasterData(br[0], br[1], br[2]);

		String pt[] = passoutType.split(",");
		passoutType = CommonFunctions.readExcelMasterData(pt[0], pt[1], pt[2]);

		String jt[] = journeyType.split(",");
		journeyType = CommonFunctions.readExcelMasterData(jt[0], jt[1], jt[2]);

		String loc[] = location.split(",");
		location = CommonFunctions.readExcelMasterData(loc[0], loc[1], loc[2]);

		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(OUVtestcontainer.reservation);
		System.out.println("Reservation value has been selected");
		CommonFunctions.attachScreenshot();

		ActionHandler.clearAndSetText(OUVtestcontainer.startDate, startDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		ActionHandler.click(OUVtestcontainer.endDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVtestcontainer.endDate, endDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		ActionHandler.click(OUVtestcontainer.startTime);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVtestcontainer.startTime, "1:00");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.wait(30);
	}

	@And("^Save the Reservation for \"([^\"]*)\"$")
	public void Save_Reservation(String bookingRef) throws Exception {
		String bref[] = bookingRef.split(",");
		bookingRef = CommonFunctions.readExcelMasterData(bref[0], bref[1], bref[2]);

		ActionHandler.click(OUVtestcontainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");

		if (VerifyHandler.verifyElementPresent(OUVtestcontainer.errorReservation)) {
			String a = ActionHandler.getElementText(OUVtestcontainer.error);
			if (("Please fill either Location or Other Location.").equals(a)) {
				Reporter.addStepLog("Error Validation Passed");
			} else {
				Reporter.addStepLog("Error Validation Failed");
			}
			ActionHandler.click(OUVtestcontainer.closeError);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.pageScrollDown();
			ActionHandler.wait(1);
			ActionHandler.click(driver.findElement(By.xpath(OUVtestcontainer.bookingTable(bookingRef))));
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(OUVtestcontainer.editRes);
			ActionHandler.wait(2);
			CommonFunctions.attachScreenshot();
		}
	}

	@Then("^Navigate to Repair menu and save the reservation$")
	public void Navigate_Repair_Menu() throws Exception {
		String repairText = "Repair";
		ActionHandler.click(OUVtestcontainer.reservationDD);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVtestcontainer.ValueSelection(repairText))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Save reservation from repair tab");

		ActionHandler.click(OUVtestcontainer.saveReservation);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Repair has been saved");
	}

	@And("^Validate the entry in the OUV Calendar for \"([^\"]*)\"$")
	public void validate_Entry_in_Calendar(String bookingRef) throws Exception {
		String bref[] = bookingRef.split(",");
		bookingRef = CommonFunctions.readExcelMasterData(bref[0], bref[1], bref[2]);

		ActionHandler.click(driver.findElement(By.xpath(OUVtestcontainer.bookingTable(bookingRef))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Details of booked calendar has been opened");
	}

	@Then("^Open Booking details using OUV Booking link$")
	public void Open_Booking_Details() throws Exception {
		ActionHandler.wait(1);
		javascriptUtil.clickElementByJS(OUVtestcontainer.OUVBookingLink);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("OUV Booking details displayed");

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(OUVtestcontainer.reservationTab);
		CommonFunctions.attachScreenshot();

		Actions reservation = new Actions(driver);
		reservation.moveToElement(OUVtestcontainer.reservationTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

	}

	// Below code is used to verify the displayed booking end date error message
	@Then("^verify the displayed booking end date error message$")
	public void verify_the_displayed_booking_end_date_error_message() throws Throwable {
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Validate Booking end date error");
		VerifyHandler.verifyElementPresent(OUVtestcontainer.errorEnddate);
	}

	// Below code is used to select tomorrows date
	public String selectTomorrowDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	// Below code is used to Search Vehicle by giving Fleet along with Start Date as
	// tomorrow and End Date as today
	@Then("^Search Vehicle by giving Fleet \"([^\"]*)\" along with Start Date as tomorrow and End Date as today$")
	public void search_Vehicle_by_giving_Fleet_along_with_Start_Date_as_tomorrow_and_End_Date_as_today(String fleet)
			throws Throwable {
		String fleetValue[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fleetValue[0], fleetValue[1], fleetValue[2]);
		System.out.println("Fetched Fleet value is = " + fleet);

		javascriptUtil.clickElementByJS(OUVtestcontainer.OUVCalendar);
		ActionHandler.wait(5);
		Reporter.addStepLog("Click on Calendar");

		Select_Market();
		ActionHandler.wait(1);
		Select fleetDD = new Select(OUVtestcontainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String endDate = sdt.format(date);
		System.out.println("Todays date is = " + date);
		System.out.println("Todays date is = " + endDate);

		String startDate = selectTomorrowDate();

		ActionHandler.click(OUVtestcontainer.BookingStartdate);
		ActionHandler.clearAndSetText(OUVtestcontainer.BookingStartdate, startDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("Select start date");

		ActionHandler.click(OUVtestcontainer.BookingEnddate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVtestcontainer.BookingEnddate, endDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select end date");

		ActionHandler.click(OUVtestcontainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on vehicle search button");
	}

	// Below code is used to select yesterdays date
	public String selectYesterdayDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, -1);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	// Below code is used to Search Vehicle by giving Fleet along with Start Date
	// prior to todays date
	@Then("^Search Vehicle by giving Fleet \"([^\"]*)\" along with Start Date prior to todays date$")
	public void search_Vehicle_by_giving_Fleet_along_with_Start_Date_prior_to_todays_date(String fleet)
			throws Throwable {
		String fleetValue[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fleetValue[0], fleetValue[1], fleetValue[2]);
		System.out.println("Fetched Fleet value is = " + fleet);

		javascriptUtil.clickElementByJS(OUVtestcontainer.OUVCalendar);
		ActionHandler.wait(5);
		Reporter.addStepLog("Click on Calendar");

		Select_Market();
		ActionHandler.wait(1);
		Select fleetDD = new Select(OUVtestcontainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		Date date = new Date();
		String endDate = sdt.format(date);
		System.out.println("Todays date is = " + date);
		System.out.println("Todays date is = " + endDate);

		String startDate = selectYesterdayDate();

		ActionHandler.click(OUVtestcontainer.BookingStartdate);
		ActionHandler.clearAndSetText(OUVtestcontainer.BookingStartdate, startDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("Select start date");

		ActionHandler.click(OUVtestcontainer.BookingEnddate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVtestcontainer.BookingEnddate, endDate);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select end date");

		ActionHandler.click(OUVtestcontainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");
	}

	// Below code is used to verify the displayed booking start date error message
	@Then("^verify the displayed booking start date error message$")
	public void verify_the_displayed_booking_start_date_error_message() throws Throwable {
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Validate Booking start date error");
		VerifyHandler.verifyElementPresent(OUVtestcontainer.errorStartdate);
	}

	// Below code is used to Search Vehicle by giving Market and Fleet
	@Then("^Search Vehicle by giving Market as \"([^\"]*)\" and Fleet as \"([^\"]*)\"$")
	public void search_Vehicle_by_giving_Market_as_and_Fleet_as(String market, String fleet) throws Throwable {
		String fleetValue[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fleetValue[0], fleetValue[1], fleetValue[2]);
		System.out.println("Fetched Fleet value is = " + fleet);

		String MarketValue[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(MarketValue[0], MarketValue[1], MarketValue[2]);
		System.out.println("Fetched Fleet value is = " + market);

		javascriptUtil.clickElementByJS(OUVtestcontainer.OUVCalendar);
		ActionHandler.wait(5);

		Select marDD = new Select(OUVtestcontainer.marketDD);
		marDD.selectByVisibleText(market);
		ActionHandler.wait(1);
		Reporter.addStepLog("Market selection has been done");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		Select fleetDD = new Select(OUVtestcontainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		ActionHandler.click(OUVtestcontainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");
	}

	// Below code is used to verify the displayed record error message
	@Then("^verify the displayed record error message$")
	public void verify_the_displayed_record_error_message() throws Throwable {
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Validate No Record Found error");
		VerifyHandler.verifyElementPresent(OUVtestcontainer.errorReservation);
	}

	// Below code is used to Search vehicle for Reservation from OUV Calendar with
	// Fleet while excluding Market information
	@Then("^Search Vehicle by giving Fleet as \"([^\"]*)\" while excluding Market information$")
	public void search_Vehicle_by_giving_Fleet_as_while_excluding_Market_information(String fleet) throws Throwable {

		String fleetValue[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fleetValue[0], fleetValue[1], fleetValue[2]);
		System.out.println("Fetched Fleet value is = " + fleet);

		javascriptUtil.clickElementByJS(OUVtestcontainer.OUVCalendar);
		ActionHandler.wait(5);

		ActionHandler.wait(1);
		Select fleetDD = new Select(OUVtestcontainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

		ActionHandler.click(OUVtestcontainer.searchVehicleBtn);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Vehicle searched successfully");
	}

	// Entering Incorrect VIN
	@Given("^Enter Incorrect VIN \"([^\"]*)\" for searching vehicle in OUV Calendar$")
	public void enter_Incorrect_VIN_for_searching_vehicle_in_OUV_Calendar(String VIN) throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(OUVtestcontainer.VIN, VIN);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Incorrect VIN is entered");
	}

	// Searching a Vehicle using Incorrect Data
	@Then("^Search a Vehicle by Incorrect Data$")
	public void search_a_Vehicle_by_Incorrect_Data() throws Throwable {
		ActionHandler.wait(1);
		ActionHandler.click(OUVtestcontainer.searchVehicleBtn);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// Validating Error Message
		Reporter.addStepLog("Validate No Record Found error");
		VerifyHandler.verifyElementPresent(OUVtestcontainer.error);
	}

	// Entering Different Primary Location
	@Then("^Enter Incorrect Primary Location as \"([^\"]*)\" for searching vehicle in OUV Calendar$")
	public void enter_Incorrect_Primary_Location_as_for_searching_vehicle_in_OUV_Calendar(String Location)
			throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(OUVtestcontainer.Location, Location);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Incorrect Primary Location is entered");

	}

	// Entering Incorrect Model Description
	@Then("^Enter Incorrect Model Description as \"([^\"]*)\" for searching vehicle in OUV Calendar$")
	public void enter_Incorrect_Model_Description_as_for_searching_vehicle_in_OUV_Calendar(String Model)
			throws Throwable {
		Select_Market();
		ActionHandler.wait(1);
		Select fleetMD = new Select(OUVtestcontainer.FleetMD);
		fleetMD.selectByVisibleText(Model);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Model Description selection is done");

	}

	// Selecting all checkboxes for Select to Display
	@Then("^Selecting All Vehicle Categories to display$")
	public void selecting_All_Vehicle_Categories_to_display() throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.click(OUVtestcontainer.AvailableforTransfer);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Available for Transfer Checkbox is selected");
		ActionHandler.wait(2);
		ActionHandler.click(OUVtestcontainer.PreSold);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Pre-Sold Checkbox is selected");
		ActionHandler.wait(2);
		ActionHandler.click(OUVtestcontainer.FleetVehicles);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Job Related Fleet Vehicles Checkbox is selected");
		ActionHandler.wait(2);
		ActionHandler.click(OUVtestcontainer.BuybackVehicles);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Buyback Vehicles Checkbox is selected");
		ActionHandler.wait(2);
		ActionHandler.click(OUVtestcontainer.ReservedVehicles);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reserved Vehicles Checkbox is selected");
		ActionHandler.wait(3);

	}

	// Searching a Vehicle by Selecting Market and Fleet
	@And("^Search a Vehicle by giving Fleet \"([^\"]*)\"$")
	public void Search_a_Vehicle_giving_Fleet(String fleet) throws Exception {

		String fleetValue[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fleetValue[0], fleetValue[1], fleetValue[2]);
		System.out.println("Fetched Fleet value is = " + fleet);

		javascriptUtil.clickElementByJS(OUVtestcontainer.OUVCalendar);
		ActionHandler.wait(3);
		Select_Market();
		ActionHandler.wait(1);
		Select fleetDD = new Select(OUVtestcontainer.fleetDD);
		fleetDD.selectByVisibleText(fleet);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Fleet selection is done");

	}

	// Used to verify field error message and close the reservation window
	@Then("^verify the displayed Complete this field error message$")
	public void verify_the_displayed_Complete_this_field_error_message() throws Throwable {
		ActionHandler.wait(1);
		ActionHandler.click(OUVtestcontainer.incomplecterror);
		VerifyHandler.verifyElementPresent(OUVtestcontainer.incomplecterror);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("verify the error message");

		ActionHandler.wait(1);
		ActionHandler.click(OUVtestcontainer.cancel);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("close reservation window");

	}

	// Below code is used to make Reservation with invalid email format
	@Then("^Make Reservation with invalid email format$")
	public void make_Reservation_with_invalid_email_format() throws Throwable {
		Reporter.addStepLog("Start making reservation");
		String internalJLREmail = "msoni3jaguarlandrover.com";

		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(OUVtestcontainer.reservation);
		System.out.println("Reservation value has been selected");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVtestcontainer.endTime);
		CommonFunctions.attachScreenshot();
		ActionHandler.clearAndSetText(OUVtestcontainer.endTime, "3:00");
		ActionHandler.wait(1);
		OUVtestcontainer.endTime.sendKeys(Keys.TAB);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		System.out.println("Start Time and End Time has been modified");

		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();

		ActionHandler.setText(OUVtestcontainer.internalJLREmail, internalJLREmail);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVtestcontainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// Below code is used to verify the displayed format error message
	@Then("^verify the displayed format error message$")
	public void verify_the_displayed_format_error_message() throws Throwable {
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(OUVtestcontainer.formaterror);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("verify the error message");

		ActionHandler.wait(1);
		ActionHandler.click(OUVtestcontainer.cancel);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("close reservation window");
	}

	// Below code is used to clear the entered driver information
	@Then("^clear the entered driver information$")
	public void clear_the_entered_driver_information() throws Throwable {
		ActionHandler.clearText(OUVtestcontainer.driver1);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("driver information is cleared");

		ActionHandler.click(OUVtestcontainer.saveReservation);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// Below code is used to verify the displayed driver error
	@Then("^verify the displayed driver error$")
	public void verify_the_displayed_driver_error() throws Throwable {
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Validate enter driver information error");
		VerifyHandler.verifyElementPresent(OUVtestcontainer.drivererror);

		ActionHandler.click(OUVtestcontainer.closeError);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("close error message window");

		ActionHandler.wait(1);
		ActionHandler.click(OUVtestcontainer.cancel);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("close reservation window");

	}

	// For Entering Future Model Year Description
	@Then("^Enter Future Model Description as \"([^\"]*)\" for searching vehicle in OUV Calendar$")
	public void enter_Future_Model_Description_as_for_searching_vehicle_in_OUV_Calendar(String MY) throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(OUVtestcontainer.ModelDescription, MY);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Incorrect Model Year Description is entered");

	}

	// Used to save the reservation
	@When("^Save the Reservation$")
	public void save_the_Reservation() throws Throwable {
		ActionHandler.wait(1);
		VerifyHandler.verifyElementPresent(OUVtestcontainer.reservation);
		System.out.println("Reservation value has been selected");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVtestcontainer.bookingQuickRef);
		ActionHandler.wait(3);
		ActionHandler.pageDown();
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVtestcontainer.saveReservation);
		ActionHandler.wait(7);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Reservation has been saved");
	}

	// Navigating to Home Page
	@Then("^Navigate to Home Page$")
	public void navigate_to_Home_Page() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(OUVtestcontainer.homeTab);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks the Home Button");
	}

	// Entering Incorrect Derivative Pack Description
	@Then("^Enter Incorrect Derivative Pack Description as \"([^\"]*)\" for searching vehicle in OUV Calendar$")
	public void enter_Incorrect_Derivative_Pack_Description_as_for_searching_vehicle_in_OUV_Calendar(String Pack)
			throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(OUVtestcontainer.DerivativePackDescription, Pack);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Incorrect Derivative Pack Description is entered");
	}

	// Entering Incorrect Paint Description
	@Then("^Enter Incorrect Paint Description as \"([^\"]*)\" for searching vehicle in OUV Calendar$")
	public void enter_Incorrect_Paint_Description_as_for_searching_vehicle_in_OUV_Calendar(String Paint)
			throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(OUVtestcontainer.PaintDescription, Paint);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Incorrect Paint Description is entered");
	}

	// Entering Incorrect Trim Description
	@Then("^Enter Incorrect Trim Description as \"([^\"]*)\" for searching vehicle in OUV Calendar$")
	public void enter_Incorrect_Trim_Description_as_for_searching_vehicle_in_OUV_Calendar(String Trim)
			throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(OUVtestcontainer.TrimDescription, Trim);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Incorrect Trim Description is entered");
	}

	// Entering Incorrect Transmission Description
	@Then("^Enter Incorrect Transmission Description as \"([^\"]*)\" for searching vehicle in OUV Calendar$")
	public void enter_Incorrect_Transmission_Description_as_for_searching_vehicle_in_OUV_Calendar(String Transmission)
			throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(OUVtestcontainer.TrimDescription, Transmission);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Incorrect Transmission Description is entered");
	}

	// Entering Incorrect Option
	@Then("^Enter Incorrect Option as \"([^\"]*)\" for searching vehicle in OUV Calendar$")
	public void enter_Incorrect_Option_as_for_searching_vehicle_in_OUV_Calendar(String Option) throws Throwable {
		ActionHandler.wait(2);
		ActionHandler.clearAndSetText(OUVtestcontainer.TrimDescription, Option);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Incorrect Option is entered");

	}
}
