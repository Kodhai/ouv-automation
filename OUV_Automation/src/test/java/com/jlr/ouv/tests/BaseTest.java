package com.jlr.ouv.tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.jlr.base.TestBaseCC;
import com.jlr.listeners.Listener;

public class BaseTest extends TestBaseCC {
	
	static Listener listener  = new Listener();
	
	@BeforeClass
	public static void start() {
		listener.onStart();
		
	}

	@AfterClass
	public static void end() {
		listener.onFinish();
		
	}
	

}
