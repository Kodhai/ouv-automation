package com.jlr.ouv.tests;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.ouv.constants.Constants;
import com.jlr.ouv.containers.ATORequestContainer;
import com.jlr.ouv.containers.OUVCalendarContainer;
import com.jlr.ouv.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ATORequestTest extends TestBaseCC {
	public WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(OUVCalendarTest.class.getName());
	public OUVCalendarContainer OUVCalendarContainer = PageFactory.initElements(driver, OUVCalendarContainer.class);
	public ATORequestContainer ATORequestContainer = PageFactory.initElements(driver, ATORequestContainer.class);
	JavaScriptUtil javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	String ID = "Booking";

	public void onStart() {
		setupTest("OUVTest");
		driver = getDriver();
		OUVCalendarContainer = PageFactory.initElements(driver, OUVCalendarContainer.class);
		ATORequestContainer = PageFactory.initElements(driver, ATORequestContainer.class);
		javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	}

	// User tries to navigate to OUV Portal
	public void navigateToOUV() throws Exception {
		String OUV = "OUV";
		ActionHandler.wait(3);
		Reporter.addStepLog("User tries to navigate to OUV Portal");
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.jlrText)) {
			ActionHandler.wait(2);
			ActionHandler.click(OUVCalendarContainer.menuBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			// Serach for OUV
			ActionHandler.setText(OUVCalendarContainer.searchBox, OUV);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			// Select OUV from list
			ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.selectMenu(OUV))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

	// User navigates to AllATORequest ATO Requests
	public void ListViewATO() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.recentlyViewedList);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.AllATORequest);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User navigates to AllATORequest ATO Requests");
	}

	// Access an OUV Portal with user and password
	@Given("^Access an OUV Portal with user \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void access_the_OUV_Portal_with_user_and_password_as(String UserName, String Password) throws Throwable {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Test Begin......");
		Reporter.addStepLog("User Access OUV Portal");
		onStart();
		driver.get(Constants.OUVURL);
		ActionHandler.wait(7);
		Reporter.addStepLog("User Logins to OUV Portal");

		// set user name
		ActionHandler.setText(OUVCalendarContainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// Set password
		ActionHandler.setText(OUVCalendarContainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// click on login button
		ActionHandler.click(OUVCalendarContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(20);

		// User have entered Verification Code
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.Verifycode)) {
			ActionHandler.wait(60);

			ActionHandler.click(OUVCalendarContainer.vCodeTextBox);
			ActionHandler.wait(5);
			Reporter.addStepLog("User have entered Verification Code");
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(OUVCalendarContainer.verifyBtn);
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			navigateToOUV();

		}

		ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);

		if (!VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome)) {
			ActionHandler.wait(4);
			driver.navigate().refresh();
			ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);
		}
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome);
	}

	// user edits the ATO request type
	@Then("^user edits the ATO request type$")
	public void user_edits_the_ATO_request_type() throws Throwable {

		ActionHandler.click(ATORequestContainer.EditATORequestType);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Edit ATO Request Type");
		CommonFunctions.attachScreenshot();

		ActionHandler.pageDown();
		ActionHandler.wait(2);
	}

	// User clicks on the save button
	@Then("^User clicks on the save button$")
	public void user_clicks_on_the_save_button() throws Throwable {

		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Save button");
		CommonFunctions.attachScreenshot();

	}

	@Then("^fill all mandatory fields like OUVReportinglevelone \"([^\"]*)\",OUVReportingleveltwo \"([^\"]*)\",OUVReportinglevelthree \"([^\"]*)\",Market \"([^\"]*)\",Fleetdate \"([^\"]*)\",Handindate \"([^\"]*)\",DisposalRoute \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_like_OUVReportinglevelone_OUVReportingleveltwo_OUVReportinglevelthree_Market_Fleetdate_Handindate(
			String OUVReportinglevelone, String OUVReportingleveltwo, String OUVReportinglevelthree, String Market,
			String Fleetdate, String HandinDate, String DisposalRoute) throws Throwable {

		String Test = "TestDemo";
		String percentage = "70";
		ActionHandler.setText(ATORequestContainer.QuickReference, Test);
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test);

		String OUVl1[] = OUVReportinglevelone.split(",");
		OUVReportinglevelone = CommonFunctions.readExcelMasterData(OUVl1[0], OUVl1[1], OUVl1[2]);

		String OUVl2[] = OUVReportingleveltwo.split(",");
		OUVReportingleveltwo = CommonFunctions.readExcelMasterData(OUVl2[0], OUVl2[1], OUVl2[2]);

		String OUVl3[] = OUVReportinglevelthree.split(",");
		OUVReportinglevelthree = CommonFunctions.readExcelMasterData(OUVl3[0], OUVl3[1], OUVl3[2]);

		String market[] = Market.split(",");
		Market = CommonFunctions.readExcelMasterData(market[0], market[1], market[2]);

		String dr[] = DisposalRoute.split(",");
		DisposalRoute = CommonFunctions.readExcelMasterData(dr[0], dr[1], dr[2]);

		String fleetdate[] = Fleetdate.split(",");
		Fleetdate = CommonFunctions.readExcelMasterData(fleetdate[0], fleetdate[1], fleetdate[2]);

		String Handin[] = HandinDate.split(",");
		HandinDate = CommonFunctions.readExcelMasterData(Handin[0], Handin[1], Handin[2]);

		// level1
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.dependencies);
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.OUVREportinglevel1);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelone))));
		ActionHandler.wait(5);
		// level2

		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportingleveltwo))));
		ActionHandler.wait(5);

		// level3
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelthree))));
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.apply);
		ActionHandler.wait(4);
		// fleet

		ActionHandler.wait(3);
		String PR = "PR NL";
		ActionHandler.setText(ATORequestContainer.fleet, PR);
		ActionHandler.wait(4);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);

		// market
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Market))));
		ActionHandler.wait(3);
		// jlr contact

		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, Test);

		// proposed fleet date
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.ProposedAddtoFleetDate, Fleetdate);
		ActionHandler.wait(2);

		// hand in date

		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);

		// percentage
		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.percentage, percentage);
		ActionHandler.wait(2);
		// disposal route

		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(DisposalRoute))));
		ActionHandler.wait(3);

		// Rework route
		ActionHandler.click(ATORequestContainer.Rework);
		ActionHandler.wait(4);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();

		ActionHandler.wait(3);

		ActionHandler.click(ATORequestContainer.Save);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("ATO Request created");

	}

	@Then("^User clicks on View File details button from down arrow column dropdown \"([^\"]*)\"$")
	public void user_clicks_on_View_File_details_button_from_down_arrow_column_dropdown(String ViewDetails)
			throws Throwable {

		String drop[] = ViewDetails.split(",");
		ViewDetails = CommonFunctions.readExcelMasterData(drop[0], drop[1], drop[2]);

		driver.navigate().refresh();
		ActionHandler.wait(5);

		ActionHandler.click(ATORequestContainer.dropdown);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.dropdown(ViewDetails))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to clicks on View File details button from down arrow column dropdown");

	}

	// verify the displayed privileges error message
	@Then("^verify the displayed privileges error message$")
	public void verify_the_displayed_privileges_error_message() throws Throwable {

		if (VerifyHandler.verifyElementPresent(ATORequestContainer.requestlineerror)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("verify that user is unable to perform changes");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("verify that user is able to perform changes");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.click(ATORequestContainer.Cancel);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on cancel button");
		CommonFunctions.attachScreenshot();

	}

	// Navigate back to ATO request
	@Then("^Navigate back to ATO request$")
	public void navigate_back_to_ATO_request() throws Throwable {
		ActionHandler.click(ATORequestContainer.ATOback);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on ATO to navigate back");
		CommonFunctions.attachScreenshot();
	}

	// User navigate to ATO Request
	@And("^User navigate to ATO Request$")
	public void user_navigate_to_ATO_Request() throws Throwable {
		ActionHandler.wait(7);
		javascriptUtil.clickElementByJS(ATORequestContainer.ATORequests);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User naviagte to ATO Request");
	}

	// User opens an ATO Request which is in Approved status
	@Then("^User opens an ATO Request which is in Approved status$")
	public void user_opens_an_ATO_Request_which_is_in_Approved_status() throws Throwable {
		ActionHandler.wait(4);
		String approved = "approved";

		// User search and select ATO Request
		ActionHandler.setText(ATORequestContainer.searchtextbox, approved);
		ActionHandler.wait(5);
		ActionHandler.pressEnter();
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.firstATO);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User search and select ATO Request");

	}

	// User navigate to ATO Request History

	@Then("^User navigate to ATO Request History$")
	public void user_navigate_to_ATO_Request_History() throws Throwable {
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.Requesthistory);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to ATO Request History");

	}

	// Validate that user is able to view ATO Request History
	@Then("^Validate that user is able to view ATO Request History$")
	public void validate_that_user_is_able_to_view_ATO_Request_History() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.ATOtitle)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("ATO Request History are visible");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("ATO Request History not found");
		}

	}

	// User click on New button
	@Then("^Click on New button$")
	public void click_on_New_button() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.NewATO);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on New button");

	}

	// fill all mandatory fields like QuickReference, ATORequestType,
	// OUVReportinglevelone etc..
	@Then("^fill all mandatory fields like QuickReference \"([^\"]*)\" ATORequestType \"([^\"]*)\" OUVReportinglevelone \"([^\"]*)\" OUVReportingleveltwo \"([^\"]*)\" OUVReportinglevelthree \"([^\"]*)\" Fleet \"([^\"]*)\" Market \"([^\"]*)\" InternalJLRContact \"([^\"]*)\" Fleetdate \"([^\"]*)\" Handindate \"([^\"]*)\" DisposalRoute \"([^\"]*)\" ReworkRoute \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_like_QuickReference_ATORequestType_OUVReportinglevelone_OUVReportingleveltwo_OUVReportinglevelthree_Fleet_Market_InternalJLRContact_Fleetdate_Handindate_DisposalRoute_ReworkRoute(
			String QuickReference, String ATORequestType, String OUVReportinglevelone, String OUVReportingleveltwo,
			String OUVReportinglevelthree, String fleet, String Market, String InternalJlrContact, String Fleetdate,
			String HandinDate, String DisposalRoute, String ReworkRoute) throws Throwable {

		String Quick[] = QuickReference.split(",");
		QuickReference = CommonFunctions.readExcelMasterData(Quick[0], Quick[1], Quick[2]);

		String ATOreqType[] = ATORequestType.split(",");
		ATORequestType = CommonFunctions.readExcelMasterData(ATOreqType[0], ATOreqType[1], ATOreqType[2]);

		String Test = "TestDemo";
		String percentage = "70";

		// User enters Quick reference
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.QuickReference, QuickReference);
		CommonFunctions.attachScreenshot();

		// User selects ATO Request type
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(ATORequestType))));
		ActionHandler.wait(3);

		CommonFunctions.attachScreenshot();
		ActionHandler.setText(ATORequestContainer.QuickReference, Test);

		// Enter Booking Justification
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test);
		CommonFunctions.attachScreenshot();

		// Read data from master excel sheet
		String OUVl1[] = OUVReportinglevelone.split(",");
		OUVReportinglevelone = CommonFunctions.readExcelMasterData(OUVl1[0], OUVl1[1], OUVl1[2]);

		String OUVl2[] = OUVReportingleveltwo.split(",");
		OUVReportingleveltwo = CommonFunctions.readExcelMasterData(OUVl2[0], OUVl2[1], OUVl2[2]);

		String OUVl3[] = OUVReportinglevelthree.split(",");
		OUVReportinglevelthree = CommonFunctions.readExcelMasterData(OUVl3[0], OUVl3[1], OUVl3[2]);

		String fle[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fle[0], fle[1], fle[2]);

		String market[] = Market.split(",");
		Market = CommonFunctions.readExcelMasterData(market[0], market[1], market[2]);

		String internaljlrc[] = InternalJlrContact.split(",");
		InternalJlrContact = CommonFunctions.readExcelMasterData(internaljlrc[0], internaljlrc[1], internaljlrc[2]);

		String fleetdate[] = Fleetdate.split(",");
		Fleetdate = CommonFunctions.readExcelMasterData(fleetdate[0], fleetdate[1], fleetdate[2]);

		String Handin[] = HandinDate.split(",");
		HandinDate = CommonFunctions.readExcelMasterData(Handin[0], Handin[1], Handin[2]);

		String dr[] = DisposalRoute.split(",");
		DisposalRoute = CommonFunctions.readExcelMasterData(dr[0], dr[1], dr[2]);

		String Rework[] = ReworkRoute.split(",");
		ReworkRoute = CommonFunctions.readExcelMasterData(Rework[0], Rework[1], Rework[2]);

		// click on view all dependencies
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.dependencies);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 1
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVREportinglevel1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelone))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 2

		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportingleveltwo))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 3

		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelthree))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.apply);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// Enter fleet

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.fleet, fleet);
		ActionHandler.wait(4);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(6);

		// Select market

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Market))));
		ActionHandler.wait(5);

		// Enter jlr contact

		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, InternalJlrContact);
		ActionHandler.wait(5);

		// proposed fleet date
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.ProposedAddtoFleetDate, Fleetdate);
		ActionHandler.wait(4);

		// hand in date

		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// percentage

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.percentage, percentage);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		// disposal route

		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(DisposalRoute))));
		ActionHandler.wait(3);

		// Rework route

		ActionHandler.click(ATORequestContainer.alldependecies);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.ReworkRoute);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ReworkRoute(ReworkRoute))));
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.apply);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("ATO Request created");

	}

	// Verify the error message
	@Then("^Verify the error message$")
	public void verify_the_error_message() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.ATOerrormessage)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("error message is visible");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("error message is not visible");
		}
	}

	// Verify that user cannot edit the ATO request
	@Then("^Verify that user cannot edit the ATO request$")
	public void verify_that_user_cannot_edit_the_ATO_request() throws Throwable {
		ActionHandler.wait(4);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(ATORequestContainer.Editpencilicon)) {
			Reporter.addStepLog("User does not see edit status button");
			CommonFunctions.attachScreenshot();
		}
	}

	// search for the ATO Request
	@Then("^search for the ATO Request with type \"([^\"]*)\"$")
	public void search_for_the_ATO_Request_with_type(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);
		ActionHandler.wait(2);

		// User views all the listed OUV Vehicles
		ActionHandler.click(ATORequestContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(ATORequestContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		Reporter.addStepLog("User views all the listed OUV Vehicles");
		ActionHandler.click(ATORequestContainer.ATOsearchbox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);

		// Search for the ATO request
		ActionHandler.clearAndSetText(ATORequestContainer.ATOsearchbox, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for the ATO request");

		CommonFunctions.attachScreenshot();
	}

	// Verify that user cannot edit Approver details for an ATO request
	@Then("^Verify that user cannot edit Approver details for an ATO request$")
	public void verify_that_user_cannot_edit_Approver_details_for_an_ATO_request() throws Throwable {
		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		if (!VerifyHandler.verifyElementPresent(ATORequestContainer.editicon)) {
			Reporter.addStepLog("User does not see edit icon");
			CommonFunctions.attachScreenshot();
		}

	}

	// Click on Edit Internal JLR Contact pencil icon under Fleet details section
	@Then("^Click on Edit Internal JLR Contact pencil icon under Fleet details section$")
	public void click_on_Edit_Internal_JLR_Contact_pencil_icon_under_Fleet_details_section() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);

		// clicks on the edit icon at internal JLR contact
		ActionHandler.click(ATORequestContainer.EditJLRinternalcontact);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
	}

	// Change Internal jlr contact
	@Then("^Change Internal jlr contact to \"([^\"]*)\" and verify$")
	public void change_Internal_jlr_contact_to_and_verify(String JLRcontact) throws Throwable {
		String JLRc[] = JLRcontact.split(",");
		JLRcontact = CommonFunctions.readExcelMasterData(JLRc[0], JLRc[1], JLRc[2]);

		//// have to clear the text and enter the new contact
		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(ATORequestContainer.jlrcontact, JLRcontact);
		ActionHandler.wait(2);
		String text = (ATORequestContainer.jlrcontact).getText();
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		// User edits the internal jlr contact
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User edits the internal jlr contact");

		// verify that internal jlr contact was updated or not

		if (text.equalsIgnoreCase(JLRcontact)) {
			Reporter.addStepLog("Internal JLR contact was updated");
		} else {
			Reporter.addStepLog("Internal JLR contact was not updated");
		}
	}

	// Logout from OUV Portal
	@Then("^Logout from OUV Portal$")
	public void Logout_OUV() throws Exception {
		javascriptUtil.clickElementByJS(ATORequestContainer.homeTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.loginImg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.logout);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Logout from OUV Portal");
		driver.quit();
	}

	// User Opens an existing ATO Request
	@And("^User Opens an existing ATO Request$")
	public void User_Opnes_Existing_ATO_Request() throws Exception {

		ActionHandler.wait(3);

		// Shows all ATO requests

		ListViewATO();
		Reporter.addStepLog("User opens AllATORequest ATO Requests");

		// User opens an exsiting ATO Request
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.firstATORequestAll);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens an exsiting ATO Request");
	}

	// User creates a New Note for an Existing ATO Request
	@Then("^User creates a New Note for an Existing ATO Request$")
	public void User_Creates_New_Note_Existing_ATO_Request() throws Exception {
		String NoteTitle = "Test Note";
		String NoteBody = "Test Note Body";

		// User navigates to Notes Section
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.Notes);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Notes Section");

		// click on new button
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.NewNoteBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// enters title
		ActionHandler.setText(ATORequestContainer.NoteTitle, NoteTitle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(ATORequestContainer.NoteBody, NoteBody);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// click on done button
		ActionHandler.click(ATORequestContainer.NoteDoneBtn);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User can create a new Note");

	}

	// User Opens an existing ATO Request which is in Pending Approval status
	@And("^User Opens an existing ATO Request which is in Pending Approval status$")
	public void User_Opens_Existing_ATO_Request_In_Pending_Status() throws Exception {
		String status = "Pending Approval";
		ActionHandler.wait(2);
		// shows all the ATO requests
		ListViewATO();
		Reporter.addStepLog("User navigates to AllATORequest ATO Requests");

		// Search for the status
		ActionHandler.setText(ATORequestContainer.searchtextbox, status);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.firstATORequestAll);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User opens an ATO Request which is in pending approval status");
	}

	// Navigate to ATO Request Line Item
	@And("^Navigate to ATO Request Line Item$")
	public void Navigate_ATO_Request_Line_Item() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.ATORequestLineItem);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to ATO Request Line Item");
	}

	// Click on any created ATO Request Line Item
	@Then("^Click on any created ATO Request Line Item$")
	public void Click_Created_ATO_Request_Line_Item() throws Exception {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		ActionHandler.click(ATORequestContainer.firstATORequestLineItem);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens created ATO Request Line Item");
	}

	// User tries to Edit ATO Request Line Item and Verify an error occurred
	@And("^User tries to Edit ATO Request Line Item and Verify an error occurred$")
	public void User_Tries_Edit_ATO_Request_Line_Item_Error_Occurred() throws Exception {
		String VistaVehicle = "00000000";

		// click on Edit Vista vehicle
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.EditVistaVehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on clear selection
		ActionHandler.click(ATORequestContainer.ClearSelection);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// set text for Vista vehicle
		ActionHandler.setText(ATORequestContainer.VistaVehicle, VistaVehicle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.showAllResultsVistaVehicle);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ATOvehicle(VistaVehicle))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User changes Vista Vehicle");

		// User tries to save ATO Request Line Item
		ActionHandler.click(ATORequestContainer.saveATORequest);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User tries to save ATO Request Line Item");

		VerifyHandler.verifyElementPresent(ATORequestContainer.errorMessage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User cannot edit ATO Request line item for Pending Approval ATO Request");

		// User cancels editing ATO Request line item
		ActionHandler.click(ATORequestContainer.closeErrorMessage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.cancelEditATORequest);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User cancels editing ATO Request line item");
	}

	// User navigates to Notes section and select created Note
	@Then("^User navigates to Notes section and select created Note$")
	public void User_Navigate_Notes_Select_Created_Note() throws Exception {
		String NoteTitle = "Test Note";
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ATOvehicle(NoteTitle))));
		ActionHandler.wait(2);
		Reporter.addStepLog("User opens created Note");
	}

	// User delete the note from ATO Request
	@And("^User delete the note from ATO Request$")
	public void User_Delete_Note() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.firstNote);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// click on delete button
		ActionHandler.click(ATORequestContainer.deleteNote);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.confirmDeleteNote);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User delets the note from ATO Request");
	}

	// Verify that user cannot view Cancel/Withdraw ATO Request button
	@Then("^Verify that user cannot view Cancel/Withdraw ATO Request button$")
	public void Verify_User_cannot_View_Cancel_Withdraw_ATO_Request() throws Exception {
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// User cannot view Cancel/Withdraw button
		ActionHandler.click(ATORequestContainer.ShowMoreActions);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cannot view Cancel/Withdraw button");
	}

	// User Opens an existing ATO Request which is in New status
	@And("^User Opens an existing ATO Request which is in New status$")
	public void User_Opens_Existing_ATO_Request_New_Status() {
		ActionHandler.wait(2);

	}

	// search for ATO Request with status as mentioned in feature file
	@Then("^search for ATO Request with status \"([^\"]*)\"$")
	public void search_for_ATO_Request_with_status(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// User views all the listed OUV ATO Request
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(ATORequestContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV ATO Request");

		// Search for an ATO Request
		ActionHandler.click(ATORequestContainer.searchATOrequestbox);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(ATORequestContainer.searchATOrequestbox, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an ATO Request");
		CommonFunctions.attachScreenshot();
	}

	// Select ATO Request
	@Then("^select the ATO Request$")
	public void select_the_Request() throws Throwable {
		ActionHandler.click(ATORequestContainer.ATOSel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select ATO Request");
		CommonFunctions.attachScreenshot();
	}

	// User recall ATO Approval Request
	@And("^User recall ATO Approval Request$")
	public void User_Recall_ATO_Approval_Request() throws Exception {

		// User clicks on Recall Button
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.recallBtn);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Recall Button");

		// User clicks on confirm Recall Button
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.ConfirmRecallBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		driver.navigate().back();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// User submits new ATO Request for an Approval
	@And("^User submits new ATO Request for an Approval$")
	public void User_submits_New_ATO_Request_Approval() throws Throwable {

		// User creates new ATO Request Line Item
		ActionHandler.wait(2);
		navigate_to_ATO_Request_Line_Items();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		user_creates_a_new_ATO_Request_Line_Item();
		ActionHandler.wait(3);
		Reporter.addStepLog("User creates new ATO Request Line Item");

		driver.navigate().back();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// User submits an ATO Request for Approval
		ActionHandler.click(ATORequestContainer.submitForApproval);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User submits an ATO Request for Approval");

	}

	// Verify that ATO Request status changes to 'New'
	@And("^Verify that ATO Request status changes to 'New'$")
	public void Verify_ATO_Request_Status_To_New() throws Exception {
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(ATORequestContainer.newStatus);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Verified that ATO Request status changes to New");
	}

	// User change the ATO Request Type
	@And("^User change the ATO Request Type to \"([^\"]*)\"$")
	public void User_Change_ATO_Request_Type(String changeATOReqType) throws Exception {
		String cATOType[] = changeATOReqType.split(",");
		changeATOReqType = CommonFunctions.readExcelMasterData(cATOType[0], cATOType[1], cATOType[2]);

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.Edit);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// User changes ATO Request Type
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(changeATOReqType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User changes ATO Request Type");

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
	}

	// Verify the ATO request is visible or not

	@Then("^verify the ATO Request$")
	public void verify_the_ATO_Request() throws Throwable {

		ActionHandler.wait(7);

		if (VerifyHandler.verifyElementPresent(ATORequestContainer.verifyATO)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("ATO Request visible");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("ATO Request is not visible");
		}
	}

	// user submit an ATO request for approval
	@Then("^user submit an ATO request for approval$")
	public void user_submit_an_ATO_request_for_approval() throws Throwable {
		ActionHandler.click(ATORequestContainer.Submitforapproval);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Submit for approval");
		CommonFunctions.attachScreenshot();
	}

	// Verify that user cannot submit an ATO request
	@Then("^Verify that user cannot submit an ATO request$")
	public void verify_that_user_cannot_submit_an_ATO_request() throws Throwable {
		VerifyHandler.verifyElementPresent(ATORequestContainer.errormessage);
		ActionHandler.wait(5);
		Reporter.addStepLog("User verifies the error message");
		CommonFunctions.attachScreenshot();
	}

	// navigate to the requested fleet
	@When("^navigate to the request fleet \"([^\"]*)\"$")
	public void navigate_to_the_request_fleet(String fleet) throws Throwable {
		String fl[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fl[0], fl[1], fl[2]);

		ActionHandler.pageDown();
		ActionHandler.wait(2);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.value(fleet))));
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on fleet information");
		CommonFunctions.attachScreenshot();
	}

	// Used to verify that user is not a member of fleet
	@When("^verify that user is not a member of fleet \"([^\"]*)\"$")
	public void verify_that_user_is_not_a_member_of_fleet(String member) throws Throwable {
		String fl[] = member.split(",");
		member = CommonFunctions.readExcelMasterData(fl[0], fl[1], fl[2]);

		ActionHandler.click(ATORequestContainer.FleetMembers);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks to view fleet members");
		CommonFunctions.attachScreenshot();

		WebElement owner = driver.findElement(By.xpath(ATORequestContainer.value(member)));

		if (VerifyHandler.verifyElementPresent(owner)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("verify that user is member of fleet");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("verify that user is not member of fleet");
			CommonFunctions.attachScreenshot();
		}
	}

	// verify that ATO request is in pending approval status
	@When("^verify that ATO request is in pending approval status$")
	public void verify_that_ATO_request_is_in_pending_approval_status() throws Throwable {
		ActionHandler.pageScrollDown();

		if (VerifyHandler.verifyElementPresent(ATORequestContainer.PendingApproval)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("verify that status in Pending Approval");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("Status is not in Pending Approval");
			CommonFunctions.attachScreenshot();
		}
	}

	// user clicks on ATO request line
	@When("^user clicks on ATO request line$")
	public void user_clicks_on_ATO_request_line() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.RequestLineItems);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Request Line Items");
		CommonFunctions.attachScreenshot();
	}

	// clicks on new ATO request line with vehicle
	@Then("^clicks on new ATO request line with vehicle \"([^\"]*)\"$")
	public void clicks_on_new_ATO_request_line_with_vehicle(String request) throws Throwable {
		String req[] = request.split(",");
		request = CommonFunctions.readExcelMasterData(req[0], req[1], req[2]);

		// User clicks on new Request Line Items
		ActionHandler.click(ATORequestContainer.RequestLinenew);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on new Request Line Items");
		CommonFunctions.attachScreenshot();

		// User enters vehicle information
		ActionHandler.setText(ATORequestContainer.searchrecords, request);
		ActionHandler.wait(5);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User enters vehicle information");
		CommonFunctions.attachScreenshot();

		// User clicks on save button"
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on save button");
		CommonFunctions.attachScreenshot();
	}

	// user edits the the vehicle of a ATO request line
	@Then("^user edits the the vehicle of a ATO request line as \"([^\"]*)\"$")
	public void user_edits_the_the_vehicle_of_a_ATO_request_line_as(String request) throws Throwable {
		String req[] = request.split(",");
		request = CommonFunctions.readExcelMasterData(req[0], req[1], req[2]);

		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User clicks on Show Actions
		ActionHandler.click(ATORequestContainer.ShowActions2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();

		// User clicks on Edit
		ActionHandler.click(ATORequestContainer.Edit2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Edit");
		CommonFunctions.attachScreenshot();

		// User clears section
		ActionHandler.click(ATORequestContainer.ClearSelection);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clears section");
		CommonFunctions.attachScreenshot();

		// User enters vehicle information
		ActionHandler.setText(ATORequestContainer.searchrecords, request);
		ActionHandler.wait(5);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("User enters vehicle information");
		CommonFunctions.attachScreenshot();

		// User clicks on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on save button");
		CommonFunctions.attachScreenshot();
	}

	// verify the displayed ATO request line error message
	@Then("^verify the displayed ATO request line error message$")
	public void verify_the_displayed_ATO_request_line_error_message() throws Throwable {
		VerifyHandler.verifyElementPresent(ATORequestContainer.requestlineerror);
		ActionHandler.wait(5);
		Reporter.addStepLog("Verify error present");
		CommonFunctions.attachScreenshot();

		// User clicks on cancel button
		ActionHandler.click(ATORequestContainer.Cancel);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on cancel button");
		CommonFunctions.attachScreenshot();
	}

	// search for the ATO Request with status
	@Then("^search for the ATO Request with status \"([^\"]*)\"$")
	public void search_for_the_ATO_Request_with_status(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);

		// User views all the listed OUV Vehicles
		ActionHandler.click(ATORequestContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Vehicles");

		// Search for the ATO request
		ActionHandler.clearAndSetText(ATORequestContainer.searchRequestbox, Search);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for the ATO request");
		CommonFunctions.attachScreenshot();
	}

	// Verify that user is able to edit ATO request line
	@Then("^Verify that user is able to edit ATO request line as \"([^\"]*)\"$")
	public void verify_that_user_is_able_to_edit_ATO_request_line_as(String FullVin) throws Throwable {
		String vin[] = FullVin.split(",");
		FullVin = CommonFunctions.readExcelMasterData(vin[0], vin[1], vin[2]);

		VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(ATORequestContainer.value(FullVin))));
		ActionHandler.wait(2);
		Reporter.addStepLog("User should be able to edit the ATO Request Line Item");
		CommonFunctions.attachScreenshot();
	}

	// Navigate to ATO Request Line Items
	@Then("^Navigate to ATO Request Line Items$")
	public void navigate_to_ATO_Request_Line_Items() throws Throwable {
		ActionHandler.wait(10);
		ActionHandler.click(ATORequestContainer.ATORequestLine);
		ActionHandler.wait(3);
		Reporter.addStepLog("User selects ATO Request Line Items");
		CommonFunctions.attachScreenshot();

	}

	// User creates a new ATO Request Line Item
	@Then("^User creates a new ATO Request Line Item$")
	public void user_creates_a_new_ATO_Request_Line_Item() throws Throwable {
		String Dummy = "00000000";

		// User clicks on New Button
		ActionHandler.click(ATORequestContainer.New);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on New Button");
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(ATORequestContainer.SearchVista, Dummy);
		ActionHandler.wait(2);

		// User searches for Vista Vehicle
		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searches for Vista Vehicle");
		CommonFunctions.attachScreenshot();

		// User clicks on Save Button
		ActionHandler.click(ATORequestContainer.SaveEdit);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Save Button");
		CommonFunctions.attachScreenshot();

	}

	@And("^User creates Multiple ATO Request Line Items$")
	public void User_Creates_Multiple_ATO_Request_Line_Items() throws Throwable {
		ActionHandler.wait(2);
		navigate_to_ATO_Request_Line_Items();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		user_creates_a_new_ATO_Request_Line_Item();
		ActionHandler.wait(3);
		Reporter.addStepLog("User creates new ATO Request Line Item");
		ActionHandler.wait(3);
		user_creates_a_new_ATO_Request_Line_Item();
		Reporter.addStepLog("User creates another ATO Request Line Item");

		driver.navigate().back();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// User edits the ATO Request Line Item
	@Then("^User edits the ATO Request Line Item$")
	public void user_edits_the_ATO_Request_Line_Item() throws Throwable {

		// User selects the newly created ATO Line Item
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.SelectATOLineItems);
		ActionHandler.wait(5);
		Reporter.addStepLog("User selects the newly created ATO Line Item");
		CommonFunctions.attachScreenshot();

		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User clicks on edit button
		ActionHandler.click(ATORequestContainer.Edit);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on edit button");
		CommonFunctions.attachScreenshot();

		// User should be able to edit the ATO Request Line Item
		ActionHandler.wait(2);
		Reporter.addStepLog("User should be able to edit the ATO Request Line Item");
		CommonFunctions.attachScreenshot();

		// User clicks on cancel button
		ActionHandler.click(ATORequestContainer.CancelEdit);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on cancel button");
		CommonFunctions.attachScreenshot();

	}

	// User deletes the ATO Request Line Item
	@Then("^User deletes the ATO Request Line Item$")
	public void user_deletes_the_ATO_Request_Line_Item() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User clicks on Show Actions
		ActionHandler.click(ATORequestContainer.ShowActions);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();

		// User clicks on delete button
		ActionHandler.click(ATORequestContainer.Delete);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on delete button");
		CommonFunctions.attachScreenshot();

		// User deletes the ATO Request Line Items
		ActionHandler.click(ATORequestContainer.DeleteLineItems);
		ActionHandler.wait(2);
		Reporter.addStepLog("User deletes the ATO Request Line Items");
		CommonFunctions.attachScreenshot();

	}

	// user clicks on the change owner button
	@When("^user clicks on the change owner button$")
	public void user_clicks_on_the_change_owner_button() throws Throwable {

		// Select the details tab
		ActionHandler.click(ATORequestContainer.detailTab);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select the details tab");
		CommonFunctions.attachScreenshot();

		// elect the change owner tab
		ActionHandler.click(ATORequestContainer.ChangeOwner);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select the change owner tab");
		CommonFunctions.attachScreenshot();
	}

	// changes the owner
	@When("^changes the owner as \"([^\"]*)\"$")
	public void changes_the_owner_as(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// Enter the owner information
		ActionHandler.clearAndSetText(ATORequestContainer.SearchUsers, Search);
		ActionHandler.wait(5);
		Reporter.addStepLog("Enter the owner information");
		CommonFunctions.attachScreenshot();

		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("select owner");

		ActionHandler.click(ATORequestContainer.confChangeOwner);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select the change owner tab");
		CommonFunctions.attachScreenshot();
	}

	// verify the displayed change owner error message
	@Then("^verify the displayed change owner error message$")
	public void verify_the_displayed_change_owner_error_message() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.recordlock)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("error message is visible");
			CommonFunctions.attachScreenshot();
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("error message is not visible");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.click(ATORequestContainer.cancel);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select cancel button");
		CommonFunctions.attachScreenshot();
	}

	// User edits the ATO Request Type as
	@Then("^User edits the ATO Request Type as \"([^\"]*)\"$")
	public void user_edits_the_ATO_Request_Type_as(String Request) throws Throwable {
		ActionHandler.pageDown();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.pageArrowUp();
		ActionHandler.wait(2);

		// User clicks on Edit Button
		ActionHandler.click(ATORequestContainer.EditATORequestType);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Edit Button");
		CommonFunctions.attachScreenshot();

		// select request type
		ActionHandler.click(ATORequestContainer.selectrequesttype);
		ActionHandler.wait(2);
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		Reporter.addStepLog("select request type");

		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects an ATO Request Type");
		ActionHandler.click(ATORequestContainer.saveATORequest);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Save Button");
		CommonFunctions.attachScreenshot();
	}

	// Verify that an error message is displayed
	@Then("^Verify that an error message is displayed$")
	public void verify_that_an_error_message_is_displayed() throws Throwable {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(ATORequestContainer.Errormsg);
		Reporter.addStepLog("User validates the error message displayed");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.cancelEditATORequest);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

	}

	// Verify user cannot delete the ATO Request line Items
	@Then("^Verify user cannot delete the ATO Request line Items$")
	public void verify_user_cannot_delete_the_ATO_Request_line_Items() throws Throwable {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(ATORequestContainer.ATORequesttypeError);
		Reporter.addStepLog("User validates the error message displayed");
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.CloseATO);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

	}

	// fill mandatory fields
	@Then("^fill fields like QuickReference \"([^\"]*)\" ATORequestType \"([^\"]*)\" OUVReportinglevelone \"([^\"]*)\" OUVReportingleveltwo \"([^\"]*)\" OUVReportinglevelthree \"([^\"]*)\" Fleet \"([^\"]*)\" Market \"([^\"]*)\" InternalJLRContact \"([^\"]*)\"$")
	public void fill_fields_like_QuickReference_ATORequestType_OUVReportinglevelone_OUVReportingleveltwo_OUVReportinglevelthree_Fleet_Market_InternalJLRContact(
			String QuickReference1, String ATORequestType, String OUVReportinglevelone, String OUVReportingleveltwo,
			String OUVReportinglevelthree, String fleet, String Market, String InternalJlrContact) throws Throwable {

		String Quick[] = QuickReference1.split(",");
		QuickReference1 = CommonFunctions.readExcelMasterData(Quick[0], Quick[1], Quick[2]);

		String ATOreqType[] = ATORequestType.split(",");
		ATORequestType = CommonFunctions.readExcelMasterData(ATOreqType[0], ATOreqType[1], ATOreqType[2]);

		String Test1 = "TestDemo";

		// Enter Quick reference
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.QuickReference, QuickReference1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(ATORequestType))));
		ActionHandler.wait(3);

		CommonFunctions.attachScreenshot();

		// Enter Booking Justification
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test1);
		CommonFunctions.attachScreenshot();

		String OUVl1[] = OUVReportinglevelone.split(",");
		OUVReportinglevelone = CommonFunctions.readExcelMasterData(OUVl1[0], OUVl1[1], OUVl1[2]);

		String OUVl2[] = OUVReportingleveltwo.split(",");
		OUVReportingleveltwo = CommonFunctions.readExcelMasterData(OUVl2[0], OUVl2[1], OUVl2[2]);

		String OUVl3[] = OUVReportinglevelthree.split(",");
		OUVReportinglevelthree = CommonFunctions.readExcelMasterData(OUVl3[0], OUVl3[1], OUVl3[2]);

		String fle[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fle[0], fle[1], fle[2]);

		String market[] = Market.split(",");
		Market = CommonFunctions.readExcelMasterData(market[0], market[1], market[2]);

		String internaljlrc[] = InternalJlrContact.split(",");
		InternalJlrContact = CommonFunctions.readExcelMasterData(internaljlrc[0], internaljlrc[1], internaljlrc[2]);

		// OUVReportinglevel1 details should be filled

		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.dependencies);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVREportinglevel1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelone))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// level2 details should be filled

		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportingleveltwo))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// level3 details should be filled

		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelthree))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.apply);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// fleet text should be entered

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.fleet, fleet);
		ActionHandler.wait(4);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(6);

		// market field should be filled

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Market))));
		ActionHandler.wait(5);

		// Internal JLRcontact field should be filled

		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, InternalJlrContact);
		ActionHandler.wait(5);

		ActionHandler.click(ATORequestContainer.Save);
		CommonFunctions.attachScreenshot();
	}

	// Verify that ATO request cannot be created and verify the error message
	@Then("^Verify that ATO request cannot be created and verify the error message$")
	public void verify_that_ATO_request_cannot_be_created_and_verify_the_error_message() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.ATOerror)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("error message is visible");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("error message is not visible");
		}
	}

	// fill mandatory fields
	@Then("^fill all fields like QuickReference \"([^\"]*)\" ATORequestType \"([^\"]*)\" Fleetdate \"([^\"]*)\" Handindate \"([^\"]*)\" DisposalRoute \"([^\"]*)\"$")
	public void fill_all_fields_like_QuickReference_ATORequestType_Fleetdate_Handindate_DisposalRoute(
			String QuickReference1, String ATORequestType, String Fleetdate, String HandinDate, String DisposalRoute)
			throws Throwable {

		String percentage1 = "70";

		String Quick[] = QuickReference1.split(",");
		QuickReference1 = CommonFunctions.readExcelMasterData(Quick[0], Quick[1], Quick[2]);

		String ATOreqType[] = ATORequestType.split(",");
		ATORequestType = CommonFunctions.readExcelMasterData(ATOreqType[0], ATOreqType[1], ATOreqType[2]);

		String fleetdate[] = Fleetdate.split(",");
		Fleetdate = CommonFunctions.readExcelMasterData(fleetdate[0], fleetdate[1], fleetdate[2]);

		String Handin[] = HandinDate.split(",");
		HandinDate = CommonFunctions.readExcelMasterData(Handin[0], Handin[1], Handin[2]);

		String dr[] = DisposalRoute.split(",");
		DisposalRoute = CommonFunctions.readExcelMasterData(dr[0], dr[1], dr[2]);

		String Test1 = "TestDemo";

		// Enter Quick reference
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.QuickReference, QuickReference1);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(ATORequestType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test1);
		CommonFunctions.attachScreenshot();

		// proposed fleet date should be filled
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.ProposedAddtoFleetDate, Fleetdate);
		ActionHandler.wait(4);

		// hand in date should be filled

		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// percentage should be filled

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.percentage, percentage1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		// disposal route should be filled

		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(DisposalRoute))));
		ActionHandler.wait(3);

		ActionHandler.click(ATORequestContainer.Save);
		CommonFunctions.attachScreenshot();
	}

	// Click on dropdown and select edit option
	@And("^Click on dropdown and select edit option$")
	public void click_on_drop_down_icon_and_select_edit_option() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User clicks on Show Actions
		ActionHandler.click(ATORequestContainer.ShowActions2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();

		// User clicks on Edit
		ActionHandler.click(ATORequestContainer.Edit2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Edit");
		CommonFunctions.attachScreenshot();

	}

	// Verify that user unable to find vista vehicle field
	@Then("^Verify that user unable to find vista vehicle field$")
	public void verify_that_user_unable_to_find_vista_vehicle_field() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.ClearSelection)) {
			Reporter.addStepLog("Vista Vehicle field is able to edit");
		} else {
			Reporter.addStepLog("Vista Vehicle field is unable to edit");
		}
		CommonFunctions.attachScreenshot();

	}

	// Navigate to Approval History
	@Then("^Navigate to Approval History$")
	public void navigate_to_Approval_History() throws Throwable {
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.ApprovalHistroy);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to approval history in related list quick links");

	}

	// verify that Recall button should not be there
	@Then("^verify that Recall button should not be there$")
	public void verify_that_Recall_button_should_not_be_there() throws Throwable {
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(ATORequestContainer.Recall);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User unable to find the recall button");

	}

	// Click on ATO line item and click on Edit vista vehicle
	@Then("^Click on ATO line item and click on Edit vista vehicle with \"([^\"]*)\"$")
	public void click_on_ATO_line_item_and_click_on_Edit_vista_vehicle_with(String VistaVehicle) throws Throwable {

		String Vista[] = VistaVehicle.split(",");
		VistaVehicle = CommonFunctions.readExcelMasterData(Vista[0], Vista[1], Vista[2]);

		ActionHandler.click(ATORequestContainer.SelectATOLineItems);
		ActionHandler.wait(4);
		Reporter.addStepLog("User selects the ATO Line Item");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.editVistaVehicle_lineitem);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on edit Vista Vehicle button");
		CommonFunctions.attachScreenshot();

		// User changes Vista Vehicle
		ActionHandler.click(ATORequestContainer.crossIcon);
		ActionHandler.wait(3);

		ActionHandler.setText(ATORequestContainer.SearchVista, VistaVehicle);
		ActionHandler.wait(2);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		Reporter.addStepLog("User changes Vista Vehicle");

		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(ATORequestContainer.saveVistaedit);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
	}

	// user can't edit the vista vehicle

	@Then("^Verify that user unable to edit vista vehicle field$")
	public void verify_that_user_unable_to_edit_madatory_fields() throws Throwable {
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.errormsg)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message caught");
		} else {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message not found");
		}

	}

	// verify if there is delete option for ATO request
	@Then("^verify if there is delete option for ATO request$")
	public void verify_if_there_is_delete_option_for_ATO_request() throws Throwable {

		ActionHandler.click(ATORequestContainer.ShowActions2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();

		if (VerifyHandler.verifyElementPresent(ATORequestContainer.Delete)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("verify that user is able to delete ATO request");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("verify that user is unable to delete ATO request");
			CommonFunctions.attachScreenshot();
		}

	}

	@Then("^Verify that user is able to search for status \"([^\"]*)\"$")
	public void verify_that_user_is_able_to_search_for_status(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		WebElement Result = driver.findElement(By.xpath(ATORequestContainer.text(Search)));

		if (VerifyHandler.verifyElementPresent(Result)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("The search result as displayed");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("The search result is not displayed");
			CommonFunctions.attachScreenshot();
		}

	}

	// user delete an ATO request line items
	@Then("^user delete an ATO request line items$")
	public void user_delete_an_ATO_request_line_items() throws Throwable {

		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.click(ATORequestContainer.ShowActions2);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.Delete);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Delete");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.DeleteLineItems);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on confirm Delete");
		CommonFunctions.attachScreenshot();
	}

	// verify the displayed delete error message
	@Then("^verify the displayed delete error message$")
	public void verify_the_displayed_delete_error_message() throws Throwable {
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.Deleteerror)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("verify that user is not able to delete items");
			CommonFunctions.attachScreenshot();

			ActionHandler.click(ATORequestContainer.closewindow);
			ActionHandler.wait(5);
			Reporter.addStepLog("User clicks on close error");
			CommonFunctions.attachScreenshot();

		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("verify that user is able to delete items");
			CommonFunctions.attachScreenshot();
		}

	}

	// verify that user is unable to find edit option at the ATO request Status
	@Then("^verify that user is unable to find edit option at the ATO request Status$")
	public void verify_that_user_is_unable_to_find_edit_option_at_the_ATO_request_Status() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(ATORequestContainer.Editpencilicon)) {

			Reporter.addStepLog("User does not see edit status button");
			CommonFunctions.attachScreenshot();
		} else {
			Reporter.addStepLog("User see edit status button");
			CommonFunctions.attachScreenshot();
		}

	}

	// click on edit Replacement Vehicle
	@Then("^click on edit Replacement Vehicle$")
	public void click_on_edit_Replacement_Vehicle() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Replacement Vehicle");

		ActionHandler.click(ATORequestContainer.ReplacementVehicle);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Replacement Vehicle icon");
	}

	// remove the added vehicle
	@Then("^remove the added vehicle$")
	public void remove_the_added_vehicle() throws Throwable {

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(5);
		ActionHandler.pageDown();
		ActionHandler.wait(5);

		ActionHandler.click(ATORequestContainer.ReplacementVehicle);

		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User click on Cancel");

		Reporter.addStepLog("User click on Replacement Vehicle icon");

		ActionHandler.click(ATORequestContainer.ClearSelection);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Clear Selection");

		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Save");
	}

	// Validate the Over Budget Status as Yes/No
	@Then("^Validate the Over Budget Status as Yes/No$")
	public void validate_the_Over_Budget_Status_as_Yes_No() throws Throwable {
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.OverBudgetNo)) {
			Reporter.addStepLog("User verifies the Over Budget Status as No");
			System.out.println("User verifies the Over Budget Status as No");
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
		} else {
			VerifyHandler.verifyElementPresent(ATORequestContainer.OverBudgetYes);
			Reporter.addStepLog("User verifies the Over Budget Status as Yes");
			System.out.println("User verifies the Over Budget Status as Yes");
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(2);
		}

	}

	// Navigate to Approval History and validate the Approver it is pending
	@Then("^Navigate to Approval History and validate the Approver it is pending to as \"([^\"]*)\"$")
	public void navigate_to_Approval_History_and_validate_the_Approver_it_is_pending_to(String UserName)
			throws Throwable {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		ActionHandler.click(ATORequestContainer.ApprovalHistroy);

		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User click on Replacement Vehicle icon");

		// User click on Clear Selection"
		ActionHandler.click(ATORequestContainer.ClearSelection);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Clear Selection");

		// User click on Save
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Save");

		ActionHandler.wait(2);
		Reporter.addStepLog("User navigates to Approval History");

		VerifyHandler
				.verifyElementPresent(driver.findElement(By.xpath(ATORequestContainer.userverification(UserName))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies the approver name it is pending to");

	}

	// Login to OUV Portal with user
	@Then("^Login to OUV Portal with user \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void login_to_OUV_Portal_with_user_and_password_as(String UserName, String Password) throws Throwable {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Test Begin......");
		Reporter.addStepLog("User Access OUV Portal");
		onStart();
		driver.get(Constants.OUVURL);
		ActionHandler.wait(7);
		Reporter.addStepLog("User Logins to OUV Portal");

		// Enter user name
		ActionHandler.setText(ATORequestContainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// enter password
		ActionHandler.setText(ATORequestContainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// click on login button
		ActionHandler.click(ATORequestContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(20);

		ActionHandler.waitForElement(ATORequestContainer.welcome, 60);

		if (!VerifyHandler.verifyElementPresent(ATORequestContainer.welcome)) {
			ActionHandler.wait(4);
			driver.navigate().refresh();
			ActionHandler.waitForElement(ATORequestContainer.welcome, 60);
		}
		VerifyHandler.verifyElementPresent(ATORequestContainer.welcome);

	}

	// Approve the ATO Request
	@Then("^Approve the ATO Request$")
	public void approve_the_ATO_Request() throws Throwable {

		// User selects the Notification Menu
		ActionHandler.click(ATORequestContainer.Notification);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the Notification Menu");
		ActionHandler.wait(2);

		// User selects the ATO Request
		ActionHandler.click(ATORequestContainer.ATORequestNotification);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects the ATO Request");

		// User selects Approve Button
		ActionHandler.click(ATORequestContainer.ApproveButton);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.confirmApprove);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User selects Approve Button");
	}

	// Navigate back to ATO Request
	@Then("^Navigate back to ATO Request$")
	public void navigate_back_to_ATO_Request() throws Throwable {
		ActionHandler.click(ATORequestContainer.BacktoATO);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User goes back to the ATO Request");
	}

	// Navigate to Notes & Attachments section
	@Then("^Navigate to Notes & Attachments section$")
	public void navigate_to_Notes_Attachments_section() throws Throwable {
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		ActionHandler.click(ATORequestContainer.Newnote);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Navigate to Notes & Attachments section");
	}

	// method to upload files
	public void uploadFile(String file) throws IOException {
		ActionHandler.wait(10);
		Runtime.getRuntime().exec(System.getProperty("user.dir") + "/upload.exe" + " " + System.getProperty("user.dir")
				+ "\\Files\\" + file);
		CommonFunctions.attachScreenshot();

	}

	// Upload any file from local directory into Notes & Attachments section
	@Then("^Upload any file from local directory into Notes & Attachments section$")
	public void upload_any_file_from_local_directory_into_Notes_Attachments_section() throws Throwable {
		ActionHandler.click(ATORequestContainer.Uploadfiles);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		uploadFile("OUV.txt");
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.Done);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Upload any file from local directory into Notes & Attachments section");

	}

	// Click on any ATO Request having ATO Request status as Approved Search ATO
	// Request
	@Then("^Click on any ATO Request having ATO Request status as Approved Search ATO Request \"([^\"]*)\"$")
	public void click_on_any_ATO_Request_having_ATO_Request_status_as_Approved_Search_ATO_Request(
			String searchATOrequest) throws Throwable {

		String Search[] = searchATOrequest.split(",");
		searchATOrequest = CommonFunctions.readExcelMasterData(Search[0], Search[1], Search[2]);

		ActionHandler.click(ATORequestContainer.SelectATORequest);
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.SelectAll);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.SearchATORequest);
		ActionHandler.wait(5);
		ActionHandler.setText(ATORequestContainer.SearchATORequest, searchATOrequest);
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.ApprovedATORequest);
		Reporter.addStepLog("User able to Click on any ATO Request having ATO Request status as Approved");

	}

	// User changes Title of the document Title
	@Then("^User changes Title of the document Title \"([^\"]*)\"$")
	public void user_changes_Title_of_the_document_Title(String Title) throws Throwable {

		String Type[] = Title.split(",");
		Title = CommonFunctions.readExcelMasterData(Type[0], Type[1], Type[2]);

		ActionHandler.click(ATORequestContainer.Title);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(ATORequestContainer.Title, Title);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to changes Title of the document Title");
	}

	// User chooses a different version of file from the local system
	@Then("^User chooses a different version of file from the local system$")
	public void user_chooses_a_different_version_of_file_from_the_local_system() throws Throwable {

		uploadFile("Accenture.txt");
		ActionHandler.wait(20);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.upload);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to chooses a different version of file from the local system");
	}

	// User selects contact to whom file can share
	@Then("^User selects contact to whom file can share$")
	public void user_selects_contact_to_whom_file_can_share() throws Throwable {
		ActionHandler.click(ATORequestContainer.SearchPeople);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to selects contact to whom file can share");
	}

	// User can select share access for uploaded file Viewer
	@Then("^User can select share access for uploaded file Viewer \"([^\"]*)\"$")
	public void user_can_select_share_access_for_uploaded_file_Viewer(String Viewer) throws Throwable {

		String View[] = Viewer.split(",");
		Viewer = CommonFunctions.readExcelMasterData(View[0], View[1], View[2]);

		ActionHandler.click(ATORequestContainer.whocanaccess);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		// Select Viewer
		ActionHandler.click(ATORequestContainer.viewer1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.viewer1(Viewer))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on share button
		ActionHandler.click(ATORequestContainer.Share2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to select share access for uploaded file Viewer");
	}

	// FIll all mandatory fields of an ATO Request
	@And("^User Fills all mandatory Fields like ATOReqType \"([^\"]*)\" RepoLOne \"([^\"]*)\" RepoLTwo \"([^\"]*)\" RepoLThree \"([^\"]*)\" Market \"([^\"]*)\" disposalRoute \"([^\"]*)\"$")
	public void User_Fills_Mandatory_Fields_With_ATO_Request_Type(String ATOReqType, String RepoLOne, String RepoLTwo,
			String RepoLThree, String market, String disposalR) throws Exception {
		String[] reqType = ATOReqType.split(",");
		ATOReqType = CommonFunctions.readExcelMasterData(reqType[0], reqType[1], reqType[2]);

		String[] ROne = RepoLOne.split(",");
		RepoLOne = CommonFunctions.readExcelMasterData(ROne[0], ROne[1], ROne[2]);

		String[] RTwo = RepoLTwo.split(",");
		RepoLTwo = CommonFunctions.readExcelMasterData(RTwo[0], RTwo[1], RTwo[2]);

		String[] RThree = RepoLThree.split(",");
		RepoLThree = CommonFunctions.readExcelMasterData(RThree[0], RThree[1], RThree[2]);

		String[] m = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String[] DR = disposalR.split(",");
		disposalR = CommonFunctions.readExcelMasterData(DR[0], DR[1], DR[2]);

		String Test = "Test ATO Request";
		String percentage = "70";
		String PR = "PR NL";

		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(ATOReqType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User changes ATO Request Type");

		ActionHandler.setText(ATORequestContainer.QuickReference, Test);
		ActionHandler.wait(4);
		javascriptUtil.scrollIntoView(ATORequestContainer.BookingJustification);
		ActionHandler.wait(2);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// level1
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.dependencies);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.OUVREportinglevel1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(RepoLOne))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// level2
		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(RepoLTwo))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// level3
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(RepoLThree))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.apply);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// fleet

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.fleet, PR);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.fleetSelection);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(PR))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// market
		// ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(market))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// jlr contact
		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, Test);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// proposed fleet date
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		String Fleetdate = CommonFunctions.selectFutureDate();
		ActionHandler.setText(ATORequestContainer.ProposedAddtoFleetDate, Fleetdate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		// hand in date

		ActionHandler.wait(1);
		String HandinDate = CommonFunctions.selectFutureDate();
		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// percentage
		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.percentage, percentage);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		// disposal route

		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(disposalR))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Rework route
		ActionHandler.click(ATORequestContainer.Rework);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		// click on save button
		ActionHandler.click(ATORequestContainer.Save);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("ATO Request created");
	}

	// User clicks on Remove from Record on pop up window
	@Then("^User clicks on Remove from Record on pop up window$")
	public void user_clicks_on_Remove_from_Record_on_pop_up_window() throws Throwable {

		// click on Remove from Record on pop up window
		ActionHandler.click(ATORequestContainer.Remove);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to click on Remove from Record on pop up window");
	}

	// User can view one delete pop up, user clicks on delete button
	@Then("^User can view one delete pop up, user clicks on delete button$")
	public void user_can_view_one_delete_pop_up_user_clicks_on_delete_button() throws Throwable {

		// click on delete button
		ActionHandler.click(ATORequestContainer.delete);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to clicks on delete button");
	}

	// User can add Description of uploaded document Description
	@Then("^User can add Description of uploaded document Description \"([^\"]*)\"$")
	public void user_can_add_Description_of_uploaded_document_Description(String Discription) throws Throwable {

		String Type1[] = Discription.split(",");
		Discription = CommonFunctions.readExcelMasterData(Type1[0], Type1[1], Type1[2]);

		ActionHandler.click(ATORequestContainer.Textarea);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(ATORequestContainer.Textarea, Discription);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to add Description of uploaded document Description");

	}

	// User clicks on Save button to save the changes of uploaded document
	@Then("^User clicks on Save button to save the changes of uploaded document$")
	public void user_clicks_on_Save_button_to_save_the_changes_of_uploaded_document() throws Throwable {

		// User clicks on Save button to save the changes of uploaded document
		ActionHandler.click(ATORequestContainer.saved);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to clicks on Save button to save the changes of uploaded document");
	}

	// User creates multiple ATO Request Line Items
	@Then("^User creates multiple ATO Request Line Items$")
	public void user_creates_multiple_ATO_Request_Line_Items() throws Throwable {
		String Dummy = "00000000";

		// User clicks on New Button
		ActionHandler.click(ATORequestContainer.New);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on New Button");
		CommonFunctions.attachScreenshot();

		// User searches for Vista Vehicle
		ActionHandler.setText(ATORequestContainer.SearchVista, Dummy);
		ActionHandler.wait(2);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searches for Vista Vehicle");
		CommonFunctions.attachScreenshot();

		// User clicks on Save and New Button
		ActionHandler.click(ATORequestContainer.SaveNew);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Save and New Button");
		CommonFunctions.attachScreenshot();

		// User searches for Vista Vehicle
		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.SearchVista, Dummy);
		ActionHandler.wait(2);

		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User searches for Vista Vehicle");
		CommonFunctions.attachScreenshot();

		// User clicks on Save Button
		ActionHandler.click(ATORequestContainer.SaveEdit);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Save Button");
		CommonFunctions.attachScreenshot();
	}

	// Navigate to Approval History and validate the status as approved,if not login
	// as user
	@Then("^Navigate to Approval History and validate the status as approved,if not login as user \"([^\"]*)\" and password as \"([^\"]*)\" ,and user \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void navigate_to_Approval_History_and_validate_the_status_as_approved_if_not_login_as_user_and_password_as_and_user_and_password_as(
			String User1, String Password1, String User2, String Password2) throws Throwable {

		String User[] = User1.split(",");
		User1 = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password1.split(",");
		Password1 = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		String user[] = User2.split(",");
		User2 = CommonFunctions.readExcelMasterData(user[0], user[1], user[2]);

		String Pass[] = Password2.split(",");
		Password2 = CommonFunctions.readExcelMasterData(Pass[0], Pass[1], Pass[2]);

		// Verify if status is approved after LL4 Approval
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.Approvedstatus)) {
			ActionHandler.wait(4);
			Reporter.addStepLog("User has approved the ATO Request");
		} else {
			// Logout of OUV Portal
			javascriptUtil.clickElementByJS(ATORequestContainer.homeTab);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(ATORequestContainer.loginImg);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(ATORequestContainer.logout);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Logout from OUV Portal");

			ActionHandler.wait(10);

			ActionHandler.setText(ATORequestContainer.userNameTextBox, User1);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(ATORequestContainer.passwordTextBox, Password1);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			// Login to OUV Portal
			ActionHandler.click(ATORequestContainer.loginBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(20);

			ActionHandler.waitForElement(ATORequestContainer.welcome, 60);

			if (!VerifyHandler.verifyElementPresent(ATORequestContainer.welcome)) {
				ActionHandler.wait(4);
				driver.navigate().refresh();
				ActionHandler.waitForElement(ATORequestContainer.welcome, 60);
			}
			VerifyHandler.verifyElementPresent(ATORequestContainer.welcome);

			// Approve the ATO Request
			approve_the_ATO_Request();

			// Logout of OUV Portal
			javascriptUtil.clickElementByJS(ATORequestContainer.homeTab);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(ATORequestContainer.loginImg);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			// click on log out
			ActionHandler.click(ATORequestContainer.logout);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Logout from OUV Portal");

			ActionHandler.wait(10);

			// enter user name
			ActionHandler.setText(ATORequestContainer.userNameTextBox, User2);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			// enter password
			ActionHandler.setText(ATORequestContainer.passwordTextBox, Password2);
			ActionHandler.wait(1);
			CommonFunctions.attachScreenshot();

			// Login to OUV Portal
			ActionHandler.click(ATORequestContainer.loginBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();
			ActionHandler.wait(20);

			ActionHandler.waitForElement(ATORequestContainer.welcome, 60);

			if (!VerifyHandler.verifyElementPresent(ATORequestContainer.welcome)) {
				ActionHandler.wait(4);
				driver.navigate().refresh();
				ActionHandler.waitForElement(ATORequestContainer.welcome, 60);
			}
			VerifyHandler.verifyElementPresent(ATORequestContainer.welcome);

			// Approve the ATO Request
			approve_the_ATO_Request();

			// Verify if the ATO Request has been approved
			if (VerifyHandler.verifyElementPresent(ATORequestContainer.Approvedstatus)) {
				ActionHandler.wait(4);
				Reporter.addStepLog("User has approved the ATO Request");
			}

		}
	}

	// User clicks uploaded file dropdown
	@Then("^User clicks uploaded file dropdown \"([^\"]*)\"$")
	public void user_clicks_uploaded_file_dropdown(String Files) throws Throwable {

		String drop1[] = Files.split(",");
		Files = CommonFunctions.readExcelMasterData(drop1[0], drop1[1], drop1[2]);

		driver.navigate().refresh();
		ActionHandler.wait(5);

		// User clicks uploaded file dropdown
		ActionHandler.click(ATORequestContainer.dropdown);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.dropdown(Files))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to clicks uploaded file dropdown");
	}

	// Replacement Vehicle information VIN
	@When("^Replacement Vehicle information VIN \"([^\"]*)\" and Vehicle Name \"([^\"]*)\" is entered click save$")
	public void replacement_Vehicle_information_VIN_and_Vehicle_Name_is_entered_click_save(String Vehicle, String Name)
			throws Throwable {
		String Veh[] = Vehicle.split(",");
		Vehicle = CommonFunctions.readExcelMasterData(Veh[0], Veh[1], Veh[2]);

		String Na[] = Name.split(",");
		Name = CommonFunctions.readExcelMasterData(Na[0], Na[1], Na[2]);

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.ClearSelection)) {
			ActionHandler.click(OUVCalendarContainer.ClearSelection);
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("User click on ClearSelection icon");
		}

		ActionHandler.clearAndSetText(ATORequestContainer.ReplacementVehicleinput, Vehicle);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Vehicle info");

		ActionHandler.click(ATORequestContainer.ShowResults);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Show Results");

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ATOvehicle(Name))));
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on vehicle");

		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Save");

	}

	// verify the error message for Replacement Vehicle
	@When("^verify the error message for Replacement Vehicle$")
	public void verify_the_error_message_for_Replacement_Vehicle() throws Throwable {

		if (VerifyHandler.verifyElementPresent(ATORequestContainer.ATOerror)) {
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that error message is displayed");
		}

		else {
			ActionHandler.wait(4);
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Verify that error message not displayed");
		}

		ActionHandler.click(ATORequestContainer.CancelEdit);
	}

	// Click on Save&New button to save the ATO request
	@Then("^Click on Save&New button to save the ATO request$")
	public void click_on_Save_New_button_to_save_the_ATO_request() throws Throwable {

		// Click on Save&New button to save the ATO request
		ActionHandler.click(ATORequestContainer.Saveandnew);
		ActionHandler.wait(6);
		driver.navigate().refresh();
		ActionHandler.wait(15);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Save&New button to save the ATO request");
	}

	@Then("^Click on any ATO Request having ATO Request status as \"([^\"]*)\"$")
	public void click_on_any_ATO_Request_having_ATO_Request_status_as(String searchATOrequest) throws Throwable {

		String Search[] = searchATOrequest.split(",");
		searchATOrequest = CommonFunctions.readExcelMasterData(Search[0], Search[1], Search[2]);

		ActionHandler.click(ATORequestContainer.SelectATORequest);
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.SelectAll);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.SearchATORequest);
		ActionHandler.wait(5);
		ActionHandler.setText(ATORequestContainer.SearchATORequest, searchATOrequest);
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.ATORequest1);
		Reporter.addStepLog("User able to Click on any ATO Request having ATO Request status as Approved");

	}

	@Then("^At the top of right hand side in ATO Request window, select the dropdown for create line items \"([^\"]*)\"$")
	public void at_the_top_of_right_hand_side_in_ATO_Request_window_select_the_dropdown_for_create_line_items(
			String SelectLines) throws Throwable {

		String dd[] = SelectLines.split(",");
		SelectLines = CommonFunctions.readExcelMasterData(dd[0], dd[1], dd[2]);
		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(ATORequestContainer.Dropdown2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.Dropdown2(SelectLines))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to add Multiple Line Items for ATO Request");

	}

	// User should enter the value and click on save

	@Then("^Enter the Value  and click on save \"([^\"]*)\"$")
	public void enter_the_Value_and_click_on_save(String SelectItemLines) throws Throwable {

		String select[] = SelectItemLines.split(",");
		SelectItemLines = CommonFunctions.readExcelMasterData(select[0], select[1], select[2]);
		ActionHandler.click(ATORequestContainer.SelectLineItems);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(ATORequestContainer.SelectLineItems, SelectItemLines);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.Save2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Enter the Value  and click on save");
	}

	// User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle
	// and ATO Request Status as Pending Approval

	@Then("^Verify the Error Message$")
	public void Verify_the_Error_Message() throws Throwable {
		VerifyHandler.verifyElementPresent(ATORequestContainer.VerifyError1);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Verify Error Message");

	}

	// User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle
	// and ATO Request Status as New

	@Then("^Verifying the Error Message$")
	public void Verifying_the_Error_Message() throws Throwable {
		VerifyHandler.verifyElementPresent(ATORequestContainer.VerifyError2);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Verify Error Message");

	}

	// verify that user unable to find dropdown

	@Then("^User is not able to view select the dropdown for create line items$")
	public void User_is_not_able_to_view_select_the_dropdown_for_create_line_items() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.Dropdown2)) {
			Reporter.addStepLog("Dropdown option is available");
		} else {
			Reporter.addStepLog("Dropdown option is not available");
		}
	}

	// Click on Save button to save the ATO request
	@Then("^Click on Save button to save the ATO request$")
	public void click_on_Save_button_to_save_the_ATO_request() throws Throwable {

		// Click on Save button to save the ATO request
		ActionHandler.click(ATORequestContainer.saveandedit);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Click on Save button to save the ATO request");
	}

	// Click on Clone button to clone the ATO request
	@Then("^Click on Clone button to clone the ATO request$")
	public void click_on_Clone_button_to_clone_the_ATO_request() throws Throwable {

		// Click on Clone button to clone the ATO request
		ActionHandler.wait(6);
		ActionHandler.click(ATORequestContainer.CloneATORequest);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Click on Clone button to clone the ATO request");
	}

	// Click on cancel/withdraw option from drop down list of an ATO Request
	@Then("^Click on cancel/withdraw option from drop down list of an ATO Request$")
	public void click_on_cancel_withdraw_option_from_drop_down_list_of_an_ATO_Request() throws Throwable {

		// Click on cancel/withdraw option from drop down list of an ATO Request
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.CancelWithdraw);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.Cancel1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Click on cancel/withdraw option from drop dowm");
	}

	// Select Cancel option from ATO Request Status drop down list Cancel Dropdown
	@Then("^Select Cancel option from ATO Request Status drop down list Cancel Dropdown \"([^\"]*)\"$")
	public void select_Cancel_option_from_ATO_Request_Status_drop_down_list_Cancel_Dropdown(String Cancelled)
			throws Throwable {

		// data read from master data excel sheet
		String Cancel[] = Cancelled.split(",");
		Cancelled = CommonFunctions.readExcelMasterData(Cancel[0], Cancel[1], Cancel[2]);

		// Select Cancel option from ATO Request Status drop down list Cancel Dropdown
		ActionHandler.click(ATORequestContainer.Canceloption);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Cancelled))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Select Cancel option from ATO Request Status drop down");

	}

	// User navigates to chatter tab

	@Then("^Navigate to Chatter tab$")
	public void navigate_to_Chatter_tab() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.Chatter);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Enter the free text in share and text box

	@Then("^Enter text in Share an update text box Share an Update \"([^\"]*)\"$")
	public void enter_text_in_Share_an_update_text_box_Share_an_Update(String ShareText) throws Throwable {

		String Text[] = ShareText.split(",");
		ShareText = CommonFunctions.readExcelMasterData(Text[0], Text[1], Text[2]);

		ActionHandler.click(ATORequestContainer.ShareText);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(ATORequestContainer.ShareTexts, ShareText);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	@Then("^Select To section from drop down list Share Dropdown \"([^\"]*)\"$")
	public void select_To_section_from_drop_down_list_Share_Dropdown(String Drop) throws Throwable {

		String DD[] = Drop.split(",");
		Drop = CommonFunctions.readExcelMasterData(DD[0], DD[1], DD[2]);

		ActionHandler.click(ATORequestContainer.Selectdrop);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.viewer1(Drop))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	@Then("^Click on Share button$")
	public void click_on_Share_button() throws Throwable {

		ActionHandler.click(ATORequestContainer.Share);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	@Then("^Click on ATOHistory$")
	public void click_on_ATOHistory() throws Throwable {

		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.ATOreqHis);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User Click on ATO request History");
	}

	// User tries to validate the ATO request History i spresent or not

	@And("^Validate the ATO request History$")
	public void validate_the_ATO_request_history() throws Throwable {
		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.ATOreqHistory)) {
			Reporter.addStepLog("ATO request History is present");
		} else {
			Reporter.addStepLog("ATO request History is not present");
		}

	}

	// fill all mandatory fields like QuickReference, ATORequestType,
	// OUVReportinglevelone etc..
	@Then("^fill all mandatory fields of ATO Request like QuickReference \"([^\"]*)\" ATORequestType \"([^\"]*)\" OUVReportinglevelone \"([^\"]*)\" OUVReportingleveltwo \"([^\"]*)\" OUVReportinglevelthree \"([^\"]*)\" Fleet \"([^\"]*)\" Market \"([^\"]*)\" InternalJLRContact \"([^\"]*)\" DisposalRoute \"([^\"]*)\" ReworkRoute \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_of_ATO_Request_like_QuickReference_ATORequestType_OUVReportinglevelone_OUVReportingleveltwo_OUVReportinglevelthree_Fleet_Market_InternalJLRContact_DisposalRoute_ReworkRoute(
			String QuickReference, String ATORequestType, String OUVReportinglevelone, String OUVReportingleveltwo,
			String OUVReportinglevelthree, String fleet, String Market, String InternalJlrContact, String DisposalRoute,
			String ReworkRoute) throws Throwable {

		String Quick[] = QuickReference.split(",");
		QuickReference = CommonFunctions.readExcelMasterData(Quick[0], Quick[1], Quick[2]);

		String ATOreqType[] = ATORequestType.split(",");
		ATORequestType = CommonFunctions.readExcelMasterData(ATOreqType[0], ATOreqType[1], ATOreqType[2]);

		String Test = "TestDemo";
		String percentage = "70";

		// User enters Quick reference
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.QuickReference, QuickReference);
		CommonFunctions.attachScreenshot();

		// User selects ATO Request type
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(ATORequestType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Enter Booking Justification
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test);
		CommonFunctions.attachScreenshot();

		// Read data from master excel sheet
		String OUVl1[] = OUVReportinglevelone.split(",");
		OUVReportinglevelone = CommonFunctions.readExcelMasterData(OUVl1[0], OUVl1[1], OUVl1[2]);

		String OUVl2[] = OUVReportingleveltwo.split(",");
		OUVReportingleveltwo = CommonFunctions.readExcelMasterData(OUVl2[0], OUVl2[1], OUVl2[2]);

		String OUVl3[] = OUVReportinglevelthree.split(",");
		OUVReportinglevelthree = CommonFunctions.readExcelMasterData(OUVl3[0], OUVl3[1], OUVl3[2]);

		String fle[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fle[0], fle[1], fle[2]);

		String market[] = Market.split(",");
		Market = CommonFunctions.readExcelMasterData(market[0], market[1], market[2]);

		String internaljlrc[] = InternalJlrContact.split(",");
		InternalJlrContact = CommonFunctions.readExcelMasterData(internaljlrc[0], internaljlrc[1], internaljlrc[2]);

		String dr[] = DisposalRoute.split(",");
		DisposalRoute = CommonFunctions.readExcelMasterData(dr[0], dr[1], dr[2]);

		String Rework[] = ReworkRoute.split(",");
		ReworkRoute = CommonFunctions.readExcelMasterData(Rework[0], Rework[1], Rework[2]);

		// click on view all dependencies
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.dependencies);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 1
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVREportinglevel1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelone))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 2

		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportingleveltwo))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 3
		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelthree))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.apply);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// Enter fleet
		ActionHandler.setText(ATORequestContainer.fleet, fleet);
		ActionHandler.wait(4);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(6);

		// Select market
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Market))));
		ActionHandler.wait(3);

		// Enter jlr contact

		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, InternalJlrContact);
		ActionHandler.wait(2);

		// proposed fleet date
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.ProposedAddtoFleetDate);
		ActionHandler.wait(3);

		ActionHandler.click(ATORequestContainer.TodayBtn);
		ActionHandler.wait(3);

		// selectFutureDate method will selects the future date in calendar
		String HandinDate;
		HandinDate = CommonFunctions.selectFutureDate();

		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// percentage
		ActionHandler.wait(1);
		ActionHandler.setText(ATORequestContainer.percentage, percentage);
		CommonFunctions.attachScreenshot();

		// disposal route
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(DisposalRoute))));
		ActionHandler.wait(3);

		// Rework route
		ActionHandler.click(ATORequestContainer.alldependecies);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.ReworkRoute);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ReworkRoute(ReworkRoute))));
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.apply);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

	}

	// fill all mandatory fields like QuickReference, ATORequestType,
	// OUVReportinglevelone etc..
	@Then("^fill all mandatory fields of multiple ATO Request like QuickReference \"([^\"]*)\" ATORequestType \"([^\"]*)\" OUVReportinglevelone \"([^\"]*)\" OUVReportingleveltwo \"([^\"]*)\" OUVReportinglevelthree \"([^\"]*)\" Fleet \"([^\"]*)\" Market \"([^\"]*)\" InternalJLRContact \"([^\"]*)\" DisposalRoute \"([^\"]*)\" ReworkRoute \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_of_multiple_ATO_Request_like_QuickReference_ATORequestType_OUVReportinglevelone_OUVReportingleveltwo_OUVReportinglevelthree_Fleet_Market_InternalJLRContact_DisposalRoute_ReworkRoute(
			String QuickReference, String ATORequestType, String OUVReportinglevelone, String OUVReportingleveltwo,
			String OUVReportinglevelthree, String fleet, String Market, String InternalJlrContact, String DisposalRoute,
			String ReworkRoute) throws Throwable {

		String Quick[] = QuickReference.split(",");
		QuickReference = CommonFunctions.readExcelMasterData(Quick[0], Quick[1], Quick[2]);

		String ATOreqType[] = ATORequestType.split(",");
		ATORequestType = CommonFunctions.readExcelMasterData(ATOreqType[0], ATOreqType[1], ATOreqType[2]);

		String Test = "TestDemo";
		String percentage = "70";

		// User enters Quick reference
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.QuickReference, QuickReference);
		CommonFunctions.attachScreenshot();

		// User selects ATO Request type
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(ATORequestType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Enter Booking Justification
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test);
		CommonFunctions.attachScreenshot();

		// Read data from master excel sheet
		String OUVl1[] = OUVReportinglevelone.split(",");
		OUVReportinglevelone = CommonFunctions.readExcelMasterData(OUVl1[0], OUVl1[1], OUVl1[2]);

		String OUVl2[] = OUVReportingleveltwo.split(",");
		OUVReportingleveltwo = CommonFunctions.readExcelMasterData(OUVl2[0], OUVl2[1], OUVl2[2]);

		String OUVl3[] = OUVReportinglevelthree.split(",");
		OUVReportinglevelthree = CommonFunctions.readExcelMasterData(OUVl3[0], OUVl3[1], OUVl3[2]);

		String fle[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fle[0], fle[1], fle[2]);

		String market[] = Market.split(",");
		Market = CommonFunctions.readExcelMasterData(market[0], market[1], market[2]);

		String internaljlrc[] = InternalJlrContact.split(",");
		InternalJlrContact = CommonFunctions.readExcelMasterData(internaljlrc[0], internaljlrc[1], internaljlrc[2]);

		String dr[] = DisposalRoute.split(",");
		DisposalRoute = CommonFunctions.readExcelMasterData(dr[0], dr[1], dr[2]);

		String Rework[] = ReworkRoute.split(",");
		ReworkRoute = CommonFunctions.readExcelMasterData(Rework[0], Rework[1], Rework[2]);

		// click on view all dependencies
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.dependenciesm);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 1
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVREportinglevel1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelone))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 2

		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportingleveltwo))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 3
		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelthree))));
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.apply);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// Enter fleet
		ActionHandler.setText(ATORequestContainer.fleet, fleet);
		ActionHandler.wait(4);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(6);

		// Select market
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Market))));
		ActionHandler.wait(3);

		// Enter jlr contact

		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, InternalJlrContact);
		ActionHandler.wait(2);

		// proposed fleet date
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.ProposedAddtoFleetDate);
		ActionHandler.wait(3);

		ActionHandler.click(ATORequestContainer.TodayBtn);
		ActionHandler.wait(3);

		// selectFutureDate method will selects the future date in calendar
		String HandinDate;
		HandinDate = CommonFunctions.selectFutureDate();

		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// percentage
		ActionHandler.wait(1);
		ActionHandler.setText(ATORequestContainer.percentage, percentage);
		CommonFunctions.attachScreenshot();

		// disposal route
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(DisposalRoute))));
		ActionHandler.wait(3);

		// Rework route
		ActionHandler.click(ATORequestContainer.alldependeciesm);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.ReworkRoute);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ReworkRoute(ReworkRoute))));
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.apply);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

	}

	// fill all mandatory fields like QuickReference, ATORequestType,
	// OUVReportinglevelone etc..
	@Then("^fill all mandatory fields like QuickReference \"([^\"]*)\" ATORequestType \"([^\"]*)\" OUVReportinglevelone \"([^\"]*)\" OUVReportingleveltwo \"([^\"]*)\" OUVReportinglevelthree \"([^\"]*)\" Fleet \"([^\"]*)\" Market \"([^\"]*)\" InternalJLRContact \"([^\"]*)\" DisposalRoute \"([^\"]*)\" ReworkRoute \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_like_QuickReference_ATORequestType_OUVReportinglevelone_OUVReportingleveltwo_OUVReportinglevelthree_Fleet_Market_InternalJLRContact_DisposalRoute_ReworkRoute(
			String QuickReference, String ATORequestType, String OUVReportinglevelone, String OUVReportingleveltwo,
			String OUVReportinglevelthree, String fleet, String Market, String InternalJlrContact, String DisposalRoute,
			String ReworkRoute) throws Throwable {

		String Quick[] = QuickReference.split(",");
		QuickReference = CommonFunctions.readExcelMasterData(Quick[0], Quick[1], Quick[2]);

		String ATOreqType[] = ATORequestType.split(",");
		ATORequestType = CommonFunctions.readExcelMasterData(ATOreqType[0], ATOreqType[1], ATOreqType[2]);

		String Test = "TestDemo";
		String percentage = "70";

		// User enters Quick reference
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.QuickReference, QuickReference);
		CommonFunctions.attachScreenshot();

		// User selects ATO Request type
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(ATORequestType))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		// Enter Booking Justification
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test);
		CommonFunctions.attachScreenshot();

		// Read data from master excel sheet
		String OUVl1[] = OUVReportinglevelone.split(",");
		OUVReportinglevelone = CommonFunctions.readExcelMasterData(OUVl1[0], OUVl1[1], OUVl1[2]);

		String OUVl2[] = OUVReportingleveltwo.split(",");
		OUVReportingleveltwo = CommonFunctions.readExcelMasterData(OUVl2[0], OUVl2[1], OUVl2[2]);

		String OUVl3[] = OUVReportinglevelthree.split(",");
		OUVReportinglevelthree = CommonFunctions.readExcelMasterData(OUVl3[0], OUVl3[1], OUVl3[2]);

		String fle[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fle[0], fle[1], fle[2]);

		String market[] = Market.split(",");
		Market = CommonFunctions.readExcelMasterData(market[0], market[1], market[2]);

		String internaljlrc[] = InternalJlrContact.split(",");
		InternalJlrContact = CommonFunctions.readExcelMasterData(internaljlrc[0], internaljlrc[1], internaljlrc[2]);

		String dr[] = DisposalRoute.split(",");
		DisposalRoute = CommonFunctions.readExcelMasterData(dr[0], dr[1], dr[2]);

		String Rework[] = ReworkRoute.split(",");
		ReworkRoute = CommonFunctions.readExcelMasterData(Rework[0], Rework[1], Rework[2]);

		// click on view all dependencies
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.dependencies);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 1
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVREportinglevel1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelone))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 2

		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(3);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportingleveltwo))));
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 3

		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelthree))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.apply);
		CommonFunctions.attachScreenshot();

		// Enter fleet
		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.fleet, fleet);
		ActionHandler.wait(3);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(3);

		// Select market

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Market))));
		ActionHandler.wait(3);

		// Enter jlr contact

		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, InternalJlrContact);
		ActionHandler.wait(2);

		// proposed fleet date
		ActionHandler.pageScrollDown();
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.ProposedAddtoFleetDate);
		ActionHandler.wait(3);

		ActionHandler.click(ATORequestContainer.TodayBtn);
		ActionHandler.wait(3);

		// selectFutureDate method will selects the future date in calendar
		String HandinDate;
		HandinDate = CommonFunctions.selectFutureDate();

		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// percentage
		ActionHandler.setText(ATORequestContainer.percentage, percentage);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		// disposal route
		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(DisposalRoute))));
		ActionHandler.wait(3);

		// Rework route
		ActionHandler.click(ATORequestContainer.alldependecies);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.ReworkRoute);
		ActionHandler.wait(3);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ReworkRoute(ReworkRoute))));
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.apply);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("ATO Request created");

	}

	@Then("^fill all mandatory fields like QuickReference \"([^\"]*)\" ATORequestType \"([^\"]*)\" OUVReportinglevelone \"([^\"]*)\" OUVReportingleveltwo \"([^\"]*)\" OUVReportinglevelthree \"([^\"]*)\" Fleet \"([^\"]*)\" Market \"([^\"]*)\" InternalJLRContact \"([^\"]*)\" DisposalRoute \"([^\"]*)\" ReworkRoute \"([^\"]*)\" Replacementvehicle \"([^\"]*)\"$")
	public void fill_all_mandatory_fields_like_QuickReference_ATORequestType_OUVReportinglevelone_OUVReportingleveltwo_OUVReportinglevelthree_Fleet_Market_InternalJLRContact_DisposalRoute_ReworkRoute_Replacementvehicle(
			String QuickReference, String ATORequestType, String OUVReportinglevelone, String OUVReportingleveltwo,
			String OUVReportinglevelthree, String fleet, String Market, String InternalJlrContact, String DisposalRoute,
			String ReworkRoute, String Replacementvehicle) throws Throwable {

		String Quick[] = QuickReference.split(",");
		QuickReference = CommonFunctions.readExcelMasterData(Quick[0], Quick[1], Quick[2]);

		String ATOreqType[] = ATORequestType.split(",");
		ATORequestType = CommonFunctions.readExcelMasterData(ATOreqType[0], ATOreqType[1], ATOreqType[2]);

		String Test = "TestDemo";
		String percentage = "70";

		// User enters Quick reference
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.QuickReference, QuickReference);
		CommonFunctions.attachScreenshot();

		// User selects ATO Request type
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(ATORequestType))));
		ActionHandler.wait(3);

		CommonFunctions.attachScreenshot();
		ActionHandler.setText(ATORequestContainer.QuickReference, Test);

		// Enter Booking Justification
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test);
		CommonFunctions.attachScreenshot();

		// Read data from master excel sheet
		String OUVl1[] = OUVReportinglevelone.split(",");
		OUVReportinglevelone = CommonFunctions.readExcelMasterData(OUVl1[0], OUVl1[1], OUVl1[2]);

		String OUVl2[] = OUVReportingleveltwo.split(",");
		OUVReportingleveltwo = CommonFunctions.readExcelMasterData(OUVl2[0], OUVl2[1], OUVl2[2]);

		String OUVl3[] = OUVReportinglevelthree.split(",");
		OUVReportinglevelthree = CommonFunctions.readExcelMasterData(OUVl3[0], OUVl3[1], OUVl3[2]);

		String fle[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fle[0], fle[1], fle[2]);

		String market[] = Market.split(",");
		Market = CommonFunctions.readExcelMasterData(market[0], market[1], market[2]);

		String internaljlrc[] = InternalJlrContact.split(",");
		InternalJlrContact = CommonFunctions.readExcelMasterData(internaljlrc[0], internaljlrc[1], internaljlrc[2]);

		String dr[] = DisposalRoute.split(",");
		DisposalRoute = CommonFunctions.readExcelMasterData(dr[0], dr[1], dr[2]);

		String Rework[] = ReworkRoute.split(",");
		ReworkRoute = CommonFunctions.readExcelMasterData(Rework[0], Rework[1], Rework[2]);

		String rv[] = Replacementvehicle.split(",");
		Replacementvehicle = CommonFunctions.readExcelMasterData(rv[0], rv[1], rv[2]);

		// select replacement vehicle
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.Replacevehicle);
		ActionHandler.wait(3);

		ActionHandler.clearAndSetText(ATORequestContainer.Replacevehicle, Replacementvehicle);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters Vehicle info");

		ActionHandler.click(ATORequestContainer.ShowResults);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User click on Show Results");

		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.selectRepV);
		ActionHandler.wait(3);

		// click on view all dependencies
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.dependencies);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 1
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVREportinglevel1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelone))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 2

		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportingleveltwo))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 3

		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelthree))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.apply);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// Enter fleet

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.fleet, fleet);
		ActionHandler.wait(4);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(6);

		// Select market

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Market))));
		ActionHandler.wait(5);

		// Enter jlr contact

		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, InternalJlrContact);
		ActionHandler.wait(5);

		// proposed fleet date
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.ProposedAddtoFleetDate);
		ActionHandler.wait(3);

		ActionHandler.click(ATORequestContainer.TodayBtn);
		ActionHandler.wait(3);

		ActionHandler.wait(4);

		// hand in date
		// selectFutureDate method will selects the future date in calendar
		String HandinDate;
		HandinDate = CommonFunctions.selectFutureDate();

		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// percentage

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.percentage, percentage);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		// disposal route

		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(DisposalRoute))));
		ActionHandler.wait(3);

		// Rework route

		ActionHandler.click(ATORequestContainer.alldependecies);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.ReworkRoute);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ReworkRoute(ReworkRoute))));
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.apply);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("ATO Request created");

	}

	@Then("^verify the error message displayed$")
	public void verify_the_error_message_displayed() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.snagmsg)) {
			Reporter.addStepLog("Error message displayed");
		} else {
			Reporter.addStepLog("Error message not displayed");
		}
	}

	@Then("^Add filter with title \"([^\"]*)\" Field \"([^\"]*)\" and value \"([^\"]*)\"$")
	public void add_filter_with_title_Field_and_value(String Title, String Field, String Value) throws Throwable {

		String ti[] = Title.split(",");
		Title = CommonFunctions.readExcelMasterData(ti[0], ti[1], ti[2]);

		String f[] = Field.split(",");
		Field = CommonFunctions.readExcelMasterData(f[0], f[1], f[2]);

		String v[] = Value.split(",");
		Value = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);

		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.ListViewControl);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Clicks on List View Control");

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.NewFilter);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on New button");

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.ListName, Title);
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.Save2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter List name and click on save button");

		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.AddFilterLink);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Clicks on Add filter conditions link");

		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.FiledDropDown);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Field))));

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.ValueDropDown);
		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.userverification(Value))));
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Selects field and value for the filter");

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.NoteDoneBtn);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Clicks on Done button");

		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.save);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Saves the filter");

	}

	@Then("^Remove added filter with title \"([^\"]*)\"$")
	public void remove_added_filter_with_title(String title) throws Throwable {

		String v[] = title.split(",");
		title = CommonFunctions.readExcelMasterData(v[0], v[1], v[2]);

		ActionHandler.wait(3);
		user_navigate_to_ATO_Request();
		ActionHandler.wait(2);

		// User views all the listed OUV ATO Request
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.wait(2);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.value(title))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views filtered list of OUV ATO Requests");

		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.ListViewControl);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Clicks on List View Control");

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.DeleteFilter);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on delete filter button");

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.confirmDeleteNote);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on confirm delete filter button");

	}

	@Then("^Validate that user is able to view the Approval History for an ATO Request$")
	public void validate_that_user_is_able_to_view_the_Approval_History_for_an_ATO_Request() throws Throwable {
		VerifyHandler.verifyElementPresent(ATORequestContainer.ApprovalHistoryTitle);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User is able to view the Approval History for an ATO Request");

	}

	@Then("^User tries to update the Proposed Hand In Date less than Proposed Add to Fleet Date$")
	public void user_tries_to_update_the_Proposed_Hand_In_Date_less_than_Proposed_Add_to_Fleet_Date() throws Throwable {

		ActionHandler.pageDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		String HandInDate = selectYesterdayDate();
		String AddToFleetDate = selectTomorrowDate();

		ActionHandler.click(ATORequestContainer.EditAddToFleetDate);
		ActionHandler.clearAndSetText(ATORequestContainer.AddToFleetDate, AddToFleetDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User selects Proposed Add To Fleet Date");

		ActionHandler.clearAndSetText(ATORequestContainer.HandInDate, HandInDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		Reporter.addStepLog("User selects Proposed Hand In Date");

		ActionHandler.click(ATORequestContainer.SaveEdit);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Save Button");

	}

	@Then("^Validate the error message displayed for Proposed Hand In Date$")
	public void validate_the_error_message_displayed_for_Proposed_Hand_In_Date() throws Throwable {
		VerifyHandler.verifyElementPresent(ATORequestContainer.ProposedHandInDateError);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies the error message");

		ActionHandler.click(ATORequestContainer.CancelEdit);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on Cancel Button");

	}

	public String selectYesterdayDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, -1);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	public String selectTomorrowDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

	@And("^User Fills all mandatory Fields like RepoLOne \"([^\"]*)\" RepoLTwo \"([^\"]*)\" RepoLThree \"([^\"]*)\" Market \"([^\"]*)\" disposalRoute \"([^\"]*)\"$")
	public void User_Fills_Mandatory_Fields(String RepoLOne, String RepoLTwo, String RepoLThree, String market,
			String disposalR) throws Exception {
		String[] ROne = RepoLOne.split(",");
		RepoLOne = CommonFunctions.readExcelMasterData(ROne[0], ROne[1], ROne[2]);

		String[] RTwo = RepoLTwo.split(",");
		RepoLTwo = CommonFunctions.readExcelMasterData(RTwo[0], RTwo[1], RTwo[2]);

		String[] RThree = RepoLThree.split(",");
		RepoLThree = CommonFunctions.readExcelMasterData(RThree[0], RThree[1], RThree[2]);

		String[] m = market.split(",");
		market = CommonFunctions.readExcelMasterData(m[0], m[1], m[2]);

		String[] DR = disposalR.split(",");
		disposalR = CommonFunctions.readExcelMasterData(DR[0], DR[1], DR[2]);

		String Test = "Test ATO Request";
		String percentage = "70";
		String PR = "PR NL";

		ActionHandler.setText(ATORequestContainer.QuickReference, Test);
		ActionHandler.wait(4);
		javascriptUtil.scrollIntoView(ATORequestContainer.BookingJustification);
		ActionHandler.wait(2);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.dependencies);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.OUVReportingLevel1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.OUVReportingLevel1(RepoLOne))));
		ActionHandler.wait(5);

		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.OUVReportingleveL2(RepoLTwo))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.OUVReportingLevel3(RepoLThree))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.apply);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(ATORequestContainer.fleet, PR);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

//        ActionHandler.click(ATORequestContainer.fleetSelection);
//        ActionHandler.wait(2);
//        CommonFunctions.attachScreenshot();
//        javascriptUtil.clickElementByJS(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(PR))));
//        ActionHandler.wait(3);
//        CommonFunctions.attachScreenshot();

		// ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.Market(market))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, Test);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		String Fleetdate = CommonFunctions.selectFutureDate();
		ActionHandler.setText(ATORequestContainer.ProposedAddtoFleetDate, Fleetdate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(1);
		String HandinDate = CommonFunctions.selectFutureDate();
		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.percentage, percentage);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.Disposalroute(disposalR))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.Rework);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Actions act1 = new Actions(driver);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.Save);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("ATO Request created");

	}

	@Then("^User clicks on Create Line Items$")
	public void user_clicks_on_Create_Line_Items() throws Throwable {
		ActionHandler.click(ATORequestContainer.Dropdown);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on the Dropdown");
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.CreateLineItems);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on create line items");
		CommonFunctions.attachScreenshot();

	}

	@Then("^User enters number of line items and click on Save$")
	public void user_enters_number_of_line_items_and_click_on_Save() throws Throwable {
		ActionHandler.setText(ATORequestContainer.NumberofLineItems, "4");
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on the Dropdown");
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.SaveLineItems);
		ActionHandler.wait(5);
		Reporter.addStepLog("User saves the Line Items");
		CommonFunctions.attachScreenshot();

	}

	@Then("^Validate the error message displayed for creating line items$")
	public void validate_the_error_message_displayed_for_creating_line_items() throws Throwable {
		ActionHandler.wait(5);
		Reporter.addStepLog("error message displayed for creating line items");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Verify user cannot edit the Proposed Add to Fleet Date for an ATO Request$")
	public void validate_the_error_message_displayed_for_Proposed_Hand_In_Date_Request() throws Throwable {
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies Proposed Add to Fleet Date for an ATO Request cannot be edited");

	}

	// fill all mandatory fields like QuickReference, ATORequestType,
	// OUVReportinglevelone etc..
	@Then("^fill all the mandatory fields like QuickReference \"([^\"]*)\" ATORequestType \"([^\"]*)\" OUVReportinglevelone \"([^\"]*)\" OUVReportingleveltwo \"([^\"]*)\" OUVReportinglevelthree \"([^\"]*)\" Fleet \"([^\"]*)\" Market \"([^\"]*)\" InternalJLRContact \"([^\"]*)\" DisposalRoute \"([^\"]*)\" ReworkRoute \"([^\"]*)\"$")
	public void fill_all_the_mandatory_fields_like_QuickReference_ATORequestType_OUVReportinglevelone_OUVReportingleveltwo_OUVReportinglevelthree_Fleet_Market_InternalJLRContact_DisposalRoute_ReworkRoute(
			String QuickReference, String ATORequestType, String OUVReportinglevelone, String OUVReportingleveltwo,
			String OUVReportinglevelthree, String fleet, String Market, String InternalJlrContact, String DisposalRoute,
			String ReworkRoute) throws Throwable {

		String Quick[] = QuickReference.split(",");
		QuickReference = CommonFunctions.readExcelMasterData(Quick[0], Quick[1], Quick[2]);

		String ATOreqType[] = ATORequestType.split(",");
		ATORequestType = CommonFunctions.readExcelMasterData(ATOreqType[0], ATOreqType[1], ATOreqType[2]);

		String Test = "TestDemo";
		String percentage = "70";

		// User enters Quick reference
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.QuickReference, QuickReference);
		CommonFunctions.attachScreenshot();

		// User selects ATO Request type
		ActionHandler.wait(4);
		ActionHandler.click(ATORequestContainer.ATORequestType);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(ATORequestType))));
		ActionHandler.wait(3);

		CommonFunctions.attachScreenshot();
		ActionHandler.setText(ATORequestContainer.QuickReference, Test);

		// Enter Booking Justification
		ActionHandler.wait(4);
		ActionHandler.setText(ATORequestContainer.BookingJustification, Test);
		CommonFunctions.attachScreenshot();

		// Read data from master excel sheet
		String OUVl1[] = OUVReportinglevelone.split(",");
		OUVReportinglevelone = CommonFunctions.readExcelMasterData(OUVl1[0], OUVl1[1], OUVl1[2]);

		String OUVl2[] = OUVReportingleveltwo.split(",");
		OUVReportingleveltwo = CommonFunctions.readExcelMasterData(OUVl2[0], OUVl2[1], OUVl2[2]);

		String OUVl3[] = OUVReportinglevelthree.split(",");
		OUVReportinglevelthree = CommonFunctions.readExcelMasterData(OUVl3[0], OUVl3[1], OUVl3[2]);

		String fle[] = fleet.split(",");
		fleet = CommonFunctions.readExcelMasterData(fle[0], fle[1], fle[2]);

		String market[] = Market.split(",");
		Market = CommonFunctions.readExcelMasterData(market[0], market[1], market[2]);

		String internaljlrc[] = InternalJlrContact.split(",");
		InternalJlrContact = CommonFunctions.readExcelMasterData(internaljlrc[0], internaljlrc[1], internaljlrc[2]);

		String dr[] = DisposalRoute.split(",");
		DisposalRoute = CommonFunctions.readExcelMasterData(dr[0], dr[1], dr[2]);

		String Rework[] = ReworkRoute.split(",");
		ReworkRoute = CommonFunctions.readExcelMasterData(Rework[0], Rework[1], Rework[2]);

		// click on view all dependencies
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.dependencies);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 1
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVREportinglevel1);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelone))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 2

		ActionHandler.click(ATORequestContainer.OUVReportingLevel2);
		ActionHandler.wait(5);

		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportingleveltwo))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// select OUV Reporting level 3

		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.OUVReportingLevel3);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(OUVReportinglevelthree))));
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.apply);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// Enter fleet

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.fleet, fleet);
		ActionHandler.wait(4);
		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(6);

		// Select market

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(ATORequestContainer.Market);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(Market))));
		ActionHandler.wait(5);

		// Enter jlr contact

		ActionHandler.setText(ATORequestContainer.Internaljlrcontact, InternalJlrContact);
		ActionHandler.wait(5);

		// proposed fleet date
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.ProposedAddtoFleetDate);
		ActionHandler.wait(3);

		ActionHandler.click(ATORequestContainer.TodayBtn);
		ActionHandler.wait(3);

		ActionHandler.wait(4);

		// hand in date
		// selectFutureDate method will selects the future date in calendar
		String HandinDate;
		HandinDate = CommonFunctions.selectFutureDate();

		ActionHandler.setText(ATORequestContainer.ProposedHandinDate, HandinDate);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		// percentage

		ActionHandler.wait(3);
		ActionHandler.setText(ATORequestContainer.percentage, percentage);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		// disposal route

		ActionHandler.click(ATORequestContainer.DisposalRoute);
		ActionHandler.wait(4);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(DisposalRoute))));
		ActionHandler.wait(3);

		// Rework route

		ActionHandler.click(ATORequestContainer.alldependecies);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(ATORequestContainer.ReworkRoute);
		ActionHandler.wait(5);
		ActionHandler.click(driver.findElement(By.xpath(ATORequestContainer.ReworkRoute(ReworkRoute))));
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.apply);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(3);

	}

	@Then("^Select a Replacement Vehicle \"([^\"]*)\"$")
	public void select_a_Replacement_Vehicle(String SearchOrdernumber) throws Throwable {
		String Text[] = SearchOrdernumber.split(",");
		SearchOrdernumber = CommonFunctions.readExcelMasterData(Text[0], Text[1], Text[2]);

		ActionHandler.pageCompleteScrollUp();
		ActionHandler.wait(2);

		// Select Replacement Vehicle
		ActionHandler.click(ATORequestContainer.Replacementvehicle);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(ATORequestContainer.Replacementvehicle, SearchOrdernumber);
		ActionHandler.wait(2);
		ActionHandler.pressEnter();
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select Replacement Vehicle");

		ActionHandler.click(ATORequestContainer.Selectvehicle);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(2);

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("ATO Request created");
	}

	@Then("^Validate that error message is displayed$")
	public void Validate_that_error_message_is_displayed() throws Throwable {

		// Validate that an error message is displayed
		VerifyHandler.verifyElementPresent(ATORequestContainer.VerifyErrorMessage);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Validate that an error message is displayed");
	}

// Select any ATO Requests from the list
	@Then("^Select any ATO Request from the list$")
	public void select_any_ATO_Request_from_the_list() throws Throwable {

		// User views all the listed OUV ATO Request
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(ATORequestContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV ATO Request");

		ActionHandler.click(ATORequestContainer.ATOSel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select ATO Request");
		CommonFunctions.attachScreenshot();
	}

	// verify the field Vehicle User is present
	@Then("^Under ATO Summary, verify the field Vehicle User is present$")
	public void under_ATO_Summary_verify_the_field_Vehicle_User_is_present() throws Throwable {

		ActionHandler.wait(4);
		ActionHandler.scrollDown();
		ActionHandler.wait(4);
		VerifyHandler.verifyElementPresent(ATORequestContainer.VehicleUser);

		Reporter.addStepLog("Vehicle User is present");
		CommonFunctions.attachScreenshot();
	}

	// verify the field Vehicle User is present
	@Then("^Under ATO Summary, verify the field Vehicle User is not present$")
	public void under_ATO_Summary_verify_the_field_Vehicle_User_is_not_present() throws Throwable {

		ActionHandler.wait(4);
		ActionHandler.scrollDown();
		ActionHandler.wait(8);
		if (VerifyHandler.verifyElementPresent(ATORequestContainer.VehicleUser)) {
			Reporter.addStepLog("Vehicle User is present");
		} else {
			Reporter.addStepLog("Vehicle User is not present");
		}
	}

}
