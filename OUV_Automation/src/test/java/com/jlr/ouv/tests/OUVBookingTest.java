package com.jlr.ouv.tests;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.ouv.constants.Constants;
import com.jlr.ouv.containers.ATORequestContainer;
import com.jlr.ouv.containers.OUVBookingContainer;
import com.jlr.ouv.containers.OUVCalendarContainer;
import com.jlr.ouv.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class OUVBookingTest extends TestBaseCC {
	public WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(OUVBookingTest.class.getName());
	public OUVCalendarContainer OUVCalendarContainer = PageFactory.initElements(driver, OUVCalendarContainer.class);
	public ATORequestContainer ATORequestContainer = PageFactory.initElements(driver, ATORequestContainer.class);
	public OUVBookingContainer OUVBookingContainer = PageFactory.initElements(driver, OUVBookingContainer.class);
	JavaScriptUtil javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	String ID = "Booking";

	public void onStart() {
		setupTest("OUVTest");
		driver = getDriver();
		OUVCalendarContainer = PageFactory.initElements(driver, OUVCalendarContainer.class);
		ATORequestContainer = PageFactory.initElements(driver, ATORequestContainer.class);
		javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
		OUVBookingContainer = PageFactory.initElements(driver, OUVBookingContainer.class);
	}

	// Used to navigate to OUV
	public void navigateToOUV() throws Exception {
		String OUV = "OUV";
		ActionHandler.wait(3);
		Reporter.addStepLog("User tries to navigate to OUV Portal");
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.jlrText)) {
			ActionHandler.wait(2);
			ActionHandler.click(OUVCalendarContainer.menuBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.setText(OUVCalendarContainer.searchBox, OUV);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.selectMenu(OUV))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

	// Used for List view in ATO
	public void ListViewATO() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.recentlyViewedList);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.AllATORequest);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User navigates to AllATORequest ATO Requests");
	}

	// User access OUV Portal
	@Given("^User access OUV Portal with UserName \"([^\"]*)\" Password \"([^\"]*)\"$")
	public void user_Access_OUV_Portal(String userName, String password) throws Exception {
		String User[] = userName.split(",");
		userName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = password.split(",");
		password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		LOGGER.debug("Test Begin......");
		Reporter.addStepLog("User Access OUV Portal");
		onStart();
		driver.get(Constants.OUVURL);
		ActionHandler.wait(7);
		Reporter.addStepLog("User Logins to OUV Portal");

		ActionHandler.setText(OUVCalendarContainer.userNameTextBox, userName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVCalendarContainer.passwordTextBox, password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(7);

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.Verifycode)) {
			ActionHandler.wait(60);

			ActionHandler.click(OUVCalendarContainer.vCodeTextBox);
			ActionHandler.wait(5);
			Reporter.addStepLog("User have entered Verification Code");
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();
			ActionHandler.click(OUVCalendarContainer.verifyBtn);
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			navigateToOUV();

		}
		ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);

		if (!VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome)) {
			ActionHandler.wait(4);
			driver.navigate().refresh();
			ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);
		}
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome);
	}

	// User Navigates to OUV Booking menu
	@And("^User Navigates to OUV Booking menu$")
	public void User_Navigates_to_OUV_Booking_menu() throws Exception {
		ActionHandler.wait(5);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.OUVBookingWindow);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to OUV Booking Window");
	}

	// User search an OUV Booking with status
	@Then("^User search an OUV Booking with status \"([^\"]*)\"$")
	public void User_Search_OUV_Booking(String status) throws Exception {
		String s[] = status.split(",");
		status = CommonFunctions.readExcelMasterData(s[0], s[1], s[2]);

		ActionHandler.wait(2);
		ListViewATO();
		Reporter.addStepLog("User navigates to all OUV Booking");

		ActionHandler.setText(OUVBookingContainer.OUVBookingSearchBox, status);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.pressEnter();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User get list of OUV Bookings");

		ActionHandler.click(OUVBookingContainer.firstOUVBooking);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User opens first OUV booking");

	}

	// User tries to cancel an OUV Booking which is pending for approval
	@And("^User tries to cancel an OUV Booking which is pending for approval$")
	public void User_Tries_Cancel_OUV_Booking() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(OUVBookingContainer.sideDropDown);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.cancelBooking);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVBookingContainer.saveBookingStatus);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User tries to cancel OUV Booking");

		VerifyHandler.verifyElementPresent(OUVBookingContainer.errorMessage);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.cancelBookingStatus);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// User do logout from OUV Portal
	@Then("^User do logout from OUV Portal$")
	public void Logout_from_OUV_Portal() throws Exception {
		ActionHandler.wait(2);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.homeTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.loginImg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.logout);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Logout from OUV Portal");
		driver.quit();
	}

	// User tries to cancel an OUV Booking which is approved
	@And("^User tries to cancel an OUV Booking which is approved$")
	public void User_tries_cancel_OUV_Booking_Approved() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVBookingContainer.cancelBookingApproved);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.saveBookingStatus);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User cancels an OUV Booking which is approved");

		VerifyHandler.verifyElementPresent(OUVBookingContainer.cancelBookingStatusVerify);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
	}

	// User is able to search for the Booking ID in the OUV Booking Tab
	@Then("^Search for an OUV Booking with Approved status Search List \"([^\"]*)\"$")
	public void search_for_an_OUV_Booking_with_Approved_status_Search_List(String searchBooking) throws Throwable {
		String Search[] = searchBooking.split(",");
		searchBooking = CommonFunctions.readExcelMasterData(Search[0], Search[1], Search[2]);

		ActionHandler.click(OUVBookingContainer.SelectBookings);
		ActionHandler.wait(5);
		ActionHandler.click(OUVBookingContainer.SelectAll1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVBookingContainer.SearchBookings);
		ActionHandler.wait(5);
		ActionHandler.setText(OUVBookingContainer.SearchBookings, searchBooking);
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Search for an OUV Booking with Approved status Search List");

	}

	// User is able to create a new Booking on OUV Booking window
	@Then("^Click on New$")
	public void click_on_New() throws Throwable {
		ActionHandler.click(OUVBookingContainer.New1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Click on New");
	}

	// Select the OUV Booking Editable radiobutton and click Next
	@Then("^Select the OUV Booking Editable radiobutton and click Next$")
	public void select_the_OUV_Booking_Editable_radiobutton_and_click_Next() throws Throwable {
		ActionHandler.click(OUVBookingContainer.Next);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Select the OUV Booking Editable radiobutton and click Next");
	}

	// Enter all the mandatory details and click on Save Booking Quick Reference
	@Then("^Enter all the mandatory details and click on Save Booking Quick Reference \"([^\"]*)\" Reason For Booking \"([^\"]*)\" Booking Type \"([^\"]*)\" Journey Type \"([^\"]*)\" Booking request type \"([^\"]*)\" Booking Justification \"([^\"]*)\" Passout Type \"([^\"]*)\" Start Date \"([^\"]*)\" End Date \"([^\"]*)\" Other Location \"([^\"]*)\" Internal JLR \"([^\"]*)\" Email Id \"([^\"]*)\" Driverone \"([^\"]*)\"$")
	public void enter_all_the_mandatory_details_and_click_on_Save_Booking_Quick_Reference_Reason_For_Booking_Booking_Type_Journey_Type_Booking_request_type_Booking_Justification_Passout_Type_Start_Date_End_Date_Other_Location_Internal_JLR_Email_Id_Driverone(
			String BookingQuick, String ReasonforBooking, String BookingType, String JourneyType,
			String Bookingrequesttype, String Bookingjustification, String PassoutType, String StratDate,
			String EndDate, String OtherLocation, String InternalJLR, String Mail, String Driverone) throws Throwable {

		String BookingQR[] = BookingQuick.split(",");
		BookingQuick = CommonFunctions.readExcelMasterData(BookingQR[0], BookingQR[1], BookingQR[2]);

		String ReasonFR[] = ReasonforBooking.split(",");
		ReasonforBooking = CommonFunctions.readExcelMasterData(ReasonFR[0], ReasonFR[1], ReasonFR[2]);

		String BookingTyp[] = BookingType.split(",");
		BookingType = CommonFunctions.readExcelMasterData(BookingTyp[0], BookingTyp[1], BookingTyp[2]);

		String journytyp[] = JourneyType.split(",");
		JourneyType = CommonFunctions.readExcelMasterData(journytyp[0], journytyp[1], journytyp[2]);

		String Bookingrqtyp[] = Bookingrequesttype.split(",");
		Bookingrequesttype = CommonFunctions.readExcelMasterData(Bookingrqtyp[0], Bookingrqtyp[1], Bookingrqtyp[2]);

		String Bookingjsti[] = Bookingjustification.split(",");
		Bookingjustification = CommonFunctions.readExcelMasterData(Bookingjsti[0], Bookingjsti[1], Bookingjsti[2]);

		String PassoutTyp[] = PassoutType.split(",");
		PassoutType = CommonFunctions.readExcelMasterData(PassoutTyp[0], PassoutTyp[1], PassoutTyp[2]);

		String StartD[] = StratDate.split(",");
		StratDate = CommonFunctions.readExcelMasterData(StartD[0], StartD[1], StartD[2]);

		String EndD[] = EndDate.split(",");
		EndDate = CommonFunctions.readExcelMasterData(EndD[0], EndD[1], EndD[2]);

		String otherlocation[] = OtherLocation.split(",");
		OtherLocation = CommonFunctions.readExcelMasterData(otherlocation[0], otherlocation[1], otherlocation[2]);

		String IJLR[] = InternalJLR.split(",");
		InternalJLR = CommonFunctions.readExcelMasterData(IJLR[0], IJLR[1], IJLR[2]);

		String EM[] = Mail.split(",");
		Mail = CommonFunctions.readExcelMasterData(EM[0], EM[1], EM[2]);

		String Drive[] = Driverone.split(",");
		Driverone = CommonFunctions.readExcelMasterData(Drive[0], Drive[1], Drive[2]);

		ActionHandler.click(OUVBookingContainer.QReffrence);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVBookingContainer.QReffrence, BookingQuick);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVBookingContainer.ReasonForBooking1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVBookingContainer.ReasonForBooking1, ReasonforBooking);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.BookingReqType);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(Bookingrequesttype))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.Dependencies);
		ActionHandler.wait(5);

		ActionHandler.click(OUVBookingContainer.BookingType);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVBookingContainer.ValueSelection(BookingType))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.BookingJustification1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(Bookingjustification))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.PassoutType1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVBookingContainer.ValueSelection(PassoutType))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.Apply);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		ActionHandler.click(OUVBookingContainer.JourneyType1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.ValueSelection(JourneyType))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.StrtDate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVBookingContainer.StrtDate, StratDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVBookingContainer.EndDate);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.setText(OUVBookingContainer.EndDate, EndDate);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVBookingContainer.OtherLocation1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVBookingContainer.OtherLocation1, OtherLocation);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVBookingContainer.JLRContact1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVBookingContainer.JLRContact1, InternalJLR);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVBookingContainer.JLRMail);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVBookingContainer.JLRMail, Mail);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVBookingContainer.Drive1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVBookingContainer.Drive1, Driverone);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(5);

		ActionHandler.click(OUVBookingContainer.Saved);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User able to Enter all the mandatory details and click on Save Booking Quick Reference");

	}

	// User is able to change the owner name for the Booking Id on OUV Booking
	// Search Window
	@Then("^Select any record and click on the Change Owner option on OUV Booking Search Window$")
	public void select_any_record_and_click_on_the_Change_Owner_option_on_OUV_Booking_Search_Window() throws Throwable {
		ActionHandler.wait(5);
		ActionHandler.click(OUVBookingContainer.ChangeDropdown);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVBookingContainer.ChangeOwner1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User able to Select any record and click on the Change Owner option on OUV Booking Search Window");
	}

	// Search for the owner name who is member of the fleet and click on submit
	// button
	@Then("^Search for the owner name who is member of the fleet \"([^\"]*)\" and click on submit button$")
	public void search_for_the_owner_name_who_is_member_of_the_fleet_and_click_on_submit_button(String FleetMember)
			throws Throwable {

		String fm[] = FleetMember.split(",");
		FleetMember = CommonFunctions.readExcelMasterData(fm[0], fm[1], fm[2]);

		ActionHandler.click(OUVBookingContainer.SearchUsers1);
		ActionHandler.wait(5);
		ActionHandler.setText(OUVBookingContainer.SearchUsers1, FleetMember);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVBookingContainer.searchfleetmem);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.SelectFleetMem);
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVBookingContainer.Submited);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"User able to Search for the owner name who is member of the fleet and click on submit button");
	}

	@And("^User navigates to OUV Bookings$")
	public void user_navigates_to_OUV_Bookings() throws Throwable {
		ActionHandler.wait(7);
		javascriptUtil.clickElementByJS(OUVCalendarContainer.OUVBookings);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User naviagte to OUV bookings");
	}

	@Then("^Click on Change owner icon and select a new owner \"([^\"]*)\"$")
	public void click_on_Change_owner_icon_and_select_a_new_owner(String changeowner) throws Throwable {

		String co[] = changeowner.split(",");
		changeowner = CommonFunctions.readExcelMasterData(co[0], co[1], co[2]);

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.ChangeOwner);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on change owner icon");

		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.searchusers);
		ActionHandler.wait(2);
		ActionHandler.setText(OUVCalendarContainer.searchusers, changeowner);

		Actions act1 = new Actions(driver);

		ActionHandler.wait(2);
		act1.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(2);
		act1.sendKeys(Keys.ENTER).build().perform();

		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.ChangeOwnerBtn);
		ActionHandler.wait(3);
	}

	// Error message will display when user tries to change the owner

	@Then("^Validate that user cannot change the owner for the booking$")
	public void validate_that_user_cannot_change_the_owner_for_the_booking() throws Throwable {

		VerifyHandler.verifyElementPresent(OUVCalendarContainer.ChangeownerError);
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies the error message displayed");
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVCalendarContainer.cancelowner);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User clicks on Cancel button");

	}

	// User searches for an OUV Booking with status
	@Then("^User searches for the OUV Booking with status \"([^\"]*)\"$")
	public void user_searches_for_the_OUV_Booking_with_status(String Search) throws Throwable {
		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		ActionHandler.click(OUVCalendarContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Bookings");

		ActionHandler.click(OUVCalendarContainer.OUVBookingsearch);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVCalendarContainer.OUVBookingsearch, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an OUV Booking");
		CommonFunctions.attachScreenshot();
	}

	@And("^User navigate to Vehicle Loan Agreements$")
	public void User_navigate_to_vehicle_loan_agreements() throws Exception {
		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.VLAAgreement);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigate to VLA Agreement page");
	}

	@And("^User view details of available VLA$")
	public void User_view_details_available_VLA() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVCalendarContainer.VLAFirst);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User can view details of available VLA");
	}

	@Then("^User navigates to Notes & Attachments section$")
	public void User_navigates_notes_attachments_section() throws Exception {
		ActionHandler.wait(3);
		ActionHandler.click(OUVBookingContainer.notesAndAttachments);
		ActionHandler.wait(6);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User navigates to Notes & Attachments section");
	}

	@And("^User is able to view PDF Document in OUV VLA$")
	public void User_is_able_to_view_PDF_Document_OUV_VLA() throws Exception {
		ActionHandler.wait(3);
		VerifyHandler.verifyElementPresent(OUVBookingContainer.PDFDocument);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(4);
		ActionHandler.click(OUVBookingContainer.PDFDocument);
		ActionHandler.wait(3);
		Reporter.addStepLog("User can open PDF document");

		Set<String> tabs = driver.getWindowHandles();
		int noOfTab = tabs.size();
		System.out.println("Number of tabs = " + noOfTab);
		driver.switchTo().window(tabs.toArray()[noOfTab - 2].toString());
		ActionHandler.wait(4);
		Reporter.addStepLog("User navigate back to main window");

		ActionHandler.wait(2);
		driver = getDriver();
		OUVCalendarContainer = PageFactory.initElements(driver, OUVCalendarContainer.class);

		ActionHandler.wait(2);
		ActionHandler.click(OUVCalendarContainer.loginImg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVCalendarContainer.logout);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Logout from OUV Portal");
		driver.quit();
	}

	@Then("^Verify that user is not able to view PDF Document in an OUV VLA$")
	public void Verify_user_is_not_able_to_view_PDF_document() throws Exception {
		ActionHandler.wait(2);
		VerifyHandler.verifyElementPresent(OUVBookingContainer.noItemsVLA);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User cannot be able to view PDF Document in an OUV VLA");
	}

	// user verifies the vehicle user details under Driver/vehicle user
	@Then("^Verify that the Vehicle user details are not present in an OUV Booking$")
	public void verify_that_the_Vehicle_user_details_are_not_present_in_an_OUV_Booking() throws Throwable {
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog(
				"The fields Encrypted Company (VLA), Encrypted Full Name (VLA), Encrypted Driver 1, Encrypted Driver 2 are not present under driver/vehicle user");
	}

	// Select any OUV Booking from the list
	@Then("^Select any OUV Booking from the list$")
	public void select_any_OUV_Booking_from_the_list() throws Throwable {

		ActionHandler.click(OUVBookingContainer.SelectBookings);
		ActionHandler.wait(5);
		ActionHandler.click(OUVBookingContainer.SelectAll1);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select any OUV Booking from the list");
		CommonFunctions.attachScreenshot();
	}

	// select the OUV Booking
	@Then("^select the OUV Bookings$")
	public void select_the_Request() throws Throwable {
		ActionHandler.click(OUVCalendarContainer.OUVSel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select OUV Booking");
		CommonFunctions.attachScreenshot();

	}

	// Under Driver/Vehicle User, verify all the fields
	@Then("^Under Driver/Vehicle User, verify all the fields$")
	public void under_Driver_Vehicle_User_verify_all_the_fields() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);
		VerifyHandler.verifyElementPresent(OUVBookingContainer.EncryptedCompany);
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies the error Encrypted Company is present");
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVBookingContainer.EncryptedFullName);
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies the Encrypted Full Name is present");
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVBookingContainer.EncryptedDriver1);
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies the Encrypted Driver 1");
		CommonFunctions.attachScreenshot();

		VerifyHandler.verifyElementPresent(OUVBookingContainer.EncryptedDriver2);
		ActionHandler.wait(2);
		Reporter.addStepLog("User verifies the Encrypted Driver 2");
		CommonFunctions.attachScreenshot();
	}

}
