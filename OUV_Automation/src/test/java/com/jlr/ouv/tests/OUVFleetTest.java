package com.jlr.ouv.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.handlers.VerifyHandler;
import com.jlr.base.TestBaseCC;
import com.jlr.ouv.constants.Constants;
import com.jlr.ouv.containers.ATORequestContainer;
import com.jlr.ouv.containers.OUVCalendarContainer;
import com.jlr.ouv.containers.OUVFleetContainer;
import com.jlr.ouv.utilities.CommonFunctions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class OUVFleetTest extends TestBaseCC {

	public WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(OUVCalendarTest.class.getName());
	public OUVCalendarContainer OUVCalendarContainer = PageFactory.initElements(driver, OUVCalendarContainer.class);
	public ATORequestContainer ATORequestContainer = PageFactory.initElements(driver, ATORequestContainer.class);
	public OUVFleetContainer OUVFleetContainer = PageFactory.initElements(driver, OUVFleetContainer.class);
	JavaScriptUtil javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	String OUVFleet = "OUV Fleets";
	String Reports = "Reports";

	public void onStart() {
		setupTest("OUVTest");
		driver = getDriver();
		OUVCalendarContainer = PageFactory.initElements(driver, OUVCalendarContainer.class);
		ATORequestContainer = PageFactory.initElements(driver, ATORequestContainer.class);
		OUVFleetContainer = PageFactory.initElements(driver, OUVFleetContainer.class);
		javascriptUtil = PageFactory.initElements(driver, JavaScriptUtil.class);
	}

	// OUV-238

	@Then("^Open any OUV Fleet from the list$")
	public void open_any_OUV_Fleet_from_the_list() throws Throwable {
		ActionHandler.click(OUVFleetContainer.SelectFleet);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// verify that user is able to view Details of Approval Route
	@Then("^verify that user is able to view Detail of Approval Route$")
	public void verify_that_user_is_able_to_view_Detail_of_Approval_Route() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.detailTab)) {
			ActionHandler.click(OUVFleetContainer.detailTab);
			ActionHandler.wait(5);
			Reporter.addStepLog("User is able to view details tab");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is not able to view details tab");
			CommonFunctions.attachScreenshot();
		}

		// Verify that market information is presen
		VerifyHandler.verifyElementPresent(OUVFleetContainer.Marketinfo);
		ActionHandler.wait(5);
		Reporter.addStepLog("Verify that market information is present");
		CommonFunctions.attachScreenshot();

	}

	// Verify the edit options are not available
	@Then("^Verify the edit options are not available$")
	public void verify_the_edit_options_are_not_available() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.ApprovalRouteNameedit)) {
			Reporter.addStepLog("Edit option is available");
		} else {
			Reporter.addStepLog("Edit option is not available");
		}
	}

	// User tries to navigate to OUV Portal
	public void navigateToOUV() throws Exception {
		String OUV = "OUV";
		ActionHandler.wait(3);
		Reporter.addStepLog("User tries to navigate to OUV Portal");
		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.jlrText)) {
			ActionHandler.wait(2);
			ActionHandler.click(OUVCalendarContainer.menuBtn);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			// Searches for OUV
			ActionHandler.setText(OUVCalendarContainer.searchBox, OUV);
			ActionHandler.wait(3);
			CommonFunctions.attachScreenshot();

			// User navigate to SVO Portal
			ActionHandler.click(driver.findElement(By.xpath(OUVCalendarContainer.selectMenu(OUV))));
			ActionHandler.wait(10);
			CommonFunctions.attachScreenshot();

			Reporter.addStepLog("User navigate to SVO Portal");
		}
	}

	// User navigates to AllATORequest ATO Requests
	public void ListViewATO() throws Exception {

		// User navigates to AllATORequest ATO Requests
		ActionHandler.wait(3);
		ActionHandler.click(ATORequestContainer.recentlyViewedList);
		ActionHandler.wait(2);
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.AllATORequest);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Reporter.addStepLog("User navigates to AllATORequest ATO Requests");
	}

	// User access OUV Portal
	@Given("^User access OUV Portal with user \"([^\"]*)\" and password as \"([^\"]*)\"$")
	public void access_the_OUV_Portal_with_user_and_password_as(String UserName, String Password) throws Throwable {
		String User[] = UserName.split(",");
		UserName = CommonFunctions.readExcelMasterData(User[0], User[1], User[2]);

		String pass[] = Password.split(",");
		Password = CommonFunctions.readExcelMasterData(pass[0], pass[1], pass[2]);

		// User Logins to OUV Portal
		LOGGER.debug("Test Begin......");
		Reporter.addStepLog("User Access OUV Portal");
		onStart();
		driver.get(Constants.OUVURL);
		ActionHandler.wait(7);
		Reporter.addStepLog("User Logins to OUV Portal");

		// User enters User name
		ActionHandler.setText(OUVCalendarContainer.userNameTextBox, UserName);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// user enters password
		ActionHandler.setText(OUVCalendarContainer.passwordTextBox, Password);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// user click on login button
		ActionHandler.click(OUVCalendarContainer.loginBtn);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(20);

		if (VerifyHandler.verifyElementPresent(OUVCalendarContainer.Verifycode)) {
			ActionHandler.wait(60);

			// User have entered Verification Code
			ActionHandler.click(OUVCalendarContainer.vCodeTextBox);
			ActionHandler.wait(5);
			Reporter.addStepLog("User have entered Verification Code");
			ActionHandler.wait(7);
			CommonFunctions.attachScreenshot();

			// User click on Verify button
			ActionHandler.click(OUVCalendarContainer.verifyBtn);
			ActionHandler.wait(15);
			CommonFunctions.attachScreenshot();

			navigateToOUV();

		}
		ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);

		if (!VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome)) {
			ActionHandler.wait(4);
			driver.navigate().refresh();
			ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);
		}
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome);
	}

	// Logout user from OUV Portal
	@Then("^Logout user from OUV Portal$")
	public void Logout_OUV() throws Exception {
		javascriptUtil.clickElementByJS(ATORequestContainer.homeTab);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// User click on Logout logo
		ActionHandler.click(ATORequestContainer.loginImg);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();

		// User click on log out button
		ActionHandler.click(ATORequestContainer.logout);
		ActionHandler.wait(1);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Logout from OUV Portal");
		driver.quit();
	}

	// User navigates to OUV Fleet
	@Then("^User navigates to OUV Fleet$")
	public void user_navigates_to_OUV_Fleet() throws Throwable {

		// Click on menu
		ActionHandler.wait(5);
		ActionHandler.click(OUVFleetContainer.Menuopen);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on menu");

		// Enter OUV fleet
		ActionHandler.clearAndSetText(OUVFleetContainer.appsearch, OUVFleet);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter OUV fleet");

		// Click on OUV Fleet
		ActionHandler.click(OUVFleetContainer.OUVFleet);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on OUV Fleet");

	}

	// search for the Fleet
	@Then("^search for the Fleet \"([^\"]*)\"$")
	public void search_for_the_Fleet(String Search) throws Throwable {

		String Sea[] = Search.split(",");
		Search = CommonFunctions.readExcelMasterData(Sea[0], Sea[1], Sea[2]);

		// click on Recently viewed list
		ActionHandler.wait(2);
		ActionHandler.click(ATORequestContainer.RecentlyViewed);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);

		// selects All from the filter list
		ActionHandler.click(ATORequestContainer.All);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(2);
		Reporter.addStepLog("User views all the listed OUV Fleet");

		// Search for an Fleet
		ActionHandler.click(OUVFleetContainer.SearchFleet);
		CommonFunctions.attachScreenshot();
		ActionHandler.wait(1);
		ActionHandler.clearAndSetText(OUVFleetContainer.SearchFleet, Search);
		CommonFunctions.attachScreenshot();
		ActionHandler.pressEnter();
		ActionHandler.wait(10);
		Reporter.addStepLog("Search for an Fleet");
		CommonFunctions.attachScreenshot();

	}

	// Select the fleet
	@Then("^Select the fleet$")
	public void select_the_fleet() throws Throwable {

		ActionHandler.click(ATORequestContainer.ATOSel1);
		ActionHandler.wait(15);
		Reporter.addStepLog("Select Fleet");
		CommonFunctions.attachScreenshot();
	}

	// user clicks on edit fleet$
	@When("^user clicks on edit fleet$")
	public void user_clicks_on_edit_fleet() throws Throwable {

		// Select edit name
		ActionHandler.click(OUVFleetContainer.editfleetname);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select edit name");
		CommonFunctions.attachScreenshot();
	}

	// User changes the fleet name and market
	@When("^User changes the fleet name to \"([^\"]*)\" and market to \"([^\"]*)\"$")
	public void user_changes_the_fleet_name_to_and_market_to(String name, String market) throws Throwable {

		String nam[] = name.split(",");
		name = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		String mar[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(mar[0], mar[1], mar[2]);

		// Enter OUV fleet name
		ActionHandler.clearAndSetText(OUVFleetContainer.fleetname, name);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter OUV fleet name");

		// Click on fleet market
		ActionHandler.click(OUVFleetContainer.fleetmarket);
		ActionHandler.wait(5);
		Reporter.addStepLog("Click on fleet market");
		CommonFunctions.attachScreenshot();

		// Select fleet market
		javascriptUtil.clickElementByJS(driver.findElement(By.xpath(ATORequestContainer.ValueSelection(market))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select fleet market");

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();

	}

	// Verify the changes in fleet
	@Then("^Verify the changes in fleet$")
	public void verify_the_changes_in_fleet() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(ATORequestContainer.CancelEdit)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("The changes are successful");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(5);
			Reporter.addStepLog("The changes are not successful");
			CommonFunctions.attachScreenshot();
		}

	}

	// Navigate to OUV Fleet members
	@Then("^Navigate to OUV Fleet members$")
	public void navigate_to_OUV_Fleet_members() throws Throwable {

		// click on Fleet members link
		ActionHandler.click(OUVFleetContainer.FleetMembers);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Fleet members link");
		CommonFunctions.attachScreenshot();
	}

	// Click on new and add new member
	@When("^click on new add new member \"([^\"]*)\"$")
	public void click_on_new_add_new_member(String member) throws Throwable {

		String nam[] = member.split(",");
		member = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		// Select Fleet members new
		ActionHandler.click(OUVFleetContainer.FleetMembersnew);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Fleet members new");
		CommonFunctions.attachScreenshot();

		// Enter OUV fleet member name
		ActionHandler.clearAndSetText(OUVFleetContainer.FleetMembersinfo, member);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter OUV fleet member name");

		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);

		// click on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();

		// Verify member present
		VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.ValueSelection(member))));
		ActionHandler.wait(5);
		Reporter.addStepLog("Verify member present");
		CommonFunctions.attachScreenshot();

	}

	// delete the added fleet member
	@Then("^delete the added fleet member$")
	public void delete_the_added_fleet_member() throws Throwable {

		// refresh the page
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User clicks on Show Actions
		ActionHandler.click(OUVFleetContainer.FleetMembersdelete);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();

		// User clicks on delete button
		ActionHandler.click(ATORequestContainer.Delete);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on delete button");
		CommonFunctions.attachScreenshot();

		// User deletes the ATO Request Line Items
		ActionHandler.click(ATORequestContainer.DeleteLineItems);
		ActionHandler.wait(2);
		Reporter.addStepLog("User deletes the ATO Request Line Items");
		CommonFunctions.attachScreenshot();

	}

	// Navigate to OUV Approval Route
	@Then("^Navigate to OUV Approval Route$")
	public void navigate_to_OUV_Approval_Route() throws Throwable {

		// Select Approval Route link
		ActionHandler.click(OUVFleetContainer.ApprovalRoute);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Approval Route link");
		CommonFunctions.attachScreenshot();
	}

	// verify that user is able to view Details of Approval Route
	@Then("^verify that user is able to view Details of Approval Route$")
	public void verify_that_user_is_able_to_view_Details_of_Approval_Route() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.detailTab)) {
			ActionHandler.click(OUVFleetContainer.detailTab);
			ActionHandler.wait(5);
			Reporter.addStepLog("User is able to view details tab");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is not able to view details tab");
			CommonFunctions.attachScreenshot();
		}

		// Verify that market information is presen
		VerifyHandler.verifyElementPresent(OUVFleetContainer.Marketinfo);
		ActionHandler.wait(5);
		Reporter.addStepLog("Verify that market information is present");
		CommonFunctions.attachScreenshot();

	}

	// user clicks on edit Approval Route fleet
	@When("^user clicks on edit Approval Route fleet$")
	public void user_clicks_on_edit_Approval_Route_fleet() throws Throwable {

		// Refresh the page
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// Select edit name
		ActionHandler.click(OUVFleetContainer.ApprovalRouteNameedit);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select edit name");
		CommonFunctions.attachScreenshot();
	}

	// User changes the Approval Route fleet name
	@When("^User changes the Approval Route fleet name to \"([^\"]*)\"$")
	public void user_changes_the_Approval_Route_fleet_name_to(String name) throws Throwable {

		String nam[] = name.split(",");
		name = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		// Enter OUV Route fleet name
		ActionHandler.clearAndSetText(OUVFleetContainer.fleetname, name);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter OUV Route fleet name");

		// click on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();

	}

	// User enters all the mandatory fields available on the form OUV Fleet Name
	@Then("^User enters all the mandatory fields available on the form OUV Fleet Name \"([^\"]*)\" Market \"([^\"]*)\" OUV Category One \"([^\"]*)\" Fleet Manager \"([^\"]*)\" Fleet Administrator One \"([^\"]*)\" Cost Center \"([^\"]*)\" Account \"([^\"]*)\" Day Pass Approve \"([^\"]*)\" Overnight Pass Approver \"([^\"]*)\" Weekend Pass Approver \"([^\"]*)\" Weekend Pass Approver Alternative \"([^\"]*)\" Approval Route \"([^\"]*)\" Registration Approver \"([^\"]*)\" Disposal Approver \"([^\"]*)\"$")
	public void user_enters_all_the_mandatory_fields_available_on_the_form_OUV_Fleet_Name_Market_OUV_Category_One_Fleet_Manager_Fleet_Administrator_One_Cost_Center_Account_Day_Pass_Approve_Overnight_Pass_Approver_Weekend_Pass_Approver_Weekend_Pass_Approver_Alternative_Approval_Route_Registration_Approver_Disposal_Approver(
			String Fleetname, String market, String Catogoryone, String FleetManager, String FleetAdministratorone,
			String CostCenter, String Account, String DayPassApprove, String OvernightPassApprover, String WeekendPA,
			String WeekendPAA, String ApprovalRoute, String RegistrationApprover, String DisposalApprover)
			throws Throwable {

		// Retrieve data from master data excel sheet
		String Ft[] = Fleetname.split(",");
		Fleetname = CommonFunctions.readExcelMasterData(Ft[0], Ft[1], Ft[2]);

		String Mt[] = market.split(",");
		market = CommonFunctions.readExcelMasterData(Mt[0], Mt[1], Mt[2]);

		String CO[] = Catogoryone.split(",");
		Catogoryone = CommonFunctions.readExcelMasterData(CO[0], CO[1], CO[2]);

		String FM[] = FleetManager.split(",");
		FleetManager = CommonFunctions.readExcelMasterData(FM[0], FM[1], FM[2]);

		String FAO[] = FleetAdministratorone.split(",");
		FleetAdministratorone = CommonFunctions.readExcelMasterData(FAO[0], FAO[1], FAO[2]);

		String CC[] = CostCenter.split(",");
		CostCenter = CommonFunctions.readExcelMasterData(CC[0], CC[1], CC[2]);

		String AC[] = Account.split(",");
		Account = CommonFunctions.readExcelMasterData(AC[0], AC[1], AC[2]);

		String DPA[] = DayPassApprove.split(",");
		DayPassApprove = CommonFunctions.readExcelMasterData(DPA[0], DPA[1], DPA[2]);

		String ONPA[] = OvernightPassApprover.split(",");
		OvernightPassApprover = CommonFunctions.readExcelMasterData(ONPA[0], ONPA[1], ONPA[2]);

		String WPA[] = WeekendPA.split(",");
		WeekendPA = CommonFunctions.readExcelMasterData(WPA[0], WPA[1], WPA[2]);

		String WPAA[] = WeekendPAA.split(",");
		WeekendPAA = CommonFunctions.readExcelMasterData(WPAA[0], WPAA[1], WPAA[2]);

		String AR[] = ApprovalRoute.split(",");
		ApprovalRoute = CommonFunctions.readExcelMasterData(AR[0], AR[1], AR[2]);

		String RA[] = RegistrationApprover.split(",");
		RegistrationApprover = CommonFunctions.readExcelMasterData(RA[0], RA[1], RA[2]);

		String DA[] = DisposalApprover.split(",");
		DisposalApprover = CommonFunctions.readExcelMasterData(DA[0], DA[1], DA[2]);

		// enters OUV name
		ActionHandler.click(OUVFleetContainer.OUVname);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.OUVname, Fleetname);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// selects market
		ActionHandler.click(OUVFleetContainer.Market);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVFleetContainer.Market(market))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Selects category one
		ActionHandler.click(OUVFleetContainer.Cetagory1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(driver.findElement(By.xpath(OUVFleetContainer.Cetagory1(Catogoryone))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Enter fleet manager
		ActionHandler.click(OUVFleetContainer.FtManager);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.FtManager, FleetManager);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		// Enters Fleet Administrator one
		ActionHandler.click(OUVFleetContainer.FtAdministrator);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.FtAdministrator, FleetAdministratorone);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		// Enters cost center
		ActionHandler.click(OUVFleetContainer.Costcenter);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.Costcenter, CostCenter);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Enters Account
		ActionHandler.click(OUVFleetContainer.Account);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.Account, Account);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(10);

		// Enters Day pass Approver
		ActionHandler.click(OUVFleetContainer.Daypass);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.Daypass, DayPassApprove);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Enter Over night pass approver
		ActionHandler.click(OUVFleetContainer.Overnight);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.Overnight, OvernightPassApprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Eneter Week end pass approver
		ActionHandler.click(OUVFleetContainer.weekendpassapprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.weekendpassapprover, WeekendPA);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Enter Week end pass Approval alternative
		ActionHandler.click(OUVFleetContainer.weekendpassapproveralternative);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.weekendpassapproveralternative, WeekendPAA);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		// Enters Approval Route
		ActionHandler.click(OUVFleetContainer.Approvalroute);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.Approvalroute, ApprovalRoute);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Enter Registration Approver
		ActionHandler.click(OUVFleetContainer.Registrationapprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.Registrationapprover, RegistrationApprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Enter disposal Approver
		ActionHandler.click(OUVFleetContainer.disposalapprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.disposalapprover, DisposalApprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Click on Save button
	@Then("^Click on Save button$")
	public void click_on_Save_button() throws Throwable {
		ActionHandler.click(OUVFleetContainer.Clicksave);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Navigate to OUV Approval Routes
	@Then("^Navigate to OUV Approval Routes$")
	public void navigate_to_OUV_Approval_Routes() throws Throwable {

		// Click on menu
		ActionHandler.wait(5);
		ActionHandler.click(OUVFleetContainer.Menuopen);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on menu");

		// Enter OUV fleet
		String Approvalroute = "Approval routes";
		ActionHandler.clearAndSetText(OUVFleetContainer.appsearch, Approvalroute);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter OUV fleet");

		// Click on OUV Fleet
		ActionHandler.click(OUVFleetContainer.ApprovalRoutes);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on OUV Fleet");

	}

	// User enter all the mandatory fields available on the form OUV Approval Route
	@Then("^User enter all the mandatory fields available on the form OUV Approval Route Name \"([^\"]*)\" Remarketing Manager \"([^\"]*)\" Finance Approver \"([^\"]*)\" Operations Approver \"([^\"]*)\" VLA Record Keeper \"([^\"]*)\" Regional Finance Director \"([^\"]*)\" Regional Managing Director \"([^\"]*)\"$")
	public void user_enter_all_the_mandatory_fields_available_on_the_form_OUV_Approval_Route_Name_Remarketing_Manager_Finance_Approver_Operations_Approver_VLA_Record_Keeper_Regional_Finance_Director_Regional_Managing_Director(
			String ApprovalRoutename, String RemarketingManager, String FinanceApprover, String OperationsApprover,
			String VLARecordKeeper, String RegionalFinanceDirector, String RegionalManagingDirector) throws Throwable {

		// data read from master excel sheet
		String Routename[] = ApprovalRoutename.split(",");
		ApprovalRoutename = CommonFunctions.readExcelMasterData(Routename[0], Routename[1], Routename[2]);

		String Mt[] = RemarketingManager.split(",");
		RemarketingManager = CommonFunctions.readExcelMasterData(Mt[0], Mt[1], Mt[2]);

		String CO[] = FinanceApprover.split(",");
		FinanceApprover = CommonFunctions.readExcelMasterData(CO[0], CO[1], CO[2]);

		String FM[] = OperationsApprover.split(",");
		OperationsApprover = CommonFunctions.readExcelMasterData(FM[0], FM[1], FM[2]);

		String FAO[] = VLARecordKeeper.split(",");
		VLARecordKeeper = CommonFunctions.readExcelMasterData(FAO[0], FAO[1], FAO[2]);

		String CC[] = RegionalFinanceDirector.split(",");
		RegionalFinanceDirector = CommonFunctions.readExcelMasterData(CC[0], CC[1], CC[2]);

		String AC[] = RegionalManagingDirector.split(",");
		RegionalManagingDirector = CommonFunctions.readExcelMasterData(AC[0], AC[1], AC[2]);

		// Enter Approval Route name
		ActionHandler.click(OUVFleetContainer.Routename);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.Routename, ApprovalRoutename);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// enters Remarketing manager
		ActionHandler.click(OUVFleetContainer.Remarketmanager);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.Remarketmanager, RemarketingManager);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Enters Finance Approver
		ActionHandler.click(OUVFleetContainer.Financeapprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.Financeapprover, FinanceApprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		// Enter Operation Approver
		ActionHandler.click(OUVFleetContainer.operationapprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.operationapprover, OperationsApprover);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// enters VLA Record Keeper
		ActionHandler.click(OUVFleetContainer.VLAkeeper);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.VLAkeeper, VLARecordKeeper);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		CommonFunctions.attachScreenshot();
		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// enter Region Finance director
		ActionHandler.click(OUVFleetContainer.financedirector);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.financedirector, RegionalFinanceDirector);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// Enter managing director
		ActionHandler.click(OUVFleetContainer.managingdirector);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		ActionHandler.setText(OUVFleetContainer.managingdirector, RegionalManagingDirector);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// selects first element in search list
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		ActionHandler.wait(3);
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// Verify that the logged in user is member of the selected fleet Add User
	@Then("^Verify that the logged in user is member of the selected fleet Add User \"([^\"]*)\"$")
	public void verify_that_the_logged_in_user_is_member_of_the_selected_fleet_Add_User(String selectpeople)
			throws Throwable {

		String FAO[] = selectpeople.split(",");
		selectpeople = CommonFunctions.readExcelMasterData(FAO[0], FAO[1], FAO[2]);

		// click on select fleet member
		ActionHandler.click(OUVFleetContainer.SelectFleetMembers);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVFleetContainer.selectnew);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on new button
		ActionHandler.click(OUVFleetContainer.selectnew);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on select people
		ActionHandler.click(OUVFleetContainer.Selectpeople);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// enters people
		ActionHandler.setText(OUVFleetContainer.Selectpeople, selectpeople);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on save button
		ActionHandler.click(OUVFleetContainer.Selectsave);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on fleet number
		ActionHandler.click(OUVFleetContainer.Gobacktofleet);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// Edit details for Fleet Name, Fleet Code and Fleet Administrators fields Fleet
	// Name
	@Then("^Edit details for Fleet Name, Fleet Code and Fleet Administrators fields Fleet Name \"([^\"]*)\" Fleet Code \"([^\"]*)\" Fleet administrators \"([^\"]*)\"$")
	public void edit_details_for_Fleet_Name_Fleet_Code_and_Fleet_Administrators_fields_Fleet_Name_Fleet_Code_Fleet_administrators(
			String Fleetname, String Fleetcode, String Fleetadmin) throws Throwable {

		// data retrieved from master data excel sheet
		String FAO[] = Fleetname.split(",");
		Fleetname = CommonFunctions.readExcelMasterData(FAO[0], FAO[1], FAO[2]);

		String FA[] = Fleetcode.split(",");
		Fleetcode = CommonFunctions.readExcelMasterData(FA[0], FA[1], FA[2]);

		String FAO1[] = Fleetadmin.split(",");
		Fleetadmin = CommonFunctions.readExcelMasterData(FAO1[0], FAO1[1], FAO1[2]);

		ActionHandler.click(OUVFleetContainer.clickedit);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on edit fleet name icon
		ActionHandler.click(OUVFleetContainer.Editfleetname);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// enters fleet name
		ActionHandler.clearAndSetText(OUVFleetContainer.Editfleetname, Fleetname);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.scrollDown();
		ActionHandler.wait(5);

		// enters fleet code
		ActionHandler.setText(OUVFleetContainer.Editfleetcode, Fleetcode);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on clear button
		ActionHandler.click(OUVFleetContainer.clearselection);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// click on enter administrator
		ActionHandler.click(OUVFleetContainer.enteradministrator);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// enters fleet admin
		ActionHandler.setText(OUVFleetContainer.enteradministrator, Fleetadmin);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(5);

		ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);

		if (!VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome))

		{
			ActionHandler.wait(4);
			driver.navigate().refresh();
			ActionHandler.waitForElement(OUVCalendarContainer.welcome, 60);
		}
		VerifyHandler.verifyElementPresent(OUVCalendarContainer.welcome);
	}

	// Click New button
	@Then("^Click New button$")
	public void click_New_Button() throws Throwable {
		ActionHandler.click(OUVFleetContainer.ClickNew);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	@Then("^validate the error message displayed$")
	public void validate_the_error_message_displayed() throws Throwable {

		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.error)) {
			Reporter.addStepLog("Error message was found");
		} else {
			Reporter.addStepLog("Error message was not found");
		}
		ActionHandler.wait(2);
		ActionHandler.click(OUVFleetContainer.Cancelchanges);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on cancel button");
	}

	// User changes the fleet name
	@When("^User changes the fleet name to \"([^\"]*)\"$")
	public void user_changes_the_fleet_name(String name) throws Throwable {

		String nam[] = name.split(",");
		name = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		// Enter OUV fleet name
		ActionHandler.clearAndSetText(OUVFleetContainer.fleetname, name);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter OUV fleet name");

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();

	}

	// user clicks on edit fleet administrator
	@When("^User edit fleet administrator and enter \"([^\"]*)\"$")
	public void user_edit_fleet_administrator_and_enter(String name) throws Throwable {

		// Select edit name
		String nam[] = name.split(",");
		name = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.click(OUVFleetContainer.fleetAdminstrator);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		ActionHandler.click(OUVFleetContainer.clearsel);
		ActionHandler.wait(3);
		ActionHandler.setText(OUVFleetContainer.fleetadmin, name);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);

		Reporter.addStepLog("Select edit name");
		CommonFunctions.attachScreenshot();

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();
	}

	// Verify the changes in fleet
	@Then("^Verify the changes in fleet name$")
	public void verify_the_changes_in_fleet_name() throws Throwable {

		if (!VerifyHandler.verifyElementPresent(OUVFleetContainer.editfleetname)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("The changes are successful");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(5);
			Reporter.addStepLog("The changes are not successful");
			CommonFunctions.attachScreenshot();
		}

	}

	// User select the vehicle
	@Then("^User select the first vehicle$")
	public void user_select_the_first_vehicle() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		// User select the vehicle
		ActionHandler.wait(5);
		ActionHandler.click(OUVFleetContainer.VehcileLink);
		ActionHandler.wait(5);
		Reporter.addStepLog("Navigate to OUV Vehicle Tab");
		CommonFunctions.attachScreenshot();
	}

	// User click on Fleet name link
	@And("^User click on Fleet name link$")
	public void user_click_on_Fleet_name_link() throws Throwable {

		ActionHandler.wait(5);
		driver.navigate().refresh();
		ActionHandler.wait(15);

		for (int i = 0; i < 5; i++) {
			ActionHandler.scrollDown();
		}

		// click on Fleet members link
		ActionHandler.wait(5);
		ActionHandler.click(OUVFleetContainer.FleetLink);
		ActionHandler.wait(5);
		Reporter.addStepLog("User click on Fleet name link");
		CommonFunctions.attachScreenshot();

	}

	@And("^Verify that fleet manager details cannot be edited$")
	public void verify_that_fleet_manager_details_cannot_be_edited() throws Throwable {

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		if (!VerifyHandler.verifyElementPresent(OUVFleetContainer.EditFleetManager)) {
			ActionHandler.wait(2);
			Reporter.addStepLog("Edit icon is enabled for editing Fleet manager");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(2);
			Reporter.addStepLog("Edit icon is disabled for editing Fleet manager");
			CommonFunctions.attachScreenshot();
		}
	}

	@Then("^Verify that Day pass approver one field cannot be edited$")
	public void verify_that_Day_pass_approver_one_field_cannot_be_edited() throws Throwable {
		ActionHandler.wait(3);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(2);

		if (!VerifyHandler.verifyElementPresent(OUVFleetContainer.EditDayPassArrover1)) {
			ActionHandler.wait(1);
			Reporter.addStepLog("Edit icon is enabled for editing Day pass approver LL6");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(2);
			Reporter.addStepLog("Edit icon is disabled for editing Day pass approver LL6");
			CommonFunctions.attachScreenshot();
		}
	}

	@Then("^Verify that Day pass approver two field cannot be edited$")
	public void verify_that_Day_pass_approver_two_field_cannot_be_edited() throws Throwable {
		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(OUVFleetContainer.EditDayPassArrover2)) {
			ActionHandler.wait(1);
			Reporter.addStepLog("Edit icon is enabled for editing Day pass approver LL5");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(2);
			Reporter.addStepLog("Edit icon is disabled for editing Day pass approver LL5");
			CommonFunctions.attachScreenshot();
		}
	}

	@Then("^Verify that Day pass approver three field cannot be edited$")
	public void verify_that_Day_pass_approver_three_field_cannot_be_edited() throws Throwable {
		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(OUVFleetContainer.EditDayPassArrover3)) {
			ActionHandler.wait(1);
			Reporter.addStepLog("Edit icon is enabled for editing Day pass approver LL4");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(1);
			Reporter.addStepLog("Edit icon is disabled for editing Day pass approver LL4");
			CommonFunctions.attachScreenshot();
		}
	}

	@And("^Verify that Approval Route cannnot be edited$")
	public void verify_that_Approval_Route_cannnot_be_edited() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.scrollDown();
		ActionHandler.scrollDown();
		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(OUVFleetContainer.EditApprovalRoute)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("Edit icon is enabled for editing Approval Route");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(2);
			Reporter.addStepLog("Edit icon is disabled for editing Approval Route");
			CommonFunctions.attachScreenshot();
		}

	}

	@Then("^Verify that Registration Approver field cannot be edited$")
	public void verify_that_Registration_Approver_field_cannot_be_edited() throws Throwable {
		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(OUVFleetContainer.EditRegistrationApprover)) {
			ActionHandler.wait(2);
			Reporter.addStepLog("Edit icon is enabled for editing Registration Approver");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(2);
			Reporter.addStepLog("Edit icon is disabled for editing Registration Approver");
			CommonFunctions.attachScreenshot();
		}

	}

	@And("^Verify that Disposal Approver filed cannot be edited$")
	public void verify_that_Disposal_Approver_filed_cannot_be_edited() throws Throwable {
		ActionHandler.wait(2);
		if (!VerifyHandler.verifyElementPresent(OUVFleetContainer.EditDisposalApprover)) {
			ActionHandler.wait(2);
			Reporter.addStepLog("Edit icon is enabled for editing Disposal Approver");
			CommonFunctions.attachScreenshot();
		}

		else {
			ActionHandler.wait(2);
			Reporter.addStepLog("Edit icon is disabled for editing Disposal Approver");
			CommonFunctions.attachScreenshot();
		}

	}

	@When("^User changes the Approval Route to \"([^\"]*)\"$")
	public void user_changes_the_Approval_Route_to(String Route) throws Throwable {
		// Select edit name
		String nam[] = Route.split(",");
		Route = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(OUVFleetContainer.EditApprovalRoute);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on Edit approval route pencil icon");

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.click(OUVFleetContainer.clearsAR);
		ActionHandler.wait(3);
		ActionHandler.setText(OUVFleetContainer.ApprovalRouteTxtBx, Route);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter new Approval route");

		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("selects new Approval route");

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();
	}

	@When("^Verify that Approval Route is updated$")
	public void verify_that_Approval_Route_is_updated() throws Throwable {
		ActionHandler.wait(4);
		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.EditApprovalRoute)) {
			Reporter.addStepLog("Approval Route is updated");
		} else {
			Reporter.addStepLog("Approval Route is not updated");
		}
	}

	@Then("^User changed the Fleet manager to \"([^\"]*)\"$")
	public void user_changed_the_Fleet_manager_to(String Manager) throws Throwable {
		// Select edit name
		String nam[] = Manager.split(",");
		Manager = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.click(OUVFleetContainer.EditFleetManager);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on Edit approval route pencil icon");

		ActionHandler.click(OUVFleetContainer.clearsFM);
		ActionHandler.wait(3);
		ActionHandler.setText(OUVFleetContainer.FleetManagerTxtBx, Manager);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter new Fleet Manager");

		Actions acts = new Actions(driver);
		ActionHandler.wait(2);
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("selects new Fleet manager");

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Navigate to Reports$")
	public void navigate_to_Reports() throws Throwable {

		// Click on menu
		ActionHandler.wait(5);
		ActionHandler.click(OUVFleetContainer.Menuopen);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on menu");

		// Enter OUV fleet
		ActionHandler.clearAndSetText(OUVFleetContainer.appsearch, Reports);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Reports");

		// Click on OUV Fleet
		ActionHandler.click(OUVFleetContainer.Reports);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on Reports");
	}

	// Select All folders under folders section
	@Then("^Select All folders under folders section$")
	public void select_All_folders_under_folders_section() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(OUVFleetContainer.AllFolders);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Select All folders under folders section");
	}

	@Then("^Select the folder \"([^\"]*)\"$")
	public void select_the_folder(String OUVBeneluxReport) throws Throwable {

		String nam[] = OUVBeneluxReport.split(",");
		OUVBeneluxReport = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(3);
		javascriptUtil
				.clickElementByJS(driver.findElement(By.xpath(OUVFleetContainer.FolderSelection(OUVBeneluxReport))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Selects Report");
	}

	@Then("^Navigate to the folder \"([^\"]*)\"$")
	public void navigate_to_the_folder(String UATtest) throws Throwable {
		String nam[] = UATtest.split(",");
		UATtest = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(3);
		ActionHandler.pageCompleteScrollDown();

		ActionHandler.wait(3);
		javascriptUtil.clickElementByJS(driver.findElement(By.xpath(OUVFleetContainer.FolderSelection(UATtest))));
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Selects Report");

	}

	@Then("^Select Report for check \"([^\"]*)\"$")
	public void select_Report_for_check(String ReportCheck) throws Throwable {
		String nam[] = ReportCheck.split(",");
		ReportCheck = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(5);
		javascriptUtil.clickElementByJS(driver.findElement(By.xpath(OUVFleetContainer.FolderSelection(ReportCheck))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Selects Report for check");

	}

	@Then("^Verify that Fleet name is updated to \"([^\"]*)\"$")
	public void verify_that_Fleet_name_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(3);
		driver.navigate().refresh();
		ActionHandler.wait(20);
		driver.switchTo().frame(0);
		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String FleetName = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (FleetName.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Fleet Name is updated to: " + FleetName);
			} else {
				Reporter.addStepLog("Fleet Name is not updated");
			}
		}
	}

	@Then("^Verify that Fleet Administrator is updated to \"([^\"]*)\"$")
	public void verify_that_Fleet_Administrator_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String FleetAdministrator = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (FleetAdministrator.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Fleet Administrator details updated to : " + FleetAdministrator);
			} else {
				Reporter.addStepLog("Fleet Administrator details not updated");
			}
		}
	}

	@Then("^Verify that Remarketing Manager is updated to \"([^\"]*)\"$")
	public void verify_that_Remarketing_Manager_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String RemarketingManager = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (RemarketingManager.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Remarketing Manager details updated to: " + RemarketingManager);
			} else {
				Reporter.addStepLog("Remarketing Manager details not updated");
			}
		}
	}

	@Then("^Verify that Finance Approver is updated to \"([^\"]*)\"$")
	public void verify_that_Finance_Approver_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String FinanaceApprover = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (FinanaceApprover.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Finance Approver is updated to:" + FinanaceApprover);
			} else {
				Reporter.addStepLog("Finance Approver is not updated");
			}
		}
	}

	@Then("^Verify that Operations Manager is updated to \"([^\"]*)\"$")
	public void verify_that_Operations_Manager_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		javascriptUtil.scrollIntoView(OUVFleetContainer.RegionalManagingDirector);
		ActionHandler.wait(2);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String OperatingManager = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (OperatingManager.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Operations Manager is updated to: " + OperatingManager);
			} else {
				Reporter.addStepLog("Operations Manager is not updated");
			}
		}
	}

	@Then("^Verify that Regional Finance Director is updated to \"([^\"]*)\"$")
	public void verify_that_Regional_Finance_Director_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String RegionaleFD = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (RegionaleFD.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Regional Finance director is updated to: " + RegionaleFD);
			} else {
				Reporter.addStepLog("Regional Finance director is not updated");
			}
		}
	}

	@Then("^Verify that Remarketing Fleet Manager is updated to \"([^\"]*)\"$")
	public void verify_that_Remarketing_Fleet_Manager_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String RemarketingManager = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (RemarketingManager.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Remarketing Manager details updated to: " + RemarketingManager);
			} else {
				Reporter.addStepLog("Remarketing Manager details not updated");
			}
		}
	}

	@Then("^Verify that Operations Approver is updated to \"([^\"]*)\"$")
	public void verify_that_Operations_Approver_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(2);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String OperatingManager = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (OperatingManager.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Operations Manager is updated to: " + OperatingManager);
			} else {
				Reporter.addStepLog("Operations Manager is not updated");
			}
		}
	}

	@Then("^Verify that Registration Approver is updated to \"([^\"]*)\"$")
	public void verify_that_Registration_Approver_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String FinanaceApprover = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (FinanaceApprover.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Registration Approver is updated to:" + FinanaceApprover);
			} else {
				Reporter.addStepLog("Registration Approver is not updated");
			}
		}
		ActionHandler.wait(3);
		driver.switchTo().parentFrame();
		ActionHandler.wait(3);
	}

	@Then("^Verify that Regional Managing Director is updated to \"([^\"]*)\"$")
	public void verify_that_Regional_Managing_Director_is_updated_to(String arg1) throws Throwable {
		String nam[] = arg1.split(",");
		arg1 = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(1);
		if (VerifyHandler.verifyElementPresent(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))))) {
			String RegionalMD = ActionHandler
					.getElementText(driver.findElement(By.xpath(OUVFleetContainer.DetailsCheck(arg1))));
			if (RegionalMD.equalsIgnoreCase(arg1)) {
				Reporter.addStepLog("Regional Managing Director is updated to: " + RegionalMD);
			} else {
				Reporter.addStepLog("Regional Managing Director not updated");
			}
		}
		ActionHandler.wait(3);
		driver.switchTo().parentFrame();
		ActionHandler.wait(3);
	}

	@When("^User changes the Registration Approver to \"([^\"]*)\"$")
	public void user_changes_the_Registration_Approver_to(String Route) throws Throwable {
		// Select edit name
		String nam[] = Route.split(",");
		Route = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		ActionHandler.wait(2);
		driver.navigate().refresh();
		ActionHandler.wait(10);

		ActionHandler.pageCompleteScrollDown();
		ActionHandler.wait(3);
		ActionHandler.click(OUVFleetContainer.EditRegistrationApprover);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("click on Edit Registration Approver pencil icon");

		ActionHandler.pageDown();
		ActionHandler.wait(3);
		ActionHandler.click(OUVFleetContainer.clearReApr);
		ActionHandler.wait(3);
		ActionHandler.setText(OUVFleetContainer.RegistrationApproverTxtBx, Route);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter Registration Approver");

		Actions acts = new Actions(driver);
		ActionHandler.wait(2);
		// acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);

		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("selects new Registration Approver");

		// Click on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();
	}

	@And("^user tries to change owner with \"([^\"]*)\"$")
	public void user_tries_to_change_owner_with(String Owner) throws Throwable {

		String own[] = Owner.split(",");
		Owner = CommonFunctions.readExcelMasterData(own[0], own[1], own[2]);

		ActionHandler.wait(5);
		ActionHandler.click(OUVFleetContainer.ChangeOwner);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on change owner icon");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVFleetContainer.SearchUsers);
		ActionHandler.wait(2);

		ActionHandler.setText(OUVFleetContainer.SearchUsers, Owner);
		ActionHandler.wait(3);
		CommonFunctions.attachScreenshot();

		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVFleetContainer.ChangeownerBtn);
		ActionHandler.wait(3);
		Reporter.addStepLog("User click on change owner button");
		CommonFunctions.attachScreenshot();

	}

	@Then("^Validate the error message$")
	public void validate_the_error_message() throws Throwable {

		ActionHandler.wait(3);
		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.error)) {

			Reporter.addStepLog("error message window displayed");
		} else {
			Reporter.addStepLog("error message window displayed");
		}
	}

	@Then("^close the window$")
	public void close_the_window() throws Throwable {

		ActionHandler.wait(2);
		ActionHandler.click(OUVFleetContainer.CloseWindow);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on error message window");
	}

	@Then("^user click on delete option$")
	public void user_click_on_delete_option() throws Throwable {
		driver.navigate().refresh();
		ActionHandler.wait(15);

		ActionHandler.wait(1);
		ActionHandler.click(OUVFleetContainer.FleetMembersdelete1);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on Show Actions");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.Delete);
		ActionHandler.wait(2);
		Reporter.addStepLog("User clicks on delete button");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(ATORequestContainer.DeleteLineItems);
		ActionHandler.wait(2);
		Reporter.addStepLog("User deletes the ATO Request Line Items");
		CommonFunctions.attachScreenshot();

	}

	@Then("^Verify the error message shown$")
	public void verify_the_error_message_shown() throws Throwable {

		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.errorwindow)) {
			Reporter.addStepLog("error message window displayed");
		} else {
			Reporter.addStepLog("error message window displayed");
		}
		ActionHandler.wait(2);
		ActionHandler.click(OUVFleetContainer.CloseWindow);
		ActionHandler.wait(3);
		Reporter.addStepLog("User clicks on error message window");

	}

	@Then("^User edits the fleet administrator with \"([^\"]*)\"$")
	public void user_edits_the_fleet_administrator_with(String fleetAdmin) throws Throwable {

		String fa[] = fleetAdmin.split(",");
		fleetAdmin = CommonFunctions.readExcelMasterData(fa[0], fa[1], fa[2]);

		ActionHandler.wait(4);
		ActionHandler.click(OUVFleetContainer.editFleetAdmin2);
		ActionHandler.wait(4);
		Reporter.addStepLog("User click on edit button");
		CommonFunctions.attachScreenshot();

		ActionHandler.wait(4);
		ActionHandler.scrollToView(OUVFleetContainer.f3header);
		ActionHandler.wait(4);
		ActionHandler.clearAndSetText(OUVFleetContainer.FleetAdmin2, fleetAdmin);
		ActionHandler.wait(3);

		Actions act = new Actions(driver);
		act.sendKeys(Keys.ARROW_DOWN).build().perform();
		act.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("User enters fleetadmin2");

	}

	@And("^user saves the changes$")
	public void user_saves_the_changes() throws Throwable {

		ActionHandler.wait(5);
		ActionHandler.click(OUVFleetContainer.save);
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on the save button");
		CommonFunctions.attachScreenshot();
	}

	@And("^validates the message displayes$")
	public void validates_the_message_displayes() throws Throwable {

		ActionHandler.wait(5);
		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.SnagMsg)) {
			CommonFunctions.attachScreenshot();
			Reporter.addStepLog("Error message displayed");
		} else {
			Reporter.addStepLog("Error message not displayed");
		}
		ActionHandler.wait(2);
		ActionHandler.click(OUVFleetContainer.Cancelchanges);
		ActionHandler.wait(5);
		Reporter.addStepLog("User clicks on cancel button");
	}

	// verify the add new error message
	@Then("^verify the add new error message$")
	public void verify_the_add_new_error_message() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.errormember)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("Verify the displayed error message");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("No error message displayed");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.click(OUVFleetContainer.Cancelchanges);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Click on the cancel button");
	}

	// User tries to add new member
	@Then("^User tries to add new member \"([^\"]*)\"$")
	public void user_tries_to_add_new_member(String member) throws Throwable {

		String nam[] = member.split(",");
		member = CommonFunctions.readExcelMasterData(nam[0], nam[1], nam[2]);

		// Select Fleet members new
		ActionHandler.click(OUVFleetContainer.FleetMembersnew);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Fleet members new");
		CommonFunctions.attachScreenshot();

		// Enter OUV fleet member name
		ActionHandler.clearAndSetText(OUVFleetContainer.FleetMembersinfo, member);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
		Reporter.addStepLog("Enter OUV fleet member name");

		Actions acts = new Actions(driver);
		acts.sendKeys(Keys.ARROW_DOWN).build().perform();
		acts.sendKeys(Keys.ENTER).build().perform();
		ActionHandler.wait(4);

		// click on save button
		ActionHandler.click(ATORequestContainer.Save);
		ActionHandler.wait(5);
		Reporter.addStepLog("Select Save");
		CommonFunctions.attachScreenshot();
	}

	// Verify that user is not able to edit the fields in an OUV Fleet
	@Then("^Verify that user is not able to edit the fields in an OUV Fleet$")
	public void verify_that_user_is_not_able_to_edit_the_fields_in_an_OUV_Fleet() throws Throwable {

		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.FleetManageredit)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is able to edit Fleet Manager");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is not able to edit Fleet Manager");
			CommonFunctions.attachScreenshot();
		}

		ActionHandler.pageScrollDown();
		ActionHandler.wait(5);

		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.DayPassApproveredit)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is able to edit Day Pass Approver");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is not able to edit Day Pass Approver");
			CommonFunctions.attachScreenshot();
		}

		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.OvernightPassApproveredit)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is able to edit Overnight Pass Approver");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is not able to edit Overnight Pass Approver");
			CommonFunctions.attachScreenshot();
		}

		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.WeekendPassApproveredit)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is able to edit Weekend Pass Approver");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is not able to edit Weekend Pass Approver");
			CommonFunctions.attachScreenshot();
		}

		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.ApprovalRouteedit)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is able to edit Approval Route");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is not able to edit Approval Route");
			CommonFunctions.attachScreenshot();
		}

		if (VerifyHandler.verifyElementPresent(OUVFleetContainer.RegistrationApproveredit)) {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is able to edit Registration Approver");
			CommonFunctions.attachScreenshot();
		} else {
			ActionHandler.wait(5);
			Reporter.addStepLog("User is not able to edit Registration Approver");
			CommonFunctions.attachScreenshot();
		}
	}

	// Change the Fleet Administrator two to any user from the list for the selected
	// Fleet Record
	@Then("^Change the Fleet Administrator two to any user from the list for the selected Fleet Record Administratortwo \"([^\"]*)\"$")
	public void change_the_Fleet_Administrator_two_to_any_user_from_the_list_for_the_selected_Fleet_Record_Administratortwo(
			String Administrstor2) throws Throwable {

		String FAO[] = Administrstor2.split(",");
		Administrstor2 = CommonFunctions.readExcelMasterData(FAO[0], FAO[1], FAO[2]);

		ActionHandler.scrollDown();
		ActionHandler.wait(5);
		ActionHandler.click(OUVFleetContainer.Editadministrator2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVFleetContainer.clearselection1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVFleetContainer.clearselection1(Administrstor2))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVFleetContainer.Save);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

	}

	// Verify that Fleet Administrator two is updated
	@Then("^Verify that Fleet Administrator two is updated$")
	public void verify_that_Fleet_Administrator_two_is_updated() throws Throwable {

		VerifyHandler.verifyElementPresent(OUVFleetContainer.Administrator2change);
		ActionHandler.wait(5);
		Reporter.addStepLog("Verify that Fleet Administrator two is updated");
		CommonFunctions.attachScreenshot();
	}

	@Then("^Edit the Fleet Administrator two back to original \"([^\"]*)\"$")
	public void edit_the_Fleet_Administrator_two_back_to_original(String Administrstor2) throws Throwable {

		String FAO[] = Administrstor2.split(",");
		Administrstor2 = CommonFunctions.readExcelMasterData(FAO[0], FAO[1], FAO[2]);

		ActionHandler.click(OUVFleetContainer.Editadministrator2);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// javascriptUtil.scrollIntoView(OUVFleetContainer.clearselection1);
		// ActionHandler.wait(2);
		ActionHandler.click(OUVFleetContainer.clearselection1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVFleetContainer.clearselection1(Administrstor2))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVFleetContainer.Save);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Verify that Fleet Administrator two is updated back to original
	@Then("^Verify that Fleet Administrator two is updated back to original$")
	public void verify_that_Fleet_Administrator_two_is_updated_back_to_original() throws Throwable {

		VerifyHandler.verifyElementPresent(OUVFleetContainer.OriginalAdministrator2);
		ActionHandler.wait(5);
		Reporter.addStepLog("Verify that Fleet Administrator two is updated back to original");
		CommonFunctions.attachScreenshot();
	}

	// Navigate to OUV Vehicles under Related list quick links
	@Then("^Navigate to OUV Vehicles under Related list quick links$")
	public void Navigate_to_OUV_Vehicles_under_Related_list_quick_links() throws Throwable {

		ActionHandler.click(OUVFleetContainer.OUVvehicles);
		ActionHandler.wait(5);
		Reporter.addStepLog("Navigate to OUV Vehicles under Related list quick links");
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVFleetContainer.Fleetvehicle);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

	// Click on the Fleet in the OUV Vehicle Details page
	@Then("^Click on the Fleet in the OUV Vehicle Details page$")
	public void Click_on_the_Fleet_in_the_OUV_Vehicle_Details_page() throws Throwable {

		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);
		ActionHandler.pageScrollDown();
		ActionHandler.wait(3);

		ActionHandler.click(OUVFleetContainer.FleetRentalNL);
		ActionHandler.wait(10);
		CommonFunctions.attachScreenshot();
		ActionHandler.scrollDown();
		ActionHandler.wait(3);
		Reporter.addStepLog("Click on the Fleet in the OUV Vehicle Details page");
	}

	// Edit the Fleet Administrator two back to original
	@Then("^Edit Fleet Administrator two back to original \"([^\"]*)\"$")
	public void edit_Fleet_Administrator_two_back_to_original(String Administrstor2) throws Throwable {

		String FAO[] = Administrstor2.split(",");
		Administrstor2 = CommonFunctions.readExcelMasterData(FAO[0], FAO[1], FAO[2]);

		ActionHandler.click(OUVFleetContainer.Editadministrator22);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		// javascriptUtil.scrollIntoView(OUVFleetContainer.clearselection1);
		// ActionHandler.wait(2);
		ActionHandler.click(OUVFleetContainer.clearselection1);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(driver.findElement(By.xpath(OUVFleetContainer.clearselection1(Administrstor2))));
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();

		ActionHandler.click(OUVFleetContainer.Save);
		ActionHandler.wait(5);
		CommonFunctions.attachScreenshot();
	}

}
