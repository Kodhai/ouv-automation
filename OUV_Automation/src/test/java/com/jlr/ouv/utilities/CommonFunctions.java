package com.jlr.ouv.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cucumber.listener.Reporter;
import com.jlr.autotest.handlers.ActionHandler;
import com.jlr.autotest.utils.Config;
import com.jlr.autotest.utils.Utility;
import com.jlr.base.TestBaseCC;
import com.jlr.ouv.constants.Constants;

public class CommonFunctions extends TestBaseCC {

	public static WebDriver driver = getDriver();
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonFunctions.class.getName());

	public CommonFunctions(WebDriver commanDriver) {
		driver = commanDriver;
	}

	public static void attachScreenshot() throws IOException {
		ActionHandler.wait(1);
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.SCREENSHOTFORMAT);
		Reporter.addScreenCaptureFromPath(Utility.takeScreenShotWithPathName(
				Constants.SVOSCREENSHOTPATH + Config.getPropertyValue("ouv.testscript") + "\\"
						+ Config.getPropertyValue("ouv.testscript") + "_" + sdf.format(Utility.fetchCurrentDate())));
		LOGGER.info("Screenshot");

	}

	public static String readExcelMasterData(String sheetName, String Column, String row1) throws Exception {
		XSSFWorkbook wb = null;
		XSSFSheet sh = null;
		String excelData = null;
		try {
			int Row = Integer.parseInt(row1);
			FileInputStream ft = new FileInputStream(new File(Constants.Master_Excel_FILE_PATH + "OUV Test Data.xlsx"));
			LOGGER.info(Constants.Master_Excel_FILE_PATH);
			wb = new XSSFWorkbook(ft);
			sh = wb.getSheet(sheetName);

			int totalRows = getRowNum(sh);

			LOGGER.info("Total line of Records : " + totalRows);
			DataFormatter df = new DataFormatter();

			System.out.println("Fetching cell value from " + Column + Row);
			Row row = sh.getRow(Row - 1);
			String cellvalue = df.formatCellValue(row.getCell(CellReference.convertColStringToIndex(Column)));
			excelData = cellvalue;

		} catch (NullPointerException e) {
			LOGGER.info("Exception - Delete blank rows after last line from Excel file :" + e);
		}

		catch (Exception e) {
			LOGGER.info("Exception : " + e);
		}
		return excelData;
	}

	public static int getRowNum(XSSFSheet sh) {
		return sh.getLastRowNum();
	}

	public int getColNum(XSSFSheet sh, int rownum) {
		return sh.getRow(rownum).getLastCellNum();

	}

	public static String selectFutureDate() {
		final DateFormat dateFormat = new SimpleDateFormat("d-M-yyyy");
		Date currentDate = new Date();

		String currentDateSTR = dateFormat.format(currentDate);

		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DATE, 1);
		Date currentDatePlusOne = c.getTime();
		String FutureDate = dateFormat.format(currentDatePlusOne);
		System.out.println("Future Date is = " + FutureDate);

		return FutureDate;

	}

	public static String selectFutureYear() {
		final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date currentDate = new Date();
		// System.out.println(dateFormat.format(currentDate));
		String currentDateSTR = dateFormat.format(currentDate);
		System.out.println("Current date is = " + currentDateSTR);
		// convert date to calendar
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		// manipulate date
		c.add(Calendar.MONTH, 1);
		c.add(Calendar.DATE, 1);
		c.add(Calendar.YEAR, 1);// same with c.add(Calendar.DAY_OF_MONTH, 1);
		// convert calendar to date
		Date currentDatePlusOne = c.getTime();
		// System.out.println(dateFormat.format(currentDatePlusOne));
		String FutureDate = dateFormat.format(currentDatePlusOne);
		System.out.println("Future Date is = " + FutureDate);
		return FutureDate;

	}

	// Select Yesterday Date
	public static String selectYesterdayDate() {
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, -1);
		dt = c.getTime();
		System.out.println(dt);

		SimpleDateFormat sdt = new SimpleDateFormat("dd-MMM-yyyy");
		String newDate = sdt.format(dt);
		System.out.println("New Date is = " + newDate);

		return newDate;
	}

}
