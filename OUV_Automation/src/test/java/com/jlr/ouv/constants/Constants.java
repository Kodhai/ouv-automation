package com.jlr.ouv.constants;

public class Constants {

	public static final String SVOSCREENSHOTPATH = "C:\\OUV_Screenshots\\";

	public static final String SCREENSHOTFORMAT = "H_mm_ss";

	public static final String Master_Excel_FILE_PATH = System.getProperty("user.dir") + "/Master Data/";

	public static final String OUVURL = "https://jlr-global--jlrglobuat.lightning.force.com/lightning/page/home";

	public static final String OUVCalendarURL = "https://jlr-global--jlrglobuat.lightning.force.com/lightning/n/Search_OUV_Vehicles";

}