
package com.jlr.ouv.runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.jlr.listeners.Listener;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/OUV_TestFeature", dryRun = false, glue = {
		"com.jlr.ouv.tests" }, plugin = {

				"com.cucumber.listener.ExtentCucumberFormatter:test-output/extentReport/extent-Report.html" }, tags = {

						"@Archana" })

public class OUVTestRunner {
	static Listener listener = new Listener();

	@BeforeClass
	public static void start() {
		listener.onStart();

	}

	@AfterClass
	public static void end() {
		listener.onFinish();

	}

}
