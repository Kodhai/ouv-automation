package com.jlr.ouv.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ATORequestContainer {
	@FindBy(how = How.XPATH, using = "(//a[@title='ATO Requests'])[1]")
	public WebElement ATORequests;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_OrderRequest__c-search-input']")
	public WebElement searchtextbox;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement firstATO;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/OUV_OrderRequest__History/')]")
	public WebElement Requesthistory;

	@FindBy(how = How.XPATH, using = "//h1[@title='ATO Request History']")
	public WebElement ATOtitle;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_ShortDescription__c']")
	public WebElement QuickReference;

	public String ValueSelection(String Value) {
		return "//span[@title='" + Value + "']";
	}

	public String ReworkRoute(String Value) {
		return "(//span[@title='" + Value + "'])[2]";
	}

	@FindBy(how = How.XPATH, using = "//textarea[@maxlength='32768']")
	public WebElement BookingJustification;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button']")
	public WebElement dependencies;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Level1Category__c']")
	public WebElement OUVReportingLevel1;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search OUV Fleets...']")
	public WebElement fleet;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Level2Category__c']")
	public WebElement OUVReportingLevel2;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Level3Category__c']")
	public WebElement OUVReportingLevel3;

	@FindBy(how = How.XPATH, using = "//button[@name='apply']")
	public WebElement apply;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[7]")
	public WebElement Market;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_User__c']")
	public WebElement Internaljlrcontact;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_AddToFleetDate__c']")
	public WebElement ProposedAddtoFleetDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_ProposedHandedInDate__c']")
	public WebElement ProposedHandinDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_TargetUtilisationPercentage__c']")
	public WebElement percentage;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[9]")
	public WebElement DisposalRoute;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_ReworkRoute__c']")
	public WebElement ReworkRoute;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button'])[5]")
	public WebElement alldependecies;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement Save;

	@FindBy(how = How.XPATH, using = "//div[text()='ATO Request']")
	public WebElement verifyATO;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-form-element__help']")
	public WebElement ATOerrormessage;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit ATO Request Status']")
	public WebElement Editpencilicon;

	@FindBy(how = How.XPATH, using = "//a[@title='Home']")
	public WebElement homeTab;

	@FindBy(how = How.XPATH, using = "(//span[@class='uiImage'])[1]")
	public WebElement loginImg;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Log Out')]")
	public WebElement logout;

	@FindBy(how = How.XPATH, using = "//a[@data-tab-value='detailTab']")
	public WebElement detailTab;

	@FindBy(how = How.XPATH, using = "//button[@title='Change Owner']")
	public WebElement ChangeOwner;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement SearchUsers;

	@FindBy(how = How.XPATH, using = "//button[@value='change owner']")
	public WebElement confChangeOwner;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_OrderRequest__c-search-input']")
	public WebElement searchATOrequestbox;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement All;

	@FindBy(how = How.XPATH, using = "//span[text()='Recently Viewed']")
	public WebElement RecentlyViewed;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement ATOSel1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_OrderRequest__c-search-input']")
	public WebElement ATOsearchbox;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Approval Fleet Manager']")
	public WebElement editicon;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Internal JLR Contact']")
	public WebElement EditJLRinternalcontact;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_User__c']")
	public WebElement jlrcontact;

	@FindBy(how = How.XPATH, using = "//button[@name='cancel']")
	public WebElement cancel;

	@FindBy(how = How.XPATH, using = "//h2[@class='slds-truncate slds-text-heading_medium'] ")
	public WebElement ATOerror;

	@FindBy(how = How.XPATH, using = "(//a[@title='New'] )")
	public WebElement NewATO;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[10]")
	public WebElement Rework;

	@FindBy(how = How.XPATH, using = "//span[@title='Belgium']")
	public WebElement reworkroute;

	@FindBy(how = How.XPATH, using = "//a[@title='Select List View']")
	public WebElement recentlyViewedList;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement AllATORequest;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/')])[1]")
	public WebElement firstATORequestAll;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/lightning/r/AttachedContentNote')]")
	public WebElement Notes;

	@FindBy(how = How.XPATH, using = "(//a[@title='New' and @class='forceActionLink'])[2]")
	public WebElement NewNoteBtn;

	@FindBy(how = How.XPATH, using = "//input[@class='inputText notesTitle flexInput input']")
	public WebElement NoteTitle;

	@FindBy(how = How.XPATH, using = "//div[@class='ql-editor ql-blank slds-rich-text-area__content slds-text-color_weak slds-grow']")
	public WebElement NoteBody;

	@FindBy(how = How.XPATH, using = "//span[text()='Done']")
	public WebElement NoteDoneBtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Close']")
	public WebElement CloseNoteWindow;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/OUV_OrderRequestLineItem__c')])[2]")
	public WebElement ATORequestLineItem;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/') and contains(text(),'Vehicle')])[1]")
	public WebElement firstATORequestLineItem;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Vista Vehicle']")
	public WebElement EditVistaVehicle;

	@FindBy(how = How.XPATH, using = "//button[@title='Clear Selection']")
	public WebElement ClearSelection;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement VistaVehicle;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results for')]")
	public WebElement showAllResultsVistaVehicle;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement saveATORequest;

	@FindBy(how = How.XPATH, using = "//div[@class='pageLevelErrors']")
	public WebElement errorMessage;

	@FindBy(how = How.XPATH, using = "//button[@title='Close error dialog']")
	public WebElement closeErrorMessage;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement cancelEditATORequest;

	@FindBy(how = How.XPATH, using = "//a[@title='Show 2 more actions']")
	public WebElement RightMenuButton;

	@FindBy(how = How.XPATH, using = "(//a[@class='slds-truncate'])[1]")
	public WebElement firstNote;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement deleteNote;

	@FindBy(how = How.XPATH, using = "(//span[text()='Delete'])[2]")
	public WebElement confirmDeleteNote;

	@FindBy(how = How.XPATH, using = " //span[contains(text(),'ATO Request Line Items')]")
	public WebElement ATORequestLine;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement New;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement SearchVista;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEdit;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveAndNew']")
	public WebElement SaveNew;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Vehicle-')]")
	public WebElement SelectATOLineItems;

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement Edit;

	@FindBy(how = How.XPATH, using = "//h2[contains(text(),'Edit Vehicle')]")
	public WebElement EditVehicle;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement CancelEdit;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions'])[12]")
	public WebElement ShowActions;

	@FindBy(how = How.XPATH, using = "//a[@title='Delete']")
	public WebElement Delete;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement DeleteLineItems;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_OrderRequest__c-search-input']")
	public WebElement searchRequestbox;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon-border-filled'])[3]")
	public WebElement ShowMoreActions;

	@FindBy(how = How.XPATH, using = "//span[@class='deleteIcon']")
	public WebElement crossIcon;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_OrderRequestLineItem__c.OUV_EditVistaVehicle']")
	public WebElement editVistaVehicle_lineitem;

	@FindBy(how = How.XPATH, using = "//span[@class='genericError uiOutputText']")
	public WebElement errormsg;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand cuf-publisherShareButton undefined uiButton'] ")
	public WebElement saveVistaedit;

	@FindBy(how = How.XPATH, using = "//div[@title='Recall']")
	public WebElement Recall;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Approval History')]")
	public WebElement ApprovalHistroy;

	@FindBy(how = How.XPATH, using = "//span[text()='Show Actions']")
	public WebElement ShowActions2;

	@FindBy(how = How.XPATH, using = "//a[@title='Edit']")
	public WebElement Edit2;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'This record is locked')]")
	public WebElement recordlock;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit ATO Request Type']")
	public WebElement EditATORequestType;

	public String ATORequestType(String Request) {
		return "//span[@title='" + Request + "']";
	}

	@FindBy(how = How.XPATH, using = "(//lightning-icon[@class='slds-icon-utility-down slds-input__icon slds-input__icon_right slds-icon_container'])[2]")
	public WebElement selectrequesttype;

	@FindBy(how = How.XPATH, using = "//strong[contains(text(),'Review the errors')]")
	public WebElement Errormsg;

	@FindBy(how = How.XPATH, using = "//span[text()='ATO Summary']")
	public WebElement ATOsummary;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'ATO line items can not be added or deleted')]")
	public WebElement ATORequesttypeError;

	@FindBy(how = How.XPATH, using = "//button[@title='Close this window']")
	public WebElement CloseATO;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'You might not have permission to edit it')]")
	public WebElement Deleteerror;

	@FindBy(how = How.XPATH, using = "//button[@title='Close this window']")
	public WebElement closewindow;

	public String text(String value) {
		return "//span[contains(text(),'" + value + "')]";
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'ATO Request Line Items')]")
	public WebElement RequestLineItems;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Replacement Vehicle']")
	public WebElement ReplacementVehicle;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search OUV Vehicles...']")
	public WebElement ReplacementVehicleinput;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Show All Results for')]")
	public WebElement ShowResults;

	public String ATOvehicle(String Request) {
		return "//a[@title='" + Request + "']";

	}

	public String Disposalroute(String DisposalRoute) {
		return "//span[@title='" + DisposalRoute + "']";
	}

	public String OUVReportingLevel1(String Reportinglevel1) {
		return "//span[@title='" + Reportinglevel1 + "']";

	}

	public String OUVReportingleveL2(String Reportinglevel2) {
		return "//span[@title='" + Reportinglevel2 + "']";
	}

	@FindBy(how = How.XPATH, using = "//div[@title='Recall']")
	public WebElement recallBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Recall']")
	public WebElement ConfirmRecallBtn;

	@FindBy(how = How.XPATH, using = "(//a[@title='New'])[2]")
	public WebElement newStatus;

	public String OUVReportingLevel3(String Reportinglevel3) {
		return "//span[@title='" + Reportinglevel3 + "']";
	}

	public String Market(String Market) {
		return "//span[@title='" + Market + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement ATORequestType;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Proposed Add To Fleet Date']")
	public WebElement EditAddToFleetDate;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Proposed Hand In Date']")
	public WebElement EditHandInDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_AddToFleetDate__c']")
	public WebElement AddToFleetDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_ProposedHandedInDate__c']")
	public WebElement HandInDate;

	@FindBy(how = How.XPATH, using = "//a[text()='Proposed Hand In Date']")
	public WebElement ProposedHandInDateError;

	@FindBy(how = How.XPATH, using = "//h1[@title='Approval History']")
	public WebElement ApprovalHistoryTitle;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[text()='Yes'])[2]")
	public WebElement OverBudgetYes;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[text()='No'])[2]")
	public WebElement OverBudgetNo;

	public String userverification(String UserName) {
		return "(//a[text()='" + UserName + "'])[1]";
	}

	@FindBy(how = How.ID, using = "username")
	public WebElement userNameTextBox;

	@FindBy(how = How.ID, using = "password")
	public WebElement passwordTextBox;

	@FindBy(how = How.ID, using = "Login")
	public WebElement loginBtn;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Welcome to the Global ')]")
	public WebElement welcome;

	@FindBy(how = How.XPATH, using = "//span[text()='Items to Approve']")
	public WebElement itemToApprove;

	@FindBy(how = How.XPATH, using = "//a[@title='View ATO Request']")
	public WebElement ATORequestID;

	@FindBy(how = How.XPATH, using = "//lightning-icon[@class='slds-button__icon slds-global-header__icon slds-icon-utility-notification slds-icon_container forceIcon']")
	public WebElement Notification;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'is requesting approval for ato request')]")
	public WebElement ATORequestNotification;

	@FindBy(how = How.XPATH, using = "//a[@title='Approve']")
	public WebElement ApproveButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Approve']")
	public WebElement confirmApprove;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'ATO-')]//parent::div)[2]")
	public WebElement BacktoATO;

	@FindBy(how = How.XPATH, using = "//span[text()='Approved']")
	public WebElement Approvedstatus;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_OrderRequest__c.Submit_for_Approval']")
	public WebElement submitForApproval;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Show All Results for')]")
	public WebElement fleetSelection;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-theme--error slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")
	public WebElement errormessage;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_OrderRequest__c.Submit_for_Approval']")
	public WebElement Submitforapproval;

	public String value(String value) {
		return "//span[text()='" + value + "']";
	}

	public String value2(String value) {
		return "//a[text()='" + value + "']";
	}

	// 194

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'OUV Fleet Members')]")
	public WebElement FleetMembers;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[text()='Pending Approval'])[2]")
	public WebElement PendingApproval;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])[2]")
	public WebElement RequestLinenew;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Records...']")
	public WebElement searchrecords;

	@FindBy(how = How.XPATH, using = "(//a[@class='forceBreadCrumbItem'])[2]")
	public WebElement ATOback;

	@FindBy(how = How.XPATH, using = "//strong[contains(text(),'Review the errors on this page.')]")
	public WebElement requestlineerror;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement Cancel;

	@FindBy(how = How.XPATH, using = "(//span[@data-aura-class='uiOutputText'])[1]")
	public WebElement SelectATORequest;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement SelectAll;

	@FindBy(how = How.XPATH, using = " //input[@name='OUV_OrderRequest__c-search-input']")
	public WebElement SearchATORequest;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[7]")
	public WebElement ApprovedATORequest;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Notes & Attachments')]")
	public WebElement Newnote;

	@FindBy(how = How.XPATH, using = "//div[@title='Upload Files']")
	public WebElement Uploadfiles;

	@FindBy(how = How.XPATH, using = "(//span[@class=' label bBody'])[5]")
	public WebElement Done;

	@FindBy(how = How.XPATH, using = "//span[text()='Show Actions']")
	public WebElement dropdown;

	public String dropdown(String drop) {
		return "(//a[@title='" + drop + "'])";
	}

	@FindBy(how = How.XPATH, using = "//textarea[@class=' textarea']")
	public WebElement Textarea;

	@FindBy(how = How.XPATH, using = "(//span[@class=' label bBody'])[4]")
	public WebElement saved;

	@FindBy(how = How.XPATH, using = "(//input[@type='text'])[3]")
	public WebElement Title;

	@FindBy(how = How.XPATH, using = "//span[text()='Upload']")
	public WebElement upload;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement delete;

	@FindBy(how = How.XPATH, using = "//input[@title='Search People']")
	public WebElement SearchPeople;

	@FindBy(how = How.XPATH, using = "//span[text()='Who Can Access']")
	public WebElement whocanaccess;

	@FindBy(how = How.XPATH, using = "(//a[text()='Viewer'])[2]")
	public WebElement viewer1;

	public String viewer1(String dp1) {
		return "(//a[@title='" + dp1 + "'])[1]";
	}

	@FindBy(how = How.XPATH, using = "(//a[text()='Viewer'])[3]")
	public WebElement viewer2;

	@FindBy(how = How.XPATH, using = "//span[text()='Remove from Record']")
	public WebElement Remove;

	@FindBy(how = How.XPATH, using = "//span[text()='Share']")
	public WebElement Share2;

	@FindBy(how = How.XPATH, using = "(//lightning-icon[@class='slds-icon-utility-down slds-input__icon slds-input__icon_right slds-icon_container'])[2]")
	public WebElement ATORequestType1;

	@FindBy(how = How.XPATH, using = "//textarea[@maxlength='32768']")
	public WebElement BuisinessReference;

	@FindBy(how = How.XPATH, using = "(//button[text()='View all dependencies'])[1]")
	public WebElement dependencies1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Level1Category__c']")
	public WebElement OUVREportinglevel1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Level2Category__c']")
	public WebElement OUVREportinglevel2;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Level3Category__c']")
	public WebElement OUVREportinglevel3;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[3]")
	public WebElement ATORequest1;

	@FindBy(how = How.XPATH, using = "//button[@name='apply']")
	public WebElement apply1;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search OUV Fleets...']")
	public WebElement SearchOUVFleet;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[7]")
	public WebElement Market1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_User__c']")
	public WebElement InternalJLRcOntact;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_AddToFleetDate__c']")
	public WebElement ProposedFleeteDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_ProposedHandedInDate__c']")
	public WebElement Handindate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_TargetUtilisationPercentage__c']")
	public WebElement Percentage;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[9]")
	public WebElement DisposalRoute1;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[10]")
	public WebElement Reworkroute;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveAndNew']")
	public WebElement Saveandnew;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement saveandedit;

	@FindBy(how = How.XPATH, using = " //button[@name='Clone']")
	public WebElement CloneATORequest;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement SelectDropdown;

	@FindBy(how = How.XPATH, using = "//span[text()='Cancel/Withdraw ATO Request']")
	public WebElement Cancel1;

	@FindBy(how = How.XPATH, using = "//a[text()='Cancelled']")
	public WebElement Canceloption;

	@FindBy(how = How.XPATH, using = "//span[text()='Show more actions']")
	public WebElement Dropdown2;

	public String Dropdown2(String drop) {
		return "(//span[text()='" + drop + "'])";
	}

	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement SelectLineItems;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[2]")
	public WebElement Save2;

	// OUV-195

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[3]")
	public WebElement ApprovedATORequest1;

	@FindBy(how = How.XPATH, using = "//li[text()='You cannot have more than one ATO request line item for Single Vehicle ATO Request Types.'")
	public WebElement VerifyError;

	@FindBy(how = How.XPATH, using = "(//span[text()='Cancel'])[2]")
	public WebElement Cancel2;

	// OUV-196

	@FindBy(how = How.XPATH, using = "//li[text()='This record is locked. If you need to edit it, contact your admin.']")
	public WebElement VerifyError1;

	// OUV-197

	@FindBy(how = How.XPATH, using = "//li[text()='insufficient access rights on object id']")
	public WebElement VerifyError2;

	@FindBy(how = How.XPATH, using = "//a[@data-label='Chatter']")
	public WebElement Chatter;

	@FindBy(how = How.XPATH, using = "//span[text()='Share an update...']")
	public WebElement ShareText;

	@FindBy(how = How.XPATH, using = "//div[@data-placeholder='Share an update...']")
	public WebElement ShareTexts;

	@FindBy(how = How.XPATH, using = "//span[text()='Jaguar Land Rover... Only'] ")
	public WebElement Selectdrop;

	public String Selectdrop(String Select) {
		return "//span[@title='" + Select + "']";
	}

	@FindBy(how = How.XPATH, using = "(//button[@title='Click, or press Ctrl+Enter']")
	public WebElement Share;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'ATO Request History')])[1]")
	public WebElement ATOreqHis;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate uiOutputDateTime'])[1]")
	public WebElement ATOreqHistory;

	@FindBy(how = How.XPATH, using = "//button[@name='today']")
	public WebElement TodayBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search OUV Vehicles...']")
	public WebElement Replacevehicle;

	@FindBy(how = How.XPATH, using = "//a[@data-recordid='a0Q0N00000LRvxKUAT']")
	public WebElement selectRepV;

	@FindBy(how = How.XPATH, using = "//h2[text()='We hit a snag.']")
	public WebElement snagmsg;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_OrderRequest__c.OUV_CancelWithdrawATORequest']")
	public WebElement CancelWithdraw;

	@FindBy(how = How.XPATH, using = "//button[@title='List View Controls']")
	public WebElement ListViewControl;

	@FindBy(how = How.XPATH, using = "(//a[@role='menuitem'])[1]")
	public WebElement NewFilter;

	@FindBy(how = How.XPATH, using = "//input[@name='title']")
	public WebElement ListName;

	@FindBy(how = How.XPATH, using = "//a[@class=' addFilter']")
	public WebElement AddFilterLink;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement FiledDropDown;

	@FindBy(how = How.XPATH, using = "//a[@class='uiButton--default uiButton--neutral uiButton picklistButton']")
	public WebElement ValueDropDown;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement save;

	@FindBy(how = How.XPATH, using = "(//a[@role='menuitem'])[7]")
	public WebElement DeleteFilter;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button'])[3]")
	public WebElement dependenciesm;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button'])[7]")
	public WebElement alldependeciesm;

	@FindBy(how = How.XPATH, using = "(//li[@class='slds-dropdown-trigger slds-dropdown-trigger_click slds-button_last overflow'])")
	public WebElement Dropdown;

	@FindBy(how = How.XPATH, using = "//runtime_platform_actions-action-renderer[@title='Create Line Items']")
	public WebElement CreateLineItems;

	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement NumberofLineItems;

	@FindBy(how = How.XPATH, using = "//span[text()='Save']")
	public WebElement SaveLineItems;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Vehicle User']")
	public WebElement VehicleUser;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[3]")
	public WebElement Replacementvehicle;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/a0Q0N00000LRhOBUA1/')]")
	public WebElement Selectvehicle;

	@FindBy(how = How.XPATH, using = "//li[text()='The Replacement Vehicle needs to have the Master status \"Live\"']")
	public WebElement VerifyErrorMessage;

}
