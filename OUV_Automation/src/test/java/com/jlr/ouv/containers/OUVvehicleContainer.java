package com.jlr.ouv.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OUVvehicleContainer {

	@FindBy(how = How.XPATH, using = "(//span[@class='uiImage'])[1]")
	public WebElement loginImg;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Log Out')]")
	public WebElement logout;

	@FindBy(how = How.XPATH, using = "//a[@title='Home']")
	public WebElement homeTab;

	@FindBy(how = How.ID, using = "username")
	public WebElement userNameTextBox;

	@FindBy(how = How.ID, using = "password")
	public WebElement passwordTextBox;

	@FindBy(how = How.ID, using = "Login")
	public WebElement loginBtn;

	@FindBy(how = How.XPATH, using = "//span[@title='JLR Retail Case Management']")
	public WebElement jlrText;

	public String selectMenu(String text) {
		return "//b[text()='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//h2[text()='Verify Your Identity']")
	public WebElement Verifycode;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Welcome to the Global ')]")
	public WebElement welcome;

	@FindBy(how = How.XPATH, using = "//input[@id='emc']")
	public WebElement vCodeTextBox;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-show']")
	public WebElement menuBtn;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input']")
	public WebElement searchBox;

	@FindBy(how = How.ID, using = "save")
	public WebElement verifyBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='OUV Vehicles']")
	public WebElement OUVvehicles;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Vehicle__c-search-input']")
	public WebElement searchVehiclebox;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement VehicleSel1;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Master Status']")
	public WebElement editMasterstatus;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement Masterstatus;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement substatus;

	public String ValueSelection(String value) {
		return "//span[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement save;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement Cancel;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement All;

	@FindBy(how = How.XPATH, using = "//span[text()='Recently Viewed']")
	public WebElement RecentlyViewed;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Master status')]")
	public WebElement StatusError;

	@FindBy(how = How.XPATH, using = "(//span[@class='test-id__section-header-title slds-truncate' and contains(text(),'Disposal')])")
	public WebElement JustificationHeader;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element__control slds-grow textarea-container'])[2]")
	public WebElement Justification;

	@FindBy(how = How.XPATH, using = "//a[text()='Primary Location']")
	public WebElement Primarylocationerror;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Accounts...']")
	public WebElement Primarylocation;

	@FindBy(how = How.XPATH, using = "(//span[text()='Full VIN'])[2]")
	public WebElement Fullvin;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_KeysTransfferedToNewManager__c']")
	public WebElement Transferedkeys;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'NL')]")
	public WebElement DefaultFleet;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'NL')]")
	public WebElement DefaultLastFleet;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'Used')]")
	public WebElement DefaultreportingLv1;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'Cars awaiting sale')]")
	public WebElement DefaultreportingLv2;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'Local')]")
	public WebElement DefaultreportingLv3;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'Handed In')]")
	public WebElement HandedIn;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Christophe Leclerc')]")
	public WebElement DefaultOwner;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[contains(text(),'202')])[6]")
	public WebElement ProposedHandIn;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[contains(text(),'202')])[7]")
	public WebElement LatestPlannedHand;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[contains(text(),'202')])[8]")
	public WebElement ForecastActual;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Sub-Status is a required field')]")
	public WebElement SubStatusError;

	@FindBy(how = How.XPATH, using = "//li[contains(text(),'Transfer - No of Keys Transferred')]")
	public WebElement KeysTransferred;

	@FindBy(how = How.XPATH, using = "(//span[@title='--None--'])[2]")
	public WebElement subnone;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit OUV Vehicle Name'])[2]")
	public WebElement editVehicleName;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement vehicleName;

	public String Fieldvalidation(String field) {
		return "(//lightning-formatted-text[text()='" + field + "'])[4]";
	}
	// OUV-130

	@FindBy(how = How.XPATH, using = "(//span[@data-aura-class='uiOutputText'])[1]")
	public WebElement SelectOUVVehicles;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement SelectAll;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Vehicle__c-search-input']")
	public WebElement SearchOUVVehicles;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[7]")
	public WebElement FleetVehicle;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Transfer - Keys transferred'])[1]")
	public WebElement SelectEdit1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_KeysTransfferedToNewManager__c']")
	public WebElement SelectCheckbox1;

	@FindBy(how = How.XPATH, using = "(//input[@name='OUV_KeysRecievedByNewManager__c'])[3]")
	public WebElement SelectCheckbox2;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Fleet']")
	public WebElement Clickedit;

	@FindBy(how = How.XPATH, using = "//button[@title='Clear Selection']")
	public WebElement ClickCancel;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[7]")
	public WebElement SearchFleet;

	@FindBy(how = How.XPATH, using = "(//button[text()='View all dependencies'])[5]")
	public WebElement dependencies;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_ReportingLevel1__c']")
	public WebElement Level1;

	public String Level1(String Reporting1) {
		return "//span[@title='" + Reporting1 + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_ReportingLevel2__c']")
	public WebElement Level2;

	public String Level2(String Reporting2) {
		return "//span[@title='" + Reporting2 + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_ReportingLevel3__c']")
	public WebElement Level3;

	public String Level3(String Reporting3) {
		return "//span[@title='" + Reporting3 + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[@name='apply']")
	public WebElement Apply;

	@FindBy(how = How.XPATH, using = "//a[text()='OUV Reporting Level 1']")
	public WebElement Verifyerror;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_neutral discardBtn']")
	public WebElement DiscardChanges;

	@FindBy(how = How.XPATH, using = "//span[text()='Home']")
	public WebElement Home;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement Saved;

	// OUV-131

	@FindBy(how = How.XPATH, using = "(//button[@class='test-id__inline-edit-trigger slds-shrink-none inline-edit-trigger slds-button slds-button_icon-bare'])[9]")
	public WebElement EditCheck;

	@FindBy(how = How.XPATH, using = "//ul[@class='errorsList slds-list_dotted slds-m-left_medium']")
	public WebElement verifyerror1;

	// OUV-182

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[3]")
	public WebElement FleetVehicle1;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='New']")
	public WebElement VerifyApproveStatus;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit New Requested Hand In Date'])[1]")
	public WebElement EditHandDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_NewRequestedHandedInDate__c']")
	public WebElement EnterHandDate;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[2]")
	public WebElement EditJustification;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element__control slds-grow textarea-container'])[2]")
	public WebElement EnterJustification;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement Save1;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Pending Approval']")
	public WebElement VerifyPendingApproveStatus;

	// OUV-183
	@FindBy(how = How.XPATH, using = "//span[text()='21 items • Sorted by OUV Vehicle Name • Filtered by All ouv vehicles • ']")
	public WebElement click;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[9]")
	public WebElement FleetVehicle2;

	@FindBy(how = How.XPATH, using = "//li[text()='This record is locked. If you need to edit it, contact your admin.']")
	public WebElement VerifyError;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_neutral discardBtn']")
	public WebElement discardchanges;

	// OUV-184

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[7]")
	public WebElement FleetVehicle3;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Master Status']")
	public WebElement EditMasterstatus;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement ClickMasterStatus;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement SubStatus;

	@FindBy(how = How.XPATH, using = "(//span[text()='Order Number'])[2]")
	public WebElement Ordernum;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[2]")
	public WebElement TypeJustification;

	public String TypeJustification(String value) {
		return "//textarea[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveStatus;

	// OUV-188

	@FindBy(how = How.XPATH, using = "(//span[text()='Outstanding RIS'])[2]")
	public WebElement Press;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_NewRequestedHandedInDate__c']")
	public WebElement Editdate;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[12]")
	public WebElement Saleschannel;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_FinalSalesPrice__c']")
	public WebElement Salesprice;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_SalePriceDate__c']")
	public WebElement Salesdate;

	// OUV-190

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[19]")
	public WebElement FleetVehicle4;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Pending Approval']")
	public WebElement Verifypending;

	// OUV-204

	@FindBy(how = How.XPATH, using = "// li[text()='Please ensure the fields \"Transfer - No of Keys Transferred\" &\r\n"
			+ "	// \"Transfer - Keys transferred\" are completed before transferring the vehicle\r\n"
			+ "	// to New Manager']")
	public WebElement Verifyerror1;

	// OUV -207

	@FindBy(how = How.XPATH, using = "//button[@title='Edit OUV Reporting Level 3']")
	public WebElement Clicklevelone;

	// OUV-229

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement SelectVehicle;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/OUV_Gatehouse_Log__c/')]")
	public WebElement gatehouselogs;
////a[starts-with(@href,'/lightning/r/OUV_Gatehouse_Log__c/')]
	// (//span[contains(text(),'OUV Gatehouse Logs')])[3]
	@FindBy(how = How.XPATH, using = "(//a[@title='R-004188'])[1]")
	public WebElement Selectreservation;

	@FindBy(how = How.XPATH, using = "//span[text()='Booking-5951']")
	public WebElement SelectBooking;

	@FindBy(how = How.XPATH, using = "(//span[text()='Closed'])[2]")
	public WebElement Checkclosedstatus;

	@FindBy(how = How.XPATH, using = "//button[text()='Check Out / Check In']")
	public WebElement Clickcheckinorout;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[16]")
	public WebElement Clickcheckout;

	@FindBy(how = How.XPATH, using = "//button[text()='Continue']")
	public WebElement Clickcontinue;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-align-middle slds-hyphenate']")
	public WebElement Checkerror1;

	@FindBy(how = How.XPATH, using = "//button[text()='Check Out / Check In']")
	public WebElement checkin;

	// OUV-230

	@FindBy(how = How.XPATH, using = "//button[text()='Complete']")
	public WebElement Clickcomplete;

	@FindBy(how = How.XPATH, using = "(//input[@class=' input'])[1]")
	public WebElement Changetopastdate;

	@FindBy(how = How.XPATH, using = "//a[@class='select']")
	public WebElement Keyschecked;

	public String Keyschecked(String value) {
		return "//a[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//span[@class='deleteIcon']")
	public WebElement Clickclear;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Accounts']")
	public WebElement Selectlocation;

	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement Clickmileage;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[4]")
	public WebElement Clicksave;

	@FindBy(how = How.XPATH, using = "//li[text()='Booking Check In date cannot be earlier than Booking start Date']")
	public WebElement verifyingerror;

	// OUV-221

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[18]")
	public WebElement SelectVehicle1;

	@FindBy(how = How.XPATH, using = "(//a[@title='R-004071'])[2]")
	public WebElement Selectreservation1;

	@FindBy(how = How.XPATH, using = "//span[text()='Booking-5852']")
	public WebElement SelectBooking1;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit New Requested Hand In Date']")
	public WebElement EditNewRequestedHandInDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_NewRequestedHandedInDate__c']")
	public WebElement NewRequestedHandInDate;

	public String HandInDateValidation(String date) {
		return "(//lightning-formatted-text[text()='" + date + "'])[2]";
	}

	public String ActualDateValidation(String date) {
		return "(//lightning-formatted-text[text()='" + date + "'])[3]";
	}

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='New']")
	public WebElement Newstatus;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit OUV Vehicle Name: Item 1']")
	public WebElement EditVehicleNamefield;

	@FindBy(how = How.XPATH, using = "(//span[@class='triggerContainer'])[1]")
	public WebElement EditVehicleName;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-grow input']")
	public WebElement VehicleName;

	@FindBy(how = How.XPATH, using = "//span[text()='Save']")
	public WebElement SaveVehicleName;

	@FindBy(how = How.XPATH, using = "//span[text()='Cancel']")
	public WebElement CancelSaveVehicleName;

	@FindBy(how = How.XPATH, using = "//span[@class='genericSummary uiOutputText']")
	public WebElement VehicleNameError;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_Vehicle__c.OUV_Check_in_Check_out']")
	public WebElement CheckInCheckOutBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[14]")
	public WebElement CheckInCheckBx;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[13]")
	public WebElement CheckOutCheckBx;

	@FindBy(how = How.XPATH, using = "//button[text()='Continue']")
	public WebElement ContinueBtn;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/OUV_Gatehouse_Log__c/')]")
	public WebElement OUVGateHouseLink;

	@FindBy(how = How.XPATH, using = "//span[text()='Check-in & Close']")
	public WebElement CheckInClose;

	@FindBy(how = How.XPATH, using = "//span[text()='Select only one option']")
	public WebElement ErrorMsg;

	@FindBy(how = How.XPATH, using = "(//a[@class='forceBreadCrumbItem'])[2]")
	public WebElement VehicleLink;

	@FindBy(how = How.XPATH, using = "//div[text()='Error!']")
	public WebElement CheckOutError;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon-border-filled']")
	public WebElement ShowActions;

	@FindBy(how = How.XPATH, using = "//textarea[@maxlength='32768']")
	public WebElement vehiclecondition;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Check-in & Complete')]")
	public WebElement CheckInComplete;

	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement Mileage;

	@FindBy(how = How.XPATH, using = "//input[@maxlength='250']")
	public WebElement DriverLogistic;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Accounts']")
	public WebElement ReturnLoc;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[3]")
	public WebElement saveButton;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'Public Relations')]")
	public WebElement livereportingLv1;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'Press/PR')]")
	public WebElement livereportingLv2;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'Local')]")
	public WebElement livereportingLv3;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'PR NL')]")
	public WebElement LiveFleet;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Jan Vroemans')]")
	public WebElement fleetOwner;

	public String validationpoint(String field) {
		return "(//lightning-formatted-text[text()='" + field + "'])";
	}

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[contains(text(),'202')])[5]")
	public WebElement ActualAddtoFleet;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit New Requested Hand In Date']")
	public WebElement NewHandinDate;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEdit;

	@FindBy(how = How.XPATH, using = "//h2[contains(text(),'We hit a snag.')]")
	public WebElement SnagMsg;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement Cancelchanges;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Show All')]")
	public WebElement ShowAll;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Approval History')]")
	public WebElement ApprovalHistory;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Pending')]")
	public WebElement Pending;

	public String ValueSelectiontext(String value) {
		return "//span[contains(text(),'" + value + "')]";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='PR NL']")
	public WebElement fleet;

	@FindBy(how = How.XPATH, using = "//span[text()='Administration']")
	public WebElement Adminstrationtitle;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[text()='New'])[1]")
	public WebElement RegistrationStatus;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Registration Required']")
	public WebElement RegistrationRequirededit;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_RegistrationRequired__c']")
	public WebElement RegCheckbox;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Pending Approval']")
	public WebElement PendingApproval;

	@FindBy(how = How.XPATH, using = "//button[@title='List View Controls']")
	public WebElement ListView;

	@FindBy(how = How.XPATH, using = "//span[text()='Clone']")
	public WebElement clonelist;

	@FindBy(how = How.XPATH, using = "//input[@name='title']")
	public WebElement clonelisttitle;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral test-confirmButton uiButton--default uiButton--brand uiButton']")
	public WebElement clonesave;

	@FindBy(how = How.XPATH, using = "//a[@class=' addFilter']")
	public WebElement addFilter;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Select an Option']")
	public WebElement SelectOption;

	@FindBy(how = How.XPATH, using = "(//span[@title='Fleet'])[2]")
	public WebElement Fleetselection;

	@FindBy(how = How.XPATH, using = "//input[@class='filterTextInput valueInput input uiInput uiInputText uiInput--default uiInput--input']")
	public WebElement valuefleet;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral doneButton uiButton']")
	public WebElement Donefilter;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand saveButton headerButton']")
	public WebElement Savefilter;

	@FindBy(how = How.XPATH, using = "//button[@title='Close Filters']")
	public WebElement Closefilter;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement deleteclonelist;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral test-confirmButton uiButton--default uiButton--brand uiButton']")
	public WebElement deleteclonelistconfor;

	@FindBy(how = How.XPATH, using = "(//button[text()='View all dependencies'])[5]")
	public WebElement Viewalldependencies;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_KeysRecievedByNewManager__c']")
	public WebElement KeysReceived;

	@FindBy(how = How.XPATH, using = "//span[@title='Registration Approval Status']")
	public WebElement RegistrationApprovalselection;

	@FindBy(how = How.XPATH, using = "//a[@class='uiButton--default uiButton--neutral uiButton picklistButton']")
	public WebElement OptionSelects;

	@FindBy(how = How.XPATH, using = "//a[@title='New']")
	public WebElement OptionSelectsNew;

	@FindBy(how = How.XPATH, using = "(//span[text()='Handed In'])[1]")
	public WebElement HandedIn1;

	@FindBy(how = How.XPATH, using = "(//span[text()='Handed In'])[2]")
	public WebElement HandedIn2;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-button__icon slds-icon_container slds-icon-utility-warning'])[1]")
	public WebElement Bulkerror1;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-button__icon slds-icon_container slds-icon-utility-warning'])[2]")
	public WebElement Bulkerror2;

	@FindBy(how = How.XPATH, using = "//span[@class='slds-checkbox_faux']")
	public WebElement BulkerrorAll;

	@FindBy(how = How.XPATH, using = "//button[text()='Dismiss']")
	public WebElement Dismiss;

	@FindBy(how = How.XPATH, using = "//button[text()='Dismiss and Close']")
	public WebElement Dismissandclose;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Fleet']")
	public WebElement EditFleet;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[1]")
	public WebElement ClearFleet;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search OUV Fleets...']")
	public WebElement Fleet;

	@FindBy(how = How.XPATH, using = "//span[text()='Editable - Live to Hand in']")
	public WebElement EditableLiveHandin;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[2]")
	public WebElement Select1;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[3]")
	public WebElement Select2;

	@FindBy(how = How.XPATH, using = "//a[@title='Change Master Status']")
	public WebElement ChangeMasterStatus;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[1]")
	public WebElement MasterStat;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	public WebElement subMaster;

	@FindBy(how = How.XPATH, using = "//div[@class='uiInput uiInputCheckbox uiInput--default uiInput--checkbox']//child::input[@type='checkbox']")
	public WebElement Keystransfer;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand cuf-publisherShareButton undefined uiButton']")
	public WebElement Savebulk;

	@FindBy(how = How.XPATH, using = "//button[@title='OK']")
	public WebElement Okaybutton;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-theme--error slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")
	public WebElement toastmsg;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[12]")
	public WebElement check1;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_neutral'])[3]")
	public WebElement dismissbutton;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Accounts']")
	public WebElement primaryLoc;

	@FindBy(how = How.XPATH, using = "(//span[text()='UAT Editable - Pre live to Live'])[1]")
	public WebElement UATEditable;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Public Relations']")
	public WebElement OUVRLone;

	@FindBy(how = How.XPATH, using = "//button[text()='Pre-Sold Admin']")
	public WebElement PreSoldAdmin;

	@FindBy(how = How.XPATH, using = "//div[text()='You do not have the privileges to update the Vehicle Pre-Sold Checkbox.']")
	public WebElement ErrorPreSold;

	@FindBy(how = How.XPATH, using = "//button[text()='Cancel']")
	public WebElement CancelSold;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle User']")
	public WebElement VehicleUser;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle User Email']")
	public WebElement VehicleUserEmail;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle User - City']")
	public WebElement UserCity;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle User – State']")
	public WebElement UserState;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle User – County']")
	public WebElement UserCountry;

	@FindBy(how = How.XPATH, using = "//span[text()='Vehicle User – ZIP Code']")
	public WebElement UserZipCode;

	@FindBy(how = How.XPATH, using = "//span[text()='Finance Classification']")
	public WebElement FinanceClassification;

	@FindBy(how = How.XPATH, using = "//span[text()='Inventory Status']")
	public WebElement InventoryStatus;

	@FindBy(how = How.XPATH, using = "//span[text()='Currency']")
	public WebElement Currency;

	@FindBy(how = How.XPATH, using = "//span[text()='Total Initial Retail Price']")
	public WebElement TotalInitialRetailPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='Retail Price']")
	public WebElement RetailPrice;

	@FindBy(how = How.XPATH, using = "//span[text()='Residual Value']")
	public WebElement ResidualValue;

	@FindBy(how = How.XPATH, using = "//span[text()='Loss On Resale']")
	public WebElement LossOnResale;

	@FindBy(how = How.XPATH, using = "//span[text()='Financials']")
	public WebElement FinancialsHeader;

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditButton;

	@FindBy(how = How.XPATH, using = "//font[text()='Administration']")
	public WebElement Admininstration;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_RegistrationDate__c']")
	public WebElement RegistrationDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_DeRegistrationDate__c']")
	public WebElement DeRegistrationDate;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit OUV Reporting Level 1']")
	public WebElement RepoLev1;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit OUV Reporting Category 1']")
	public WebElement RepoCategory1;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_Vehicle__c.Pre_Sold_Admin']")
	public WebElement PresoldAdmin;

	@FindBy(how = How.XPATH, using = "//h4[contains(text(),'Error')]")
	public WebElement errorMsg;

	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Cancel')]")
	public WebElement cancelbtn;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit New Requested Hand In Approval Status']")
	public WebElement NewHandinEdit;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[13]")
	public WebElement NewHandinclick;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_NewRequestedHandedInDate__c']")
	public WebElement Handinfiled;

	@FindBy(how = How.XPATH, using = "//button[@name='today']")
	public WebElement Today;

	@FindBy(how = How.XPATH, using = "//textarea[@maxlength='1300']")
	public WebElement Vehiclecond;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_CustomerIdentifier__c']")
	public WebElement Cid;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[15]")
	public WebElement SalesChannel;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_FinalSalesPrice__c']")
	public WebElement Finalprice;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_SalePriceDate__c']")
	public WebElement DateofSale;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Invoice Number')]")
	public WebElement Invoicelabel;

// OUV-208

	@FindBy(how = How.XPATH, using = "//strong[contains(text(),'Review the errors')]")
	public WebElement Errormsg;
	@FindBy(how = How.XPATH, using = "//span[text()='Handed-in']")
	public WebElement HandedInHeader;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Approved']")
	public WebElement ApprovedHandedIn;

	@FindBy(how = How.XPATH, using = "(//button[text()='View all dependencies'])[3]")
	public WebElement ViewDependencies;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[7]")
	public WebElement VehicleSel7;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement Save;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[44]")
	public WebElement VerifyCheckBox;

	@FindBy(how = How.XPATH, using = "(//span[@class='uiImage'])[1]")
	public WebElement Logoutimage;

	@FindBy(how = How.XPATH, using = "//a[text()='Add Username']")
	public WebElement Addusername;

	@FindBy(how = How.XPATH, using = "//a[@title='Defender P400 ex PR - nu PB']")
	public WebElement SelectSameVehicle;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit New Requested Hand In Date']")
	public WebElement EditHandinDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_NewRequestedHandedInDate__c']")
	public WebElement EnterHandinDate;

	@FindBy(how = How.XPATH, using = "//div[text()='This field cannot be modified because the Vehicle Pre-sold is checked']")
	public WebElement VerifyHandindateError;

	@FindBy(how = How.XPATH, using = "//button[text()='Cancel']")
	public WebElement Clickcancel;

	@FindBy(how = How.XPATH, using = "//a[text()='New Requested Hand In Date']")
	public WebElement ClickHandindateError;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[2]")
	public WebElement justification;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[2]")
	public WebElement MultipleCheck1;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[3]")
	public WebElement MultipleCheck2;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox--faux'])[4]")
	public WebElement MultipleCheck3;

	@FindBy(how = How.XPATH, using = "//a[@title='Change Master Status']")
	public WebElement Changemasterstatus;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[1]")
	public WebElement masterstatus;

	public String masterstatus(String value) {
		return "//a[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[2]")
	public WebElement Substatus;

	public String Substatus(String value) {
		return "//a[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@class=' default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup']")
	public WebElement selectlocation;

	public String selectlocation(String value) {
		return "//a[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@data-interactive-lib-uid='12']")
	public WebElement TransferCheck;

	@FindBy(how = How.XPATH, using = "(//a[@class='select'])[3]")
	public WebElement Keysreceived;

	public String Keysreceived(String value) {
		return "//a[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[3]")
	public WebElement saved;

	@FindBy(how = How.XPATH, using = "//span[text()='OK']")
	public WebElement ok;

	@FindBy(how = How.XPATH, using = "(//span[text()='Recently Viewed'])[2]")
	public WebElement RecentlyViews;

	@FindBy(how = How.XPATH, using = "(//span[text()='Live'])[1]")
	public WebElement validateliveststus;

	@FindBy(how = How.XPATH, using = "(//span[text()='Live'])[2]")
	public WebElement validateliveststus1;

	@FindBy(how = How.XPATH, using = "(//span[text()='Live'])[2]")
	public WebElement validateliveststus2;

	@FindBy(how = How.XPATH, using = "//span[text()='Editable - Pre live to Live']")
	public WebElement Editable;

	@FindBy(how = How.XPATH, using = "//span[text()='Review the quick action errors.']")
	public WebElement verifyerrormsg;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button']//parent::lightning-button)[1]")
	public WebElement MasterStatusselect;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_VehicleMasterStatus__c']")
	public WebElement Masterstatus1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_VehicleSubStatus__c']")
	public WebElement substatus1;

	@FindBy(how = How.XPATH, using = "//font[contains(text(),'Insufficient permissions')]")
	public WebElement PermissionsError;

}
