package com.jlr.ouv.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OUVTestContainer 
{
	@FindBy(how = How.ID, using = "username")
    public WebElement userNameTextBox;
	
    @FindBy(how = How.ID, using = "password")
    public WebElement passwordTextBox;
    
    @FindBy(how = How.ID, using = "Login")
    public WebElement loginBtn;
    
    @FindBy(how = How.XPATH, using = "//span[text()='OUV Calendar']")
    public WebElement OUVCalendar;
    
    @FindBy(how = How.XPATH, using = "(//select[contains(@class,'single slds-select')])[1]")
    public WebElement marketDD;
    
    @FindBy(how = How.XPATH, using = "(//select[contains(@class,'uiInput--select')])[3]")
    public WebElement fleetDD;
    
    @FindBy(how = How.XPATH, using = "//button[contains(@class,'slds-m-top--medium')]")
    public WebElement searchVehicleBtn;
    
    @FindBy(how = How.XPATH, using = "(//li[starts-with(text(),'VIN: SALRA2BK')])[1]")
    public WebElement VINCheck;
    
    @FindBy(how = How.XPATH, using = "//button[text()='day']")
    public WebElement dayBtn;

    @FindBy(how = How.XPATH, using = "//button[text()='week']")
    public WebElement weekBtn;
    
    @FindBy(how = How.XPATH, using = "//button[text()='month']")
    public WebElement monthBtn;
    
    @FindBy(how = How.XPATH, using = "//div[@class='scheduler_default_cell scheduler_default_cell_business' and contains(@style,'660px') and contains(@style,'top: 88.8px')]")
    public WebElement SourceWeek;
    
    @FindBy(how = How.XPATH, using = "//div[@class='scheduler_default_cell scheduler_default_cell_business' and contains(@style,'690px') and contains(@style,'top: 88.8px')]")
    public WebElement DestinationWeek;
    
    @FindBy(how= How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[190]")
    public WebElement calendarSelection;
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[1]")
    public WebElement reservation;
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[15]")
    public WebElement startDate;
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[16]")
    public WebElement endDate;
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[5]")
    public WebElement startTime;
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[6]")
    public WebElement endTime;
    
    @FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Booking Quick Reference']")
    public WebElement bookingQuickRef;
    
    @FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Reason for Booking']")
    public WebElement reasonBooking;
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[4]")
    public WebElement bookingJustification;
    
    public String ValueSelection(String value) {
        return "//span[@title='"+value+"']";
    }
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[5]")
    public WebElement bookingRefType;
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[6]")
    public WebElement passoutType;
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[7]")
    public WebElement journeyType;
    
    @FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Internal JLR Contact']")
    public WebElement internalJLR;
    
    @FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Internal JLR Contact Email']")
    public WebElement internalJLREmail;
    
    @FindBy(how = How.XPATH, using = "//input[@class='slds-input slds-combobox__input' and @placeholder='Search Accounts...']")
    public WebElement location;
    
    @FindBy(how = How.XPATH, using = "(//li[@class='slds-listbox__item'])[3]")
    public WebElement selectLocation;
    
    @FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Other Location']")
    public WebElement otherLocation;
    
    @FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Driver 1']")
    public WebElement driver1;
    
    @FindBy(how = How.XPATH, using = "//button[@value='save']")
    public WebElement saveReservation;
    
    @FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[1]")
    public WebElement reservationDD;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Reservation cannot be created')]")
    public WebElement errorReservation;
    
    @FindBy(how = How.XPATH, using = "//button[contains(@class,'slds-button_icon-bare') and @title='Close']")
    public WebElement closeError;
    
    @FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon slds-modal__close'])[3]")
    public WebElement closeReservation;
    
    @FindBy(how = How.XPATH, using = "//li[contains(text(),'Test Booking')]")
    public WebElement calendarTable;
    
    public String bookingTable(String value) {
        return "//li[contains(text(),'"+value+"')]";
    }
    
    @FindBy(how = How.XPATH, using = "(//span[@class='uiImage'])[1]")
    public WebElement loginImg;
    
    @FindBy(how = How.XPATH, using = "//a[contains(text(),'Log Out')]")
    public WebElement logout;
    
    @FindBy(how = How.XPATH, using = "//span[@class='toastMessage slds-text-heading--small forceActionsText']")
    public WebElement error;
    
    @FindBy(how = How.XPATH, using = "//button[text()='Edit']")
    public WebElement editRes;
    
    @FindBy(how = How.XPATH, using = "//a[starts-with(text(),'Booking')]")
    public WebElement OUVBookingLink;
    
    @FindBy(how = How.XPATH, using = "(//span[contains(text(),'Reservations (1)')])[1]")
    public WebElement reservationTab;
    
    @FindBy(how = How.XPATH, using = "//a[@title='Home']")
    public WebElement homeTab;
    
    @FindBy(how = How.XPATH, using = "//input[@name='BookingStartDate']")
    public WebElement BookingStartdate;
    
    @FindBy(how = How.XPATH, using = "//input[@name='BookingEndDate']")
    public WebElement BookingEnddate;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Booking end date must be greater or equal to start date')]")
    public WebElement errorEnddate;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Booking Start date must equal today or greater')]")
    public WebElement errorStartdate;
    
    @FindBy(how = How.XPATH, using = "//input[@name='FullVIN']")
    public WebElement VIN;
    
    @FindBy(how = How.XPATH, using = "//input[@name='PrimaryLocation']")
    public WebElement Location;
    
    @FindBy(how = How.XPATH, using = "(//select[contains(@class,'uiInput--select')])[5]")
    public WebElement FleetMD;
    
    @FindBy(how = How.XPATH, using = "//input[@name='RegistrationPlate']")
    public WebElement RegistrationPlate;
    
    @FindBy(how = How.XPATH, using = "//span[text()='Available for Transfer']")
    public WebElement AvailableforTransfer;
    
    @FindBy(how = How.XPATH, using = "//span[text()='Pre-Sold']")
    public WebElement PreSold;
    
    @FindBy(how = How.XPATH, using = "//span[text()='Job Related Fleet Vehicles']")
    public WebElement FleetVehicles;
    
    @FindBy(how = How.XPATH, using = "//span[text()='Buyback Vehicles']")
    public WebElement BuybackVehicles;
    
    @FindBy(how = How.XPATH, using = "//span[text()='Reserved Vehicles']")
    public WebElement ReservedVehicles;
    
    @FindBy(how = How.XPATH, using = "//input[@name='ModelDescription']")
    public WebElement ModelDescription;
    
    @FindBy(how = How.XPATH, using = "//div[contains(text(),'Complete this field')]")
    public WebElement incomplecterror;
    
    @FindBy(how = How.XPATH, using = "(//button[contains(text(),'Cancel')])[3]")
    public WebElement cancel;
    
    @FindBy(how = How.XPATH, using = "//div[contains(text(),'You have entered an invalid format')]")
    public WebElement formaterror;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Please fill the Driver 1')]")
    public WebElement drivererror;
  
    @FindBy(how = How.XPATH, using = "//input[@name='DerivativePackDescription']")
    public WebElement DerivativePackDescription;
    
    @FindBy(how = How.XPATH, using = "//input[@name='PaintDescription']")
    public WebElement PaintDescription;
  
    @FindBy(how = How.XPATH, using = "//input[@name='TrimDescription']")
    public WebElement TrimDescription;
    
    @FindBy(how = How.XPATH, using = "//input[@name='TransmissionDescription']")
    public WebElement TransmissionDescription;
    
    @FindBy(how = How.XPATH, using = "//input[@name='Option']")
    public WebElement OptionDescription;
}
