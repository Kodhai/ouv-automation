package com.jlr.ouv.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OUVCalendarContainer {
	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[115]")
	public WebElement calendarSelection;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[93]")
	public WebElement Calendarselection;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[1]")
	public WebElement reservation;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'Reservation')]")
	public WebElement reservationSelection;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[12]")
	public WebElement Calendarselection1;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[1]")
	public WebElement startDate;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[2]")
	public WebElement endDate;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement startTime;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement endTime;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Booking Quick Reference']")
	public WebElement bookingQuickRef;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Reason for Booking']")
	public WebElement reasonBooking;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[4]")
	public WebElement bookingJustification;

	public String ValueSelection(String value) {
		return "//span[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[5]")
	public WebElement bookingRefType;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[6]")
	public WebElement passoutType;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input' and @placeholder='Select an Option'])[7]")
	public WebElement journeyType;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Internal JLR Contact']")
	public WebElement internalJLR;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Internal JLR Contact Email']")
	public WebElement internalJLREmail;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input slds-combobox__input' and @placeholder='Search Accounts...']")
	public WebElement location;

	@FindBy(how = How.XPATH, using = "(//li[@class='slds-listbox__item'])[3]")
	public WebElement selectLocation;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Other Location']")
	public WebElement otherLocation;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input' and @aria-label='Driver 1']")
	public WebElement driver1;

	@FindBy(how = How.XPATH, using = "(//button[contains(@class,'slds-button_last') and text()='Save'])[3]")
	public WebElement saveReservation;

	@FindBy(how = How.XPATH, using = "//span[@class='toastMessage slds-text-heading--small forceActionsText']")
	public WebElement error;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Booking end date must be greater or equal to start date')]")
	public WebElement errorPriorEnd;

	@FindBy(how = How.XPATH, using = "//button[@value='dayView']")
	public WebElement dayView;

	@FindBy(how = How.XPATH, using = "//button[@value='weekView']")
	public WebElement weekView;

	@FindBy(how = How.XPATH, using = "//button[@value='monthView']")
	public WebElement monthView;

	@FindBy(how = How.XPATH, using = "//div[@class='scheduler_default_cell']")
	public WebElement unavailableVehcReser;

	@FindBy(how = How.XPATH, using = "//div[@class='scheduler_default_cell scheduler_default_cell_business']")
	public WebElement availableVehcReser;

	@FindBy(how = How.XPATH, using = "//button[@value='b25-cancel-new-record']")
	public WebElement cancelReser;

	@FindBy(how = How.ID, using = "//a[@class='slds-button slds-button_reset slds-context-bar__label-action']")
	public WebElement MoreBtn;

	@FindBy(how = How.ID, using = "(//span[text()='OUV Calendar'])[2]")
	public WebElement OUVCalendardrpdwn;

	@FindBy(how = How.ID, using = "//span[text()='No Record Found']")
	public WebElement NoRecordError;

	@FindBy(how = How.ID, using = "username")
	public WebElement userNameTextBox;

	@FindBy(how = How.ID, using = "password")
	public WebElement passwordTextBox;

	@FindBy(how = How.ID, using = "Login")
	public WebElement loginBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='OUV Calendar']")
	public WebElement OUVCalendar;

	@FindBy(how = How.XPATH, using = "//div[text()='Search Vehicle']")
	public WebElement searchVehicleText;

	@FindBy(how = How.XPATH, using = "(//select[contains(@class,'single slds-select')])[1]")
	public WebElement marketDD;

	@FindBy(how = How.XPATH, using = "(//select[contains(@class,'uiInput--select')])[3]")
	public WebElement fleetDD;

	@FindBy(how = How.XPATH, using = "//button[contains(@class,'slds-m-top--medium')]")
	public WebElement searchVehicleBtn;

	@FindBy(how = How.XPATH, using = "//button[text()='day']")
	public WebElement dayBtn;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[1]")
	public WebElement SelectVehicleFromDay;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon slds-button_icon-border-filled slds-button_last'])")
	public WebElement rightButton;

	@FindBy(how = How.XPATH, using = "//button[text()='week']")
	public WebElement weekBtn;

	@FindBy(how = How.XPATH, using = "//button[text()='month']")
	public WebElement monthBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='uiImage'])[1]")
	public WebElement loginImg;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Log Out')]")
	public WebElement logout;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Reservations�(1)')])[1]")
	public WebElement reservationTab;

	@FindBy(how = How.XPATH, using = "//a[@title='Home']")
	public WebElement homeTab;

	@FindBy(how = How.XPATH, using = "(//a[@title='New'])[2]")
	public WebElement OUVBookingNewStatus;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'Cancel')])[3]")
	public WebElement cancel;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-show']")
	public WebElement menuBtn;

	@FindBy(how = How.XPATH, using = "//input[@class='slds-input']")
	public WebElement searchBox;

	@FindBy(how = How.XPATH, using = "//span[@title='JLR Retail Case Management']")
	public WebElement jlrText;

	public String selectMenu(String text) {
		return "//b[text()='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//h2[text()='Verify Your Identity']")
	public WebElement Verifycode;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Welcome to the Global ')]")
	public WebElement welcome;

	@FindBy(how = How.XPATH, using = "//input[@id='emc']")
	public WebElement vCodeTextBox;

	@FindBy(how = How.ID, using = "save")
	public WebElement verifyBtn;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'1:00')]")
	public WebElement dayformat;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Wed')]")
	public WebElement weekformat;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'April')]")
	public WebElement monthformat;

	@FindBy(how = How.XPATH, using = "//div[@class='scheduler_default_rowheader_scroll']")
	public WebElement VehicleList;

	@FindBy(how = How.XPATH, using = "(//select[contains(@class,'single slds-select')])[2]")
	public WebElement BrandDD;

	@FindBy(how = How.XPATH, using = "//option[@label='Jaguar']")
	public WebElement selectBrand;

	public String SelectBrand(String Brand) {
		return "//option[@label='" + Brand + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@name='BookingStartDate']")
	public WebElement BookingStartdate;

	@FindBy(how = How.XPATH, using = "//input[@name='BookingEndDate']")
	public WebElement BookingEnddate;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Booking Start date must equal today or greater')]")
	public WebElement errorStartdate;

	@FindBy(how = How.XPATH, using = "//button[text()='Edit' and @value='b25-edit']")
	public WebElement editRes;

	@FindBy(how = How.XPATH, using = "(//span[text()='Internal JLR Contact Email'])[3]")
	public WebElement internalJLRCntLabel;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Internal JLR Contact Email'])[3]")
	public WebElement EditinternalJLRCnt;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Internal_JLR_Contact_Email__c']")
	public WebElement internalJLRCntTxtBox;

	@FindBy(how = How.XPATH, using = "(//li[contains(text(),'test')])[1]")
	public WebElement firstOUVBooking;

	@FindBy(how = How.XPATH, using = "(//li[contains(text(),'test')])[25]")
	public WebElement secondOUVBooking;

	public String bookingTable(String value) {
		return "//li[contains(text(),'" + value + "')]";
	}

	@FindBy(how = How.XPATH, using = "//button[text()='Delete']")
	public WebElement deleteOUVBooking;

	@FindBy(how = How.XPATH, using = "//button[@value='confirm']")
	public WebElement ConfirmDeleteOUVBooking;

	@FindBy(how = How.XPATH, using = "//div[contains(@class,'slds-notify--toast forceToastMessage')]")
	public WebElement deleteErrorMessage;

	@FindBy(how = How.XPATH, using = " //button[@title='Close' and contains(@class,'slds-button_icon-bare')]")
	public WebElement closeDeleteErrorMessage;

	@FindBy(how = How.XPATH, using = " (//button[@title='Close' and contains(@class,'slds-button slds-button_icon slds-modal__close')])[5]")
	public WebElement closeReservationWindow;

	@FindBy(how = How.XPATH, using = "//button[@value='View in Salesforce']")
	public WebElement viewInSalesForce;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-media__body'])[1]")
	public WebElement reservationText;

	@FindBy(how = How.XPATH, using = "//button[@value='b25-cancel-editing']")
	public WebElement cancelEditOUVBooking;

	@FindBy(how = How.XPATH, using = "//span[contains(@class,'slds-checkbox_standalone')]//child::input[@type='checkbox']")
	public WebElement PrepTrans;

	@FindBy(how = How.XPATH, using = "(//button[@title='Select a date'])[6]")
	public WebElement SelectPrepDate;

	@FindBy(how = How.XPATH, using = "(//button[@title='Select a date'])[5]")
	public WebElement SelectEndDate;

	@FindBy(how = How.XPATH, using = "(//button[@title='Select a date'])[4]")
	public WebElement SelectStartDate;

	@FindBy(how = How.XPATH, using = "//lightning-icon[@class='slds-icon-custom-custom31 slds-combobox__input-entity-icon slds-icon_container']")
	public WebElement Vehicle;

	@FindBy(how = How.XPATH, using = "(//button[@title='Close'])[5]")
	public WebElement Close;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon slds-button_icon-border-filled slds-button_first'])")
	public WebElement Previousmonth;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_event_inner'])[1]")
	public WebElement firstcell;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Booking-')]//parent::c-output-lookup")
	public WebElement BookingID;

	@FindBy(how = How.XPATH, using = "//span[text()='Driver/Vehicle User']")
	public WebElement VehicleLoan;

	@FindBy(how = How.XPATH, using = "//span[text()='Booked Pending Approval']")
	public WebElement BookingPendingApproLabel;

	@FindBy(how = How.XPATH, using = "//span[text()='Details']")
	public WebElement Details;

	@FindBy(how = How.XPATH, using = "//slot[text()='Reservation (Passout)']")
	public WebElement Passout;

	@FindBy(how = How.XPATH, using = "//a[@title='Reject']")
	public WebElement RejectOUVBooking;

	@FindBy(how = How.XPATH, using = "//span[text()='Reject']")
	public WebElement RejectOUVBookingBtn;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Auto Generate VLA'])")
	public WebElement EditVLA;

	@FindBy(how = How.XPATH, using = "(//input[@name='OUV_VehicleLoanAgreement__c'])")
	public WebElement AutoVLA;

	@FindBy(how = How.XPATH, using = "(//button[text()='View all dependencies'])[5]")
	public WebElement ViewdependTemplate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_LoanCategory__c']")
	public WebElement VLATemplate;

	@FindBy(how = How.XPATH, using = "//span[@title='Individual - Jaguar (with cost)']")
	public WebElement Jaguarwithcost;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Loan_Agreement_Language__c']")
	public WebElement VLALanguage;

	@FindBy(how = How.XPATH, using = "//span[text()='English']")
	public WebElement English;

	@FindBy(how = How.XPATH, using = "//button[text()='Apply']")
	public WebElement ApplyVLA;

	@FindBy(how = How.XPATH, using = "//runtime_platform_actions-action-renderer[@title='Save']")
	public WebElement Savechanges;

	@FindBy(how = How.XPATH, using = "//h2[text()='Related List Quick Links']")
	public WebElement Relatedlist;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Open OUV Vehicle Loan Agreements')])")
	public WebElement OpenVLAAgreement;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/OUV_Vehicle_Loan_Agreement__c')])")
	public WebElement VLAAgreement;

	@FindBy(how = How.XPATH, using = "(//div[text()='New'])")
	public WebElement NewVLA;

	@FindBy(how = How.XPATH, using = " //span[text()='Frequent Driver']")
	public WebElement frequentDriverLabel;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search OUV Frequent Drivers...']")
	public WebElement SearchDrivers;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_First_Name__c']")
	public WebElement VLAFirstName;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Last_Name__c']")
	public WebElement VLALastName;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Email__c']")
	public WebElement VLAEmailID;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[1]")
	public WebElement VLACompany;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Phone__c']")
	public WebElement VLAPhone;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Date_of_Birth__c']")
	public WebElement VLADOB;

	@FindBy(how = How.XPATH, using = " //input[@name='OUV_Occupation__c']")
	public WebElement VLAOccupation;

	@FindBy(how = How.XPATH, using = " //input[@name='Address_Line_1__c']")
	public WebElement VLAAddress1;

	@FindBy(how = How.XPATH, using = " //input[@name='Address_Line_2__c']")
	public WebElement VLAAddress2;

	@FindBy(how = How.XPATH, using = " //input[@name='Address_Line_3__c']")
	public WebElement VLAAddress3;

	@FindBy(how = How.XPATH, using = " //input[@name='Address_Line_4__c']")
	public WebElement VLAAddress4;

	@FindBy(how = How.XPATH, using = " //input[@name='Address_Line_5__c']")
	public WebElement VLAPostcode;

	@FindBy(how = How.XPATH, using = " //input[@name='OUV_Passport_ID_number__c']")
	public WebElement VLAPassportNumber;

	@FindBy(how = How.XPATH, using = " //input[@name='OUV_Driver_License_Number__c']")
	public WebElement VLALicenceNumber;

	@FindBy(how = How.XPATH, using = " (//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement VLAInsuredBy;

	@FindBy(how = How.NAME, using = "OUV_Country_of_issue__c")
	public WebElement VLACountry;

	@FindBy(how = How.NAME, using = "OUV_Licence_Start_Date__c")
	public WebElement VLALicStartDate;

	@FindBy(how = How.NAME, using = "OUV_Licence_Expiry_Date__c")
	public WebElement VLALicExpiryDate;

	@FindBy(how = How.NAME, using = "OUV_Insurance_company__c")
	public WebElement VLAInsCompany;

	@FindBy(how = How.XPATH, using = "  (//span[@class='uiOutputText'])[2]")
	public WebElement backToBookingVLA;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Show All Results for')]")
	public WebElement ShowAllResults;

	public String Driverselection(String Frequentdriver) {
		return "//a[text()='" + Frequentdriver + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement EditTypeofDriver;

	@FindBy(how = How.XPATH, using = " //button[text()='Today']")
	public WebElement TodayCalendar;

	@FindBy(how = How.XPATH, using = "//span[@title='Individual']")
	public WebElement Individual;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEdit;

	@FindBy(how = How.XPATH, using = "//button[text()='Submit for Approval']")
	public WebElement Submit;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Booking-')])")
	public WebElement Backtobooking;

	@FindBy(how = How.XPATH, using = " //ul[@class='errorsList slds-list_dotted slds-m-left_medium']")
	public WebElement internalJLRCntError;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Reservation successfully')]")
	public WebElement Confirmationmsg;

	public String Fieldvalidation(String fields) {
		return "//lightning-formatted-text[text()='" + fields + "']";
	}

	public String Fieldvalidation2(String field) {
		return "(//lightning-formatted-text[text()='" + field + "'])[2]";
	}

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Booking Quick Reference'])[2]")
	public WebElement editBookingQuickRef;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_BookingShortDescription__c']")
	public WebElement bookingQuickRefField;

	@FindBy(how = How.XPATH, using = "//li[text()='The fields Loan Agreement Market, Loan Agreement Category and Loan Agreement Language must be filled.']")
	public WebElement VLAerror;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement Cancelchanges;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-truncate'])[2]")
	public WebElement Homebutton;

	@FindBy(how = How.XPATH, using = "//span[text()='Booking Approvers']")
	public WebElement BookingApprover;

	@FindBy(how = How.XPATH, using = "(//span[text()='Aldo van Troost'])[1]")
	public WebElement FleetManager;

	@FindBy(how = How.XPATH, using = "//li[@title='Details']")
	public WebElement DetailsTab;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Service Reservations')]//parent::a")
	public WebElement ServiceReservations;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Booking-')]//parent::a")
	public WebElement BookingId;

	@FindBy(how = How.XPATH, using = "//p[text()='No items to display.']")
	public WebElement Noaccessories;

	@FindBy(how = How.XPATH, using = "//span[@class='uiOutputText' and contains(text(),'R-')]//parent::a")
	public WebElement BacktoReservations;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[15]")
	public WebElement startDates;

	@FindBy(how = How.XPATH, using = "//a[@title='Annual Market Summary']")
	public WebElement AnnualMarketSummary;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_AnnualMarketSummary__c-search-input']")
	public WebElement SearchAMS;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'OUV Forecast Snapshots')]//parent::a")
	public WebElement OUVForecastSnapshots;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'OUV Forecast Snapshots')]//parent::a)[3]")
	public WebElement OUVForecastSnapshot;

	@FindBy(how = How.XPATH, using = "//button[@title='Show quick filters']")
	public WebElement Showquickfilters;

	public String status(String status) {
		return "(//span[text()='" + status + "']//parent::label)";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Status']")
	public WebElement Editstatusbutton;

	@FindBy(how = How.XPATH, using = "//h2[text()='Related List Quick Links']")
	public WebElement Relatedlistquicklinks;

	@FindBy(how = How.XPATH, using = "(//lightning-icon[@class='slds-icon-utility-down slds-input__icon slds-input__icon_right slds-icon_container'])[2]")
	public WebElement Statusdropdown;

	public String statusupdate(String status) {
		return "(//span[@title='" + status + "'])";
	}

	@FindBy(how = How.XPATH, using = "(//td[@class='dataCol'])[1]")
	public WebElement userRole;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Approval History')])[1]")
	public WebElement ApprovalHistory;

	@FindBy(how = How.XPATH, using = " //a[text()='Add Username']")
	public WebElement addUserName;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Frequent_Drivers__c-search-input']")
	public WebElement searchBarDriver;

	@FindBy(how = How.XPATH, using = "//p[text()='No items to display.']")
	public WebElement driverNotFound;

	public String manager(String fleetmanager) {
		return "//a[text()='" + fleetmanager + "']";
	}

	@FindBy(how = How.XPATH, using = "//div[@title='Recall']")
	public WebElement Recall;

	@FindBy(how = How.XPATH, using = "//span[text()='Recall']")
	public WebElement RecallButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Recalled']")
	public WebElement Recalled;

	@FindBy(how = How.XPATH, using = "//lightning-icon[@class='slds-button__icon slds-global-header__icon slds-icon-utility-notification slds-icon_container forceIcon']")
	public WebElement Notification;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[starts-with(text(),'Booking-')])[1]")
	public WebElement BookingHeader;

	public String Booking(String ID) {
		return "(//span[contains(text(),'" + ID + "')])";
	}

	@FindBy(how = How.XPATH, using = "//div[@title='Approve']")
	public WebElement Approve;

	@FindBy(how = How.XPATH, using = "//a[@title='All Folders']")
	public WebElement allFolders;

	@FindBy(how = How.XPATH, using = "//a[@title='OUV Benelux Reports']")
	public WebElement OUVBeneluxReport;

	@FindBy(how = How.XPATH, using = "//a[@title='UAT test']")
	public WebElement UATTestFolder;

	@FindBy(how = How.XPATH, using = "//a[@title='Benelux Users Report']")
	public WebElement beneluxUserReport;

	@FindBy(how = How.XPATH, using = "//span[text()='Approve']")
	public WebElement ApproveButton;

	@FindBy(how = How.XPATH, using = "(//span[text()='Approved'])[1]")
	public WebElement ApprovedStatus;

	@FindBy(how = How.XPATH, using = "//span[text()='OUV Bookings']")
	public WebElement OUVBookingWindow;

	@FindBy(how = How.XPATH, using = "//span[text()='OUV Vehicles']")
	public WebElement OUVVehicles;

	@FindBy(how = How.XPATH, using = "(//span[text()='Approved'])[3]")
	public WebElement OUVBookingApproved;

	@FindBy(how = How.XPATH, using = "(//span[text()='Rejected'])[4]")
	public WebElement OUVBookingRejected;

	@FindBy(how = How.XPATH, using = "//span[text()='OUV Vehicles']//parent::li")
	public WebElement OUVVehicleHeader;

	@FindBy(how = How.XPATH, using = " (//a[contains(@href,'/lightning/r/')])[1]")
	public WebElement firstOUVBookingFrmAll;

	@FindBy(how = How.XPATH, using = "//span[text()='Recently Viewed']")
	public WebElement RecentlyViewed;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement All;

	@FindBy(how = How.XPATH, using = "//span[text()='Frequent Drivers owner']")
	public WebElement freqDriOwner;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate outputLookupLink')])[2]")
	public WebElement freqDriOwnerFirstName;

	@FindBy(how = How.XPATH, using = "//span[text()='More']")
	public WebElement moreBtn;

	@FindBy(how = How.XPATH, using = "(//a[contains(@class,'slds-truncate outputLookupLink slds-truncate')])[1]")
	public WebElement firstFreqDriver;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Vehicle__c-search-input']")
	public WebElement SearchVehicle;

	@FindBy(how = How.XPATH, using = "//span[@title='Master Status']")
	public WebElement MasterStatus;

	@FindBy(how = How.XPATH, using = "(//a[@class='flex-wrap-ie11'])[2]")
	public WebElement driverOwner;

	@FindBy(how = How.XPATH, using = "//div[@title='User Detail']")
	public WebElement userDetailBtn;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement SelectVehicle;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Pre-Live']")
	public WebElement PreLiveStatus;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Master Status']")
	public WebElement EditMasterStatus;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/lightning/r/SSign__SSEnvelope__c/')]")
	public WebElement S_SignVLA;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'S-Sign Request')]")
	public WebElement FirstS_Sign;

	@FindBy(how = How.XPATH, using = "//a[contains(@class,'slds-truncate outputLookupLink slds-truncate')]")
	public WebElement FirstVLA;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_Vehicle_Loan_Agreement__c.OUV_Request_More_Info']")
	public WebElement collectDriverInfo;

	@FindBy(how = How.XPATH, using = "//button[text()='Send Sign Request']")
	public WebElement sendSignBtn;

	@FindBy(how = How.XPATH, using = "//span[text()='Encrypted Full Name (VLA)']")
	public WebElement encryptedFullName;

	@FindBy(how = How.XPATH, using = "//span[text()='Encrypted Driver 1']")
	public WebElement encryptedDriver1;

	@FindBy(how = How.XPATH, using = "//span[text()='Driver/Vehicle User']")
	public WebElement driverInfo;

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/lightning/r/')]//child::span[@id='window' and contains(text(),'Booking')]")
	public WebElement backToBooking;

	@FindBy(how = How.XPATH, using = "(//button[text()='View all dependencies'])[2]")
	public WebElement Viewalldependencies;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_VehicleMasterStatus__c']")
	public WebElement SelectMasterStatus;

	@FindBy(how = How.XPATH, using = "//span[@title='Live']")
	public WebElement Live;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_VehicleSubStatus__c']")
	public WebElement SelectSubStatus;

	@FindBy(how = How.XPATH, using = "//span[@title='Available / Idle']")
	public WebElement Available;

	@FindBy(how = How.XPATH, using = "//h2[text()='We hit a snag.']")
	public WebElement Error;

	@FindBy(how = How.XPATH, using = "(//button[@title='List View Controls'])[1]")
	public WebElement ListViewControls;

	@FindBy(how = How.XPATH, using = "//span[text()='Clone']")
	public WebElement Clone;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[2]")
	public WebElement SaveClone;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Select an Option'])[1]")
	public WebElement Field;

	public String SelectFleet(String fleet) {
		return "(//span[text()='" + fleet + "'])[2]";
	}

	public String SelectmasterStatus(String masterstatus) {
		return "(//span[text()='" + masterstatus + "'])[2]";
	}

	@FindBy(how = How.XPATH, using = "(//span[text()='Copy of All'])[1]")
	public WebElement CopyofAll;

	@FindBy(how = How.XPATH, using = "//span[text()='Show filters']")
	public WebElement Showfilter;

	@FindBy(how = How.XPATH, using = "//a[text()='Add Filter']")
	public WebElement AddFilter;

	@FindBy(how = How.XPATH, using = "//input[@class='filterTextInput valueInput input uiInput uiInputText uiInput--default uiInput--input']")
	public WebElement EnterValue;

	@FindBy(how = How.XPATH, using = "//span[text()='Done']")
	public WebElement Done;

	@FindBy(how = How.XPATH, using = "//a[text()='0 options selected']")
	public WebElement ValueMasterStatus;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement SaveFilter;

	@FindBy(how = How.XPATH, using = "//button[@title='Close Filters']")
	public WebElement CloseFilter;

	@FindBy(how = How.XPATH, using = "//button[@name='apply']")
	public WebElement apply;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[5]")
	public WebElement passOutTypeDD;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[6]")
	public WebElement JourneyTypeDD;

	@FindBy(how = How.XPATH, using = " (//span[contains(@class,'slds-truncate') and text()='Vehicle Loan Agreement'])[2]")
	public WebElement VehicleLoanAgreementLabel;

	public String VLAFieldvalidation(String field) {
		return "(//lightning-formatted-text[text()='" + field + "'])[4]";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='OUV Bookings']")
	public WebElement OUVBookings;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Booking__c-search-input']")
	public WebElement OUVBookingsearch;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_Booking__c.OUV_Generate_Auction_Document']")
	public WebElement GenerateDocument;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Template']")
	public WebElement EditTemplate;

	@FindBy(how = How.XPATH, using = "(//input[@aria-autocomplete='none'])[9]")
	public WebElement NoTemplate;

	@FindBy(how = How.XPATH, using = "//span[text()='Reassign']")
	public WebElement ReassignButton;

	@FindBy(how = How.XPATH, using = "//span[text()='Change Owner']")
	public WebElement Changeowner;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement searchusers;

	@FindBy(how = How.XPATH, using = "//button[@name='change owner']")
	public WebElement changeownerbutton;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-theme--error slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")
	public WebElement ChangeownerError;

	@FindBy(how = How.XPATH, using = "//button[@name='cancel']")
	public WebElement cancelowner;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand cuf-publisherShareButton undefined uiButton']")
	public WebElement SaveDocument;

	@FindBy(how = How.XPATH, using = "//li[text()='Please fill the Template field in the Document Template section below']")
	public WebElement ErrorTemplate;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral cuf-publisherCancelButton uiButton--default uiButton--brand uiButton']")
	public WebElement CancelDocument;

	@FindBy(how = How.XPATH, using = "(//li[@class='slds-dropdown-trigger slds-dropdown-trigger_click slds-button_last overflow'])")
	public WebElement EditDropdown;

	@FindBy(how = How.XPATH, using = "//runtime_platform_actions-action-renderer[@title='Cancel Booking']")
	public WebElement CancelBooking;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand cuf-publisherShareButton undefined uiButton']")
	public WebElement Savecancelbooking;

	@FindBy(how = How.XPATH, using = "(//a[@data-tab-name='Cancelled'])[3]")
	public WebElement Cancelled;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement OUVSel1;

	@FindBy(how = How.XPATH, using = "//button[@name='Edit']")
	public WebElement EditButton;

	@FindBy(how = How.XPATH, using = "(//lightning-icon[@class='slds-icon-utility-down slds-input__icon slds-input__icon_right slds-icon_container'])[2]")
	public WebElement SelectBookingRequest;

	@FindBy(how = How.XPATH, using = "//strong[contains(text(),'Review the errors')]")
	public WebElement Errormsg;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon slds-modal__close closeIcon slds-button_icon-bare slds-button_icon-inverse']")
	public WebElement CloseButton;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[starts-with(text(),'VLA-')]")
	public WebElement VLANumber;

	@FindBy(how = How.XPATH, using = "//a[starts-with(text(),'Booking-')]")
	public WebElement BookingId1;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[contains(text(),'Created')]")
	public WebElement VLAstatus;

	@FindBy(how = How.XPATH, using = "(//span[@class='notification-text uiOutputText'])[1]")
	public WebElement firstNotification;

	@FindBy(how = How.XPATH, using = "(//div[@class='month_default_event'])[1]")
	public WebElement firstReservation;

	@FindBy(how = How.XPATH, using = "(//div[@class='month_default_event'])[2]")
	public WebElement SecondReservation;

	@FindBy(how = How.XPATH, using = "//div[@class='month_default_event' and contains(@style,'--background-color: #A569BD')] ")
	public WebElement firstReservationcolour;

	@FindBy(how = How.XPATH, using = "//div[@class='month_default_event' and contains(@style,'--background-color: #229954')]")
	public WebElement SecondReservationcolour;

	public String textdriverclick(String ID) {
		return "//*[contains(text(),'" + ID + "')]";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='Temporary Accessories']")
	public WebElement TempAccessories;

	@FindBy(how = How.XPATH, using = "(//a[contains(@title,'AR-')])[2]")
	public WebElement AccessoryReservation2;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Reservations')])[2]")
	public WebElement Reseravtions;

	@FindBy(how = How.XPATH, using = "//a[starts-with(text(),'R-')]")
	public WebElement ReservationName;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Permanent Accessories')])[1]")
	public WebElement PermanentAccessories;

	@FindBy(how = How.XPATH, using = "(//div[@title='New'])")
	public WebElement NewBtn;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Accessories...']")
	public WebElement Accessory;

	@FindBy(how = How.XPATH, using = "//a[starts-with(text(),'PAR-')]")
	public WebElement verifyAccessory;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement DeleteBtn;

	public String Booking2(String ID) {
		return "(//span[contains(text(),'" + ID + "')])[2]";
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Review the errors')]")
	public WebElement CancelError;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Reservations')]//parent::a)[2]")
	public WebElement Reservations;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'R-')]")
	public WebElement SelectReservation;

	@FindBy(how = How.XPATH, using = "//span[text()='Reservation Administration']")
	public WebElement ReservationAdministration;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Keys Checked Out']")
	public WebElement EditKeyscheckbox;

	@FindBy(how = How.XPATH, using = "//input[@name='Keys_Checked_Out__c']")
	public WebElement selectKeyscheckbox;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Contact your administrator for help.')]")
	public WebElement DeleteReservationError;

	@FindBy(how = How.XPATH, using = "//button[@title='Close this window']")
	public WebElement Closewindow;

	@FindBy(how = How.XPATH, using = "//div[@title='Delete']")
	public WebElement DeleteResevation;

	@FindBy(how = How.XPATH, using = "//button[@title='Delete']")
	public WebElement Confirmdelete;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'R-')])")
	public WebElement ReservationsName;

	@FindBy(how = How.XPATH, using = "//button[@name='B25__Reservation__c.Check_Out_Vehicle']")
	public WebElement IssueKeys;

	@FindBy(how = How.XPATH, using = "//button[@name='B25__Reservation__c.Check_In_Vehicle']")
	public WebElement completeReservation;

	@FindBy(how = How.XPATH, using = "//a[@class='select']")
	public WebElement numberofKeys;

	public String Number(String Num) {
		return "//a[@title='" + Num + "'] ";
	}

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Review the errors on this page.')]")
	public WebElement Reviewerrors;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral cuf-publisherCancelButton uiButton--default uiButton--brand uiButton']")
	public WebElement Cancel2;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand cuf-publisherShareButton undefined uiButton']")
	public WebElement Save;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Temporary Accessories')]")
	public WebElement TemporaryAccessories;

	@FindBy(how = How.XPATH, using = "(//div[text()='New'])[2]")
	public WebElement New;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Accessories...']")
	public WebElement SearchAccessories;

	@FindBy(how = How.XPATH, using = "//li[text()='insufficient access rights on cross-reference id']")
	public WebElement ErrorAccessories;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[2]")
	public WebElement SelectsecondVehicle;

	@FindBy(how = How.XPATH, using = "//a[@data-label='OUV Calendar']")
	public WebElement OUVCalendardetails;

	@FindBy(how = How.XPATH, using = "//label[text()='Primary Location']")
	public WebElement PrimaryLocation;

	@FindBy(how = How.XPATH, using = "(//lightning-icon[@class='slds-icon-utility-down slds-input__icon slds-input__icon_right slds-icon_container'])[2]")
	public WebElement ReservationPassoutdropdown;

	@FindBy(how = How.XPATH, using = "//span[@title='Repair']")
	public WebElement RepairType;

	@FindBy(how = How.XPATH, using = "(//span[text()='Reserved Vehicles'])[2]")
	public WebElement ReservedVehicles;

	@FindBy(how = How.XPATH, using = "(//button[contains(@class,'slds-m-top--medium')])[2]")
	public WebElement searchVehicle;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Booking-')]")
	public WebElement backtoBookingID;

	@FindBy(how = How.XPATH, using = "//span[text()='Show Actions']")
	public WebElement ShowActions2;

	@FindBy(how = How.XPATH, using = "//a[@title='Delete']")
	public WebElement Delete;

	@FindBy(how = How.XPATH, using = "//span[text()='Delete']")
	public WebElement DeleteItems;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'VLA-')]")
	public WebElement vehicleloanAgreement;

	@FindBy(how = How.XPATH, using = "//a[contains(@title,'AR-')]")
	public WebElement AccessoryReservation;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Reservations')])[2]")
	public WebElement Reservations2;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Temporary Accessories')])[2]")
	public WebElement TemporaryAccessories2;

	@FindBy(how = How.XPATH, using = "(//button[@name='Clone'])[2]")
	public WebElement Clone3;

	@FindBy(how = How.XPATH, using = "(//div[text()='New'])")
	public WebElement Newone;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'R-')])[7]")
	public WebElement ReservationsName2;

	@FindBy(how = How.XPATH, using = "(//button[@name='Clone'])")
	public WebElement Clone2;

	@FindBy(how = How.XPATH, using = "//li[text()='Single Vehicle Bookings can only have 1 Reservation per Booking']")
	public WebElement ReservationCloneerror;

	@FindBy(how = How.XPATH, using = "//span[text()='Insurance Details']")
	public WebElement InsuranceDetails;

	@FindBy(how = How.XPATH, using = "(//lightning-icon[@class='slds-icon-utility-down slds-input__icon slds-input__icon_right slds-icon_container'])[3]")
	public WebElement Insuredby;

	@FindBy(how = How.XPATH, using = "(//lightning-icon[@class='slds-icon-utility-down slds-input__icon slds-input__icon_right slds-icon_container'])[2]")
	public WebElement SelectDrivertype;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Insurance_company__c']")
	public WebElement InsuranceCompany;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Insurance_policy_number__c']")
	public WebElement InsurancePolicyNumber;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[2]")
	public WebElement VehicleStorage;

	@FindBy(how = How.XPATH, using = "(//textarea[@class='slds-textarea'])[3]")
	public WebElement DrivingExperience;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions'])")
	public WebElement ShowActions;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'VLA-')]")
	public WebElement VLAname;

	public String ValueValidation(String value) {
		return "//lightning-formatted-text[text()='" + value + "']";
	}

	public String TextSelection(String text) {
		return "//span[text()='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//div[@title='Reassign']")
	public WebElement Reassign;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement SearchUsers;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),' A VLA record is required before submitting this booking for approval.')]")
	public WebElement VLArecerror;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon slds-button_icon-border-filled slds-button_last']")
	public WebElement rightButton2;

	@FindBy(how = How.XPATH, using = "//span[text()='Submit for Approval']")
	public WebElement SubmitforApproval;

	@FindBy(how = How.XPATH, using = "//div[text()='Message']")
	public WebElement SubmitError;

	@FindBy(how = How.XPATH, using = "(//button[@name='Edit'])[2]")
	public WebElement EditLoanAgreement;

	@FindBy(how = How.XPATH, using = "//span[text()='First Name']")
	public WebElement FirstNameheader;

	public String EmailValidation(String email) {
		return "//a[text()='" + email + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[3]")
	public WebElement timeslot1;

	@FindBy(how = How.XPATH, using = "//span[@class='toastMessage slds-text-heading--small forceActionsText']")
	public WebElement ConflictMsg;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_icon toastClose slds-notify__close slds-button--icon-inverse slds-button_icon-bare']")
	public WebElement CloseErrorWindow;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[7]")
	public WebElement SelectVehicleFromDay1;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'Cancel')])[3]")
	public WebElement cancleBtn;

	@FindBy(how = How.XPATH, using = "(//label[text()='Time'])[1]")
	public WebElement TimeLable;

	@FindBy(how = How.XPATH, using = "//button[@title='Change Owner']")
	public WebElement ChangeOwner;

	@FindBy(how = How.XPATH, using = "//button[@name='change owner']")
	public WebElement ChangeOwnerBtn;

	@FindBy(how = How.XPATH, using = "(//button[contains(text(),'Cancel')])")
	public WebElement cancleBtn1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Vehicle__c-search-input']")
	public WebElement OUVvehicleSearch;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit New Requested Hand In Date']")
	public WebElement NewHandinDate;

	@FindBy(how = How.XPATH, using = "//h2[contains(text(),'We hit a snag.')]")
	public WebElement SnagMsg;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Associated Booking']")
	public WebElement AssociatedBooking;

	@FindBy(how = How.XPATH, using = "//button[@title='Clear Selection']")
	public WebElement ClearSelection;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search OUV Bookings...']")
	public WebElement AssociatedBookinginput;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'attempt to violate hierarchy constraints')]")
	public WebElement AssociatedBookingerror;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Booking Quick Reference']")
	public WebElement Quick;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Internal JLR Contact']")
	public WebElement JLRContact;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Other Location']")
	public WebElement OtherLocation;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Internal JLR Contact Email']")
	public WebElement JLRContactMail;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Driver 1']")
	public WebElement Driver1;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Driver 2']")
	public WebElement Driver2;

	@FindBy(how = How.XPATH, using = "(//button[text()='Save'])[3]")
	public WebElement Save2;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell'])[2]")
	public WebElement NewBookingRecord;

	@FindBy(how = How.XPATH, using = "//button[text()='Edit']")
	public WebElement Edit;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element__static'])[5]")
	public WebElement RecordId;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Booking-')]//parent::c-output-lookup")
	public WebElement BookingID1;

	@FindBy(how = How.XPATH, using = "//label[text()='OUV Booking']")
	public WebElement OuvBooking;

	// OUV-155

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell'])[2]")
	public WebElement NewBookingRecord1;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[3]")
	public WebElement ClickCheckbox;

	@FindBy(how = How.XPATH, using = "(//button[@title='Select a date'])[6]")
	public WebElement Calender;

	@FindBy(how = How.XPATH, using = "//button[@name='today']")
	public WebElement Today;

	@FindBy(how = How.XPATH, using = "//label[@for='input-413']")
	public WebElement Today1;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[17]")
	public WebElement PrepEndDate;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[8]")
	public WebElement Time;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Booking Quick Reference']")
	public WebElement Quickref;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Reason for Booking']")
	public WebElement ReasonForBooking;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[8]")
	public WebElement BookingJustification;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[9]")
	public WebElement BookingRequestType;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[10]")
	public WebElement PassoutType;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[11]")
	public WebElement JourneyType;

	@FindBy(how = How.XPATH, using = "//input[@aria-label='Driver 1']")
	public WebElement Driverone;

	@FindBy(how = How.XPATH, using = "(//button[text()='Save'])[3]")
	public WebElement ClickSave;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-align-middle slds-hyphenate']")
	public WebElement VerifyErrorMsg;

	@FindBy(how = How.XPATH, using = "(//span[text()='Close'])[7]")
	public WebElement CloseError;

	@FindBy(how = How.XPATH, using = "(//button[text()='Cancel'])[3]")
	public WebElement Cancel;

	// OUV-167

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_rowheader_inner'])[2]")
	public WebElement SelectVehicle1;

	@FindBy(how = How.XPATH, using = "//span[text()='View Record in Salesforce']")
	public WebElement SalesForce;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='tab-name'])[2]")
	public WebElement VerifyLiverStage;

	@FindBy(how = How.XPATH, using = "//button[text()='Check Out / Check In']")
	public WebElement Checkinorout;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[13]")
	public WebElement ClickChechout;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_brand']")
	public WebElement Continue;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-align-middle slds-hyphenate']")
	public WebElement Checkerror;

	// OUV-235

	@FindBy(how = How.XPATH, using = "(//span[text()='Remco van Rookhuizen is requesting approval for ouv booking'])[1]")
	public WebElement clicknotification;

	@FindBy(how = How.XPATH, using = "//button[text()='Clone']")
	public WebElement Clickclone;

	@FindBy(how = How.XPATH, using = "//input[@name='Driver_1__c']")
	public WebElement selectdriver;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement saved;

	@FindBy(how = How.XPATH, using = "//legend[contains(text(),'Start (local time)')]")
	public WebElement Dh;

	@FindBy(how = How.XPATH, using = "//button[@name='today']")
	public WebElement today;

	@FindBy(how = How.XPATH, using = "//a[@title='Select List View']")
	public WebElement SelectBookings;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement SelectAll1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Booking__c-search-input']")
	public WebElement SearchBookings;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[2]")
	public WebElement Rvehicle;

	@FindBy(how = How.XPATH, using = "//div[text()='Error!']")
	public WebElement CheckOutError;

	@FindBy(how = How.XPATH, using = "//span[text()='This vehicle cannot be checked-out. There does not appear to any approved reservation for this vehicle.']")
	public WebElement CheckOutError1;
	@FindBy(how = How.XPATH, using = "(//a[text()='OUV Calendar'])")
	public WebElement clickcalender;

	@FindBy(how = How.XPATH, using = "//button[text()='Search Vehicle']")
	public WebElement Search;

	@FindBy(how = How.XPATH, using = "(//button[text()='Save'])[3]")
	public WebElement save1;

	@FindBy(how = How.XPATH, using = "(//button[text()='Submit for Approval'])[2]")
	public WebElement Submitforapproval;

	@FindBy(how = How.XPATH, using = "//span[text()=' A Reservation is required before submitting this booking for approval.'")
	public WebElement ErrorChecked;

	// OUV-200

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='New']")
	public WebElement checknewstatus;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Booking Quick Reference']")
	public WebElement clickeditbtn;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_BookingShortDescription__c']")
	public WebElement quickref1;

	@FindBy(how = How.XPATH, using = "//textarea[text()='Test Booking']")
	public WebElement bookingreason;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[2]")
	public WebElement bookingreq1;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[6]")
	public WebElement journeytyp;

	@FindBy(how = How.XPATH, using = "(//button[text()='Save'])[3]")
	public WebElement saving;

	// OUV-231

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[1]")
	public WebElement SelectVehicleFromDaynext;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[37]")
	public WebElement SelectVehicleFromDaynext1;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/')])[3]")
	public WebElement secondOUVBookingFrmAll;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/B25__Reservation__c/')]")
	public WebElement clickreservations;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[2]]")
	public WebElement Clickouvvehicle;

	// OUV-170

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement EndTime;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[3]")
	public WebElement selecttime;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/B25__Reservation__c/')]")
	public WebElement Clickreservation;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[2]")
	public WebElement clickouvvehicle;

	@FindBy(how = How.XPATH, using = "//select[@class='slds-select']")
	public WebElement clickdriver1;

	@FindBy(how = How.XPATH, using = "//option[text()='DHL']")
	public WebElement selectdrive;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement Clicksave;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[14]")
	public WebElement ClickChechin;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/OUV_Gatehouse_Log__c/')]")
	public WebElement gatehouselogs;

	// OUV-172

	@FindBy(how = How.XPATH, using = "//span[text()='Show more actions']")
	public WebElement clickdropdown;

	@FindBy(how = How.XPATH, using = "//span[text()='Check-in & Complete']")
	public WebElement checkinandcomplete;

	@FindBy(how = How.XPATH, using = "//input[@class=' input']")
	public WebElement driverfield;

	@FindBy(how = How.XPATH, using = "//input[@class='input uiInputSmartNumber']")
	public WebElement mileagefield;

	@FindBy(how = How.XPATH, using = "//a[@class='select']")
	public WebElement selectkey;

	public String selectkey(String value) {
		return "//a[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@class=' default input uiInput uiInputTextForAutocomplete uiInput--default uiInput--input uiInput uiAutocomplete uiInput--default uiInput--lookup']")
	public WebElement location1;

	@FindBy(how = How.XPATH, using = "//textarea[@class=' textarea']")
	public WebElement condition;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[4]")
	public WebElement save;

	// OUV-222

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/OUV_Vehicle_Utilization__c/')]")
	public WebElement utilizationlogs;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveAndNew']")
	public WebElement SaveandNew;

	@FindBy(how = How.XPATH, using = "//li[contains(text(),'Only 1 VLA record per Booking is allowed.')]")
	public WebElement VLAError;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement NextButton;

	@FindBy(how = How.XPATH, using = "(//button[@name='Edit'])[2]")
	public WebElement Editbutton;

	@FindBy(how = How.XPATH, using = "//h4[text()='Confirmation']")
	public WebElement SubmitConfirmation;

	@FindBy(how = How.XPATH, using = "//button[text()='YES']")
	public WebElement Yes;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Reservation is required')]")
	public WebElement ReservationError;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[2]")
	public WebElement ClickReserve;

	public String ClickReserve(String re) {
		return "//span[@title='" + re + "']";
	}

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-date-time[contains(text(),':')])[1]")
	public WebElement StartDate;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-date-time[contains(text(),':')])[2]")
	public WebElement EndDate;

	@FindBy(how = How.XPATH, using = "(//button[text()='Cancel'])[3]")
	public WebElement CancelBtn;

	@FindBy(how = How.XPATH, using = "(//button[text()='Cancel'])")
	public WebElement CancelPopup;

	@FindBy(how = How.XPATH, using = "(//a[starts-with(@href,'/lightning/r/B25__Reservation__c/')])")
	public WebElement ReservationLink;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[2]")
	public WebElement VehcileLink;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_Vehicle__c.OUV_Check_in_Check_out']")
	public WebElement CheckInCheckOutBtn;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[13]")
	public WebElement CheckInCheckBx;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[12]")
	public WebElement CheckOutCheckBx;

	@FindBy(how = How.XPATH, using = "//button[text()='Continue']")
	public WebElement ContinueBtn;

	@FindBy(how = How.XPATH, using = "//div[text()='Error!']")
	public WebElement ErrorCheckOut;

	@FindBy(how = How.XPATH, using = "(//a[@target='_blank'])[1]")
	public WebElement BookingNumber;

	@FindBy(how = How.XPATH, using = "//span[text()='Booked Pending Approval']")
	public WebElement PendingStatus;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Booking Quick Reference']")
	public WebElement BookingQuickRefPencilIcon;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_BookingShortDescription__c']")
	public WebElement BookingQuickRefTxtBx;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement SaveEdits;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement CancelEdits;

	@FindBy(how = How.XPATH, using = "//h2[text()='We hit a snag.']")
	public WebElement snagMsg;

	@FindBy(how = How.XPATH, using = "(//select[contains(@class,'single slds-select')])[4]")
	public WebElement VehicleSelection;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement StartTime;

	@FindBy(how = How.XPATH, using = "//select[@class='slds-select']")
	public WebElement driverLogistics;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/OUV_Gatehouse_Log__c/')]")
	public WebElement OUVGateHouseLink;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-checkbox_faux'])[14]")
	public WebElement CheckIncloseCheckBx;

	@FindBy(how = How.XPATH, using = "(//a[@title='Confirmed'])[1]")
	public WebElement StatusConfirmed;

	public String textsearch(String ID) {
		return "(//span[text()='" + ID + "'])";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[16]")
	public WebElement Enddate;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement starttime;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-form-element__help']")
	public WebElement errormail;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input'])[17]")
	public WebElement TransDate;

	@FindBy(how = How.XPATH, using = "//span[text()='Approved']")
	public WebElement ApprovedSts;

	@FindBy(how = How.XPATH, using = "//a[@href='/lightning/r/a0K7a000003O4icEAC/view']")
	public WebElement OUVbookingid;

	@FindBy(how = How.XPATH, using = "//span[@title='Permanent Accessories']")
	public WebElement PermAcc;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accessories...'])[2]")
	public WebElement PermAccessories;

	@FindBy(how = How.XPATH, using = "//lightning-icon[@class='slds-icon-standard-default slds-media__figure slds-listbox__option-icon slds-icon_container']")
	public WebElement PermAcc1;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[14]")
	public WebElement SelectVehicleFromDayB;

	@FindBy(how = How.XPATH, using = "//span[@title='Temporary Accessories']")
	public WebElement TemporaryAccessory;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accessories...'])[1]")
	public WebElement TemporaryAccTextBx;

	@FindBy(how = How.XPATH, using = "//span[@title='Service Reservations']")
	public WebElement ServiceReservation;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Services...']")
	public WebElement ServResTextBx;

	@FindBy(how = How.XPATH, using = "//span[@title='Permanent Accessories']")
	public WebElement PermanentAccessory;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search Accessories...'])[2]")
	public WebElement PermAccTxtBx;

	@FindBy(how = How.XPATH, using = "//span[text()='Reserved Vehicles']")
	public WebElement ResevationCheckbx;

	public String AccSelection(String text) {
		return "//b[text()='" + text + "']";
	}

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search apps and items...']")
	public WebElement menuSearchBar;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@title,'VLA-') and contains(@href,'/lightning/r/')]")
	public WebElement VLAFirst;

	@FindBy(how = How.XPATH, using = "//span[text()='OUV Vehicles']")
	public WebElement OUVvehicle;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Vehicle__c-search-input']")
	public WebElement searchVehicleB;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[3]")
	public WebElement VehicleSel2;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Registration Required']")
	public WebElement EditRegistrationRequired;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_RegistrationRequired__c']")
	public WebElement RegReqCheckBox;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Pending Approval']")
	public WebElement PendingApproval;

	@FindBy(how = How.XPATH, using = "//lightning-formatted-text[text()='Approved]")
	public WebElement Approved;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement FirstReservation;

	@FindBy(how = How.XPATH, using = "(//a[@title='Confirmed'])[1]")
	public WebElement ConfirmedStatus1;

	@FindBy(how = How.XPATH, using = "(//a[@title='Confirmed'])[2]")
	public WebElement ConfirmedStatus2;

	@FindBy(how = How.XPATH, using = "//li[contains(text(),'QuickReff')]")
	public WebElement Reservation;

	@FindBy(how = How.XPATH, using = "//button[@title='Personalize your nav bar']")
	public WebElement PersonalizeNav;

	@FindBy(how = How.XPATH, using = "//button[text()='Add More Items']")
	public WebElement AddItems;

	@FindBy(how = How.XPATH, using = "//a[text()='All']")
	public WebElement AllItems;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search all items...']")
	public WebElement SearchItems;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button--neutral uiButton uiButton--default uiButton--neutral button--neutral'])[2]")
	public WebElement CancelSearch;

	@FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button--neutral uiButton uiButton--default uiButton--neutral button--neutral']")
	public WebElement CancelAddItems;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search OUV Bookings...'])[1]")
	public WebElement OUVBooking;

	@FindBy(how = How.XPATH, using = "//span[text()='Annual Market Summary']")
	public WebElement AnnualMarket;

	@FindBy(how = How.XPATH, using = "//span[text()='Benelux']")
	public WebElement Benelux;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_AnnualMarketSummary__c-search-input']")
	public WebElement AnnualMarketSearch;

	@FindBy(how = How.XPATH, using = "(//a[contains(text(),'AMS-')])[1]")
	public WebElement FirstAnnualMarket;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Annual Budget Threshold']")
	public WebElement ThresholdButton;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_AnnualBudgetThreshold__c']")
	public WebElement ThresholdInput;

	@FindBy(how = How.XPATH, using = "//span[text()='Annual Budget Threshold']")
	public WebElement ThresholdElement;

	@FindBy(how = How.XPATH, using = "//a[@id='detailTab__item']")
	public WebElement DetailsElement;

	@FindBy(how = How.XPATH, using = "//a[@id='customTab__item']")
	public WebElement CalenderTab;

	@FindBy(how = How.XPATH, using = "//select[@class='single slds-select select uiInput uiInputSelect uiInput--default uiInput--select']")
	public WebElement Brand;

	@FindBy(how = How.XPATH, using = "(//input[@class='myCheckbox'])[4]")
	public WebElement buyback;

	@FindBy(how = How.XPATH, using = "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[1]")
	public WebElement selectv1;

	@FindBy(how = How.XPATH, using = "//div[@class='slds-icon-waffle']")
	public WebElement Menuopen;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search apps and items...']")
	public WebElement appsearch;

	@FindBy(how = How.XPATH, using = "//b[text()='OUV Fleets']")
	public WebElement OUVFleet;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Fleet__c-search-input']")
	public WebElement SearchFleet;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement fleetname;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement ATOSel1;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Fleet Administrator 2']")
	public WebElement fleetAdminstrator;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[3]")
	public WebElement clearsel;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search People...'])[1]")
	public WebElement fleetadmin;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Approval Route']")
	public WebElement EditApprovalRoute;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[10]")
	public WebElement clearsAR;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search OUV Approval Routes...']")
	public WebElement ApprovalRouteTxtBx;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Fleet Manager'])")
	public WebElement EditFleetManager;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[1]")
	public WebElement clearsFM;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search People...'])[1]")
	public WebElement FleetManagerTxtBx;

	@FindBy(how = How.XPATH, using = "//b[text()='Reports']")
	public WebElement Reports;

	@FindBy(how = How.XPATH, using = "//a[@title='All Folders']")
	public WebElement AllFolders;

	public String FolderSelection(String text) {
		return "//a[text()='" + text + "']";
	}

	public String DetailsCheck(String text) {
		return "(//a[text()='" + text + "'])[1]";
	}

	@FindBy(how = How.XPATH, using = "(//span[text()='Regional Managing Director'])[1]")
	public WebElement RegionalManagingDirector;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit OUV Fleet Name']")
	public WebElement EditFleetName;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Registration Approver']")
	public WebElement EditRegistrationApprover;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[11]")
	public WebElement clearReApr;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search People...'])[3]")
	public WebElement RegistrationApproverTxtBx;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Overnight Pass Approver (LL5)']")
	public WebElement OVRapprover;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[7]")
	public WebElement clearselect;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search People...'])[2]")
	public WebElement ovrplace;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement FreqDriver;

	@FindBy(how = How.XPATH, using = "(//lightning-formatted-text[text()='Event/Sales activity'])[1]")
	public WebElement bookingJust;

	@FindBy(how = How.XPATH, using = "(//span[@class='slds-grid slds-grid--align-spread'])[13]")
	public WebElement passmanager;

	@FindBy(how = How.XPATH, using = "//span[text()='Booked Pending Approval']")
	public WebElement bookedpending;

	@FindBy(how = How.XPATH, using = "//span[text()='Submitted']")
	public WebElement submitted;

	@FindBy(how = How.XPATH, using = "(//a[starts-with(text(),'PAR-')])[1]")
	public WebElement verifyAccessories;

	public String cellsearch(String ID) {
		return "(//div[@class='scheduler_default_cell scheduler_default_cell_business'])[" + ID + "]";
	}

	@FindBy(how = How.XPATH, using = "//span[text()='Close this window']")
	public WebElement CloseVLA;

	@FindBy(how = How.XPATH, using = "(//span[text()='Remco van Rookhuizen is requesting approval for ouv booking'])[1]")
	public WebElement notificationApprove;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[17]")
	public WebElement services;

	public String services(String ID) {
		return "(//span[text()='" + ID + "'])";
	}

	@FindBy(how = How.XPATH, using = "//a[text()='JLRCHARG5']")
	public WebElement services1;

	@FindBy(how = How.XPATH, using = "//a[text()='JLRADB']")
	public WebElement services2;

	@FindBy(how = How.XPATH, using = "//a[text()='JLRCHARG2']")
	public WebElement services3;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Booking-')]//parent::c-output-lookup")
	public WebElement Bookingid;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Reservations')])[2]")
	public WebElement selectreservation;

	@FindBy(how = How.XPATH, using = "//a[starts-with(text(),'R-')]")
	public WebElement reservationnum;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Service Reservations')])[2]")
	public WebElement Servicereservation;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[18]")
	public WebElement Accessories;

}
