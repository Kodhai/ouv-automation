package com.jlr.ouv.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OUVFleetContainer {

	@FindBy(how = How.XPATH, using = "//div[@class='slds-icon-waffle']")
	public WebElement Menuopen;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search apps and items...']")
	public WebElement appsearch;

	@FindBy(how = How.XPATH, using = "//b[text()='OUV Fleets']")
	public WebElement OUVFleet;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Fleet__c-search-input']")
	public WebElement SearchFleet;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit OUV Fleet Name']")
	public WebElement editfleetname;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement fleetname;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement fleetmarket;

	@FindBy(how = How.XPATH, using = "//div[text()='New']")
	public WebElement ClickNew;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement OUVname;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement Market;

	public String Market(String value) {
		return "//span[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement Cetagory1;

	public String Cetagory1(String Value) {
		return "//span[@title='" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[3]")
	public WebElement FtManager;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[4]")
	public WebElement FtAdministrator;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[9]")
	public WebElement Daypass;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[11]")
	public WebElement Overnight;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[13]")
	public WebElement weekendpassapprover;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[14]")
	public WebElement weekendpassapproveralternative;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[15]")
	public WebElement Approvalroute;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[16]")
	public WebElement Registrationapprover;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[17]")
	public WebElement disposalapprover;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Cost_Centre__c']")
	public WebElement Costcenter;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Account__c']")
	public WebElement Account;

	@FindBy(how = How.XPATH, using = "//span[text()='Information']")
	public WebElement Clickscroll;

	@FindBy(how = How.XPATH, using = "(//span[text()='Booking Approval Details'])[1]")
	public WebElement Clickscroll1;

	@FindBy(how = How.XPATH, using = "(//span[text()='ATO/Vehicle Approval Details'])[1]")
	public WebElement Clickscroll2;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement Clicksave;

	// OUV-237

	@FindBy(how = How.XPATH, using = "//b[text()='Approval Routes'] ")
	public WebElement ApprovalRoutes;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement Routename;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[2]")
	public WebElement Remarketmanager;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[4]")
	public WebElement Financeapprover;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[6]")
	public WebElement operationapprover;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[7]")
	public WebElement VLAkeeper;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[8]")
	public WebElement financedirector;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[9]")
	public WebElement managingdirector;

	@FindBy(how = How.XPATH, using = "(//span[text()='In-Market Levels'])[2]")
	public WebElement Scrolldown3;

	@FindBy(how = How.XPATH, using = "(//span[text()='Regional Director Levels'])[2]")
	public WebElement Scrolldown4;

	// OUV-238

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[7]")
	public WebElement SelectFleet;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'OUV Fleet Members')]")
	public WebElement SelectFleetMembers;

	@FindBy(how = How.XPATH, using = "//div[@title='New']")
	public WebElement selectnew;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement Selectpeople;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement Selectsave;

	@FindBy(how = How.XPATH, using = "(//span[@class='uiOutputText'])[8]")
	public WebElement Gobacktofleet;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit OUV Fleet Name']")
	public WebElement clickedit;

	@FindBy(how = How.XPATH, using = "//input[@name='Name']")
	public WebElement Editfleetname;

	@FindBy(how = How.XPATH, using = "(//span[text()='Market'])[3]")
	public WebElement clickscroll;

	@FindBy(how = How.XPATH, using = "//input[@name='Fleet_Code__c']")
	public WebElement Editfleetcode;

	@FindBy(how = How.XPATH, using = "//span[text()='Clear Selection']")
	public WebElement clearselection;

	@FindBy(how = How.XPATH, using = "(//input[@aria-haspopup='listbox'])[2]")
	public WebElement enteradministrator;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'OUV Fleet Members')]")
	public WebElement FleetMembers;

	@FindBy(how = How.XPATH, using = "//a[@title='New']")
	public WebElement FleetMembersnew;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search People...']")
	public WebElement FleetMembersinfo;

	public String ValueSelection(String Value) {
		return "//a[text()='" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions'])[6]")
	public WebElement FleetMembersdelete;

	@FindBy(how = How.XPATH, using = "//a[text()='OUV Approval Route Details']")
	public WebElement ApprovalRoute;

	@FindBy(how = How.XPATH, using = "//a[@id='detailTab__item']")
	public WebElement detailTab;

	@FindBy(how = How.XPATH, using = "//span[text()='Market']")
	public WebElement Marketinfo;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit OUV Approval Route Name'])")
	public WebElement ApprovalRouteNameedit;

	@FindBy(how = How.XPATH, using = "//li[text()='insufficient access rights on cross-reference id']")
	public WebElement errormember;

	@FindBy(how = How.XPATH, using = "//button[@name='CancelEdit']")
	public WebElement Cancelchanges;

	@FindBy(how = How.XPATH, using = "//button[@title='Fleet Manager']")
	public WebElement FleetManageredit;

	@FindBy(how = How.XPATH, using = "//button[@title='Day Pass Approver (LL6)']")
	public WebElement DayPassApproveredit;

	@FindBy(how = How.XPATH, using = "//button[@title='Overnight Pass Approver (LL5)']")
	public WebElement OvernightPassApproveredit;

	@FindBy(how = How.XPATH, using = "//button[@title='Weekend Pass Approver (LL4)']")
	public WebElement WeekendPassApproveredit;

	@FindBy(how = How.XPATH, using = "//button[@title='Approval Route']")
	public WebElement ApprovalRouteedit;

	@FindBy(how = How.XPATH, using = "//button[@title='Registration Approver']")
	public WebElement RegistrationApproveredit;

	@FindBy(how = How.XPATH, using = "//h2[text()='We hit a snag.']")
	public WebElement error;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Fleet Administrator 2']")
	public WebElement fleetAdminstrator;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[2]")
	public WebElement clearsel;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search People...']")
	public WebElement fleetadmin;

	@FindBy(how = How.XPATH, using = "(//a[@data-refid='recordId'])[1]")
	public WebElement VehcileLink;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-grid'])[6]")
	public WebElement FleetLink;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Fleet Manager'])")
	public WebElement EditFleetManager;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Disposal Approver(LL6)'])")
	public WebElement EditDayPassArrover1;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Day Pass Approver(LL5)'])")
	public WebElement EditDayPassArrover2;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Disposal Approver(LL4)'])")
	public WebElement EditDayPassArrover3;

	@FindBy(how = How.XPATH, using = "(//button[@title='Edit Disposal Approver'])")
	public WebElement EditDisposalApprover;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Approval Route']")
	public WebElement EditApprovalRoute;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Registration Approver']")
	public WebElement EditRegistrationApprover;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[10]")
	public WebElement clearsAR;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search OUV Approval Routes...']")
	public WebElement ApprovalRouteTxtBx;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[1]")
	public WebElement clearsFM;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search People...'])[1]")
	public WebElement FleetManagerTxtBx;

	@FindBy(how = How.XPATH, using = "//b[text()='Reports']")
	public WebElement Reports;

	@FindBy(how = How.XPATH, using = "//a[@title='All Folders']")
	public WebElement AllFolders;

	public String FolderSelection(String text) {
		return "//a[text()='" + text + "']";
	}

	public String DetailsCheck(String text) {
		return "(//a[text()='" + text + "'])[1]";
	}

	@FindBy(how = How.XPATH, using = "(//span[text()='Regional Managing Director'])[1]")
	public WebElement RegionalManagingDirector;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit OUV Fleet Name']")
	public WebElement EditFleetName;

	@FindBy(how = How.XPATH, using = "(//button[@title='Clear Selection'])[11]")
	public WebElement clearReApr;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search People...'])[3]")
	public WebElement RegistrationApproverTxtBx;

	@FindBy(how = How.XPATH, using = "//button[@title='Change Owner']")
	public WebElement ChangeOwner;

	@FindBy(how = How.XPATH, using = "//button[@name='change owner']")
	public WebElement ChangeownerBtn;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement SearchUsers;

	@FindBy(how = How.XPATH, using = "//button[@title='Close this window']")
	public WebElement CloseWindow;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions'])[1]")
	public WebElement FleetMembersdelete1;

	@FindBy(how = How.XPATH, using = "//div[@class= 'detail slds-text-align--center']")
	public WebElement errorwindow;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Fleet Administrator 2']")
	public WebElement editFleetAdmin2;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Search People...'])[1]")
	public WebElement FleetAdmin2;

	@FindBy(how = How.XPATH, using = "//h2[contains(text(),'We hit a snag.')]")
	public WebElement SnagMsg;

	@FindBy(how = How.XPATH, using = "//label[contains(text(),'Fleet Administrator 3')]")
	public WebElement f3header;

	@FindBy(how = How.XPATH, using = "//button[@name='SaveEdit']")
	public WebElement save;

	@FindBy(how = How.XPATH, using = "//span[text()='Edit Fleet Administrator 2']")
	public WebElement Editadministrator2;

	@FindBy(how = How.XPATH, using = "(//span[text()='Clear Selection'])[2]")
	public WebElement clearselection1;

	public String clearselection1(String Value) {
		return "//span[@title='" + Value + "']";
	}

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement Save;

	@FindBy(how = How.XPATH, using = "(//input[@placeholder='Remco van Rookhuizen'])[2]")
	public WebElement Administrator2change;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Kris Verleye']")
	public WebElement OriginalAdministrator2;

	@FindBy(how = How.XPATH, using = "(//span[text()='Rental NL'])[2]")
	public WebElement FleetRentalNL;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/a0Q7a000008Yf94EAC/')]")
	public WebElement Fleetvehicle;

	@FindBy(how = How.XPATH, using = "(//span[text()='Edit Fleet Administrator 2'])[2]")
	public WebElement Editadministrator22;

	@FindBy(how = How.XPATH, using = "//a[starts-with(@href,'/lightning/r/OUV_Vehicle__c/')]")
	public WebElement OUVvehicles;

}
