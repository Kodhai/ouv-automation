package com.jlr.ouv.containers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OUVBookingContainer {
	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Booking__c-search-input']")
	public WebElement OUVBookingSearchBox;

	@FindBy(how = How.XPATH, using = "(//a[contains(@href,'/lightning/r/')])[1]")
	public WebElement firstOUVBooking;

	@FindBy(how = How.XPATH, using = "(//button[@class='slds-button slds-button_icon-border-filled'])[1]")
	public WebElement sideDropDown;

	@FindBy(how = How.XPATH, using = "//a[@name='OUV_Booking__c.OUV_Cancel_Booking']")
	public WebElement cancelBooking;

	@FindBy(how = How.XPATH, using = "(//span[text()='Save'])[3]")
	public WebElement saveBookingStatus;

	@FindBy(how = How.XPATH, using = "//div[@class='desktop forcePageError']")
	public WebElement errorMessage;

	@FindBy(how = How.XPATH, using = "(//span[text()='Cancel'])[3]")
	public WebElement cancelBookingStatus;

	@FindBy(how = How.XPATH, using = "//button[@name='OUV_Booking__c.OUV_Cancel_Booking']")
	public WebElement cancelBookingApproved;

	@FindBy(how = How.XPATH, using = "(//a[@title='Cancelled'])[2]")
	public WebElement cancelBookingStatusVerify;

	// ouv-177

	@FindBy(how = How.XPATH, using = "(//span[text()='OUV Bookings'])[1]")
	public WebElement OUVBookings1;

	@FindBy(how = How.XPATH, using = "(//span[@data-aura-class='uiOutputText'])[1]")
	public WebElement SelectBookings;

	@FindBy(how = How.XPATH, using = "//span[text()='All']")
	public WebElement SelectAll1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Booking__c-search-input']")
	public WebElement SearchBookings;
	// ouv-178

	@FindBy(how = How.XPATH, using = "//div[text()='New']")
	public WebElement New1;

	@FindBy(how = How.XPATH, using = "//span[text()='Next']")
	public WebElement Next;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_BookingShortDescription__c']")
	public WebElement QReffrence;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element__control slds-grow textarea-container'])[1]")
	public WebElement ReasonForBooking1;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[2]")
	public WebElement BookingReqType;

	@FindBy(how = How.XPATH, using = "(//button[text()='View all dependencies'])[2]")
	public WebElement Dependencies;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[20]")
	public WebElement BookingType;

	public String ValueSelection(String value) {
		return "//span[@title='" + value + "']";
	}

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[21]")
	public WebElement BookingJustification1;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[22]")
	public WebElement PassoutType1;

	@FindBy(how = How.XPATH, using = "//button[text()='Apply']")
	public WebElement Apply;

	@FindBy(how = How.XPATH, using = "(//input[@class='slds-input slds-combobox__input'])[6]")
	public WebElement JourneyType1;

	@FindBy(how = How.XPATH, using = "(//input[@name='OUV_Booking_Start_Time__c'])[1]")
	public WebElement StrtDate;

	@FindBy(how = How.XPATH, using = "(//input[@name='OUV_Booking_End_Time__c'])[1]")
	public WebElement EndDate;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_OtherLocation__c']")
	public WebElement OtherLocation1;

	@FindBy(how = How.XPATH, using = "(//div[@class='slds-form-element__control slds-grow textarea-container'])[2]")
	public WebElement JLRContact1;

	@FindBy(how = How.XPATH, using = "//input[@name='OUV_Internal_JLR_Contact_Email__c']")
	public WebElement JLRMail;

	@FindBy(how = How.XPATH, using = "(//input[@name='Driver_1__c'])[1]")
	public WebElement Drive1;

	@FindBy(how = How.XPATH, using = "//button[text()='Save']")
	public WebElement Saved;

	@FindBy(how = How.XPATH, using = "(//span[text()='Show Actions'])[1]")
	public WebElement ChangeDropdown;

	@FindBy(how = How.XPATH, using = "//a[@title='Change Owner']")
	public WebElement ChangeOwner1;

	@FindBy(how = How.XPATH, using = "//input[@title='Search Users']")
	public WebElement SearchUsers1;

	@FindBy(how = How.XPATH, using = "//span[text()='Submit']")
	public WebElement Submited;

	@FindBy(how = How.XPATH, using = "//div[@class='searchButton itemContainer slds-lookup__item-action--label slds-text-link--reset slds-grid slds-grid--vertical-align-center slds-truncate forceSearchInputLookupDesktopActionItem lookup__header highlighted']")
	public WebElement searchfleetmem;

	@FindBy(how = How.XPATH, using = "//A[@class='textUnderline outputLookupLink slds-truncate outputLookupLink-0050N0000087fOKQAY-82:5652;a forceOutputLookup']")
	public WebElement SelectFleetMem;

	@FindBy(how = How.XPATH, using = "(//span[contains(text(),'Notes & Attachments')])[3]")
	public WebElement notesAndAttachments;

	@FindBy(how = How.XPATH, using = "//span[contains(@title,'.pdf')]")
	public WebElement PDFDocument;

	@FindBy(how = How.XPATH, using = "(//div[@class='emptyContentInner slds-text-align_center'])[2]")
	public WebElement noItemsVLA;

	@FindBy(how = How.XPATH, using = "(//span[text()='Encrypted Company (VLA)'])[2]")
	public WebElement EncryptedCompany;

	@FindBy(how = How.XPATH, using = "(//span[text()='Encrypted Full Name (VLA)'])[2]")
	public WebElement EncryptedFullName;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Encrypted Driver 1']")
	public WebElement EncryptedDriver1;

	@FindBy(how = How.XPATH, using = "//button[@title='Edit Encrypted Driver 2']")
	public WebElement EncryptedDriver2;

	@FindBy(how = How.XPATH, using = "//span[text()='OUV Bookings']")
	public WebElement OUVBooking;

}
