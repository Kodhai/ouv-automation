Feature: Validate OUV vehicle

  #Below is the scenario for User editing the Master status to Live from Pre-Live without adding primary location
  @OUV @OUV_066 @OUVBatch
  Scenario: User edits the Master status to Live from Pre-Live without adding primary location
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,7"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    Then User edits sub status to "OUV vehicles,C,7"
    And save the changes performed
    Then Verify the displayed status error message for Primary Location
    Then Logout from the OUV

  #Below is the scenario for User editing only primary location and updates Master status to Live from Pre-Live
  @OUV @OUV_067 @OUVBatch
  Scenario: User edits only primary location and updates Master status to Live from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,8"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    And save the changes performed
    Then Verify the displayed status error message for Primary Location
    And User enters Primary Location
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Pre-Live from Live
  @OUV @OUV_068 @OUVBatch
  Scenario: User edits the Master status to Pre-Live from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,8"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User edits the Master status to Handed In from Pre-Live
  @OUV @OUV_069 @OUVBatch
  Scenario: User edits the Master status to Handed In from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,2"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,2"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #In this Scenario user edits the name for an OUV Vehicle
  @OUV @OUVVehicle @OUV_136
  Scenario: User is able to edit the name for an OUV Vehicle (Dummy) in the OUV Vehicle Tab
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    And select the vehicle
    And User edits the name of OUV Vehicle
    Then Logout from the OUV

  #User edits the Master status to Disposed with sub status Return to Stock or In FMS from Pre-Live
  @OUV @OUV_070 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status Return to Stock or In FMS from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,3"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,3"
    Then User edits sub status to "OUV vehicles,C,3"
    And save the changes performed
    Then Logout from the OUV

  #User edits the Master status to Disposed with sub status other than Return to Stock or In FMS from Pre-Live
  @OUV @OUV_071 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status other than Return to Stock or In FMS from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,4"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,4"
    Then User edits sub status to "OUV vehicles,C,4"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User tries to edit the New Requested Hand In Date on OUV Vehicle window
  @OUV @OUV_182 @OUVBatch
  Scenario: User tries to edit the New Requested Hand In Date on OUV Vehicle window
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,4"
    Then User selects the vehicle to open the vehicle related pages
    Then Validate that New Requested Hand in Approval status should be approved / New
    Then User tries to edit the New Requested Hand In Date on OUV Vehicle window HandinDate
    Then Enter any free text in justification on OUV Vehicle window and click on Save Justification "OUV vehicles,P,2"
    Then Validate that New Requested Hand in Approval status is changed to Pending Approval status
    Then Logout from the OUV

  #User tries to edit New Requested Hand In date when New Requested Hand In Approval status is Pending Approval
  @OUV @OUV_183 @OUVBatch
  Scenario: User tries to edit New Requested Hand In date when New Requested Hand In Approval status is Pending Approval
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,4"
    Then User selects the vehicle to open the vehicle related pagess
    Then Validate that New Requested Hand in Approval status should be pending approval / New
    Then User tries to edit the New Requested Hand In Date on OUV Vehicle window HandinDate
    Then Verify that User cannot edit New Requested Hand In date if the New Requested Hand In Apporval status is Pending Approval
    Then Logout from the OUV

  #User can update Master status for an OUV Vehicle which is in Handed in status
  @OUV @OUV_184 @OUVBatch
  Scenario: User can update Master status for an OUV Vehicle which is in Handed in status
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle related pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,2"
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Click on Save
    Then Logout from the OUV

  #User can update Master status for an OUV vehicle which is in Handed in status with past date
  @OUV @OUV_188 @OUVBatch
  Scenario: User can update Master status for an OUV vehicle which is in Handed in status with past date
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle related pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,3"
    Then Update New Extended Hand In date in past HandIndate
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Update sales channel, rate and date Sales channel "OUV vehicles,Q,2"  Sales Rate "OUV vehicles,S,2" Sales date "OUV vehicles,R,3"
    Then Click on Save
    Then Logout from the OUV

  #User cannot update Master status for an OUV vehicle which is in Handed in status with future date
  @OUV @OUV_189 @OUVBatch
  Scenario: User cannot update Master status for an OUV vehicle which is in Handed in status with future date
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle related pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,3"
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Update New Extended Hand In date in future HandIndate
    Then Click on Save
    Then Verify the error message for date
    Then Logout from the OUV

  #User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
  @OUV @OUV_190 @OUVBatch
  Scenario: User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,3"
    Then Verify that New Requested Handed In Approval Status should be approved before saving the record
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Click on Save
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle without updating reporting levels and without keys transferred selected
  @OUV @OUV_204 @OUVBatch @batch200
  Scenario: User tries to change fleet for an OUV Vehicle without updating reporting levels and without keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,2"
    Then User select the vehicle to open the vehicle related page
    Then Select any other fleet from the Fleet "OUV vehicles,H,3"
    Then User click on save button
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle without updating reporting levels with keys transferred selected
  @OUV @OUV_205 @OUVBatch @batch200
  Scenario: User tries to change fleet for an OUV Vehicle without updating reporting levels with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,2"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,H,3"
    Then User clicks on save button
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle with updating reporting levels with keys transferred selected
  @OUV @OUV_206 @OUVBatch @batch200
  Scenario: User tries to change fleet for an OUV Vehicle with updating reporting levels with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,3"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,H,2"
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then clicks on save button
    Then Logout from the OUV

  #User tries to edit the OUV Reporting Level 1,2,3 in OUV Vehicle Details Page
  @OUV @OUV_207 @OUVBatch @batch200
  Scenario: User tries to edit the OUV Reporting Level 1,2,3 in OUV Vehicle Details Page
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,3"
    Then User select the vehicle to open the vehicle related page
    Then User edits the OUV Reporting Levelone
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then User click the save button
    Then Logout from the OUV

  #User edits the Master status to Disposed with sub status Return to Stock or In FMS from Pre-Live
  @OUV @OUV_070 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status Return to Stock or In FMS from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,3"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,3"
    Then User edits sub status to "OUV vehicles,C,3"
    And save the changes performed
    Then Logout from the OUV

  #User edits the Master status to Disposed with sub status other than Return to Stock or In FMS from Pre-Live
  @OUV @OUV_071 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status other than Return to Stock or In FMS from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,4"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,4"
    Then User edits sub status to "OUV vehicles,C,4"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
  @OUV @OUV_074 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Live from Handed In
  @OUV @OUV_075 @OUVBatch
  Scenario: User edits the Master status to Live from Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,5"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status Write-off and Justification input from Live
  @OUV @OUV_076 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status Write-off and Justification input from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,6"
    Then User adds Justification
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status other than Write-off from Live
  @OUV @OUV_077 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status other than Write-off from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,3"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
  @OUV @OUV_072 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then validate the status change to Handed In from Live
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
  @OUV @OUV_073 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    And save the changes performed
    Then Verify the displayed Keys transfer error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Live
  @OUV @OUV_078 @OUVBatch
  Scenario: User edits the sub status to None when the Master status is in Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,10"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,10"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Handed In
  @OUV @OUV_079 @OUVBatch
  Scenario: User edits the sub status to None when the Master status is in Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,11"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,11"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User tries to do CheckOut/CheckIn procces for the OUV vehicle which is having a closed OUV booking
  @OUV @OUV_229 @OUVBatch @batch200
  Scenario: User tries to do CheckOut/CheckIn procces for the OUV vehicle which is having a closed OUV booking
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,6"
    Then Open any live OUV Vehicle for which CheckIn & Close is completed
    Then Verify that the reservation is completed for which CheckIn & Close is completed
    Then Verify that the OUV Booking status is closed which is associated with reservation
    Then User navigate to OUV vehicles
    Then Open any live OUV Vehicle for which CheckIn & Close is completed
    Then Click CheckOut/CheckIn button
    Then Select CheckOut from the checkbox
    Then Click Continue button
    Then Verify that an error occurred when user tries to do CheckOut for Closed OUV Booking
    Then Logout from the OUV

  #User tries to give keys returned date earlier than OUV booking start date at the time of completing reservation
  @OUV @OUV_230 @OUVBatch @batch200
  Scenario: User tries to give keys returned date earlier than OUV booking start date at the time of completing reservation
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,6"
    Then Open any live OUV Vehicle for which CheckIn & Close is completed
    Then Verify that reservation is completed for which CheckIn & Close is completed
    Then Click on Complete button
    Then Enter Keys Returned Date earlier than OUV Booking Start Date Start date
    Then Fill No. Keys CheckedIn, Return Location, Mileage fields Keys CheckedIn "OUV vehicles,W,2" Return Location "OUV vehicles,X,2" Mileage "OUV vehicles,Y,2"
    Then Verify that an error occurred when user tries to enter Keys Returned date in past
    Then Logout from the OUV

  #User is not be able to do CheckOut after doing CheckIn and Complete
  @OUV @OUV_221 @OUVBatch @batch200
  Scenario: User is not be able to do CheckOut after doing CheckIn & Complete
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,6"
    Then Open any live OUV Vehicle for which CheckIn & complete is completed
    Then Verify that the reservation is completed for which CheckIn & complete is completed
    Then Verify that the OUV Booking status is closed which is associated with reservation
    Then User navigate to OUV vehicles
    Then Open any live OUV Vehicle for which CheckIn & complete is completed
    Then Click CheckOut/CheckIn button
    Then Select CheckOut from the checkbox
    Then Click Continue button
    Then Verify that an error occurred when user tries to do CheckOut for Closed OUV Booking
    Then Logout from the OUV
    
    #User is able to complete the reservation after doing CheckIn & Close
  @OUV @OUV_227 @OUVBatch @batch200  
    Scenario: User is able to complete the reservation after doing CheckIn & Close
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User searches for live vehicle "OUV Calendar,AC,2"
    And select an availble slot for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User click on CheckOut CheckIn
    Then checkbox the checkout and click on continue
    And User selects driver or logistics and save it
    When User click on CheckOut CheckIn
    Then checkbox checkin and close then click on continue
    Then Logout from OUV

  #Below is the scenario for User editing the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
  @OUV @OUV_074 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then Logout from the OUV

  #User edits the Master status to Pre-Live from Live
  @OUV @OUV_201 @OUVVehicle @batch200
  Scenario: User edits the Master status to Pre-Live from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,15"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,15"
    Then User edits sub status to "OUV vehicles,C,15"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User is able to edit the Vehicle Name for an OUV Vehicle
  @OUV @OUV_202 @OUVVehicle @batch200
  Scenario: User is able to edit the Vehicle Name for an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,13"
    Then select and edit the vehicle	name
    And Enter the Vehicle Name as "OUV vehicles,T,2"
    Then Validate that user should be able to save the changes to the record
    Then Logout from the OUV

  #User is able to edit the Vehicle Name for an OUV Vehicle
  @OUV @OUV_203 @OUVVehicle @batch200
  Scenario: User is not able to edit the Vehicle Name for an OUV Vehicle of a different fleet
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,14"
    Then select and edit the vehicle	name
    And Enter the Vehicle Name as "OUV vehicles,T,3"
    Then Validate that user should not be able to save the changes to the record
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle with updating reporting levels to ones that do not match fleet with keys transferred selected
  @OUV @OUV_130 @OUVBatch
  Scenario: User tries to change fleet for an OUV Vehicle with updating reporting levels to ones that do not match fleet with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,2"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,H,3"
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then User clicks on save button
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle they are not a member of the fleet with updating reporting levels with keys transferred selected
  @OUV @OUV_131 @OUVBatch
  Scenario: User tries to change fleet for an OUV Vehicle they are not a member of the fleet with updating reporting levels with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,I,3"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,I,2"
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then User clicks on saved button
    Then Logout from the OUV

  #User tries to edit New Requested Hand In date when New Requested Hand In Approval status is Pending Approval
  @OUV @OUV_183 @OUVBatch
  Scenario: User tries to edit New Requested Hand In date when New Requested Hand In Approval status is Pending Approval
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,4"
    Then User selects the vehicle to open the vehicle related pagess
    Then Validate that New Requested Hand in Approval status should be pending approval / New
    Then User tries to edit the New Requested Hand In Date on OUV Vehicle window HandinDate "OUV vehicles,M,2"
    Then Verify that User cannot edit New Requested Hand In date if the New Requested Hand In Apporval status is Pending Approval
    Then Logout from the OUV

  #User can update Master status for an OUV Vehicle which is in Handed in status
  @OUV @OUV_184 @OUVBatch
  Scenario: User can update Master status for an OUV Vehicle which is in Handed in status
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle related pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,2"
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Click on Save
    Then Logout from the OUV

  #User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
  @OUV @OUV_190 @OUVBatch
  Scenario: User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,3"
    Then Verify that New Requested Handed In Approval Status should be approved before saving the record
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Click on Save
    Then Logout from the OUV

  @OUV @OUV_212 @OUVVehicle @batch200
  Scenario: User verifies that the Latest Planned Hand In Date and Actual Hand In Date gets updated after entering  New Requested Hand In Date for OUV Reporting Level as 'Buybacks'
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,16"
    And select the vehicle
    Then User enters New Requested Hand In Date and verifies that the Latest Planned Hand In Date and Actual Hand In Date gets updated
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Live from Handed In
  @OUV @OUV_075 @OUVBatch
  Scenario: User edits the Master status to Live from Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,5"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status Write-off and Justification input from Live
  @OUV @OUV_076 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status Write-off and Justification input from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,6"
    Then User adds Justification
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status other than Write-off from Live
  @OUV @OUV_077 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status other than Write-off from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,3"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
  @OUV @OUV_072 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then validate the status change to Handed In from Live
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
  @OUV @OUV_073 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    And save the changes performed
    Then Verify the displayed Keys transfer error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Live
  @OUV @OUV_078 @OUVBatch
  Scenario: User edits the sub status to None when the Master status is in Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,10"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,10"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Handed In
  @OUV @OUV_079 @OUVBatch
  Scenario: User edits the sub status to None when the Master status is in Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,11"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,11"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User tries to select both CheckIn and CheckOut checkbox for an OUV Vehicle
  @OUV @OUV_223 @OUVBatch @batch200
  Scenario: User tries to select both CheckIn and CheckOut checkbox for an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,17"
    And select the vehicle
    When User click on CheckOut CheckIn button
    Then checkbox checkout and checkin at a time to proceed
    And Verify the error message displayed to select one option
    Then Logout from the OUV

  #User tries to do CheckOut after doing CheckIn & Close
  @OUV @OUV_225 @OUVBatch @batch200
  Scenario: User tries to do CheckOut after doing CheckIn & Close
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,18"
    And select the vehicle
    When User navigate to OUVGatehouseLogs tab
    Then Verify the ActionType which is in Checkin and close
    And Navigate back to selected OUV Vehicle
    When User click on CheckOut CheckIn button
    Then checkbox checkout and click on continue
    And Verify the error message displayed
    Then Logout from the OUV

  #User tries to do Checkout/Check-in to OUV vehicle which is pre-live stage
  @OUV @OUV_174 @OUVBatch @SOUV-426
  Scenario: User tries to do Checkout/Check-in to OUV vehicle which is pre-live stage
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,B,8"
    And select the vehicle
    When User click on CheckOut CheckIn button
    Then checkbox checkout and click on continue
    And Verify the error message displayed
    Then Logout from the OUV

  #User tries to fill Mileage at close booking as lesser than Vehicle Mileage in Vehicle details page
  @OUV @OUV_173 @OUVBatch @SOUV-425
  Scenario: User tries to fill Mileage at close booking as lesser than Vehicle Mileage in Vehicle details page
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,B,10"
    And select the vehicle
    Then click on drop down and select CheckIn and complete
    Then fill fields like driver logistics "OUV vehicles,P,2", milage at close booking with "OUV vehicles,W,2", return location "OUV vehicles,W,2" and vehicle condition "OUV vehicles,D,2"
    And Click on save
    Then Update sales channel, rate and date Sales channel "OUV vehicles,Q,2"  Sales Rate "OUV vehicles,S,2" Sales date "OUV vehicles,R,3"
    Then Click on Save
    Then Logout from the OUV

  #User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
  @OUV @OUV_190 @OUVBatch
  Scenario: User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,3"
    Then Verify that New Requested Handed In Approval Status should be approved before saving the record
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Click on Save
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle without updating reporting levels and without keys transferred selected
  @OUV @OUV_204 @OUVBatch @batch200
  Scenario: User tries to change fleet for an OUV Vehicle without updating reporting levels and without keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,2"
    Then User select the vehicle to open the vehicle related page
    Then Select any other fleet from the Fleet "OUV vehicles,H,3"
    Then User click on save button
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle without updating reporting levels with keys transferred selected
  @OUV @OUV_205 @OUVBatch @batch200
  Scenario: User tries to change fleet for an OUV Vehicle without updating reporting levels with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,2"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,H,3"
    Then User clicks on save button
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle with updating reporting levels with keys transferred selected
  @OUV @OUV_206 @OUVBatch @batch200
  Scenario: User tries to change fleet for an OUV Vehicle with updating reporting levels with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,3"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,H,2"
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then clicks on save button
    Then Logout from the OUV

  #User tries to edit the OUV Reporting Level 1,2,3 in OUV Vehicle Details Page
  @OUV @OUV_207 @OUVBatch @batch200
  Scenario: User tries to edit the OUV Reporting Level 1,2,3 in OUV Vehicle Details Page
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,3"
    Then User select the vehicle to open the vehicle related page
    Then User edits the OUV Reporting Levelone
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then User click the save button
    Then Logout from the OUV

  #User edits the Master status to Disposed with sub status Return to Stock or In FMS from Pre-Live
  @OUV @OUV_070 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status Return to Stock or In FMS from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,3"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,3"
    Then User edits sub status to "OUV vehicles,C,3"
    And save the changes performed
    Then Logout from the OUV

  #User edits the Master status to Disposed with sub status other than Return to Stock or In FMS from Pre-Live
  @OUV @OUV_071 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status other than Return to Stock or In FMS from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,4"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,4"
    Then User edits sub status to "OUV vehicles,C,4"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
  @OUV @OUV_074 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Live from Handed In
  @OUV @OUV_075 @OUVBatch
  Scenario: User edits the Master status to Live from Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,5"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status Write-off and Justification input from Live
  @OUV @OUV_076 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status Write-off and Justification input from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,6"
    Then User adds Justification
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status other than Write-off from Live
  @OUV @OUV_077 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status other than Write-off from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,3"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
  @OUV @OUV_072 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then validate the status change to Handed In from Live
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
  @OUV @OUV_073 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    And save the changes performed
    Then Verify the displayed Keys transfer error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Live
  @OUV @OUV_078 @OUVBatch
  Scenario: User edits the sub status to None when the Master status is in Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,10"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,10"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Handed In
  @OUV @OUV_079 @OUVBatch
  Scenario: User edits the sub status to None when the Master status is in Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,11"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,11"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User tries to do CheckOut/CheckIn procces for the OUV vehicle which is having a closed OUV booking
  @OUV @OUV_229 @OUVBatch @batch200
  Scenario: User tries to do CheckOut/CheckIn procces for the OUV vehicle which is having a closed OUV booking
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,6"
    Then Open any live OUV Vehicle for which CheckIn & Close is completed
    Then Verify that the reservation is completed for which CheckIn & Close is completed
    Then Verify that the OUV Booking status is closed which is associated with reservation
    Then User navigate to OUV vehicles
    Then Open any live OUV Vehicle for which CheckIn & Close is completed
    Then Click CheckOut/CheckIn button
    Then Select CheckOut from the checkbox
    Then Click Continue button
    Then Verify that an error occurred when user tries to do CheckOut for Closed OUV Booking
    Then Logout from the OUV

  #User tries to give keys returned date earlier than OUV booking start date at the time of completing reservation
  @OUV @OUV_230 @OUVBatch @batch200
  Scenario: User tries to give keys returned date earlier than OUV booking start date at the time of completing reservation
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,6"
    Then Open any live OUV Vehicle for which CheckIn & Close is completed
    Then Verify that reservation is completed for which CheckIn & Close is completed
    Then Click on Complete button
    Then Enter Keys Returned Date earlier than OUV Booking Start Date Start date "OUV vehicles,V,2"
    Then Fill No. Keys CheckedIn, Return Location, Mileage fields Keys CheckedIn "OUV vehicles,W,2" Return Location "OUV vehicles,X,2" Mileage "OUV vehicles,Y,2"
    Then Verify that an error occurred when user tries to enter Keys Returned date in past
    Then Logout from the OUV

  #User is not be able to do CheckOut after doing CheckIn and Complete
  @OUV @OUV_221 @OUVBatch @batch200
  Scenario: User is not be able to do CheckOut after doing CheckIn & Complete
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,6"
    Then Open any live OUV Vehicle for which CheckIn & complete is completed
    Then Verify that the reservation is completed for which CheckIn & complete is completed
    Then Verify that the OUV Booking status is closed which is associated with reservation
    Then User navigate to OUV vehicles
    Then Open any live OUV Vehicle for which CheckIn & complete is completed
    Then Click CheckOut/CheckIn button
    Then Select CheckOut from the checkbox
    Then Click Continue button
    Then Verify that an error occurred when user tries to do CheckOut for Closed OUV Booking
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
  @OUV @OUV_074 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then Logout from the OUV

  #User edits the Master status to Pre-Live from Live
  @OUV @OUV_201 @OUVVehicle
  Scenario: User edits the Master status to Pre-Live from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,15"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,15"
    Then User edits sub status to "OUV vehicles,C,15"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User is able to edit the Vehicle Name for an OUV Vehicle
  @OUV @OUV_202 @OUVVehicle @batch200
  Scenario: User is able to edit the Vehicle Name for an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,13"
    Then select and edit the vehicle	name
    And Enter the Vehicle Name as "OUV vehicles,T,2"
    Then Validate that user should be able to save the changes to the record
    Then Logout from the OUV

  #User is able to edit the Vehicle Name for an OUV Vehicle
  @OUV @OUV_203 @OUVVehicle @batch200
  Scenario: User is not able to edit the Vehicle Name for an OUV Vehicle of a different fleet
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,14"
    Then select and edit the vehicle	name
    And Enter the Vehicle Name as "OUV vehicles,T,3"
    Then Validate that user should not be able to save the changes to the record
    Then Logout from the OUV

  #User tries to edit New Requested Hand In date when New Requested Hand In Approval status is Pending Approval
  @OUV @OUV_183 @OUVBatch
  Scenario: User tries to edit New Requested Hand In date when New Requested Hand In Approval status is Pending Approval
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,4"
    Then User selects the vehicle to open the vehicle related pagess
    Then Validate that New Requested Hand in Approval status should be pending approval / New
    Then User tries to edit the New Requested Hand In Date on OUV Vehicle window HandinDate "OUV vehicles,M,2"
    Then Verify that User cannot edit New Requested Hand In date if the New Requested Hand In Apporval status is Pending Approval
    Then Logout from the OUV

  #User can update Master status for an OUV Vehicle which is in Handed in status
  @OUV @OUV_184 @OUVBatch
  Scenario: User can update Master status for an OUV Vehicle which is in Handed in status
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle related pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,2"
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Click on Save
    Then Logout from the OUV

  #User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
  @OUV @OUV_190 @OUVBatch
  Scenario: User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,3"
    Then Verify that New Requested Handed In Approval Status should be approved before saving the record
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Click on Save
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Live from Handed In
  @OUV @OUV_075 @OUVBatch
  Scenario: User edits the Master status to Live from Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,5"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status Write-off and Justification input from Live
  @OUV @OUV_076 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status Write-off and Justification input from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,6"
    Then User adds Justification
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status other than Write-off from Live
  @OUV @OUV_077 @OUVBatch
  Scenario: User edits the Master status to Disposed with sub status other than Write-off from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,3"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
  @OUV @OUV_072 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then validate the status change to Handed In from Live
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
  @OUV @OUV_073 @OUVBatch
  Scenario: User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    And save the changes performed
    Then Verify the displayed Keys transfer error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Live
  @OUV @OUV_078 @OUVBatch
  Scenario: User edits the sub status to None when the Master status is in Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,10"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,10"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Handed In
  @OUV @OUV_079 @OUVBatch
  Scenario: User edits the sub status to None when the Master status is in Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,11"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,11"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User tries to select both CheckIn and CheckOut checkbox for an OUV Vehicle
  @OUV @OUV_223 @OUVBatch @batch200
  Scenario: User tries to select both CheckIn and CheckOut checkbox for an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,17"
    And select the vehicle
    When User click on CheckOut CheckIn button
    Then checkbox checkout and checkin at a time to proceed
    And Verify the error message displayed to select one option
    Then Logout from the OUV

  #User tries to do CheckOut after doing CheckIn & Close
  @OUV @OUV_225 @OUVBatch @batch200
  Scenario: User tries to do CheckOut after doing CheckIn & Close
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,18"
    And select the vehicle
    When User navigate to OUVGatehouseLogs tab
    Then Verify the ActionType which is in Checkin and close
    And Navigate back to selected OUV Vehicle
    When User click on CheckOut CheckIn button
    Then checkbox checkout and click on continue
    And Verify the error message displayed
    Then Logout from the OUV

  #User is able to edit primary location and Sub status to update Master status to Live from Pre-Live
  @OUV @OUV_065 @OUVBatch @SOUV-317
  Scenario: User is able to edit primary location and Sub status to update Master status to Live from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,3"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    Then User edits sub status to "OUV vehicles,C,7"
    And save the changes performed
    Then Verify the displayed status error message for Primary Location
    And User enters Primary Location
    And save the changes performed
    Then validate the status change to Live from Pre-Live
    Then Logout from the OUV

  #User cannot edit the New Requested Hand In Date for a different fleet on OUV Vehicle window
  @OUV @OUVCal @OUV_181
  Scenario: User cannot edit the New Requested Hand In Date for a different fleet on OUV Vehicle window
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV Calendar,M,4"
    And select the vehicle
    Then User edits the New Requested Hand In Date on OUV Vehicle window
    Then Verify the error message for date
    Then Logout from OUV

  #User is not able to transfer vehicles to a different fleet if an outstanding approval is in the selected vehicle
  @OUV @OUV_Phase2_003 @OUVBatch
  Scenario: User is not able to transfer vehicles to a different fleet if an outstanding approval is in the selected vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,21"
    And select the vehicle
    And navigate to fleet section
    Then Select any other fleet from the Fleet "OUV vehicles,I,4"
    And save the changes performed
    Then Verify the displayed fleet error message
    And navigate to Approval History quick link and verify pending status
    Then Logout from the OUV

  #User validates the owner name of the Vehicle with the fleet manager of the fleet
  @OUV @OUV_Phase2_006 @OUVBatch
  Scenario: User validates the owner name of the Vehicle with the fleet manager of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then clone the list with name "OUV vehicles,Z,2"
    And add the filter with fleet as "OUV vehicles,I,6"
    Then search for the vehicle in the list with status "OUV vehicles,A,8"
    And select the vehicle
    Then Verify the owner name of vehicle as "OUV vehicles,AA,2"
    And navigate to fleet section
    Then Select any other fleet from the Fleet "OUV vehicles,I,2"
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    And click on transfer and recieved keys
    And save the changes performed
    Then Verify the owner name of vehicle as "Users,C,3"
    Then click on the fleet and verify the fleet manager as "Users,C,3"
    And delete the created list view "OUV vehicles,Z,2"
    Then Logout from the OUV

  #User is able to request for Registration if the user is a member of the fleet
  @OUV @OUV_Phase2_007 @OUVBatch
  Scenario: User is able to request for Registration if the user is a member of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then clone the list with name "OUV vehicles,Z,2"
    And add the filter with Registration Approval Status as New
    And add the filter with fleet as "OUV vehicles,I,2"
    Then search for the vehicle in the list with status "OUV vehicles,A,2"
    And select the vehicle
    Then validate that the Registration Approval Status is set as New
    And click on edit icon for Registration Required and select the checkbox
    And save the changes performed
    And delete the created list view "OUV vehicles,Z,2"
    Then Logout from the OUV

  #User is able to transfer vehicles to a different fleet if the user is a member of the fleet(Both fleets should have same OUV Repoting Level 1)
  @OUV @OUV_phase2_002 @OUVBatch
  Scenario: User is able to transfer vehicles to a different fleet if the user is a member of the fleet(Both fleets should have same OUV Repoting Level 1)
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,4"
    And select the vehicle
    Then User change the fleet to "OUV vehicles,I,4"
    Then Logout from the OUV

  #User is not able to request for Registration if the user is not a member of the fleet
  @OUV @OUV_phase2_008 @OUVBatch
  Scenario: User is not able to request for Registration if the user is not a member of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,10"
    And select the vehicle
    Then validate the Registration Approval Status is New
    And click on edit icon for Registration Required and select the checkbox
    And save the changes performed
    Then Verify the error message for date
    Then Logout from the OUV

  #User verifies the Registration Approval Status is changed to 'Pending Approval' when user selects the Registration Request checkbox
  @OUV @OUV_phase2_009 @OUVBatch
  Scenario: User verifies the Registration Approval Status is changed to 'Pending Approval' when user selects the Registration Request checkbox
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,11"
    And select the vehicle
    Then validate the Registration Approval Status is New
    And click on edit icon for Registration Required and select the checkbox
    And save the changes performed
    Then validate the Registration Approval Status is Pending Approval
    Then Logout from the OUV

  #User is not able to perform bulk update of Master status from Live to Handed In if user is not a member of the fleet
  @OUV @OUV_Phase2_015 @OUVBatch
  Scenario: User is not able to perform bulk update of Master status from Live to Handed In if user is not a member of the fleet
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the Editable list with status "OUV vehicles,I,2"
    And Select the bulk vehicles
    Then click on change Master status and select master status as "OUV vehicles,B,9" and sub as "OUV vehicles,C,12"
    And click on Keys transferred checkbox
    And click on save for bulk status change
    Then Verify the error message for bulk status change and click on dismiss
    Then Logout from the OUV

  #User is not able to perform bulk update of Master status from Pre-Live to Live if the Vehicle has an outstanding approval
  @OUV @OUV_Phase2_022 @OUVBatch
  Scenario: User is not able to perform bulk update of Master status from Pre-Live to Live if the Vehicle has an outstanding approval
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the UAT Editable list with status "OUV vehicles,I,2"
    And Select the bulk vehicles
    Then click on change Master status and select master status as "OUV vehicles,B,10" and sub as "OUV vehicles,C,16"
    And click on Keys transferred checkbox
    And click on save for bulk status change
    Then Verify the error message for bulk status change and click on dismiss
    Then Logout from the OUV
    
  #User is not able to transfer vehicles to a different fleet if the user is not a member of the fleet
  @OUV @OUVCal @OUV_Phase2_001
  Scenario: User is not able to transfer vehicles to a different fleet if the user is not a member of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,7"
    And select the vehicle
    Then User change the fleet to "OUV vehicles,I,2"
    Then Verify the error message for date
    Then Logout from the OUV
    
  #User validates the OUV Reporting Level 1 of the fleet matches the OUV Reporting Level 1 of the fleet it is getting transferred to
  @OUV @OUVCal @OUV_Phase2_004
  Scenario: User validates the OUV Reporting Level 1 of the fleet matches the OUV Reporting Level 1 of the fleet it is getting transferred to
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,9"
    And select the vehicle
    When User verify the OUV Reporting level one of the fleet matches to "OUV vehicles,J,2"
    Then User change the fleet to "OUV vehicles,I,2"
    When User verify the OUV Reporting level one of the fleet matches to "OUV vehicles,J,2"
    Then Logout from the OUV
    
  #User verifies the error message when the OUV Reporting Level 1 of the fleet do not match the OUV Reporting Level 1 of the fleet it is getting transferred to
  @OUV @OUVCal @OUV_Phase2_005
  Scenario: User verifies the error message when the OUV Reporting Level 1 of the fleet do not match the OUV Reporting Level 1 of the fleet it is getting transferred to
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,9"
    And select the vehicle
    Then User change the fleet to "OUV vehicles,H,3"
    Then Verify the error message for date
    Then Logout from the OUV
    
  #User is not able to Pre-Sold an OUV Vehicle
  @OUV @OUVCal @OUV_Phase2_011
  Scenario: User is not able to Pre-Sold an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,A,24"
    And select the vehicle
    Then User click on Pre sold Admin icon
    And Verify the error message then click on cancel
    Then Logout from the OUV
  
    #User is able to view Vehicle user details in an OUV Vehicle
	 @OUV @OUV_Phase2_053 @OUVBatch
    Scenario:  User is able to view Vehicle user details in an OUV Vehicle\
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then verify the core information
    And Verify the other detials
    Then Logout from the OUV 
    
    #User is not able to view Vehicle user details in an OUV Vehicle
    @OUV @OUV_Phase2_054 @OUVBatch
    Scenario: User is not able to view Vehicle user details in an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then verify the core information
    And Verify the other detials
    Then Logout from the OUV 
    
    #User is able to view all the fields for Financial section in an OUV Vehicle
    @OUV  @OUV_phase2_064 
    Scenario: User is able to view all the fields for Financial section in an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then User view all the fields for Financial section in an OUV Vehicle		
    Then Logout from the OUV
    
 #User is able to view only selected fields for Financial section in an OUV Vehicle
    @OUV  @OUV_phase2_065 
    Scenario: User is able to view only selected fields for Financial section in an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,16" and password as "Users,E,16"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then User view only the selected fields for Financial section in an OUV Vehicle				
    Then Logout from the OUV
    
 #User is able to edit the Registration Date and De-Registration Date in an OUV Vehicle
    @OUV  @OUV_phase2_066 
    Scenario: User is able to edit the Registration Date and De-Registration Date in an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,16" and password as "Users,E,16"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then User edits the Registration Date and DeRegistration Date	
    And save the changes performed	
    Then Logout from the OUV	 
    
 #User is not able to edit the name for an OUV Vehicle (Dummy) in the OUV Vehicle Tab 
    @OUV @OUVCal @OUV_136
    Scenario: User is not able to edit the name for an OUV Vehicle (Dummy) in the OUV Vehicle Tab 
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2" 
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,A,26" 
    Then select and edit the vehicle	name
    And Enter the Vehicle Name as "OUV vehicles,T,2" 
    Then Validate that user should not be able to save the changes to the record
    Then Logout from the OUV
    
  #User tries to do CheckIn after completing CheckIn for an OUV Vehicle
  @OUV @OUV_250 @OUVBatch
  Scenario: User tries to do CheckIn after completing CheckIn for an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,19"
    And select the vehicle
    When User navigate to OUVGatehouseLogs tab
    Then Verify the ActionType which is in Checkin
    And Navigate back to selected OUV Vehicle
    When User click on CheckOut CheckIn button
    Then Select Check-In checkbox and click on continue
    And Verify the error message displayed for Check-In
    Then Logout from the OUV
    
    #User is able to perform bulk update of Master status from Live to Handed In if user is a member of the fleet
  @OUV @OUV_Phase2_014 @OUVBatch
  Scenario: User is able to perform bulk update of Master status from Live to Handed In if user is a member of the fleet
		Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
		Then User navigate to OUV vehicles
		Then search for the vehicle in the Editable list with status "OUV vehicles,A,6"
		And Select the bulk vehicles
		Then click on change Master status and select master status as "OUV vehicles,B,9" and sub as "OUV vehicles,C,12"
		And click on Keys transferred checkbox 
		And click on save for bulk status change 
		Then change list to recently viewed 
		Then search for the vehicle with status "OUV vehicles,B,9"
		And Verify the change in status to handed In 
		Then Logout from the OUV 
		
#User is not able to perform bulk update of Master status from Live to Handed In if the Vehicle has an outstanding approval
  @OUV @OUV_Phase2_016 @OUVBatch
  Scenario: User is not able to perform bulk update of Master status from Live to Handed In if the Vehicle has an outstanding approval
		Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
		Then User navigate to OUV vehicles
		Then search for the vehicle in the Editable list with status "OUV vehicles,A,25"
		And Select the bulk vehicles
		Then click on change Master status and select master status as "OUV vehicles,B,9" and sub as "OUV vehicles,C,12"
		And click on Keys transferred checkbox 
		And click on save for bulk status change
		And verify the error message for bulk status update
		Then Logout from the OUV