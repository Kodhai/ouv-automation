Feature: Validate OUV scripts phase two

 #User is not able to transfer vehicles to a different fleet if the user is not a member of the fleet
  @OUV @OUVCal  @Shefali @OUV_Phase2_001 
  Scenario: User is not able to transfer vehicles to a different fleet if the user is not a member of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,7"
    And select the vehicle
    Then User change the fleet to "OUV vehicles,I,2"
    Then Verify the error message for date
    Then Logout from the OUV

#User is able to transfer vehicles to a different fleet if the user is a member of the fleet(Both fleets should have same OUV Repoting Level 1)
  @OUV  @Shefali @OUV_Phase2_002 @OUVBatch
  Scenario: User is able to transfer vehicles to a different fleet if the user is a member of the fleet(Both fleets should have same OUV Repoting Level 1)
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,4"
    And select the vehicle
    Then User change the fleet to "OUV vehicles,I,4"
    Then Logout from the OUV

#User is not able to transfer vehicles to a different fleet if an outstanding approval is in the selected vehicle
  @OUV  @Shefali @OUV_Phase2_003 @OUVBatch 
  Scenario: User is not able to transfer vehicles to a different fleet if an outstanding approval is in the selected vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,21"
    And select the vehicle
    And navigate to fleet section
    Then Select any other fleet from the Fleet "OUV vehicles,I,4"
    And save the changes performed
    Then Verify the displayed fleet error message
    And navigate to Approval History quick link and verify pending status
    Then Logout from the OUV
    
     #User validates the OUV Reporting Level 1 of the fleet matches the OUV Reporting Level 1 of the fleet it is getting transferred to
  @OUV @OUVCal  @Shefali @OUV_Phase2_004 
  Scenario: User validates the OUV Reporting Level 1 of the fleet matches the OUV Reporting Level 1 of the fleet it is getting transferred to
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,9"
    And select the vehicle
    When User verify the OUV Reporting level one of the fleet matches to "OUV vehicles,J,2"
    Then User change the fleet to "OUV vehicles,I,2"
    When User verify the OUV Reporting level one of the fleet matches to "OUV vehicles,J,2"
    Then Logout from the OUV
    
  #User verifies the error message when the OUV Reporting Level 1 of the fleet do not match the OUV Reporting Level 1 of the fleet it is getting transferred to
  @OUV @OUVCal  @Shefali @OUV_Phase2_005 @Shefali
  Scenario: User verifies the error message when the OUV Reporting Level 1 of the fleet do not match the OUV Reporting Level 1 of the fleet it is getting transferred to
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,9"
    And select the vehicle
    Then User change the fleet to "OUV vehicles,H,3"
    Then Verify the error message for date
    Then Logout from the OUV

  #User validates the owner name of the Vehicle with the fleet manager of the fleet
  @OUV  @Shefali @OUV_Phase2_006 @OUVBatch 
  Scenario: User validates the owner name of the Vehicle with the fleet manager of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then clone the list with name "OUV vehicles,Z,2"
    And add the filter with fleet as "OUV vehicles,I,6"
    Then search for the vehicle in the list with status "OUV vehicles,A,8"
    And select the vehicle
    Then Verify the owner name of vehicle as "OUV vehicles,AA,2"
    And navigate to fleet section
    Then Select any other fleet from the Fleet "OUV vehicles,I,2"
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    And click on transfer and recieved keys
    And save the changes performed
    Then Verify the owner name of vehicle as "Users,C,3"
    Then click on the fleet and verify the fleet manager as "Users,C,3"
    And delete the created list view "OUV vehicles,Z,2"
    Then Logout from the OUV

  #User is able to request for Registration if the user is a member of the fleet
  @OUV  @Shefali @OUV_Phase2_007 @OUVBatch 
  Scenario: User is able to request for Registration if the user is a member of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then clone the list with name "OUV vehicles,Z,2"
    And add the filter with Registration Approval Status as New
    And add the filter with fleet as "OUV vehicles,I,2"
    Then search for the vehicle in the list with status "OUV vehicles,A,2"
    And select the vehicle
    Then validate that the Registration Approval Status is set as New
    And click on edit icon for Registration Required and select the checkbox
    And save the changes performed
    And delete the created list view "OUV vehicles,Z,2"
    Then Logout from the OUV
    
    #User is not able to request for Registration if the user is not a member of the fleet
  @OUV  @Shefali @OUV_Phase2_008 @OUVBatch 
  Scenario: User is not able to request for Registration if the user is not a member of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,10"
    And select the vehicle
    Then validate the Registration Approval Status is New
    And click on edit icon for Registration Required and select the checkbox
    And save the changes performed
    Then Verify the error message for date
    Then Logout from the OUV

  #User verifies the Registration Approval Status is changed to 'Pending Approval' when user selects the Registration Request checkbox
  @OUV  @Shefali @OUV_Phase2_009 @OUVBatch 
  Scenario: User verifies the Registration Approval Status is changed to 'Pending Approval' when user selects the Registration Request checkbox
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,11"
    And select the vehicle
    Then validate the Registration Approval Status is New
    And click on edit icon for Registration Required and select the checkbox
    And save the changes performed
    Then validate the Registration Approval Status is Pending Approval
    Then Logout from the OUV

#User is able to approve a Registration Request
  @OUV  @Shefali @OUV_Phase2_010 
  Scenario: User is able to approve a Registration Request
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User Navigate to OUV Vehicle
    And User search the Vehicle "OUV vehicles,A,7"
    When User selects vehicle
    And User checkbox the Registration Required
    Then Verify the Registration Approval Status is changed to Pending Approval
    And User navigates to Approval History
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Verify the Registration Approval Status is changed to Approved
    Then Logout from OUV
    
     #User is not able to Pre-Sold an OUV Vehicle
  @OUV @OUVCal  @Vinaykumar @OUV_Phase2_011 @Vinaykumar
  Scenario: User is not able to Pre-Sold an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,A,24"
    And select the vehicle
    Then User click on Pre sold Admin icon
    And Verify the error message then click on cancel
    Then Logout from the OUV
    
    	#User is able to Pre-Sold an OUV Vehicle
  @OUV  @Vinaykumar @OUV_Phase2_012 @OUVBatch
  Scenario: User is able to Pre-Sold an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,15"
    And select vehicle
    Then Click on Pre-Sold Admin button
    Then Navigate to Vehicle Pre-sold check box in Re-marketing section and validate that check box is ticked
		Then Logout from the OUV 
		
			#User is not able to extend the Hand In Date if the vehicle is Pre-Sold
  @OUV  @Vinaykumar @OUV_Phase2_013 @OUVBatch
  Scenario: User is not able to extend the Hand In Date if the vehicle is Pre-Sold
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,15"
    And select vehicle
    Then Click on Pre-Sold Admin button
    Then Navigate to Vehicle Pre-sold check box in Re-marketing section and validate that check box is ticked
		Then Logout of OUV Portal
		Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
		Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,15"
    And select the available vehicle
    Then Edit the New Requested Hand In Date
    Then Select any future date 
    Then Enter Justification and click on Save "OUV vehicles,P,2" 
    Then Validate that an error message is displayed while saving
    Then Validate that a warning message is displayed for New Requested Hand In Date
    Then Logout from the OUV 
    
     #User is able to perform bulk update of Master status from Live to Handed In if user is a member of the fleet
  @OUV  @Vinaykumar @OUV_Phase2_014 @OUVBatch
  Scenario: User is able to perform bulk update of Master status from Live to Handed In if user is a member of the fleet
		Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
		Then User navigate to OUV vehicles
		Then search for the vehicle in the Editable list with status "OUV vehicles,A,6"
		And Select the bulk vehicles
		Then click on change Master status and select master status as "OUV vehicles,B,9" and sub as "OUV vehicles,C,12"
		And click on Keys transferred checkbox 
		And click on save for bulk status change 
		Then change list to recently viewed 
		Then search for the vehicle with status "OUV vehicles,B,9"
		And Verify the change in status to handed In 
		Then Logout from the OUV
    
     #User is not able to perform bulk update of Master status from Live to Handed In if user is not a member of the fleet
  @OUV  @Vinaykumar @OUV_Phase2_015 @OUVBatch
  Scenario: User is not able to perform bulk update of Master status from Live to Handed In if user is not a member of the fleet
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the Editable list with status "OUV vehicles,I,2"
    And Select the bulk vehicles
    Then click on change Master status and select master status as "OUV vehicles,B,9" and sub as "OUV vehicles,C,12"
    And click on Keys transferred checkbox
    And click on save for bulk status change
    Then Verify the error message for bulk status change and click on dismiss
    Then Logout from the OUV
    
    #User is not able to perform bulk update of Master status from Live to Handed In if the Vehicle has an outstanding approval
  @OUV  @Vinaykumar @OUV_Phase2_016 @OUVBatch
  Scenario: User is not able to perform bulk update of Master status from Live to Handed In if the Vehicle has an outstanding approval
		Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
		Then User navigate to OUV vehicles
		Then search for the vehicle in the Editable list with status "OUV vehicles,A,25"
		And Select the bulk vehicles
		Then click on change Master status and select master status as "OUV vehicles,B,9" and sub as "OUV vehicles,C,12"
		And click on Keys transferred checkbox 
		And click on save for bulk status change
		And verify the error message for bulk status update
		Then Logout from the OUV

#User is able to create a new ATO Request with a Live Replacement Vehicle
  @OUV @OUVATO  @Vinaykumar @OUV_Phase2_017
  Scenario: User is able to create a new ATO Request with a Live Replacement Vehicle
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,6" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2" Replacementvehicle "ATO Requests,Q,2"
    Then Navigate to ATO Request Line Items
    And User creates a new ATO Request Line Item
    Then Logout from OUV Portal

  #User is not able to create a new ATO Request with a Live Replacement Vehicle which is already used by another ATO
  @OUV @OUVATO  @Vinaykumar @OUV_Phase2_018
  Scenario: User is not able to create a new ATO Request with a Live Replacement Vehicle which is already used by another ATO
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,6" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2" Replacementvehicle "ATO Requests,Q,2"
    Then verify the error message displayed
    Then Logout from OUV Portal
    
    #User tries to Clone the ATO Request
  @OUV  @Vinaykumar @OUV_Phase2_019 @OUVBatch
  Scenario: User tries to Clone the ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all the mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,6" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Select a Replacement Vehicle "ATO Requests,AD,2"
    Then Validate that error message is displayed
    Then Logout from OUV Portal
    
     #User is able to perform bulk update of Master status from Pre-Live to Live if user is a member of the fleet
  @OUV  @Vinaykumar @OUV_Phase2_020 @OUVBatch
  Scenario: User is able to perform bulk update of Master status from Pre-Live to Live if user is a member of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search the vehicle with status "OUV vehicles,A,26" 
    Then Select multiple checkbox for OUV Vehicle with fleet as PR NL in OUV Vehicle search list window
    Then Click on Change Master Status button
    Then Add Mandatory details Master Status "OUV vehicles,N,3" Sub Status "OUV vehicles,O,4" Location "OUV vehicles,X,2" No of Keys "OUV vehicles,W,2" 
    Then User navigate to OUV vehicles
    Then Click on the List View dropdown and select All "OUV vehicles,A,26" 
    Then Validate the Master status for all the vehicles changed to Live
    Then Logout from the OUV 
    
     #User is not able to perform bulk update of Master status from Pre-Live to Live if user is not a member of the fleet
  @OUV  @Vinaykumar @OUV_Phase2_021 @OUVBatch
  Scenario: User is not able to perform bulk update of Master status from Pre-Live to Live if user is not a member of the fleet
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search vehicle with status "OUV vehicles,A,7" 
    Then Select multiple checkbox for OUV Vehicle with fleet as PR NL in OUV Vehicle search list window
    Then Click on Change Master Status button
    Then Add Mandatory details Master Status "OUV vehicles,N,3" Sub Status "OUV vehicles,O,4" Location "OUV vehicles,X,2" No of Keys "OUV vehicles,W,2" 
    Then Validate error message is displayed
    Then Select the checkboxes, and click on dismiss
    Then Logout from the OUV 
    
    #User is not able to perform bulk update of Master status from Pre-Live to Live if the Vehicle has an outstanding approval
  @OUV  @Vinaykumar @OUV_Phase2_022 @OUVBatch
  Scenario: User is not able to perform bulk update of Master status from Pre-Live to Live if the Vehicle has an outstanding approval
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the UAT Editable list with status "OUV vehicles,I,2"
    And Select the bulk vehicles
    Then click on change Master status and select master status as "OUV vehicles,B,10" and sub as "OUV vehicles,C,16"
    And click on Keys transferred checkbox
    And click on save for bulk status change
    Then Verify the error message for bulk status change and click on dismiss
    Then Logout from the OUV
    
     #User is able to create and approve a booking with Transportation time
  @OUV @OUVCal  @Vinaykumar @OUV_Phase2_023
  Scenario: User is able to create and approve a booking with Transportation time
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And verify the approved status
    Then Logout from OUV
    
    #User is able to validate the status as Confirmed for the two Reservations for an approved Booking with Transportation time
  @OUV  @Vinaykumar @OUV_Phase2_024
  Scenario: User is able to validate the status as Confirmed for the two Reservations for an approved Booking with Transportation time
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then Verify that two Reservations are created on the same vehicle
    And Verify that status  is Confirmed for both the Reservations
    Then Logout from OUV

      #User is able to view the booking details of an OUV Booking with transportation time in Reservation Calendar
  @OUV  @Vinaykumar @OUV_Phase2_025 @OUV_Phase2
  Scenario: User is able to view the booking details of an OUV Booking with transportation time in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User Navigate to calender tab
    Then Verify that two Reservations are created in the Calendar for the booking "OUV Calendar,F,7"
    Then Logout from OUV

  #User is able to verify the start time and end time for the two reservations created for an OUV Booking with Transportation time
  @OUV  @Vinaykumar @OUV_Phase2_026 @OUV_Phase2
  Scenario: User is able to verify the start time and end time for the two reservations created for an OUV Booking with Transportation time
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User Navigate to calender tab
    And Navigate to Reservations Tab
    And Navigate to first reservation
    Then verify the start time of booking and end time of transportation
    And Navigate to Second reservation
    Then verify the start time of booking and end time of transportation
    Then Logout from OUV
    
    #User is able to view the OUV Booking for Preparation/Transportation type highlighted in two different colours
  @OUV  @Vinaykumar @OUV_Phase2_027 @OUVBatch @OUV_Phase2
  Scenario: User is able to view the OUV Booking for Preparation/Transportation type highlighted in two different colours
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User Navigate to calender tab
    And Verify that Transportation reservation is highlighted in purple color
    And the second reservation is highlighted in green color
    Then Logout from OUV

  #User is able to create a new OUV Booking with Temporary Accessories
  @OUV @OUVBooking  @Vinaykumar @OUV_Phase2_028 @OUV_Phase2
  Scenario: User is able to create a new OUV Booking with Temporary Accessories
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,9" reason Of Booking "OUV Calendar,AB,4" Booking Justification "OUV Calendar,G,4" Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4" Internal JLR Contact "OUV Calendar,V,2" email "OUV Calendar,P,4" Location "OUV Calendar,K,2" and Driver "OUV Calendar,AG,2"
    Then enter the Temporary Accessories as "OUV Bookings,C,2"  in OUV Booking
    Then enter the Temporary Accessories as "OUV Bookings,C,3"  in OUV Booking
    And click on save booking option
    Then Logout from OUV

  #User is able to verify the Temporary Accessories listed under Reservation for an OUV Booking
  @OUV @OUVBooking  @Vinaykumar @OUV_Phase2_029 @OUV_Phase2
  Scenario: User is able to verify the Temporary Accessories listed under Reservation for an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,9" reason Of Booking "OUV Calendar,AB,4" Booking Justification "OUV Calendar,G,4" Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4" Internal JLR Contact "OUV Calendar,V,2" email "OUV Calendar,P,4" Location "OUV Calendar,K,2" and Driver "OUV Calendar,AG,2"
    Then enter the Temporary Accessories as "OUV Bookings,C,2"  in OUV Booking
    Then enter the Temporary Accessories as "OUV Bookings,C,3"  in OUV Booking
    And click on save booking option
    Then User clicks on Booking Quick Reference "OUV Calendar,F,9" and navigates to OUV Booking ID
    Then navigate to the reservation and select Temporary Accessories quick link
    Then verify that user is able to add two new Temporary Accessories
    Then Logout from OUV
    
    #User is able to create a new OUV Booking with Services
  @OUV  @Vinaykumar @OUV_Phase2_030 @OUVBatch @OUV_Phase2
  Scenario: User is able to create a new OUV Booking with Services
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then Enter three Services from the list of Services Serviceone "OUV Calendar,AJ,2" Servicetwo "OUV Calendar,AJ,3" Servicethree "OUV Calendar,AJ,4" Booking Quick Reference "OUV Calendar,F,6" 
     Then Logout from OUV
    
    
    #User is able to verify the Services listed under Reservation for an OUV Booking
  @OUV  @Vinaykumar @OUV_Phase2_031 @OUVBatch @OUV_Phase2
  Scenario: User is able to verify the Services listed under Reservation for an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then Enter three Services from the list of Services Serviceone "OUV Calendar,AJ,2" Servicetwo "OUV Calendar,AJ,3" Servicethree "OUV Calendar,AJ,4" Booking Quick Reference "OUV Calendar,F,6" 
    Then Click on the OUV Booking hyperlink
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select Booking from the Notifications Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then Navigate to Reservations under Related list quick links
    Then Navigate to Service Reservations under Related list quick links
    Then Verify the three Services created and listed for a reservation
    Then Logout from OUV
    
     #User is able to create a new OUV Booking with Permanent Accessories
  @OUV  @Vinaykumar @OUV_Phase2_032 @OUVBatch @OUV_Phase2
  Scenario: User is able to create a new OUV Booking with Permanent Accessories
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then Enter two Permanent Accessories from the list of Permanent Accessories Accessoriesone "OUV Calendar,AL,2" Accessoriestwo "OUV Calendar,AL,3" Accessoriesthree "OUV Calendar,AL,4" Booking Quick Reference "OUV Calendar,F,6" 
     Then Logout from OUV
    
    #User is able to verify the Permanent Accessories listed under Reservation for an OUV Booking
  @OUV @OUVCal  @Vinaykumar @OUV_Phase2_033 @OUV_Phase2
  Scenario: User is able to verify the Permanent Accessories listed under Reservation for an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    And fill permanent accessories with "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    Then User navigate to reservations and select a reservation
    Then User navigate to Permanent Accessories
    Then Verify the accessory created
    Then Logout from OUV

  #User is able to view the Accessories in an OUV Vehicle for an Approved Booking with Permanent Accessories
  @OUV @OUVCal  @Vinaykumar @OUV_Phase2_034 @OUV_Phase2
  Scenario: User is able to view the Accessories in an OUV Vehicle for an Approved Booking with Permanent Accessories
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    And fill permanent accessories with "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And verify the approved status
    Then Navigate to booking window
    Then User navigate to reservations and select a reservation
    Then User navigate to Permanent Accessories
    Then Verify the accessory created
    Then Logout from OUV

  #User is able to create a Multiple Vehicle Booking with all the Accessories
  @OUV  @Vinaykumar @OUV_Phase2_035 @OUV_Phase2
  Scenario: User is able to create a Multiple Vehicle Booking with all the Accessories
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And User select two temporary Accessories "OUV Calendar,AK,2" and "OUV Calendar,AK,3"
    Then User select three Service Reservations "OUV Calendar,AJ,2" and "OUV Calendar,AJ,3" and "OUV Calendar,AJ,4"
    And User select two Permanent accessories "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User set local system date and time for the reservation to calendar "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    Then Logout from OUV
    
    #User is able to create another booking in a multiple vehicle booking type and add the accessories to the booking
  @OUV  @Vinaykumar @OUV_Phase2_036 @OUV_Phase2
  Scenario: User is able to create another booking in a multiple vehicle booking type and add the accessories to the booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And User select two temporary Accessories "OUV Calendar,AK,2" and "OUV Calendar,AK,3"
    Then User select three Service Reservations "OUV Calendar,AJ,2" and "OUV Calendar,AJ,3" and "OUV Calendar,AJ,4"
    And User select two Permanent accessories "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    Then User navigates to OUV Booking ID
    When User Navigate to calender tab
    And Search vehicles after Check boxing the Reserved Vehicles
    And select a slot of week for selected vehicle
    Then User select three Service Reservations "OUV Calendar,AJ,4" and "OUV Calendar,AJ,5" and "OUV Calendar,AJ,2"
    And User select two Permanent accessories "OUV Calendar,AL,4" and "OUV Calendar,AL,5"
    Then User save the Reservation
    Then Logout from OUV
    
    #User verifies the accessories for the two Reservations created for Multiple Vehicle Booking type
  @OUV  @Vinaykumar @OUV_Phase2_037 @OUV_Phase2
  Scenario: User verifies the accessories for the two Reservations created for Multiple Vehicle Booking type
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And User select two temporary Accessories "OUV Calendar,AK,2" and "OUV Calendar,AK,3"
    Then User select three Service Reservations "OUV Calendar,AJ,2" and "OUV Calendar,AJ,3" and "OUV Calendar,AJ,4"
    And User select two Permanent accessories "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User set local system date and time for the reservation to calendarss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"	
    Then User navigates to OUV Booking ID
    When User Navigate to calender tab
    And Search vehicles after Check boxing the Reserved Vehicles	
    And select a slot of week for selected vehicle
    Then User select three Service Reservations "OUV Calendar,AJ,4" and "OUV Calendar,AJ,5" and "OUV Calendar,AJ,2"
    And User select two Permanent accessories "OUV Calendar,AL,4" and "OUV Calendar,AL,5"
    Then User save the Reservation 
    Then Navigate to Reservation 
    And Select the first Reservation		
    Then User navigate to Permanent Accessories
    And Verify the accessories created			
    Then User navigate to Temporary Accessories
    And Verify the accessories created
    Then User navigate to Service Reservations 
    And Verify the accessories created
    Then Navigate back to Booking
    Then Navigate to Reservation 
    And Select the second Reservation	
    Then User navigate to Permanent Accessories
    And Verify the accessories created
    Then Logout from OUV
    
  #User is able to verify that only Permanent Accessories gets copied to the second reservation created for Multiple Vehicle Booking Type
  @OUV  @Vinaykumar @OUV_Phase2_038 @OUV_Phase2
  Scenario: User is able to verify that only Permanent Accessories gets copied to the second reservation created for Multiple Vehicle Booking Type
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And User select two temporary Accessories "OUV Calendar,AK,2" and "OUV Calendar,AK,3"
    Then User select three Service Reservations "OUV Calendar,AJ,2" and "OUV Calendar,AJ,3" and "OUV Calendar,AJ,4"
    And User select two Permanent accessories "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User set local system date and time for the reservation to calendarss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    Then User navigates to OUV Booking ID
    When User Navigate to calender tab
    And Search vehicles after Check boxing the Reserved Vehicles	
    And select a slot of week for selected vehicle
    Then User select three Service Reservations "OUV Calendar,AJ,4" and "OUV Calendar,AJ,5" and "OUV Calendar,AJ,2"
    And User select two Permanent accessories "OUV Calendar,AL,4" and "OUV Calendar,AL,5"
    Then User save the Reservation 
    Then Navigate to Reservation 
    And Select the first Reservation	
    Then User navigate to Permanent Accessories
    And Verify the accessories created	
    Then User navigate to Temporary Accessories
    And Verify the accessories created
    Then User navigate to Service Reservations 
    And Verify the accessories created
    Then Navigate back to Booking
    Then Navigate to Reservation 
    And Select the second Reservation	
    Then User navigate to Permanent Accessories
    And Verify the accessories created
    Then User navigate to Temporary Accessories
    And Verify no accessory is created	
    Then User navigate to Service Reservations 
    And Verify no accessory is created
    Then Logout from OUV
    
     #User is able to edit the Fleet name for an OUV Fleet
  @OUV @OUVF  @Vinaykumar @OUV_Phase2_039 @OUV_Phase2
  Scenario: User is able to edit the Fleet name for an OUV Fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,6"
    Then Verify the changes in fleet name
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,5"
    Then Logout user from OUV Portal

  #User is able to verify the updated Fleet name in an OUV Vehicle
  @OUV @OUVF  @Vinaykumar @OUV_Phase2_040 @OUV_Phase2
  Scenario: User is able to verify the updated Fleet name in an OUV Vehicle
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,6"
    Then Verify the changes in fleet name
    And User navigate to OUV Vehicles
    Then User select the first vehicle
    And User click on Fleet name link
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,5"
    Then Verify the changes in fleet name
    Then Logout user from OUV Portal
    
     #User is able to edit the Fleet Administrator 2 for an OUV Fleet
  @OUV @OUVCal  @Vinaykumar @OUV_Phase2_041 @OUV_Phase2
  Scenario: User is able to edit the Fleet Administrator 2 for an OUV Fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    Then Change the Fleet Administrator two to any user from the list for the selected Fleet Record Administratortwo "Fleet,Z,2" 
    Then Verify that Fleet Administrator two is updated
    Then Edit the Fleet Administrator two back to original "Fleet,Z,3"
    Then Verify that Fleet Administrator two is updated back to original
    Then Logout user from OUV Portal

  #User is able to verify the updated Fleet Administrator 2 in an OUV Vehicle
  @OUV @OUVCal  @Vinaykumar @OUV_Phase2_042 @OUV_Phase2
  Scenario: User is able to verify the updated Fleet Administrator 2 in an OUV Vehicle
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    Then Change the Fleet Administrator two to any user from the list for the selected Fleet Record Administratortwo "Fleet,Z,2" 
    Then Verify that Fleet Administrator two is updated
    And Navigate to OUV Vehicles under Related list quick links
    Then Click on the Fleet in the OUV Vehicle Details page
    Then Edit Fleet Administrator two back to original "Fleet,Z,3"
    Then Verify that Fleet Administrator two is updated back to original
    Then Logout user from OUV Portal

  #User is able to add and delete Fleet Members in an OUV fleet
  @OUV @OUVF  @Vinaykumar @OUV_Phase2_043 @OUV_Phase2
  Scenario: User is able to add and delete Fleet Members in an OUV fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And Navigate to OUV Fleet members
    When click on new add new member "Users,C,12"
    Then delete the added fleet member
    Then Logout user from OUV Portal

  #User is not able to edit the fields in an OUV Fleet record
  @OUV @OUVF  @Vinaykumar @OUV_Phase2_044 @OUV_Phase2
  Scenario: User is not able to edit the fields in an OUV Fleet record
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And Verify that fleet manager details cannot be edited
    Then Verify that Day pass approver one field cannot be edited
    And Verify that Day pass approver two field cannot be edited
    Then Verify that Day pass approver three field cannot be edited
    And Verify that Approval Route cannnot be edited
    Then Verify that Registration Approver field cannot be edited
    And Verify that Disposal Approver filed cannot be edited
    Then Logout user from OUV Portal
    
     #User is not able to edit the Fleet name for an OUV Fleet
  @OUV @OUVF  @Vinaykumar @OUV_Phase2_045 @OUV_Phase2
  Scenario: User is not able to edit the Fleet name for an OUV Fleet
    Given User access OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,3"
    Then validate the error message displayed
    Then Logout user from OUV Portal

  #User is not able to edit the Fleet Administrator 2 for an OUV Fleet
  @OUV @OUVF  @Vinaykumar @OUV_Phase2_046 @OUV_Phase2
  Scenario: User is not able to edit the Fleet Administrator 2 for an OUV Fleet
    Given User access OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When User edit fleet administrator and enter "Users,C,9"
    Then validate the error message displayed
    Then Logout user from OUV Portal
    
     #User is not able to add and delete Fleet Members in an OUV fleet
 @OUV @OUVF  @Vinaykumar @OUV_Phase2_047 @OUV_Phase2
  Scenario: User is not able to add and delete Fleet Members in an OUV fleet
    Given User access OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And Navigate to OUV Fleet members
    Then User tries to add new member "Users,C,12"
    Then verify the add new error message
    Then Logout user from OUV Portal
    
#User is not able to edit the fields in an OUV Fleet record
 @OUV @OUVF  @Vinaykumar @OUV_Phase2_048 @OUV_Phase2
  Scenario: User is not able to edit the fields in an OUV Fleet record
    Given User access OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And Verify that user is not able to edit the fields in an OUV Fleet
    Then Logout user from OUV Portal
    
    #User is able to update the Approval Routes for an OUV Fleet Record
  @OUV @OUVF  @Vinaykumar @OUV_Phase2_049 @OUV_Phase2
  Scenario: User is able to update the Approval Routes for an OUV Fleet Record
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When User changes the Approval Route to "Fleet,C,4"
    And Verify that Approval Route is updated
    When User changes the Approval Route to "Fleet,C,3"
    Then Logout user from OUV Portal

  #User is able to update the Fleet Record and validate the details in the run report ATO Request Updates_Check
  @OUV @OUVF  @Vinaykumar @OUV_Phase2_050 @OUV_Phase2
  Scenario: User is able to update the Fleet Record and validate the details in the run report ATO Request Updates_Check
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And User changes the fleet name to "Fleet,A,6"
    When User edit fleet administrator and enter "Users,C,4"
    Then User changed the Fleet manager to "Users,C,3"
    When User changes the Approval Route to "Fleet,C,4"
    And Navigate to Reports
    Then Select All folders under folders section
    And Select the folder "Fleet,X,2"
    Then Navigate to the folder "Fleet,X,3"
    And Select Report for check "Fleet,Y,2"
    Then Verify that Fleet name is updated to "Fleet,A,6"
    And Verify that Fleet Administrator is updated to "Users,C,4"
    Then Verify that Remarketing Manager is updated to "Users,C,6"
    And Verify that Finance Approver is updated to "Users,C,18"
    Then Verify that Operations Manager is updated to "Users,C,3"
    And Verify that Regional Finance Director is updated to "Users,C,19"
    Then Verify that Regional Managing Director is updated to "Users,C,20"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,6"
    Then Select the fleet
    And User changes the fleet name to "Fleet,A,5"
    When User edit fleet administrator and enter "Users,C,11"
    Then User changed the Fleet manager to "Users,C,21"
    When User changes the Approval Route to "Fleet,C,3"
    Then Logout user from OUV Portal

  #User is able to update the Fleet Record and validate the details in the run report OUV Vehicle  Updates_Check
  @OUV @OUVF  @Vinaykumar @OUV_Phase2_051 @OUV_Phase2
  Scenario: User is able to update the Fleet Record and validate the details in the run report OUV Vehicle  Updates_Check
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And User changes the fleet name to "Fleet,A,6"
    When User edit fleet administrator and enter "Users,C,4"
    Then User changed the Fleet manager to "Users,C,3"
    And User changes the Registration Approver to "Users,C,18"
    When User changes the Approval Route to "Fleet,C,4"
    And Navigate to Reports
    Then Select All folders under folders section
    And Select the folder "Fleet,X,2"
    Then Navigate to the folder "Fleet,X,3"
    And Select Report for check "Fleet,Y,3"
    Then Verify that Fleet name is updated to "Fleet,A,6"
    And Verify that Fleet Administrator is updated to "Users,C,4"
    Then Verify that Remarketing Fleet Manager is updated to "Users,C,6"
    And Verify that Finance Approver is updated to "Users,C,18"
    Then Verify that Operations Approver is updated to "Users,C,3"
    And Verify that Registration Approver is updated to "Users,C,18"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,6"
    Then Select the fleet
    And User changes the fleet name to "Fleet,A,5"
    When User edit fleet administrator and enter "Users,C,11"
    Then User changed the Fleet manager to "Users,C,21"
    And User changes the Registration Approver to "Users,C,11"
    When User changes the Approval Route to "Fleet,C,3"
    Then Logout user from OUV Portal
    
    #User is able to update the Fleet Record and validate the details in the run report OUV Booking Updates_Check
  @OUV @OUVCal  @Vinaykumar @OUV_Phase2_052 @OUV_Phase2
  Scenario: User is able to update the Fleet Record and validate the details in the run report OUV Booking Updates_Check
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,5" and Brand "OUV Calendar,AM,2"
    And select buyback checkbox
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,2",ReasonOfBooking "OUV Calendar,F,2",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,2",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation to calendars "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,2",ReasonOfBooking "OUV Calendar,F,2",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,2",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation to calendarss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation to calendarsss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation to calendarssss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    Then Login with Fleet Manager as "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleets
    And search for the Fleet with "Fleet,A,5"
    Then Select the fleets
    And User changes fleet name to "Fleet,A,6"
    When User edit fleet administrators and enter "Users,C,4"
    Then User changed the Fleet manager to "Users,C,3"
    And User changes the llfive approver to "Users,C,5"
    And Navigates to Reports "OUV Calendar,AN,2"
    Then Selects All folders under folders section
    And Select the folders "Fleet,X,2"
    Then Navigates to the folder "Fleet,X,3"
    And Selects Report for check "Fleet,Y,3"
    Then Verifys that Fleet name is updated to "Fleet,A,6"
    And Verifys that Fleet Administrator is updated to "Users,C,4"
    Then Verifys that approver is updated to "User,C,5"
    Then User navigates to OUV Fleets
    And search for the Fleet "Fleet,A,6"
    Then Select the fleets
    And User changes fleet name to "Fleet,A,5"
    When User edit fleet administrators and enter "Users,C,11"
    Then User changed the Fleet manager to "Users,C,21"
    And User changes the llfive approver to "User,C,22"
    Then Logout from OUV
    
    #User is able to view Vehicle user details in an OUV Vehicle
	 @OUV  @Vinaykumar @OUV_Phase2_053 @OUVBatch @OUV_Phase2
    Scenario:  User is able to view Vehicle user details in an OUV Vehicle\
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then verify the core information
    And Verify the other detials
    Then Logout from the OUV 
    
    #User is not able to view Vehicle user details in an OUV Vehicle
    @OUV  @Vinaykumar @OUV_Phase2_054 @OUVBatch @OUV_Phase2
    Scenario: User is not able to view Vehicle user details in an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then verify the core information
    And Verify the other detials
    Then Logout from the OUV 
    
     #User is able to view Vehicle user details in an ATO Request
   @OUV  @Vinaykumar @OUV_Phase2_055 @OUVBatch @OUV_Phase2
  Scenario: User is able to view Vehicle user details in an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Select any ATO Request from the list
    Then Under ATO Summary, verify the field Vehicle User is present
   Then Logout from OUV Portal
   
    #User is not able to view Vehicle user details in an ATO Request
   @OUV  @Vinaykumar @OUV_Phase2_056 @OUVBatch @OUV_Phase2
  Scenario: User is not able to view Vehicle user details in an ATO Request
    Given Access an OUV Portal with user "Users,D,7" and password as "Users,E,7"
    And User navigate to ATO Request
    Then Select any ATO Request from the list
    Then Under ATO Summary, verify the field Vehicle User is not present
   Then Logout from OUV Portal
   
    #User is able to view Vehicle user details in an OUV Booking
  @OUV  @Vinaykumar @OUV_Phase2_057 @OUVBatch @OUV_Phase2
  Scenario: User is able to view Vehicle user details in an OUV Booking
    Given User access OUV Portal with UserName "Users,D,2" Password "Users,E,2"
    And User Navigates to OUV Booking menu
    Then Select any OUV Booking from the list
    And select the OUV Bookings
    Then Under Driver/Vehicle User, verify all the fields
    Then User do logout from OUV Portal
    
   #User is not able to view Vehicle user details in an OUV Booking
   @OUV @OUVBooking  @Vinaykumar @OUV_Phase2_058 @OUV_Phase2
   Scenario: User is not able to view Vehicle user details in an OUV Booking
   	Given User access OUV Portal with UserName "Users,D,7" Password "Users,E,7" 
	  And User Navigates to OUV Booking menu
	  Then User search an OUV Booking with status "OUV Bookings,A,4" 
	  And Verify that the Vehicle user details are not present in an OUV Booking
	  Then User do logout from OUV Portal	
	  
	  #User is not able to search for Frequent Drivers field in an OUV Vehicle Loan Agreement
  @OUV  @Vinaykumar @OUV_Phase2_059 @OUV_Phase2
  Scenario: User is not able to search for Frequent Drivers field in an OUV Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,7" and password as "Users,E,7" 
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    And User Navigate to OUV Vehicle Loan Agreements
    Then Verify that User cannot see Frequent Drivers in New OUV Vehicle Loan Agreement window
    Then Logout from OUV
    
  #User is not able to add items for OUV Frequent Drivers in OUV App Navigation Items
  @OUV  @Vinaykumar @OUV_Phase2_060 @OUV_Phase2
  Scenario: User is not able to add items for OUV Frequent Drivers in OUV App Navigation Items
    Given Access the OUV Portal with user "Users,D,7" and password as "Users,E,7" 
    And User clicks on OUV App navigation items
    Then User clicks on Add more items and searches for "OUV Frequent Drivers" under All section
    Then Verify the OUV Frequent Drivers is not available in OUV App navigation items
    Then Logout from OUV 
	  
	  #User is able to view the list for all OUV Frequent Drivers in OUV Frequent Drivers tab
    @OUV  @Vinaykumar @OUV_Phase2_061
    Scenario: User is able to view the list for all OUV Frequent Drivers in OUV Frequent Drivers tab
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    Then Verify that User can view All OUV Frequent drivers
    Then Logout from OUV
    
   #User is able to view the PDF document in an OUV Vehicle Agreement for an Approved OUV Booking with Signature
   @OUV  @Vinaykumar @OUV_Phase2_062 @OUV_Phase2
   Scenario: User is able to view the PDF document in an OUV Vehicle Agreement for an Approved OUV Booking with Signature
    Given User access OUV Portal with UserName "Users,D,7" Password "Users,E,7"
    And User Navigates to OUV Booking menu
    Then User search an OUV Booking with status "OUV Bookings,A,7"
    And User navigate to Vehicle Loan Agreements
    And User view details of available VLA
    Then User navigates to Notes & Attachments section
    And User is able to view PDF Document in OUV VLA
    
    #User is not able to view the PDF document in an OUV Vehicle Agreement for an Approved OUV Booking with Signature
   	@OUV  @Vinaykumar @OUV_Phase2_063 @OUV_Phase2
    Scenario: User is not able to view the PDF document in an OUV Vehicle Agreement for an Approved OUV Booking with Signature
    Given User access OUV Portal with UserName "Users,D,4" Password "Users,E,4"
    And User Navigates to OUV Booking menu
    Then User search an OUV Booking with status "OUV Bookings,A,7"
    And User navigate to Vehicle Loan Agreements
    Then Verify that user is not able to view PDF Document in an OUV VLA
    Then User do logout from OUV Portal
    
      #User is able to view all the fields for Financial section in an OUV Vehicle
    @OUV   @Vinaykumar @OUV_Phase2_064 @OUV_Phase2
    Scenario: User is able to view all the fields for Financial section in an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then User view all the fields for Financial section in an OUV Vehicle		
    Then Logout from the OUV
    
 #User is able to view only selected fields for Financial section in an OUV Vehicle
    @OUV   @Vinaykumar @OUV_Phase2_065 @OUV_Phase2
    Scenario: User is able to view only selected fields for Financial section in an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,16" and password as "Users,E,16"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then User view only the selected fields for Financial section in an OUV Vehicle				
    Then Logout from the OUV
    
 #User is able to edit the Registration Date and De-Registration Date in an OUV Vehicle
    @OUV   @Vinaykumar @OUV_Phase2_066 @OUV_Phase2
    Scenario: User is able to edit the Registration Date and De-Registration Date in an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,16" and password as "Users,E,16"
    Then User navigate to OUV vehicles
    And select the vehicle
    Then User edits the Registration Date and DeRegistration Date	
    And save the changes performed	
    Then Logout from the OUV	
    
 #User is able to update the Master status from Live to Handed In in an OUV Vehicle
 @OUV  @Vinaykumar @OUV_Phase2_067 @OUV_Phase2
		Scenario: User is able to update the Master status from Live to Handed In in an OUV Vehicle
		Given Access to the OUV Portal with user "Users,D,16" and password as "Users,E,16"
		Then User navigate to OUV vehicles
		Then search for the vehicle with status "OUV vehicles,A,27"
		And select the vehicle
		Then select the edit button for the OUV Vehicle
		Then User updates master status to "OUV vehicles,B,2"
		Then User updates sub status to "OUV vehicles,C,12"
		Then User navigate to Keys transfer option and select it
		And save the changes performed
		Then Logout from the OUV
		
	#User is not able to update the Master status from Handed In to Live in an OUV Vehicle
	@OUV  @Vinaykumar @OUV_Phase2_068 @OUV_Phase2
	Scenario: User is not able to update the Master status from Handed In to Live in an OUV Vehicle
		Given Access to the OUV Portal with user "Users,D,16" and password as "Users,E,16"
		Then User navigate to OUV vehicles
		Then search for the vehicle with status "OUV vehicles,A,11"
		And select the vehicle
		Then select the edit button for the OUV Vehicle
		Then User updates master status to "OUV vehicles,B,5"
		Then User edits sub status to "OUV vehicles,C,16"
		Then User navigate to Keys transfer option and select it
		And save the changes performed
		Then Verify the displayed error message for insufficient permissions
		Then Logout from the OUV
    
    #User is able to view the OUV Frequent Driver list of Benelux Market if user is a member of Benelux Market Team
    @OUV  @Vinaykumar @OUV_Phase2_069 @OUV_Phase2
    Scenario: User is able to view the OUV Frequent Driver list of Benelux Market if user is a member of Benelux Market Team
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    Then Verify that User can view All OUV Frequent drivers
    And User opens any Frequent Driver from the list
    Then Verify that User is able to view OUV Frequent Driver of "OUV_Frequent_Driver,A,2" Market Team
    Then Logout from OUV
    
    #User is not able to view the OUV Frequent Driver list of Italian Market if user is a member of Benelux Market Team
    @OUV  @Vinaykumar @OUV_Phase2_070 @OUV_Phase2
    Scenario: User is not able to view the OUV Frequent Driver list of Italian Market if user is a member of Benelux Market Team
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    Then Verify that User can view All OUV Frequent drivers
    And User searches "OUV_Frequent_Driver,B,2" Frequent Driver from the list
    Then Verify that No result found for searched Frequent Driver
    Then Logout from OUV
    
    #User is able to verify OUV Frequent Driver owner and validate the details in the run report Benelux Users Report
    @OUV  @Vinaykumar @OUV_Phase2_071 @OUV_Phase2
    Scenario: User is able to verify OUV Frequent Driver owner and validate the details in the run report Benelux Users Report
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    And Verify that User can view OUV Frequent drivers Owner
    Then User verifies the Owner Name from the list
    And User Navigates to Reports
    Then User opens OUV Benelux Reports folder
    And User opens Benelux Users Report
    Then Verify Owner Name from Benelux Users Report
    Then Logout from OUV
    
    #User is only able to view the Annual Market Summary for the markets in the Benelux Market Team
  @OUV  @Vinaykumar @OUV_Phase2_072 @OUV_Phase2
  Scenario: User is only able to view the Annual Market Summary for the markets in the Benelux Market Team
    Given Access the OUV Portal with user "Users,D,5" and password as "Users,E,5" 
    And User navigates to Annual Market Summary
    Then User selects All from list view and verify AMS for markets Netherands and Belgium
    Then Logout from OUV 
    
  #User is able to edit the Annual Budget Threshold for an Annual Market Summary
  @OUV  @Vinaykumar @OUV_Phase2_073 @OUV_Phase2
  Scenario: User is able to edit the Annual Budget Threshold for an Annual Market Summary
    Given Access the OUV Portal with user "Users,D,17" and password as "Users,E,17" 
    And User navigates to Annual Market Summary
    Then User selects Benelux from list view
    Then Select an AMS for fiscal year "2021/2022" 
    Then Edit the field for Annual budget threshold and save
    Then Logout from OUV 
    
  #User is not able to edit the Annual Budget Threshold for an Annual Market Summary
  @OUV  @Vinaykumar @OUV_Phase2_074 @OUV_Phase2
  Scenario: User is not able to edit the Annual Budget Threshold for an Annual Market Summary
    Given Access the OUV Portal with user "Users,D,5" and password as "Users,E,5" 
    And User navigates to Annual Market Summary
    Then User selects All from list view and verify AMS for markets Netherands and Belgium
    Then Select an AMS for fiscal year "2021/2022" 
    Then Verify user cannot edit the Annual Budget Threshold field
    Then Logout from OUV
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2" 
    And User navigates to Annual Market Summary
    Then User selects All from list view and verify AMS for markets Netherands and Belgium
    Then Select an AMS for fiscal year "2021/2022" 
    Then Verify Annual Budget Threshold field is not present under Details section 
    Then Logout from OUV
    
    #User is able to update the status from 'In Progress' to 'Submitted'  for an OUV Forecast Snapshot for an Annual Market Summary
  @OUV @Vinaykumar @OUV_Phase2_075 @OUV_Phase2
  Scenario: User is able to update the status from 'In Progress' to 'Submitted'  for an OUV Forecast Snapshot for an Annual Market Summary
    Given Access the OUV Portal with user "Users,D,5" and password as "Users,E,5"
    And User navigate to Annual Market Summary	
    Then User selects an AMS with Fiscal Year "AMS,A,2"
    And Navigate to OUV Forecast Snapshot
    Then User selects an OUV Forecast Snapshot with status "AMS,B,2" 
    And User update the status to "AMS,B,3" and saves the record
    And Verify that user cannot edit the status again
    Then Logout from OUV
    Given Access the OUV Portal with user "Users,D,17" and password as "Users,E,17"
    And User navigate to Annual Market Summary
    Then User selects an AMS with Fiscal Year "AMS,A,2"
    And Navigate to OUV Forecast Snapshots	
    Then User selects an OUV Forecast Snapshot with status "AMS,B,3" 
    And User update the status to "AMS,B,2" and saves the record