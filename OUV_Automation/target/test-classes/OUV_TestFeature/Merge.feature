#End to End Negative Scenarios for OUV Calender
Feature: End to End Negative Scenarios for OUV Calender

@End2End_NegativeOUV
Scenario: Searching vehicle from OUV Calender Negative Flow
   Given Access OUV Portal
   Then Navigate to OUV Calendar
	 
#Below is the scenario for Searching a vehicle from OUV Calender	using Incorrect VIN    	 
   And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
	 When Enter Incorrect VIN "SALEA7BU6L2000938" for searching vehicle in OUV Calendar 
	 Then Search a Vehicle by Incorrect Data
	 And Navigate to Home Page
	 
#Below is the scenario for Searching a vehicle from OUV Calender	using Incorrect Primary Location
   And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
	 When Enter Incorrect Primary Location as "Australia" for searching vehicle in OUV Calendar 
	 Then Search a Vehicle by Incorrect Data	
	 And Navigate to Home Page		

#Below is the scenario for Searching a vehicle from OUV Calender	using Incorrect Model Description	
   And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
	 When Enter Incorrect Model Description as "Freelander" for searching vehicle in OUV Calendar
	 Then Search a Vehicle by Incorrect Data
	 And Navigate to Home Page 	
	 
#Below is the scenario for Searching a vehicle from OUV Calender	using Incorrect Register Plate 
   And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
	 When Enter Incorrect Register Plate Number as "BJ67UMP" for searching vehicle in OUV Calendar
	 Then Search a Vehicle by Incorrect Data	
	 And Navigate to Home Page
	 
#Below is the scenario for Searching a vehicle from OUV Calender	using Incorrect Model Year Description
   And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
	 When Enter Future Model Description as "2021" for searching vehicle in OUV Calendar
	 Then Search a Vehicle by Incorrect Data	
	 And Navigate to Home Page 	 		

#Below is the scenario for Searching a vehicle from OUV Calender	for all Vehicle Categories
   And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
	 When Selecting All Vehicle Categories to display
	 Then Search a Vehicle by Incorrect Data	
	 And Navigate to Home Page
	 
#Below scenario is used to Search vehicle for Reservation from OUV Calendar with end date prior start date
	Then Navigate to OUV Calendar
	And Search Vehicle by giving Fleet "OUV Calendar,A,3" along with Start Date as tomorrow and End Date as today
	Then verify the displayed booking end date error message 
	And Navigate to Home Page

#Below scenario is used to Search vehicle for Reservation from OUV Calendar with start date prior to todays date		

	Then Navigate to OUV Calendar
	And Search Vehicle by giving Fleet "OUV Calendar,A,3" along with Start Date prior to todays date
	Then verify the displayed booking start date error message 
	And Navigate to Home Page
		
#Below scenario is used to Search vehicle for Reservation from OUV Calendar with Market and Fleet being of different locations 

	Then Navigate to OUV Calendar
	And Search Vehicle by giving Market as "OUV Calendar,K,3" and Fleet as "OUV Calendar,A,3" 
	Then verify the displayed record error message 
	And Navigate to Home Page
	
#Below scenario is used to Search vehicle for Reservation from OUV Calendar while excluding Market information

  Then Navigate to OUV Calendar
  And Search Vehicle by giving Fleet as "OUV Calendar,A,3" while excluding Market information
  Then verify the displayed record error message
  And Navigate to Home Page
    
#Below scenario is used to save a Reservation from OUV Calendar while excluding all the mandatory information

  Then Navigate to OUV Calendar
  And Search Vehicle by giving Fleet "OUV Calendar,A,3"
  And The entry shows in Reservation Calendar in Days,Weeks and Months 
  When Select vehicle and week for Reservation
  And Save the Reservation
  Then verify the displayed Complete this field error message
  And Navigate to Home Page
  	
#Below scenario is used to enter email in invalid format when user creates a Reservation from OUV Calendar

  Then Navigate to OUV Calendar
  And Search Vehicle by giving Fleet "OUV Calendar,A,3"
  And The entry shows in Reservation Calendar in Days,Weeks and Months 
  When Select vehicle and week for Reservation
  Then Make Reservation with invalid email format
  And verify the displayed format error message 
  Then Logout from OUV Portal