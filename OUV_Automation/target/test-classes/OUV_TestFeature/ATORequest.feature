Feature: Validate ATO Request

  #User creates New ATO Request
  @OUV @OUVATO @OUV_080 @OUV_Batch_2 
  Scenario: User creates New ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then verify the ATO Request
    Then Logout from OUV Portal

  #User creates Multiple Vehicle ATO Requests
  @OUV @OUVATO @OUV_081 @SOUV-333 @OUV_Batch_2 
  Scenario: User creates Multiple Vehicle ATO Requests
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields of ATO Request like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Click on Save&New button to save the ATO request
    Then fill all mandatory fields of multiple ATO Request like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Click on Save button to save the ATO request
    Then Logout from OUV Portal

  #User tries to enter Proposed Hand In Date less than Proposed Add to Fleet Date in ATO Request
  @OUV @OUVATO @OUV_082 @OUV_Batch_2 
  Scenario: User tries to enter Proposed Hand In Date less than Proposed Add to Fleet Date in ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,3" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Verify the error message
    Then Logout from OUV Portal

  #User tries to Clone the ATO Request
  @OUV @OUVATO @OUV_083 @SOUV-335 @OUV_Batch_2 
  Scenario: User tries to Clone the ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields of ATO Request like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Click on Save button to save the ATO request
    Then Click on Clone button to clone the ATO request
    Then Click on Save button to save the ATO request
    Then Logout from OUV Portal

  #User tries to edit the ATO Request Status
  @OUV @OUVATO @OUV_084 @OUV_Batch_2 
  Scenario: User tries to edit the ATO Request Status
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Verify that user cannot edit the ATO request
    Then Logout from OUV Portal

  #User tries to create New ATO Request line item
  @OUV @OUVATO @OUV_085 @OUV_Batch_2 
  Scenario: User tries to create New ATO Request line item
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Navigate to ATO Request Line Items
    And User creates a new ATO Request Line Item
    Then Logout from OUV Portal

  #User tries to create multiple ATO Request line items
  @OUV @OUVATO @OUV_086 @OUV_Batch_2 
  Scenario: User tries to create multiple ATO Request line items
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Navigate to ATO Request Line Items
    And User creates multiple ATO Request Line Items
    Then Logout from OUV Portal

  #User tries to Edit the ATO Request line items
  @OUV @OUVATO @OUV_087 @OUV_Batch_2 
  Scenario: User tries to Edit the ATO Request line items
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Navigate to ATO Request Line Items
    And User creates a new ATO Request Line Item
    And User edits the ATO Request Line Item
    Then Logout from OUV Portal

  #Pre-Requisite:Need to add create a new ATO Request scenario
  @OUV @OUVATO @OUV_088 @OUV_Batch_2 
  Scenario: User tries to Delete the ATO Request line item
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Navigate to ATO Request Line Items
    And User creates a new ATO Request Line Item
    And User deletes the ATO Request Line Item
    Then Logout from OUV Portal

  #User tries to Change the Owner of an ATO Request
  @OUV @OUVATO @OUV_089 @OUV_Batch_2 
  Scenario: User tries to Change the Owner of an ATO Request
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    When user clicks on the change owner button
    And changes the owner as "ATO Requests,J,3"
    Then verify the displayed change owner error message
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #User tries to Re-open the status of an Approved ATO Request
  @OUV @OUVATO @OUV_090 @OUV_Batch_2 
  Scenario: User tries to Re-open the status of an Approved ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    Then Verify that user cannot edit the ATO request
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #User tries to edit the Approver details for an ATO Request
  @OUV @OUVATO @OUV_091 @OUV_Batch_2 
  Scenario: User tries to edit the Approver details for an ATO Request
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for the ATO Request with type "ATO Requests,A,2"
    And select the ATO Request
    Then Verify that user cannot edit Approver details for an ATO request
    Then Logout from OUV Portal

  #User Edits the fields under Fleet details section of an ATO Request
  @OUV @OUVATO @OUV_092 @OUV_Batch_2 
  Scenario: User Edits the fields under Fleet details section of an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,3" Field "ATO Requests,T,7" and value "ATO Requests,O,2"
    And select the ATO Request
    Then Click on Edit Internal JLR Contact pencil icon under Fleet details section
    Then Change Internal jlr contact to "ATO Requests,M,2" and verify
    Then Remove added filter with title "ATO Requests,O,3"
    Then Logout from OUV Portal

  #User tries to Withdraw the ATO Request
  @OUV @OUVATO @OUV_093 @SOUV-345 @OUV_Batch_2 
  Scenario: User tries to Withdraw the ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    Then Click on cancel/withdraw option from drop down list of an ATO Request
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #User send an Update on ATO Request Through Chatter Post
  @OUV @OUVATO @OUV_094 @SOUV-346 @OUV_Batch_2 
  Scenario: User send an Update on ATO Request Through Chatter Post
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    Then Navigate to Chatter tab
    Then Enter text in Share an update text box Share an Update "ATO Requests,V,2"
    Then Select To section from drop down list Share Dropdown "ATO Requests,W,3"
    Then Click on Share button
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #This Scenario is creating a new Note for an existing ATO Request
  @OUV @OUVATO @OUV_095 @OUV_Batch_2 
  Scenario: User tries to create New note for an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    And User Opens an existing ATO Request
    Then User creates a New Note for an Existing ATO Request
    Then Logout from OUV Portal

  #This Scenario is try to edit ATO Request Line Item for Pending Approval ATO Request
  @OUV @OUVATO @OUV_096 @OUV_Batch_2 
  Scenario: User cannot Edit the ATO Request line items for an ATO Request with 'Pending' status
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    And Navigate to ATO Request Line Item
    Then Click on any created ATO Request Line Item
    And User tries to Edit ATO Request Line Item and Verify an error occurred
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #This Scenario is deleting Note from ATO Request
  @OUV @OUVATO @OUV_097 @OUV_Batch_2 
  Scenario: User tries to delete the note for an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    And User Opens an existing ATO Request
    Then User creates a New Note for an Existing ATO Request
    And User delete the note from ATO Request
    Then Logout from OUV Portal

  #User tries to create an ATO Request without entering Vehicle details
  @OUV @OUVATO @OUV_098 @OUV_Batch_2 
  Scenario: User tries to create an ATO Request without entering Vehicle details
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2"
    Then Verify that ATO request cannot be created and verify the error message
    Then Logout from OUV Portal

  #User tries to create a ATO Request without entering Fleet Details
  @OUV @OUVATO @OUV_099 @OUV_Batch_2 
  Scenario: User tries to create a ATO Request without entering Fleet Details
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2"
    Then Verify that ATO request cannot be created and verify the error message
    Then Logout from OUV Portal

  #User is able to view ATO Request History for any open ATO Request
  @OUV @OUVATO @OUV_100 @OUV_Batch_2 
  Scenario: User is able to view ATO Request History for any open ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    Then Click on ATOHistory
    And Validate the ATO request History
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #User can Upload any files to an open ATO Request from Notes & Attachments section
  @OUV @OUVATO @OUV_101 @SOUV-353
  Scenario: User can Upload any files to an open ATO Request from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then Logout from OUV Portal

  #User is able to view files details of any uploaded files from Notes & Attachments section
  @OUV @OUVATO @OUV_102 @SOUV-354
  Scenario: User is able to view files details of any uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks on View File details button from down arrow column dropdown "ATORequests,U,2"
    Then Logout from OUV Portal

  #User is able to edit files details of any uploaded files from Notes & Attachments section
  @OUV @OUVATO @OUV_103 @SOUV-355
  Scenario: User is able to edit files details of any uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,3"
    Then User changes Title of the document Title "ATORequests,W,2"
    Then User can add Description of uploaded document Description "ATORequests,X,2"
    Then User clicks on Save button to save the changes of uploaded document
    Then Logout from OUV Portal

  #User can upload new version of already uploaded files from Notes & Attachments section
  @OUV @OUVATO @OUV_104 @SOUV-356
  Scenario: User can upload new version of already uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,4"
    Then User chooses a different version of file from the local system
    Then Logout from OUV Portal

  #User is able to delete uploaded files from Notes & Attachments section
  @OUV @OUVATO @OUV_105 @SOUV-357
  Scenario: User is able to delete uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,5"
    Then User can view one delete pop up, user clicks on delete button
    Then Logout from OUV Portal

  #User is able to download uploaded files from Notes & Attachments section
  @OUV @OUVATO @OUV_106 @SOUV-358
  Scenario: User is able to download uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,6"
    Then Logout from OUV Portal

  #User is able to share the uploaded files to anyone from Notes & Attachments section
  @OUV @OUVATO @OUV_107 @SOUV-359
  Scenario: User is able to share the uploaded files to anyone from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,7"
    Then User selects contact to whom file can share
    Then User can select share access for uploaded file Viewer "ATORequests,Y,2"
    Then Logout from OUV Portal

  #User can able to remove uploaded files from the record under Notes & Attachments section
  @OUV @OUVATO @OUV_108 @SOUV-360
  Scenario: User can able to remove uploaded files from the record under Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,8"
    Then User clicks on Remove from Record on pop up window

  #This Scenario is verifing User cannot cancel or Withdraw ATO Request
  @OUV @OUVATO @OUV_109 @SOUV-361
  Scenario: User can cancel or withdraw created ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    And User Opens an existing ATO Request which is in Pending Approval status
    Then Verify that user cannot view Cancel/Withdraw ATO Request button
    And User Opens an existing ATO Request which is in New status
    Then Verify that user cannot view Cancel/Withdraw ATO Request button
    Then Logout from OUV Portal

  #User cannot submit an ATO request for approval which is assigned to any other owner
  @OUV @OUVATO @OUV_110 @SOUV-362
  Scenario: User cannot submit an ATO request for approval which is assigned to any other owner
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,4"
    And select the ATO Request
    Then user submit an ATO request for approval
    Then Verify that user cannot submit an ATO request
    When navigate to the request fleet "ATO Requests,L,4"
    And verify that user is not a member of fleet "Users,C,2"
    Then Logout from OUV Portal

  #User changes the ATO request status from New to pending Approval
  @OUV @OUVATO @OUV_111 @SOUV-363
  Scenario: User changes the ATO request status from New to pending Approval
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like OUVReportinglevelone "ATO Requests,B,2",OUVReportingleveltwo "ATO Requests,C,2",OUVReportinglevelthree "ATO Requests,D,2",Market "ATO Requests,E,2",Fleetdate "ATO Requests,F,2",Handindate "ATO Requests,G,2",DisposalRoute "ATO Requests,H,2"
    Then verify the ATO Request
    When user clicks on ATO request line
    Then clicks on new ATO request line with vehicle "ATO Requests,P,2"
    And Navigate back to ATO request
    When user submit an ATO request for approval
    And verify that ATO request is in pending approval status
    Then Logout from OUV Portal

  #User tries to edit the ATO request line item When ATO request Status is in Pending Approval
  @OUV @OUVATO @OUV_112 @SOUV-364
  Scenario: User tries to edit the ATO request line item When ATO request Status is in Pending Approval
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like OUVReportinglevelone "ATO Requests,B,2",OUVReportingleveltwo "ATO Requests,C,2",OUVReportinglevelthree "ATO Requests,D,2",Market "ATO Requests,E,2",Fleetdate "ATO Requests,F,2",Handindate "ATO Requests,G,2",DisposalRoute "ATO Requests,H,2"
    Then verify the ATO Request
    When user clicks on ATO request line
    Then clicks on new ATO request line with vehicle "ATO Requests,P,2"
    And Navigate back to ATO request
    When user submit an ATO request for approval
    When user clicks on ATO request line
    Then user edits the the vehicle of a ATO request line as "ATO Requests,P,3"
    And verify the displayed ATO request line error message

  #In this Scenario user tries to recall Approval Request
  @OUV @OUVATO @OUV_113 @SOUV-365
  Scenario: User is able to recall Approval request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    And User Fills all mandatory Fields like RepoLOne "ATORequests,B,2" RepoLTwo "ATORequests,C,2" RepoLThree "ATORequests,D,2" Market "ATORequests,E,2" disposalRoute "ATORequests,H,2"
    And User submits new ATO Request for an Approval
    Then User navigate to ATO Request History
    And User recall ATO Approval Request
    And Verify that ATO Request status changes to 'New'
    Then Logout from OUV Portal

  #This Scenario changes ATO Request Type
  @OUV @OUVATO @OUV_114 @SOUV-366
  Scenario: User is able to change the ATO request type
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for the ATO Request with type "ATORequests,A,3"
    And User change the ATO Request Type to "ATORequests,A,5"
    Then Logout from OUV Portal

  #This Scenario tests that User is able to add Multiple ATO Request Line Items
  @OUV @OUVATO @OUV_115 @SOUV-367
  Scenario: User is able to add Multiple ATO request line items when ATO request type is Multiple Vehicle-events
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    And User Fills all mandatory Fields like ATOReqType "ATORequests,A,5" RepoLOne "ATORequests,B,2" RepoLTwo "ATORequests,C,2" RepoLThree "ATORequests,D,2" Market "ATORequests,E,2" disposalRoute "ATORequests,H,2"
    And User creates Multiple ATO Request Line Items
    Then Logout from OUV Portal

  #User cannot edit the Request line items for Approved ATO request
  @OUV @OUVATO @OUV_116
  Scenario: User cannot edit the Request line items for Approved ATO request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,2"
    And select the ATO Request
    Then Navigate to ATO Request Line Items
    And Click on dropdown and select edit option
    Then Verify that user unable to find vista vehicle field
    Then Logout from OUV Portal

  #User Cannot recall Approval request for Approved ATO request in Approval history
  @OUV @OUVATO @OUV_117
  Scenario: User Cannot recall Approval request for Approved ATO request in Approval history
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,2"
    And select the ATO Request
    Then Navigate to Approval History
    Then verify that Recall button should not be there
    Then Logout from OUV Portal

  #User is able to Edit Vista Vehicle for ATO request line item in new ATO request
  @OUV @OUVATO @OUV_118 @SOUV-370
  Scenario: User is able to Edit Vista Vehicle for ATO request line item in new ATO request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    And User Fills all mandatory Fields like ATOReqType "ATORequests,A,5" RepoLOne "ATORequests,B,2" RepoLTwo "ATORequests,C,2" RepoLThree "ATORequests,D,2" Market "ATORequests,E,2" disposalRoute "ATORequests,H,2"
    Then verify the ATO Request
    When user clicks on ATO request line
    Then clicks on new ATO request line with vehicle "ATO Requests,P,3"
    And Navigate back to ATO request
    When user clicks on ATO request line
    Then user edits the the vehicle of a ATO request line as "ATO Requests,P,2"
    And Verify that user is able to edit ATO request line as "ATO Requests,Q,2"
    Then Logout from OUV Portal

  #User is able to search for the ATO Request in the ATO Request Tab
  @OUV @OUVATO @OUV_119 @SOUV-371
  Scenario: User is able to search for the ATO Request in the ATO Request Tab
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,2"
    And Verify that user is able to search for status "ATO Requests,I,2"
    Then Logout from OUV Portal

  #User is not able to delete the ATO request line items
  @OUV @OUVATO @OUV_120 @SOUV-372
  Scenario: User is not able to delete the ATO request line items
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,5"
    And select the ATO Request
    When user clicks on ATO request line
    Then user delete an ATO request line items
    And verify the displayed delete error message
    Then Logout from OUV Portal

  #User is not able to Edit Vista Vehicle for ATO request line item in Approved ATO request
  @OUV @OUVATO @OUV_121
  Scenario: User is not able to Edit Vista Vehicle for ATO request line item in Approved ATO request
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,2"
    And select the ATO Request
    Then Navigate to ATO Request Line Items
    And Click on ATO line item and click on Edit vista vehicle with "ATO Requests,P,2"
    Then Verify that user unable to edit vista vehicle field
    Then Logout from OUV Portal

  #User unable to find Recall button at Approval History for ATO request which is in Pending Approval stage
  @OUV @OUVATO @OUV_122
  Scenario: User unable to find Recall button at Approval History for ATO request which is in Pending Approval stage
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,3"
    And select the ATO Request
    Then Navigate to Approval History
    Then verify that Recall button should not be there
    Then Logout from OUV Portal

  #User is not able to edit fleet details for ATO request
  @OUV @OUVATO @OUV_123 @SOUV-375
  Scenario: User is not able to edit fleet details for ATO request
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,3"
    And select the ATO Request
    Then user edits the ATO request type
    And User clicks on the save button
    Then verify the displayed privileges error message
    Then Logout from OUV Portal

  #User unable to find delete option for ATO request
  @OUV @OUVATO @OUV_124 @SOUV-376
  Scenario: User unable to find delete option for ATO request
    Given Access an OUV Portal with user "Users,D,5" and password as "Users,E,5"
    And User navigate to ATO Request
    Then verify if there is delete option for ATO request
    Then Logout from OUV Portal

  #User Unable to find edit option at the ATO request Status
  @OUV @OUVATO @OUV_125 @SOUV-377
  Scenario: User Unable to find edit option at the ATO request Status
    Given Access an OUV Portal with user "Users,D,6" and password as "Users,E,6"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,7"
    And select the ATO Request
    Then verify that user is unable to find edit option at the ATO request Status
    Then Logout from OUV Portal

  #User cannot change an ATO Request type
  @OUV @OUVATO @OUV_126
  Scenario: User cannot change ATO Request type
    Given Access an OUV Portal with user "Users,D,6" and password as "Users,E,6"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,A,3"
    And select the ATO Request
    Then User edits the ATO Request Type as "ATO Requests,A,5"
    Then Verify that an error message is displayed
    Then Logout from OUV Portal

  #User cannot delete the ATO request line items in Approved ATO request
  @OUV @OUVATO @OUV_127
  Scenario: User cannot delete the ATO request line items in Approved ATO request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,6"
    And select the ATO Request
    Then Navigate to ATO Request Line Items
    And User deletes the ATO Request Line Item
    Then Verify user cannot delete the ATO Request line Items
    Then Logout from OUV Portal

  #User cannot delete the ATO request line items When ATO request is in pending approval stage
  @OUV @OUVATO @OUV_128
  Scenario: User cannot delete the ATO request line items When ATO request is in pending approval stage
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,8"
    And select the ATO Request
    Then Navigate to ATO Request Line Items
    And User deletes the ATO Request Line Item
    Then Verify user cannot delete the ATO Request line Items
    Then Logout from OUV Portal

  #User can add Multiple Line Items for ATO Request Type as Multiple Vehicle and ATO Request Status as New
  @OUV @OUVATO @OUV_194
  Scenario: User User can add Multiple Line Items for ATO Request Type as Multiple Vehicle and ATO Request Status as New
    Given Access an OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as "ATORequests,T,4"
    Then At the top of right hand side in ATO Request window, select the dropdown for create line items "ATORequests,AA,3"
    Then Enter the Value  and click on save "ATORequests,AB,2"
    Then Logout from OUV Portal

  #User cannot add Multiple Line Items with ATO Request Type as Single Vehicle and ATO Request Status as Approved
  @OUV @OUVATO @OUV_195
  Scenario: User User cannot add Multiple Line Items with ATO Request Type as Single Vehicle and ATO Request Status as Approved
    Given Access an OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as "ATORequests,T,5"
    Then User is not able to view select the dropdown for create line items
    Then Logout from OUV Portal

  #User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle and ATO Request Status as Pending Approval
  @OUV @OUVATO @OUV_196
  Scenario: User User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle and ATO Request Status as Pending Approval
    Given Access an OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as "ATORequests,T,6"
    Then At the top of right hand side in ATO Request window, select the dropdown for create line items "ATORequests,AA,3"
    Then Enter the Value  and click on save "ATORequests,AB,2"
    Then Verify the Error Message
    Then Logout from OUV Portal

  #User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle and ATO Request Status as New
  @OUV @OUVATO @OUV_197
  Scenario: User User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle and ATO Request Status as New
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as "ATORequests,T,4"
    Then At the top of right hand side in ATO Request window, select the dropdown for create line items "ATORequests,AA,3"
    Then Enter the Value  and click on save "ATORequests,AB,2"
    Then Verifying the Error Message
    Then Logout from OUV Portal

  #User submits and approves an ATO Request
  @OUV @OUVATO @OUV_213
  Scenario: User submits and approves an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    And User Fills all mandatory Fields like RepoLOne "ATORequests,B,2" RepoLTwo "ATORequests,C,2" RepoLThree "ATORequests,D,2" Market "ATORequests,E,2" disposalRoute "ATORequests,H,2"
    Then user clicks on ATO request line
    And clicks on new ATO request line with vehicle "ATORequests,P,2"
    And Navigate back to ATO Request
    And user submit an ATO request for approval
    And Validate the Over Budget Status as Yes/No
    Then Navigate to Approval History and validate the Approver it is pending to as "Users,C,3"
    Then Logout from OUV Portal
    And Login to OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then Approve the ATO Request
    And Navigate back to ATO Request
    Then Navigate to Approval History and validate the Approver it is pending to as "Users,C,4"
    Then Logout from OUV Portal
    And Login to OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then Approve the ATO Request
    And Navigate back to ATO Request
    Then Navigate to Approval History and validate the Approver it is pending to as "Users,C,5"
    Then Logout from OUV Portal
    And Login to OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then Approve the ATO Request
    And Navigate back to ATO Request
    Then Navigate to Approval History and validate the Approver it is pending to as "Users,C,6"
    Then Logout from OUV Portal
    And Login to OUV Portal with user "Users,D,6" and password as "Users,E,6"
    Then Approve the ATO Request
    Then Navigate to Approval History and validate the status as approved,if not login as user "Users,D,6" and password as "Users,E,6" ,and user "Users,D,6" and password as "Users,E,6"
    Then Logout from OUV Portal

  #User tries to add a Replacement Vehicle which is in Pre-Live status
  @OUV @OUVATO @OUV_217
  Scenario: User tries to add a Replacement Vehicle which is in Pre-Live status
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then verify the ATO Request
    Then click on edit Replacement Vehicle
    When Replacement Vehicle information VIN "ATO Requests,Q,4" and Vehicle Name "ATO Requests,AC,4" is entered click save
    And verify the error message for Replacement Vehicle
    Then Logout from OUV Portal

  #User tries to add a Replacement Vehicle which is in 'Live' status
  @OUV @OUVATO @OUV_218
  Scenario: User tries to add a Replacement Vehicle which is in 'Live' status
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then verify the ATO Request
    Then click on edit Replacement Vehicle
    When Replacement Vehicle information VIN "ATO Requests,Q,3" and Vehicle Name "ATO Requests,AC,3" is entered click save
    Then remove the added vehicle
    Then Logout from OUV Portal

  #User is able to create a new ATO Request with a Live Replacement Vehicle
  @OUV @OUVATO @OUV_phase2_017
  Scenario: User is able to create a new ATO Request with a Live Replacement Vehicle
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,6" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2" Replacementvehicle "ATO Requests,Q,2"
    Then Navigate to ATO Request Line Items
    And User creates a new ATO Request Line Item
    Then Logout from OUV Portal

  #User is not able to create a new ATO Request with a Live Replacement Vehicle which is already used by another ATO
  @OUV @OUVATO @OUV_phase2_018
  Scenario: User is not able to create a new ATO Request with a Live Replacement Vehicle which is already used by another ATO
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,6" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2" Replacementvehicle "ATO Requests,Q,2"
    Then verify the error message displayed
    Then Logout from OUV Portal
