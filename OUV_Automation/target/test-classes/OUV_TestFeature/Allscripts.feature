Feature: Validate OUV scripts

#Batch of 001 to 050 scripts

 #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) for Brand - Jaguar
  @OUVnewRun @OUV_001 @OUVBatch @SOUV-253 @batch001to050 @Archana  
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL for Brand - Jaguar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles for Brand "Jaguar"
    And Search for Vehicles
    Then Logout from OUV

  #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) for Brand - Land Rover
  @OUVnewRun @OUV_002 @OUVBatch @batch001to050 @Archana  
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL for Brand - LandRover
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles for Brand "Land Rover"
    And Search for Vehicles
    Then Logout from OUV

  #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) without selecting any Brand
  @OUVnewRun @OUV_003 @OUVBatch @batch001to050 @Archana  
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL without selecting any Brand
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search for Vehicles
    Then Logout from OUV

  #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) by selecting Booking Start Date and End Date
  @OUVnewRun @OUV_004 @OUVBatch @batch001to050 @Archana  
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL by selecting Future Booking Start Date and End Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles by selecting Future Booking Start Date and End Date
    And Search for Vehicles
    Then Logout from OUV

  #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) by selecting Prior Booking Start Date
  @OUVnewRun @OUV_005 @OUVBatch @batch001to050 @Archana  
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL by selecting Prior Booking Start Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles by selecting Prior Booking Start Date
    And Search for Vehicles
    Then Verify the displayed Booking Start Date error message
    Then Logout from OUV

  #User searches for Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) by selecting Prior Booking End Date
  @OUVnewRun @OUV_006 @batch001to050 @Archana  
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL by selecting Prior Booking End Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles by selecting Prior Booking End Date
    And Search for Vehicles
    Then Verify the error message displayed on selecting prior booking end date
    Then Logout from OUV

  #Verification code needed
  #User searches for Vehicle by selecting only the mandatory fields
  @OUVnewRun @OUV_007 @batch001to050 @Archana  
  Scenario: User searches for Vehicle by selecting only the mandatory fields
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by selecting Future Booking Start Date and End Date
    And Search for Vehicles
    Then Verify the error message displayed for No records found
    Then Logout from OUV

  #User should be able to see unavailable vehicles which is greyed out in Reservation Calendar
  @OUVnewRun @OUV_008 @batch001to050 @Archana  
  Scenario: User should be able to see unavailable vehicles greyed out in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search for Vehicles
    Then Verify that unavailable vehicles are greyed out
    Then Logout from OUV

  #User should be able to highlight available vehicles in Reservation Calendar
  @OUVnewRun @OUV_009 @batch001to050 @Archana  
  Scenario: User should be able to highlight available vehicles in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search for Vehicles
    Then Verify that user is able to select an available Vehicle
    Then Logout from OUV

  #User should be able to see an entry for Vehicle in Reservation Calendar in days, weeks and months
  @OUVnewRun @OUV_010 @batch001to050 @Archana 
  Scenario: User should be able to see an entry for Vehicle in Reservation Calendar in days, weeks and months
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search for Vehicles
    Then Select Day, Week and Month Tab in the Reservation Calendar
    Then Logout from OUV

  #User is able to view the entry of a Vehicle in time format under Day Tab in the Reservation Calendar
  @OUVnewRun @OUV_0011 @batch001to050 @Archana 
  Scenario: User is able to view the entry of a Vehicle in time format under Day Tab in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    Then select the day tab in calendar and verify its format
    Then Logout from OUV

  #User is able to view the entry of a Vehicle in Days format under Week Tab in the Reservation Calendar
  @OUVnewRun @OUV_0012 @batch001to050 @Archana 
  Scenario: User is able to view the entry of a Vehicle in Days format under Week Tab in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    Then select the Week tab in calendar and verify its format
    Then Logout from OUV

  #User is able to view the entry of a Vehicle in Months format under Month Tab in the Reservation Calendar
  @OUVnewRun @OUV_0013 @batch001to050 @Archana 
  Scenario: User is able to view the entry of a Vehicle in Months format under Month Tab in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    Then select the Month tab in calendar and verify its format
    Then Logout from OUV

  #User is able to view all the searched vehicles in the Reservation Calendar
  @OUVnewRun @OUV_0014 @batch001to050 @Archana 
  Scenario: User is able to view all the searched vehicles in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    Then verify the View list of searched vehicles in the Reservation Calendar
    Then Logout from OUV

  #User should be able to see an OUV Booking Pop Up after selecting the available vehicle for Reservation in the Reservation Calendar
  @OUVnewRun @OUV_015 @batch001to050 @Archana 
  Scenario: User should be able to see an OUV Booking Pop Up after selecting the available vehicle for Reservation in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User should be able to select any option [Reservation(Passout)] from dropdown from OUV Booking window
  @OUVnewRun @OUVCal @OUV_016 @batch001to050 @Archana 
  Scenario: User should be able to select any option ReservationPassout from dropdown from OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User should be able to edit Start Date and Start Time while doing OUV Booking
  @OUVnewRun @OUVCal @OUV_017 @OUVBatch
  Scenario: User should be able to edit Start Date and Start Time while doing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    And User edit Start Date "OUV Calendar,B,2" and Start Time "OUV Calendar,C,2" on OUV Booking window
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User should be able to edit End Date and End Time while doing OUV Booking
  @OUVnewRun @OUVCal @OUV_018 @batch001to050 @Archana 
  Scenario: User should be able to edit End Date and End Time while doing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    And User edit End Date "OUV Calendar,D,2" and End Time "OUV Calendar,E,2" on OUV Booking window
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User saves an OUV Booking by giving prior Start Date
  @OUVnewRun @OUVCal @OUV_019 @batch001to050 @Archana 
  Scenario: User saves an OUV Booking by giving prior Start Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    And User saves an OUV Booking with Booking Quick Reference "OUV Calendar,F,3",Start Date "OUV Calendar,B,3",Booking Justification "OUV Calendar,G,3",Booking Request Type "OUV Calendar,H,3",Passout Type "OUV Calendar,I,3",Journey Type "OUV Calendar,J,3"
    Then An error occurred when user saves an OUV Booking with prior Start Date
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User saves an OUV Booking by giving prior End Date
  @OUVnewRun @OUVCal @OUV_020 @batch001to050 @Archana  
  Scenario: User saves an OUV Booking by giving prior End Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    And User saves an OUV Booking with Booking Quick Reference "OUV Calendar,F,4",Start Date "OUV Calendar,B,4",Booking Justification "OUV Calendar,G,4",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then An error occurred when user saves an OUV Booking with prior End Date
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User saves an OUV Booking by selecting only the mandatory fields
  @OUVnewRun @OUV_021 @batch001to050 @Archana  @RC
  Scenario: User saves an OUV Booking by selecting only the mandatory fields
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,2"	
    Then User saves an OUV Booking with Booking Quick Reference "Calendarcell,A,2",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    And An error occurred when user saves an OUV Booking
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User saves an OUV Booking by selecting all the mandatory fields and selecting other Location field
  @OUVnewRun @OUV_022 @batch001to050 @Archana  @RC
  Scenario: User saves an OUV Booking by selecting all the mandatory fields and selecting other Location field
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,3"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,3",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User is able to view the details of an OUV Booking in Reservation Calendar
  @OUVnewRun @OUV_023 @batch001to050 @Archana  @RC
  Scenario: User is able to view the details of an OUV Booking in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,4"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,4",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    And Validate the details in the Reservation Calender
    Then Logout from OUV

  #User saves an OUV Booking by Including Preparation/Transportation field
  @OUVnewRun @OUV_024 @batch001to050 @Archana  @RC
  Scenario: User saves an OUV Booking by Including Preparation/Transportation field
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,5"
    Then User saves an OUV Booking including Preparation/Transportation field with Booking Quick Reference "Calendarcell,A,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Logout from OUV

  #User is able to edit an existing OUV Booking
  @OUVnewRun @OUVCal @OUV_025 @batch001to050 @Archana 
  Scenario: User is able to edit an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,1"
    Then User edits details of OUV Booking
    Then Logout from OUV

  #User is able to delete an existing OUV Booking
  @OUVnewRun @OUVCal @OUV_026 @batch001to050 @Archana 
  Scenario: User is able to delete an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,1"
    Then User deletes created OUV Booking
    And Verify that User cannot delete OUV Booking
    Then Logout from OUV

  #User is able to view in Salesforce for an existing OUV Booking
  @OUVnewRun @OUVCal @OUV_027 @batch001to050 @Archana 
  Scenario: User is able to view in Salesforce for an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,1"
    Then User clicks on View In Salesforce button and verify all details are displayed
    Then Logout from OUV

  #User is able to cancel any changes made in an existing OUV Booking
  @OUVnewRun @OUVCal @OUV_028 @batch001to050 @Archana 
  Scenario: User is able to cancel any changes made in an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,1"
    Then User cancels any changes made in an existing OUV Booking
    Then Logout from OUV

  #User is able to validate that OUV Vehicle is pre-populated in the OUV Booking
  @OUVnewRun @OUVCal @OUV_029 @batch001to050 @Archana  @RC
  Scenario: User is able to validate that OUV Vehicle is pre-populated in the OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,6"
    Then User validate that OUV Vehicle is pre-populated in the OUV Booking window
    Then Logout from OUV

  #(Need to add approved scenario)
  #User is able to view the Approved OUV Booking for Reservation(Passout)�highlighted in green color in Reservation Calendar
  @OUVnewRun @OUVCal @OUV_030 @batch001to050 @Archana 
  Scenario: User is able to view the Approved OUV Booking highlighted in green color in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And Validate that an approved OUV Booking is highlighted in green color
    Then Logout from OUV

  #User is able to view the Non-Approved OUV Booking for Reservation(Passout)�highlighted in blue color in Reservation Calendar
  @OUVnewRun @OUVCal @OUV_031 @batch001to050 @Archana 
  Scenario: User is able to view the Non-Approved OUV Booking highlighted in blue color in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And Validate that a Non Approved OUV Booking is highlighted in blue color
    Then Logout from OUV

  #User creates an OUV Booking and Submit for Approval
  @OUVnewRun @OUVCal @OUV_032 @batch001to050 @Archana  @RC
  Scenario: User creates an OUV Booking and Submit for Approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,7"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,7",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    Then Logout from OUV

  #User is able to see Booking Start Date and Booking end Date pre-populated while searching a vehicle
  @OUVnewRun @OUVCal @OUV_033 @batch001to050 @Archana 
  Scenario: User is able to see Booking Start Date and Booking end Date pre-populated while searching a vehicle
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,8"
    And Verify that Booking Start Date and Booking End date is pre-populated
    Then Logout from OUV

  #User is able to see Start Date and Start Time pre-populated in OUV Booking window
  @OUVnewRun @OUVCal @OUV_034 @batch001to050 @Archana 
  Scenario: User is able to see Start Date and Start Time pre-populated in OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,8"
    And Verify that User can see Start date and Start Time are Pre-populated
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User is able to see End Date and End Time pre-populated in OUV Booking window
  @OUVnewRun @OUVCal @OUV_035 @batch001to050 @Archana 
  Scenario: User is able to see End Date and End Time pre-populated in OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,8"
    And Verify that User can see End date and End Time are Pre-populated
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User does not enter Preparation/Transportation End Date while including  Preparation/Transportation checkbox in OUV Booking window
  @OUVnewRun @OUVCal @OUV_036 @batch001to050 @Archana 
  Scenario: User does not enter Preparation/Transportation End Date while including  Preparation/Transportation checkbox in OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,8"
    And User saves an OUV Booking without entering Preparation/Transportation End date Field with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then An error occurred while saving new OUV Booking without entering Preparation/Trasportation End Date
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User is able to see an OUV Booking ID after saving an OUV Booking
  @OUVnewRun @OUVCal @OUV_037 @batch001to050 @Archana 
  Scenario: User is able to see an OUV Booking ID after saving an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,9"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,9",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Validate that user is able to see the Booking ID generated
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User is able to see a confirmation message for Reservation successfully saved after saving an OUV Booking
  @OUVnewRun @OUVCal @OUV_038 @batch001to050 @Archana 
  Scenario: User is able to see a confirmation message for Reservation successfully saved after saving an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,10"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,10",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    And Validate that user sees a confirmation message for Reservation successfully saved
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User is able to navigate to the OUV Booking ID after saving an OUV Booking
  @OUVnewRun @OUVCal @OUV_039 @batch001to050 @Archana 
  Scenario: User is able to navigate to the OUV Booking ID after saving an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,11"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,11",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then Logout from OUV

  #User verifies all the details in the OUV Booking ID Page are auto-populated after saving an OUV Booking
  @OUVnewRun @OUVCal @OUV_040 @batch001to050 @Archana 
  Scenario: User verifies all the details in the OUV Booking ID Page are auto-populated after saving an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,12"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,12",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User is able to verify all the details are auto-populated with Booking Quick Reference "Calendarcell,A,12",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Logout from OUV

  #User is able to edit the details in the OUV Booking ID Page
  @OUVnewRun @OUVCal @OUV_041 @batch001to050 @Archana 
  Scenario: User is able to edit the details in the OUV Booking ID Page
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,13"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,13",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User edits the details in the OUV Booking ID Page
    Then Logout from OUV

  #User is able to see VLA Market pre-populated under Vehicle Loan Agreement in OUV Booking ID Page
  @OUVnewRun @OUVCal @OUV_042 @batch001to050 @Archana 
  Scenario: User is able to see VLA Market pre-populated under Vehicle Loan Agreement in OUV Booking ID Page
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,14"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,14",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User verify that VLA Market field is Auto populated by verifying Market "OUV Calendar,A,2"
    Then Logout from OUV

  #User enters an invalid Internal JLR contact Email in OUV Booking window
  @OUVnewRun @OUVCal @OUV_043 @batch001to050 @Archana 
  Scenario: User enters an invalid Internal JLR contact Email in OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,15"
    Then User saves an OUV Booking including other location and invalid emial with Booking Quick Reference "Calendarcell,A,15",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And verify the error message
    Then Logout from OUV

  #User should be able to verify the owner name from OUV Booking ID window
  @OUVnewRun @OUVCal @OUV_044 @batch001to050 @Archana 
  Scenario: User should be able to verify the owner name from OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,16"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,16",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User verifies the owner name "Users,D,2" from OUV Booking ID Window
    Then Logout from OUV

  #User selects Auto Generate VLA in the OUV Booking ID window
  @OUVnewRun @OUVCal @OUV_045 @batch001to050 @Archana 
  Scenario: User selects Auto Generate VLA in the OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,17"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,17",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA
    Then Logout from OUV

  #User selects Auto Generate VLA and saves the record in the OUV Booking ID window
  @OUVnewRun @OUVCal @OUV_046 @batch001to050 @Archana 
  Scenario: User selects Auto Generate VLA and saves the record in the OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,18"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,18",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA
    And User saves the record and validate that an error message is displayed
    Then Logout from OUV

  #User selects Auto Generate VLA and enters Market,Template and Language for the Loan Agreement and saves the record in the OUV Booking ID window
  @OUVnewRun @OUVCal @OUV_047 @batch001to050 @Archana 
  Scenario: User selects Auto Generate VLA and enters Market,Template and Language for the Loan Agreement and saves the record in the OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,19"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,19",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then Logout from OUV

  #User is able to add an OUV Vehicle Loan Agreement Name under Loan Agreements listed under Related Quick links
  @OUVnewRun @OUVCal @OUV_048 @batch001to050 @Archana 
  Scenario: User is able to add an OUV Vehicle Loan Agreement Name under Loan Agreements
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,20"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,20",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then Logout from OUV

  #User cannot search for Frequent Drivers in New OUV Vehicle Loan Agreement
  @OUVnewRun @OUVCal @OUV_049 @batch001to050 @Archana 
  Scenario: User cannot search for Frequent Drivers in New OUV Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,21"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,21",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User Navigate to OUV Vehicle Loan Agreements
    Then Verify that User cannot see Frequent Drivers in New OUV Vehicle Loan Agreement window
    Then Logout from OUV

  #User searches for Frequent Drivers in New OUV Vehicle Loan Agreement and saves the Agreement
  @OUVnewRun @OUVCal @OUV_050 @batch001to050 @Archana 
  Scenario: User searches for Frequent Drivers in New OUV Vehicle Loan Agreement and saves the Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,22"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,22",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User Navigate to OUV Vehicle Loan Agreements
    #Funtional Error, no dropdown displays for Frequent Driver
    And User enters only Frequent Driver "OUV Calendar,N,2" and verify the error upon saves the Agreement
    Then Logout from OUV

 #Batch of 051 to 100 scripts
 
  #User searches for Frequent Drivers and enters the driver information in New OUV Vehicle Loan Agreement and saves the Agreement
  @OUVnewRun @OUVCal @OUV_051 @Archana 
  Scenario: User searches for Frequent Drivers and enters the driver information in New OUV Vehicle Loan Agreement and saves the Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,32"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,32",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User Navigate to OUV Vehicle Loan Agreements
    And User enters only Frequent Driver "OUV Calendar,N,2" and Type of Driver "OUV Calendar,O,2" and saves VLA
    Then Logout from OUV

  #User creates an OUV Booking and approve it
  @OUVnewRun @OUVCal @OUV_052 @Archana 
  Scenario: User creates an OUV Booking and approve it
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,33"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,33",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Verify that OUV Booking status is Approved if Booking Justification "OUV Calendar,G,6"
    Then Logout from OUV

  #User verifies the Booking Approval Status as 'Booked Pending Approval' in the OUV Booking ID window after submitting for Approval
  @OUVnewRun @OUVCal @OUV_053 @Archana 
  Scenario: User verifies the Booking Approval Status as 'Booked Pending Approval' in the OUV Booking ID window after submitting for Approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,34"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,34",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Verify OUV Booking status is Booked Pending Approval
    Then Logout from OUV

  #User verifies the status as 'Submitted' in Approval History Tab under Related Quick Links after submitting it for approval
  @OUVnewRun @OUVCal @OUV_054 @Archana 
  Scenario: User verifies the status as 'Submitted' in Approval History Tab under Related Quick Links after submitting it for approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,35"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,35",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Verify OUV Booking status is Booked Pending Approval
    And User navigates to Approval History
    Then Verify that status of OUV Booking as submitted
    Then Logout from OUV

  #User verifies that after Submitting for Approval, the first approval is assigned to the 'Fleet Manager'
  @OUVnewRun @OUVCal @OUV_055 @Archana 
  Scenario: User verifies that after Submitting for Approval, the first approval is assigned to the 'Fleet Manager'
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,36"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,36",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    Then User verifies First level of approval is Fleet Manager from Booking Approvers List
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Logout from OUV

  #User is able to complete the reservation after doing CheckIn & Close
  @OUVnewRun @OUV_056 @OUVBatch @Archana 
  Scenario: User verifies that when selecting Passout Type as 'Overnight', the second approval is assigned to the 'Overnight Pass Approver (LL5)'
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,37"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,37",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6" FrequentDriver "OUV Calendar,N,2" DriverType "OUV Calendar,O,2" EmailId "OUV Calendar,P,2"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    And Verify that Second Level of Approval is Pass Manager for PassoutType "OUV Calendar,I.4"
    Then Logout from OUV

  #Need to approve
  @OUVnewRun @OUVCal @OUV_057 @Archana 
  Scenario: User verifies that when selecting Passout Type as Day the second approval is assigned to the Day Pass Approver (LL6)
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,38"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,38",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,5",Journey Type "OUV Calendar,J,5"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Validate that first approval is assigned to the Fleet Manager
    And Verify that the Day Pass Approver information as "Users,C,3"
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then verify that the Pending Approval is assigned to the Day Pass Approver "Users,C,3"
    Then Logout from OUV

  #User verifies that when selecting Passout Type as 'Weekend', the second approval is assigned to the 'Weekend Pass Approver (LL4)'
  @OUVnewRun @OUVCal @OUV_058 @Archana 
  Scenario: User verifies that after Submitting for Approval, the first approval is assigned to the 'Fleet Manager'
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,39"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,39",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Validate that first approval is assigned to the Fleet Manager
    Then Logout from OUV

  #User is able to Recall an approval from Approval History
  @OUVnewRun @OUVCal @OUV_059 @Archana 
  Scenario: User is able to Recall an approval from Approval History
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,40"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,40",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    Then User recalls an Approval
    Then Logout from OUV

  #User is able to validate the Booking Approval status changes from 'Pending' to 'Recalled' in the Approval History Tab after recalling an approval
  @OUVnewRun @OUVCal @OUV_060 @Archana 
  Scenario: User is able to validate the Booking Approval status changes from Pending to Recalled in the Approval History Tab after recalling an approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,41"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,41",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    Then User recalls an Approval
    And User validates that the Approval status changes to Recalled
    Then Logout from OUV

  #User is able to validate that a Recall Request was sent to the Approver
  @OUVnewRun @OUVCal @OUV_061 @Archana 
  Scenario: User is able to validate that a Recall Request was sent to the Approver
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,42"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,42",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then User recalls an Approval
    And User validates that the Approval status changes to Recalled
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    And User verifies OUV Booking status is Recalled
    Then Logout from OUV

  #User is able to validate that the Booking Approval Status changes from 'Booked Pending Approval' to 'New' after the Approval is recalled
  @OUVnewRun @OUVCal @OUV_062 @Archana 
  Scenario: User is able to validate that the Booking Approval Status changes from 'Booked Pending Approval' to 'New' after the Approval is recalled
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,43"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,43",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then User recalls an Approval
    And User validates that the Approval status changes to Recalled
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then User verify that the OUV Booking status moves to New from Booked Pending Approval
    Then Logout from OUV

  #User is able to view the Submit for Approval Button after recalling an approval
  @OUVnewRun @OUVCal @OUV_063 @Archana 
  Scenario: User is able to view the Submit for Approval Button after recalling an approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,44"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,44",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then User recalls an Approval
    And User validates that the Approval status changes to Recalled
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then User verify that the OUV Booking status moves to New from Booked Pending Approval
    And User submit an OUV Booking for an Approval after recalling
    Then Logout from OUV

  #"User is able to Reject an approval	"
  @OUVnewRun @OUVCal @OUV_064 @Archana 
  Scenario: User is able to Reject an approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,45"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,45",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User rejects the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently rejected OUV Booking
    And User navigates to Approval History
    Then Verify that OUV Booking status is Rejected if Booking Justification "OUV Calendar,G,6"
    Then Logout from OUV

  #User is able to edit primary location and Sub status to update Master status to Live from Pre-Live
  @OUVnewRun @OUV_065 @OUVBatch @SOUV-317 @Archana 
  Scenario: User is able to edit primary location and Sub status to update Master status to Live from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,3"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    Then User edits sub status to "OUV vehicles,C,7"
    And save the changes performed
    Then Verify the displayed status error message for Primary Location
    And User enters Primary Location
    And save the changes performed
    Then validate the status change to Live from Pre-Live
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Live from Pre-Live without adding primary location
  @OUVnewRun @OUV_066 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Live from Pre-Live without adding primary location
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,7"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    Then User edits sub status to "OUV vehicles,C,7"
    And save the changes performed
    Then Verify the displayed status error message for Primary Location
    Then Logout from the OUV

  #Below is the scenario for User editing only primary location and updates Master status to Live from Pre-Live
  @OUVnewRun @OUV_067 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits only primary location and updates Master status to Live from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,8"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    And save the changes performed
    Then Verify the displayed status error message for Primary Location
    And User enters Primary Location
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Pre-Live from Live
  @OUVnewRun @OUV_068 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Pre-Live from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,8"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User edits the Master status to Handed In from Pre-Live
  @OUVnewRun @OUV_069 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Handed In from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,2"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,2"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User edits the Master status to Disposed with sub status Return to Stock or In FMS from Pre-Live
  @OUVnewRun @OUV_070 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Disposed with sub status Return to Stock or In FMS from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,3"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,3"
    Then User edits sub status to "OUV vehicles,C,3"
    And save the changes performed
    Then Logout from the OUV

  #User edits the Master status to Disposed with sub status other than Return to Stock or In FMS from Pre-Live
  @OUVnewRun @OUV_071 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Disposed with sub status other than Return to Stock or In FMS from Pre-Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,4"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,4"
    Then User edits sub status to "OUV vehicles,C,4"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
  @OUVnewRun @OUV_072 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Handed In from Live with any sub status and also selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then validate the status change to Handed In from Live
    Then Logout from the OUV

  #User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
  @OUVnewRun @OUV_073 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Handed In from Live with any sub status without selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,9"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    And save the changes performed
    Then Verify the displayed Keys transfer error message
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
  @OUVnewRun @OUV_074 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Handed In from Live without any sub status and only selecting Keys transfer option
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,9"
    Then User navigate to Keys transfer option and select it
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Live from Handed In
  @OUVnewRun @OUV_075 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Live from Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,5"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,5"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status Write-off and Justification input from Live
  @OUVnewRun @OUV_076 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Disposed with sub status Write-off and Justification input from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,6"
    Then User adds Justification
    And save the changes performed
    Then Logout from the OUV

  #Below is the scenario for User editing the Master status to Disposed with sub status other than Write-off from Live
  @OUVnewRun @OUV_077 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the Master status to Disposed with sub status other than Write-off from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,6"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,3"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Live
  @OUVnewRun @OUV_078 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the sub status to None when the Master status is in Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,10"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,10"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User edits the sub status to None when the Master status is in Handed In
  @OUVnewRun @OUV_079 @OUVBatch @batch051to100 @Mauli
  Scenario: User edits the sub status to None when the Master status is in Handed In
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,11"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,11"
    Then User edits sub status to None
    And save the changes performed
    Then Verify the displayed substatus error message
    Then Logout from the OUV

  #User creates New ATO Request
  @OUVnewRun @OUVATO @OUV_080 @batch051to100 @Mauli
  Scenario: User creates New ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then verify the ATO Request
    Then Logout from OUV Portal

  #User creates Multiple Vehicle ATO Requests
  @OUVnewRun @OUVATO @OUV_081 @SOUV-333 @batch051to100 @Mauli
  Scenario: User creates Multiple Vehicle ATO Requests
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields of ATO Request like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Click on Save&New button to save the ATO request
    Then fill all mandatory fields of multiple ATO Request like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Click on Save button to save the ATO request
    Then Logout from OUV Portal

  #User tries to enter Proposed Hand In Date less than Proposed Add to Fleet Date in ATO Request
  @OUVnewRun @OUVATO @OUV_082 @batch051to100 @Mauli
  Scenario: User tries to enter Proposed Hand In Date less than Proposed Add to Fleet Date in ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,3" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Verify the error message
    Then Logout from OUV Portal

  #User tries to Clone the ATO Request
  @OUVnewRun @OUVATO @OUV_083 @SOUV-335 @batch051to100 @Mauli
  Scenario: User tries to Clone the ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields of ATO Request like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Click on Save button to save the ATO request
    Then Click on Clone button to clone the ATO request
    Then Click on Save button to save the ATO request
    Then Logout from OUV Portal

  #User tries to edit the ATO Request Status
  @OUVnewRun @OUVATO @OUV_084 @batch051to100 @Mauli
  Scenario: User tries to edit the ATO Request Status
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Verify that user cannot edit the ATO request
    Then Logout from OUV Portal

  #User tries to create New ATO Request line item
  @OUVnewRun @OUVATO @OUV_085 @batch051to100 @Mauli
  Scenario: User tries to create New ATO Request line item
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Navigate to ATO Request Line Items
    And User creates a new ATO Request Line Item
    Then Logout from OUV Portal

  #User tries to create multiple ATO Request line items
  @OUVnewRun @OUVATO @OUV_086 @batch051to100 @Mauli
  Scenario: User tries to create multiple ATO Request line items
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Navigate to ATO Request Line Items
    And User creates multiple ATO Request Line Items
    Then Logout from OUV Portal

  #User tries to Edit the ATO Request line items
  @OUVnewRun @OUVATO @OUV_087 @batch051to100 @Mauli
  Scenario: User tries to Edit the ATO Request line items
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Navigate to ATO Request Line Items
    And User creates a new ATO Request Line Item
    And User edits the ATO Request Line Item
    Then Logout from OUV Portal

  #Pre-Requisite:Need to add create a new ATO Request scenario
  @OUVnewRun @OUVATO @OUV_088 @batch051to100 @Mauli
  Scenario: User tries to Delete the ATO Request line item
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then Navigate to ATO Request Line Items
    And User creates a new ATO Request Line Item
    And User deletes the ATO Request Line Item
    Then Logout from OUV Portal

  #User tries to Change the Owner of an ATO Request
  @OUVnewRun @OUVATO @OUV_089 @batch051to100 @Mauli
  Scenario: User tries to Change the Owner of an ATO Request
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    When user clicks on the change owner button
    And changes the owner as "ATO Requests,J,3"
    Then verify the displayed change owner error message
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #User tries to Re-open the status of an Approved ATO Request
  @OUVnewRun @OUVATO @OUV_090 @batch051to100 @Mauli
  Scenario: User tries to Re-open the status of an Approved ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    Then Verify that user cannot edit the ATO request
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #User tries to edit the Approver details for an ATO Request
  @OUVnewRun @OUVATO @OUV_091 @batch051to100 @Mauli
  Scenario: User tries to edit the Approver details for an ATO Request
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for the ATO Request with type "ATO Requests,A,2"
    And select the ATO Request
    Then Verify that user cannot edit Approver details for an ATO request
    Then Logout from OUV Portal

  #User Edits the fields under Fleet details section of an ATO Request
  @OUVnewRun @OUVATO @OUV_092 @batch051to100 @Mauli
  Scenario: User Edits the fields under Fleet details section of an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,3" Field "ATO Requests,T,7" and value "ATO Requests,O,2"
    And select the ATO Request
    Then Click on Edit Internal JLR Contact pencil icon under Fleet details section
    Then Change Internal jlr contact to "ATO Requests,M,2" and verify
    Then Remove added filter with title "ATO Requests,O,3"
    Then Logout from OUV Portal

  #User tries to Withdraw the ATO Request
  @OUVnewRun @OUVATO @OUV_093 @SOUV-345 @batch051to100 @Mauli
  Scenario: User tries to Withdraw the ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    Then Click on cancel/withdraw option from drop down list of an ATO Request
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #User send an Update on ATO Request Through Chatter Post
  @OUVnewRun @OUVATO @OUV_094 @SOUV-346 @batch051to100 @Mauli
  Scenario: User send an Update on ATO Request Through Chatter Post
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    Then Navigate to Chatter tab
    Then Enter text in Share an update text box Share an Update "ATO Requests,V,2"
    Then Select To section from drop down list Share Dropdown "ATO Requests,W,3"
    Then Click on Share button
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #This Scenario is creating a new Note for an existing ATO Request
  @OUVnewRun @OUVATO @OUV_095 @batch051to100 @Mauli
  Scenario: User tries to create New note for an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    And User Opens an existing ATO Request
    Then User creates a New Note for an Existing ATO Request
    Then Logout from OUV Portal

  #This Scenario is try to edit ATO Request Line Item for Pending Approval ATO Request
  @OUVnewRun @OUVATO @OUV_096 @batch051to100 @Mauli
  Scenario: User cannot Edit the ATO Request line items for an ATO Request with 'Pending' status
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    And Navigate to ATO Request Line Item
    Then Click on any created ATO Request Line Item
    And User tries to Edit ATO Request Line Item and Verify an error occurred
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #This Scenario is deleting Note from ATO Request
  @OUVnewRun @OUVATO @OUV_097 @batch051to100 @Mauli
  Scenario: User tries to delete the note for an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    And User Opens an existing ATO Request
    Then User creates a New Note for an Existing ATO Request
    And User delete the note from ATO Request
    Then Logout from OUV Portal

  #User tries to create an ATO Request without entering Vehicle details
  @OUVnewRun @OUVATO @OUV_098 @batch051to100 @Mauli
  Scenario: User tries to create an ATO Request without entering Vehicle details
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2"
    Then Verify that ATO request cannot be created and verify the error message
    Then Logout from OUV Portal

  #User tries to create a ATO Request without entering Fleet Details
  @OUVnewRun @OUVATO @OUV_099 @batch051to100 @Mauli
  Scenario: User tries to create a ATO Request without entering Fleet Details
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2"
    Then Verify that ATO request cannot be created and verify the error message
    Then Logout from OUV Portal

  #User is able to view ATO Request History for any open ATO Request
  @OUVnewRun @OUVATO @OUV_100 @batch051to100 @Mauli
  Scenario: User is able to view ATO Request History for any open ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Add filter with title "ATO Requests,O,4" Field "ATO Requests,T,7" and value "ATO Requests,I,3"
    And select the ATO Request
    Then Click on ATOHistory
    And Validate the ATO request History
    Then Remove added filter with title "ATO Requests,O,4"
    Then Logout from OUV Portal

  #Batch of 101 to 150 scripts
  #User can Upload any files to an open ATO Request from Notes & Attachments section
  @OUVnewRun @OUVATO @OUV_101 @SOUV-353 @batch101to150 @Mauli
  Scenario: User can Upload any files to an open ATO Request from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then Logout from OUV Portal

  #User is able to view files details of any uploaded files from Notes & Attachments section
  @OUVnewRun @OUVATO @OUV_102 @SOUV-354 @batch101to150 @Mauli
  Scenario: User is able to view files details of any uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks on View File details button from down arrow column dropdown "ATORequests,U,2"
    Then Logout from OUV Portal

  #User is able to edit files details of any uploaded files from Notes & Attachments section
  @OUVnewRun @OUVATO @OUV_103 @SOUV-355 @batch101to150 @Mauli
  Scenario: User is able to edit files details of any uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,3"
    Then User changes Title of the document Title "ATORequests,W,2"
    Then User can add Description of uploaded document Description "ATORequests,X,2"
    Then User clicks on Save button to save the changes of uploaded document
    Then Logout from OUV Portal

  #User can upload new version of already uploaded files from Notes & Attachments section
  @OUVnewRun @OUVATO @OUV_104 @SOUV-356 @batch101to150 @Mauli
  Scenario: User can upload new version of already uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,4"
    Then User chooses a different version of file from the local system
    Then Logout from OUV Portal

  #User is able to delete uploaded files from Notes & Attachments section
  @OUVnewRun @OUVATO @OUV_105 @SOUV-357 @batch101to150 @Mauli
  Scenario: User is able to delete uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,5"
    Then User can view one delete pop up, user clicks on delete button
    Then Logout from OUV Portal

  #User is able to download uploaded files from Notes & Attachments section
  @OUVnewRun @OUVATO @OUV_106 @SOUV-358 @batch101to150 @Mauli
  Scenario: User is able to download uploaded files from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,6"
    Then Logout from OUV Portal

  #User is able to share the uploaded files to anyone from Notes & Attachments section
  @OUVnewRun @OUVATO @OUV_107 @SOUV-359 @batch101to150 @Mauli
  Scenario: User is able to share the uploaded files to anyone from Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,7"
    Then User selects contact to whom file can share
    Then User can select share access for uploaded file Viewer "ATORequests,Y,2"
    Then Logout from OUV Portal

  #User can able to remove uploaded files from the record under Notes & Attachments section
  @OUVnewRun @OUVATO @OUV_108 @SOUV-360 @batch101to150 @Mauli
  Scenario: User can able to remove uploaded files from the record under Notes & Attachments section
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as Approved Search ATO Request "ATORequests,T,3"
    Then Navigate to Notes & Attachments section
    Then Upload any file from local directory into Notes & Attachments section
    Then User clicks uploaded file dropdown "ATORequests,U,8"
    Then User clicks on Remove from Record on pop up window

  #This Scenario is verifing User cannot cancel or Withdraw ATO Request
  @OUVnewRun @OUVATO @OUV_109 @SOUV-361 @batch101to150 @Mauli
  Scenario: User can cancel or withdraw created ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    And User Opens an existing ATO Request which is in Pending Approval status
    Then Verify that user cannot view Cancel/Withdraw ATO Request button
    And User Opens an existing ATO Request which is in New status
    Then Verify that user cannot view Cancel/Withdraw ATO Request button
    Then Logout from OUV Portal

  #User cannot submit an ATO request for approval which is assigned to any other owner
  @OUVnewRun @OUVATO @OUV_110 @SOUV-362 @batch101to150 @Mauli
  Scenario: User cannot submit an ATO request for approval which is assigned to any other owner
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,4"
    And select the ATO Request
    Then user submit an ATO request for approval
    Then Verify that user cannot submit an ATO request
    When navigate to the request fleet "ATO Requests,L,4"
    And verify that user is not a member of fleet "Users,C,2"
    Then Logout from OUV Portal

  #User changes the ATO request status from New to pending Approval
  @OUVnewRun @OUVATO @OUV_111 @SOUV-363 @batch101to150 @Mauli
  Scenario: User changes the ATO request status from New to pending Approval
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like OUVReportinglevelone "ATO Requests,B,2",OUVReportingleveltwo "ATO Requests,C,2",OUVReportinglevelthree "ATO Requests,D,2",Market "ATO Requests,E,2",Fleetdate "ATO Requests,F,2",Handindate "ATO Requests,G,2",DisposalRoute "ATO Requests,H,2"
    Then verify the ATO Request
    When user clicks on ATO request line
    Then clicks on new ATO request line with vehicle "ATO Requests,P,2"
    And Navigate back to ATO request
    When user submit an ATO request for approval
    And verify that ATO request is in pending approval status
    Then Logout from OUV Portal

  #User tries to edit the ATO request line item When ATO request Status is in Pending Approval
  @OUVnewRun @OUVATO @OUV_112 @SOUV-364 @batch101to150 @Mauli
  Scenario: User tries to edit the ATO request line item When ATO request Status is in Pending Approval
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like OUVReportinglevelone "ATO Requests,B,2",OUVReportingleveltwo "ATO Requests,C,2",OUVReportinglevelthree "ATO Requests,D,2",Market "ATO Requests,E,2",Fleetdate "ATO Requests,F,2",Handindate "ATO Requests,G,2",DisposalRoute "ATO Requests,H,2"
    Then verify the ATO Request
    When user clicks on ATO request line
    Then clicks on new ATO request line with vehicle "ATO Requests,P,2"
    And Navigate back to ATO request
    When user submit an ATO request for approval
    When user clicks on ATO request line
    Then user edits the the vehicle of a ATO request line as "ATO Requests,P,3"
    And verify the displayed ATO request line error message

  #In this Scenario user tries to recall Approval Request
  @OUVnewRun @OUVATO @OUV_113 @SOUV-365 @batch101to150 @Mauli
  Scenario: User is able to recall Approval request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    And User Fills all mandatory Fields like RepoLOne "ATORequests,B,2" RepoLTwo "ATORequests,C,2" RepoLThree "ATORequests,D,2" Market "ATORequests,E,2" disposalRoute "ATORequests,H,2"
    And User submits new ATO Request for an Approval
    Then User navigate to ATO Request History
    And User recall ATO Approval Request
    And Verify that ATO Request status changes to 'New'
    Then Logout from OUV Portal

  #This Scenario changes ATO Request Type
  @OUVnewRun @OUVATO @OUV_114 @SOUV-366 @batch101to150 @Mauli
  Scenario: User is able to change the ATO request type
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for the ATO Request with type "ATORequests,A,3"
    And User change the ATO Request Type to "ATORequests,A,5"
    Then Logout from OUV Portal

  #This Scenario tests that User is able to add Multiple ATO Request Line Items
  @OUVnewRun @OUVATO @OUV_115 @SOUV-367 @batch101to150 @Mauli
  Scenario: User is able to add Multiple ATO request line items when ATO request type is Multiple Vehicle-events
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    And User Fills all mandatory Fields like ATOReqType "ATORequests,A,5" RepoLOne "ATORequests,B,2" RepoLTwo "ATORequests,C,2" RepoLThree "ATORequests,D,2" Market "ATORequests,E,2" disposalRoute "ATORequests,H,2"
    And User creates Multiple ATO Request Line Items
    Then Logout from OUV Portal

  #User cannot edit the Request line items for Approved ATO request
  @OUVnewRun @OUVATO @OUV_116 @batch101to150 @Mauli
  Scenario: User cannot edit the Request line items for Approved ATO request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,2"
    And select the ATO Request
    Then Navigate to ATO Request Line Items
    And Click on dropdown and select edit option
    Then Verify that user unable to find vista vehicle field
    Then Logout from OUV Portal

  #User Cannot recall Approval request for Approved ATO request in Approval history
  @OUVnewRun @OUVATO @OUV_117 @batch101to150 @Mauli
  Scenario: User Cannot recall Approval request for Approved ATO request in Approval history
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,2"
    And select the ATO Request
    Then Navigate to Approval History
    Then verify that Recall button should not be there
    Then Logout from OUV Portal

  #User is able to Edit Vista Vehicle for ATO request line item in new ATO request
  @OUVnewRun @OUVATO @OUV_118 @SOUV-370 @batch101to150 @Mauli
  Scenario: User is able to Edit Vista Vehicle for ATO request line item in new ATO request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    And User Fills all mandatory Fields like ATOReqType "ATORequests,A,5" RepoLOne "ATORequests,B,2" RepoLTwo "ATORequests,C,2" RepoLThree "ATORequests,D,2" Market "ATORequests,E,2" disposalRoute "ATORequests,H,2"
    Then verify the ATO Request
    When user clicks on ATO request line
    Then clicks on new ATO request line with vehicle "ATO Requests,P,3"
    And Navigate back to ATO request
    When user clicks on ATO request line
    Then user edits the the vehicle of a ATO request line as "ATO Requests,P,2"
    And Verify that user is able to edit ATO request line as "ATO Requests,Q,2"
    Then Logout from OUV Portal

  #User is able to search for the ATO Request in the ATO Request Tab
  @OUVnewRun @OUVATO @OUV_119 @SOUV-371 @batch101to150 @Mauli
  Scenario: User is able to search for the ATO Request in the ATO Request Tab
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,2"
    And Verify that user is able to search for status "ATO Requests,I,2"
    Then Logout from OUV Portal

  #User is not able to delete the ATO request line items
  @OUVnewRun @OUVATO @OUV_120 @SOUV-372 @batch101to150 @Mauli
  Scenario: User is not able to delete the ATO request line items
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,5"
    And select the ATO Request
    When user clicks on ATO request line
    Then user delete an ATO request line items
    And verify the displayed delete error message
    Then Logout from OUV Portal

  #User is not able to Edit Vista Vehicle for ATO request line item in Approved ATO request
  @OUVnewRun @OUVATO @OUV_121 @batch101to150 @Mauli
  Scenario: User is not able to Edit Vista Vehicle for ATO request line item in Approved ATO request
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,2"
    And select the ATO Request
    Then Navigate to ATO Request Line Items
    And Click on ATO line item and click on Edit vista vehicle with "ATO Requests,P,2"
    Then Verify that user unable to edit vista vehicle field
    Then Logout from OUV Portal

  #User unable to find Recall button at Approval History for ATO request which is in Pending Approval stage
  @OUVnewRun @OUVATO @OUV_122 @batch101to150 @Mauli
  Scenario: User unable to find Recall button at Approval History for ATO request which is in Pending Approval stage
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,3"
    And select the ATO Request
    Then Navigate to Approval History
    Then verify that Recall button should not be there
    Then Logout from OUV Portal

  #User is not able to edit fleet details for ATO request
  @OUVnewRun @OUVATO @OUV_123 @SOUV-375 @batch101to150 @Mauli
  Scenario: User is not able to edit fleet details for ATO request
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,3"
    And select the ATO Request
    Then user edits the ATO request type
    And User clicks on the save button
    Then verify the displayed privileges error message
    Then Logout from OUV Portal

  #User unable to find delete option for ATO request
  @OUVnewRun @OUVATO @OUV_124 @SOUV-376 @batch101to150 @Mauli
  Scenario: User unable to find delete option for ATO request
    Given Access an OUV Portal with user "Users,D,5" and password as "Users,E,5"
    And User navigate to ATO Request
    Then verify if there is delete option for ATO request
    Then Logout from OUV Portal

  #User Unable to find edit option at the ATO request Status
  @OUVnewRun @OUVATO @OUV_125 @SOUV-377 @batch101to150 @Mauli
  Scenario: User Unable to find edit option at the ATO request Status
    Given Access an OUV Portal with user "Users,D,6" and password as "Users,E,6"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,7"
    And select the ATO Request
    Then verify that user is unable to find edit option at the ATO request Status
    Then Logout from OUV Portal

  #User cannot change an ATO Request type
  @OUVnewRun @OUVATO @OUV_126 @batch101to150 @Mauli
  Scenario: User cannot change ATO Request type
    Given Access an OUV Portal with user "Users,D,6" and password as "Users,E,6"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,A,3"
    And select the ATO Request
    Then User edits the ATO Request Type as "ATO Requests,A,5"
    Then Verify that an error message is displayed
    Then Logout from OUV Portal

  #User cannot delete the  ATO request line items in Approved ATO request
  @OUVnewRun @OUVATO @OUV_127 @batch101to150 @Mauli
  Scenario: User cannot delete the  ATO request line items in Approved ATO request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,6"
    And select the ATO Request
    Then Navigate to ATO Request Line Items
    And User deletes the ATO Request Line Item
    Then Verify user cannot delete the ATO Request line Items
    Then Logout from OUV Portal

  #User cannot delete the ATO request line items When ATO request is in pending approval stage
  @OUVnewRun @OUVATO @OUV_128 @batch101to150 @Mauli
  Scenario: User cannot delete the ATO request line items When ATO request is in pending approval stage
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for ATO Request with status "ATO Requests,I,8"
    And select the ATO Request
    Then Navigate to ATO Request Line Items
    And User deletes the ATO Request Line Item
    Then Verify user cannot delete the ATO Request line Items
    Then Logout from OUV Portal

  #User should be able to verify the owner name from OUV Booking ID window
  @OUVnewRun @OUVCal @OUV_129 @batch101to150 @Mauli
  Scenario: User should be able to verify the owner name from OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,46"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,46",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User tries to change the owner as "Users,C,3"
    Then Logout from OUV

  #User tries to change fleet for an OUV Vehicle with updating reporting levels to ones that do not match fleet with keys transferred selected
  @OUVnewRun @OUV_130 @OUVBatch @batch101to150 @Mauli
  Scenario: User tries to change fleet for an OUV Vehicle with updating reporting levels to ones that do not match fleet with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,2"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,H,3"
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then User clicks on save button
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle they are not a member of the fleet with updating reporting levels with keys transferred selected
  @OUVnewRun @OUV_131 @OUVBatch @batch101to150 @Shifa
  Scenario: User tries to change fleet for an OUV Vehicle they are not a member of the fleet with updating reporting levels with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,I,3"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,I,2"
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then User clicks on saved button
    Then Logout from the OUV

  #User cannot generate document for any new OUV Booking, if document template section is blank
  @OUVnewRun @OUVCal @OUV_132 @batch101to150 @Shifa
  Scenario: User cannot generate document for any new OUV Booking, if document template section is blank
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then User validates that document template section is blank
    And User clicks on generate document
    Then Validate that an error message is displayed
    Then Logout from OUV

  #User can cancel an OUV Booking from OUV Booking window
  @OUVnewRun @OUVCal @OUV_133 @batch101to150 @Shifa
  Scenario: User can cancel an OUV Booking from OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then User clicks on Cancel Booking
    And Validate that the Booking status is moved to cancelled
    Then Logout from OUV

  #User is unable to edit the details of OUV bookings when OUV booking status is in Booked Pending Approval status
  @OUVnewRun @OUVCal @OUV_134 @batch101to150 @Shifa
  Scenario: User is unable to edit the details of OUV bookings when OUV booking status is at Booked Pending Approval
    Given Access the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,3"
    And select the OUV Booking
    Then User clicks on Edit Option
    And User tries to change the Booking Request Type
    Then Validate that user is not able to save the record
    Then Logout from OUV

  #User is able to view the booking ID requested for approval listed under Items to Approve tab for an Approver
  @OUVnewRun @OUV_135 @batch101to150 @Shifa
  Scenario: User is able to view the booking ID requested for approval listed under Items to Approve tab for an Approver
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,47"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,47",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    And User Navigate to OUV Vehicle Loan Agreements
    And User enters Frequent Driver as "OUV Calendar,N,4" and Type of Driver as "OUV Calendar,Q,2" and saves VLA
    Then User submits the booking for Approval
    Then Logout from OUV
    Given Access the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then Validate that the Requested Approval will be listed on the Home Page under Items to Approve Tab
    Then Logout from OUV

  #User is not able to edit the name for an OUV Vehicle (Dummy) in the OUV Vehicle Tab
  @OUVnewRun @OUVCal @OUV_136 @batch101to150 @Shifa
  Scenario: User is not able to edit the name for an OUV Vehicle (Dummy) in the OUV Vehicle Tab
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,A,26"
    Then select and edit the vehicle	name
    And Enter the Vehicle Name as "OUV vehicles,T,2"
    Then Validate that user should not be able to save the changes to the record
    Then Logout from the OUV

  #In this scenario user tries to cancel booking which is in Pending Approval
  @OUVnewRun @OUVBooking @OUV_137 @batch101to150 @Shifa
  Scenario: User is not able to cancel the booking for a Booking Approval Status as 'Booked Pending Approval'
    Given User access OUV Portal with UserName "Users,D,3" Password "Users,E,3"
    And User Navigates to OUV Booking menu
    Then User search an OUV Booking with status "OUV Bookings,A,3"
    And User tries to cancel an OUV Booking which is pending for approval
    Then User do logout from OUV Portal

  #In this scenario user tries to cancel booking which is in Approved
  @OUVnewRun @OUVBooking @OUV_138 @batch101to150 @Shifa
  Scenario: User is able to cancel the booking for a Booking Approval Status as 'Approved'
    Given User access OUV Portal with UserName "Users,D,3" Password "Users,E,3"
    And User Navigates to OUV Booking menu
    Then User search an OUV Booking with status "OUV Bookings,A,4"
    And User tries to cancel an OUV Booking which is approved
    Then User do logout from OUV Portal

  #User should be able to see an error message if no VLA record is found when user submits the booking (Auto Generate VLA checkbox is selected) for approval
  @OUVnewRun @OUVCal @OUV_139 @batch101to150 @Shifa
  Scenario: User should be able to see an error message if no VLA record is found when user submits the booking (Auto Generate VLA checkbox is selected) for approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,48"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,48",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then User submits the booking for Approval
    And verify the error message for no VLA record
    Then Logout from OUV

  #User should be able to reassign an approval request
  @OUVnewRun @OUVCal @OUV_140 @batch101to150 @Shifa
  Scenario: User should be able to reassign an approval request
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
     And View all searched Vehicle and select an available vehicle "Calendarcell,B,49"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,49",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    Then User reassign the approval request to "Users,C,4"
    Then Logout from OUV

  #User is able to see VLA number auto-populated in OUV Booking ID Page, after creating a new Vehicle Loan Agreement
  @OUVnewRun @OUVCal @OUV_141 @batch101to150 @Shifa
  Scenario: User is able to see VLA number auto-populated in OUV Booking ID Page, after creating a new Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,50"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,50",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then Verify the VLA NUmber should be auto-populated in OUV booking ID page
    Then Logout from OUV

  #User is able to see VLA Status as 'Created' under Vehicle Loan Agreements for an Approved Booking
  @OUVnewRun @OUVCal @OUV_142 @batch101to150 @Shifa
  Scenario: User is able to see VLA Status as 'Created' under Vehicle Loan Agreements for an Approved Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,51"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,51",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Verify the VLA status should be updated as created
    Then Logout from OUV

  #User is not able to Submit a booking for Approval with a Booking Approval Status as 'Approved'
  @OUVnewRun @OUVCal @OUV_143 @batch101to150 @Shifa
  Scenario: User is not able to Submit a booking for Approval with a Booking Approval Status as 'Approved'
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,4"
    And select the OUV Booking
    Then User selects submit for approval button
    And Validate that user is not able to submit the booking for approval
    Then Logout from OUV

  #User is able to add an OUV Vehicle Loan Agreement Name under Loan Agreements by providing Drivers Information
  @OUVnewRun @OUVCal @OUV_144 @batch101to150 @Shifa
  Scenario: User is able to add an OUV Vehicle Loan Agreement Name under Loan Agreements by providing Drivers Information
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,52"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,52",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User saves the VLA
    Then Verify all the details are autopopulated in the VLA Details Page with Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    Then Logout from OUV

  #User is able to edit the details for an OUV Vehicle Loan Agreement
  @OUVnewRun @OUVCal @OUV_145 @batch101to150 @Shifa
  Scenario: User is able to edit the details for an OUV Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,53"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,53",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User Navigate to OUV Vehicle Loan Agreements
    And User edits the details for an OUV Vehicle Loan Agreement
    Then Logout from OUV

  #User is able to add Insurance details to an OUV Vehicle Loan Agreement
  @OUVnewRun @OUVCal @OUV_146 @batch101to150 @Shifa
  Scenario: User is able to add Insurance details to an OUV Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,54"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,54",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User enters all the Insurance details like Insurance Company "OUV Calendar,R,2" Insurance Policy Number "OUV Calendar,S,2" Vehicle Storage "OUV Calendar,T,2" and Driving Experience "OUV Calendar,U,2"
    And User saves the VLA
    Then Verify all the details are autopopulated in the VLA Details Page with Insurance Company "OUV Calendar,R,2" Insurance Policy Number "OUV Calendar,S,2" Vehicle Storage "OUV Calendar,T,2" and Driving Experience "OUV Calendar,U,2"
    Then Logout from OUV

  #User not able to change the owner of the Approved OUV Booking
  @OUVnewRun @OUVCal @OUV_147 @batch101to150 @Shifa
  Scenario: User not able to change the owner of the Approved OUV Booking
    Given Access the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,4"
    And select the OUV Booking
    Then User tries to change the owner as "Users,C,4"
    And Validate that user cannot change the owner for the booking
    Then Logout from OUV

  #User unable to cancel OUV booking which is in New stage
  @OUVnewRun @OUVCal @OUV_148 @batch101to150 @Shifa
  Scenario: User unable to cancel OUV booking which is in New stage
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then User clicks on Cancel Booking
    And Validate that user is not able to cancel an OUV Booking
    Then Logout from OUV

  #User tries to edit Reservation in Approved OUV booking
  @OUVnewRun @OUVCal @OUV_149 @batch101to150 @Shifa
  Scenario: User tries to edit Reservation in Approved OUV booking
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,4"
    And select the OUV Booking
    Then Navigate to Reservation
    And Select the Reservation
    And User tries to edit Reservation in Approved Booking
    Then Validate that an error message is displayed while editing Reservation
    Then Logout from OUV

  #User cannot delete Reservation for any OUV booking
  @OUVnewRun @OUVCal @OUV_150 @batch101to150 @Shifa
  Scenario: User cannot delete Reservation for any OUV booking
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then Navigate to Reservation
    And User tries to delete the Reservation
    Then Validate that an error message is displayed while deleting Reservation
    Then Logout from OUV
    
#Batch of 151 to 200 scripts

#User tries to add issue Keys to Reservation in Approved OUV booking
  @OUVnewRun @OUVCal @OUV_151 @batch151to200 @Shifa
  Scenario: User tries to add issue Keys to Reservation in Approved OUV booking
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,4"
    And select the OUV Booking
    Then select Reservation quick link
    And select the Reservation name
    Then click on the Issue keys and enter number of keys as "OUV Bookings,B,2"
    When click on the save button
    And verify the displayed Reservation error message
    Then Logout from OUV

  #User tries to mark  Reservation as complete in Approved OUV booking
  @OUVnewRun @OUVCal @OUV_152 @batch151to200 @Shifa
  Scenario: User tries to mark  Reservation as complete in Approved OUV booking
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,4"
    And select the OUV Booking
    Then select Reservation quick link
    And select the Reservation name
    Then click on the complete and enter number of keys as "OUV Bookings,B,2"
    When click on the save button
    And verify the displayed Reservation error message
    Then Logout from OUV

  #User tries to Add Temporary Accessories to reservations in 'Booked pending Approval' OUV booking
  @OUVnewRun @OUVCal @OUV_153 @batch151to200 @Shifa
  Scenario: User tries to Add Temporary Accessories to reservations in Booked pending Approval OUV booking
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,3"
    And select the OUV Booking
    Then select Reservation quick link
    And select the Reservation name
    Then select Temporary Accessories quick link
    And click on New and fill the mandatory details like accessory "OUV Bookings,C,2"
    When save the new Temporary Accessories
    And verify the displayed Temporary Accessories error message
    Then Logout from OUV
    
    #User is able to Add a new Booking with repair type
  @OUVnewRun @OUVCal @OUV_154 @batch151to200 @Shifa
  Scenario: User is able to Add a new Booking with repair type
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,55"
    Then Select Repair option as booking type from drop down Booking Type "OUV Calendar,W,2"
    Then Fill out all Mandatory fields in Booking Window Booking Quick Reference "Calendarcell,A,55" Internal JLR "OUV Calendar,X,2" Other Location "OUV Calendar,Y,2" Email Id "OUV Calendar,P,3" Driverone "OUV Calendar,Z,2" Drivertwo "OUV Calendar,AA,2"
    Then Click on Save, and user will be navigated back to Calendar
    Then Click on newly created OUV Booking record in the Reservation Calendar for the pop up to appear again
    Then User navigates to OUV Booking ID
    Then Logout from OUV
    
   #User tries to give Preparation/Transportation End date as greater than Reservation End date
  @OUVnewRun @OUVCal @OUV_155 @batch151to200 @Shifa
  Scenario: User tries to give Preparation/Transportation End date as greater than Reservation End date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,56"
    Then Click on Check box under includes Preparation/Transportation
    Then Select date which is greater than Reservation end date for OUV booking End Date
    Then Fill out all Mandatory fields in Booking Window Booking Quick Reference "Calendarcell,A,56" Internal JLR "OUV Calendar,X,2" Other Location "OUV Calendar,Y,2" Email Id "OUV Calendar,P,3" Driverone "OUV Calendar,Z,2" Drivertwo "OUV Calendar,AA,2"
    Then Click on save button
    Then Verifying error message
    Then Logout from OUV

#User is able to add permanent Accessories to reservation in New stage OUV booking
  @OUVnewRun @OUVCal @OUV_156 @batch151to200 @Shifa
  Scenario: User is able to add permanent Accessories to reservation in New stage OUV booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then User navigate to reservations and select a reservation
    Then User navigate to Permanent Accessories and click on New
    And Fill Mandatory fields like Accessory with "OUV Bookings,C,2" and click on save
    Then Verify the accessory created
    Then Logout from OUV

  #User is able to delete the permanent Accessories in Reservation which is In New stage OUV booking
  @OUVnewRun @OUVCal @OUV_157 @batch151to200 @Shifa
  Scenario: User is able to delete the permanent Accessories in Reservation which is In New stage OUV booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then User navigate to reservations and select a reservation
    Then User navigate to Permanent Accessories and click on New
    And Fill Mandatory fields like Accessory with "OUV Bookings,C,2" and click on save
    Then Click on drop down and select delete option
    Then Logout from OUV
    
    #User is able delete the OUV vehicle loan Agreement
  @OUVnewRun @OUVCal @OUV_158 @batch151to200 @Shifa
  Scenario: User is able delete the OUV vehicle loan Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,57"
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User Navigate to OUV Vehicle Loan Agreements
    And delete the OUV vehicle loan Agreement
    Then verify that user is able to delete vehicle loan Agreement
    Then Logout from OUV

  #User is able to add temporary Accessories to reservation in New stage OUV booking
  @OUVnewRun @OUVCal @OUV_159 @batch151to200 @Shifa
  Scenario: User is able to add temporary Accessories to reservation in New stage OUV booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,58"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,58",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then navigate to the reservation and select Temporary Accessories quick link
    And click on New button and fill the mandatory details like accessory "OUV Bookings,C,2"
    When save the new Temporary Accessories
    Then verify that user is able to add new Temporary Accessories
    Then Logout from OUV

  #User is able to Clone the  temporary Accessories to reservation in New stage OUV booking
  @OUVnewRun @OUVCal @OUV_160 @batch151to200 @Shifa
  Scenario: User is able to Clone the  temporary Accessories to reservation in New stage OUV booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,59"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,59",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then navigate to the reservation and select Temporary Accessories quick link
    And click on New button and fill the mandatory details like accessory "OUV Bookings,C,2"
    When save the new Temporary Accessories
    Then click on the Temporary Accessories and clone it
    Then navigate to the Temporary Accessories
    Then verify that user is able to add new Temporary Accessories
    Then Logout from OUV
    
     #User tries to Clone the Reservation for OUV booking which Is having Booking request type as Single Vehicle Booking
  @OUVnewRun @OUVCal @OUV_161 @batch151to200 @Shifa
  Scenario: User tries to Clone the Reservation for OUV booking which Is having Booking request type as Single Vehicle Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,60"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,60",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then navigate to the reservation and clone it
    And Verify the reservation clone error
    Then Logout from OUV
    
        #User is able to view the OUV Booking for Repair Type highlighted in yellow color in Reservation Calendar
  @OUVnewRun @OUVCal @OUV_162 @batch151to200 @Shifa
  Scenario: User is able to view the OUV Booking for Repair Type highlighted in yellow color in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,61"
    Then User saves a Repair Type OUV Booking including other location with Booking Quick Reference "Calendarcell,A,61",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,5",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then User submits the booking for Approval
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User approves the Booking
    Then Navigate back to the Booking ID
    Then User navigates to OUV Calendar and searches for Vehicle
    And Validate that an approved repaired OUV Booking is highlighted in yellow color
    Then Logout from OUV

  #User tries to add permanent Accessories to reservation in OUV booking which is in New stage
  @OUVnewRun @OUVCal @OUV_163 @batch151to200 @Shifa
  Scenario: User tries to add permanent Accessories to reservation in OUV booking which is in New stage
    Given Access the OUV Portal with user "Users,D,6" and password as "Users,E,6"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then Navigate to Reservation
    And Select the Reservation
    Then select Permanent Accessories quick link
    And click on New and fill the mandatory details like accessory "OUV Bookings,C,2"
    When save the new Permanent Accessories
    And verify the displayed Permanent Accessories error message
    Then Logout from OUV

  #User adds a OUV booking in OUV calendar with booking request type as 'Multiple Vehicle Booking'
  @OUVnewRun @OUVCal @OUV_164 @batch151to200 @Shifa
  Scenario: User adds a OUV booking in OUV calendar with booking request type as Multiple Vehicle Booking
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,62"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,62",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,5",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    And Validate the details in the Reservation Calender
    Then Logout from OUV
    
     #User is able to add Multiple reservations to OUV booking with request type as Multiple Vehicle Booking
  @OUVnewRun @OUVCal @OUV_165 @batch151to200 @Shifa
  Scenario: User is able to add Multiple reservations to OUV booking with request type as Multiple Vehicle Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,63"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,63",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,5",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then User navigates to OUV Calendar and searches for Vehicle
    And view the searched vehicles and select another vehicle which is available
    Then User saves the booking for Multiple reservations
    Then Logout from OUV
    
    #User tries to create a new reservation for same vehicle in between the reservation period
  @OUVnewRun @OUVCal @OUV_166 @batch151to200 @Shifa
  Scenario: User tries to create a new reservation for same vehicle in between the reservation period
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,64"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,64",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    And Select the time slot next to previous booking
    Then User saves an OUV Booking including other location with Booking start time Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,5",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Verify the error message displayed and click on close and cancel
    Then Logout from OUV
    
      #User tries to Checkout for OUV vehicle without having an Approved reservation
  @OUVnewRun @OUVCal @OUV_167 @batch151to200 @Shifa
  Scenario: User tries to Checkout for OUV vehicle without having an Approved reservation
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    Then Select a Vehicle which is having an OUV booking and click on View in Salesforce
    Then Logout from OUV
    
    #User need to verify the start date and time before performing check out
  @OUVnewRun @OUV_168 @OUVBatch @batch151to200 @Shifa
  Scenario: User need to verify the start date and time before performing check out
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,65"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,65",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Verify that start date and time is before end date and time
    Then Logout from OUV
    
    #User is able to do Checkout for the OUV vehicle
  @OUVnewRun @OUVBooking @OUV_169 @batch151to200 @Shifa
  Scenario: User is able to do Checkout for the OUV vehicle
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User searches for live vehicle "OUV Calendar,AI,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,66"
    Then User saves an OUV Booking including other location and time with Booking Quick Reference "Calendarcell,A,66",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,5",Journey Type "OUV Calendar,J,5"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Fleet Manager as "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    Then Search for an OUV Booking with Approved status Search List with "OUV Bookings,E,2"
    And select the OUV Booking
    Then Navigate to Reservation
    And click on vehicle
    When click on checkout button and select checkout option
    Then select the driver from the list and click on save
    Then Logout from OUV
    
    #User is able to do Check-in for the OUV vehicle
  @OUVnewRun @OUVCal @OUV_170 @batch151to200 @Shifa
  Scenario: User is able to do Check-in for the OUV vehicle
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,67"
    Then User save an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,67",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notification Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then Verify the check-out record in OUV Gatehouse Logs
    Then Click on Check-out/Check-in button
    Then Select Checkbox for Check-out and click on Continue button
    Then Check-in Window should be displayed and Select driver
    Then Click on Check-out/Check-in button
    Then Select Checkbox for Check-in and click on Continue button
    Then Check-in Window should be displayed and Select driver
    Then Verify the check-in record in OUV Gatehouse Logs
    Then Logout from OUV
    
    #User tries to do Checkout after Checkout of OUV vehicle
  @OUVnewRun @OUVBooking @OUV_171 @batch151to200 @Shifa
  Scenario: User tries to do Checkout after Checkout of OUV vehicle
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User searches for live vehicle "OUV Calendar,AI,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,68"
    Then User saves an OUV Booking including other location and time with Booking Quick Reference "Calendarcell,A,68",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,5",Journey Type "OUV Calendar,J,5"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Fleet Manager as "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    Then Search for an OUV Booking with Approved status Search List with "OUV Bookings,E,2"
    And select the OUV Booking
    Then Navigate to Reservation
    And click on vehicle
    When click on checkout button and select checkout option
    Then select the driver from the list and click on save
    Then click on checkout button and select checkout option
    And Validate the error message
    Then Logout from OUV
    
      #User is able to do Checkin & Complete after Checking Out the Vehicle
  @OUVnewRun @OUVCal @OUV_172 @batch151to200 @Shifa
  Scenario: User is able to do Checkin & Complete after Checking Out the Vehicle
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,69"
    Then User save an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,69",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notification Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then Verify the check-out record in OUV Gatehouse Logs
    Then Click on Check-out/Check-in button
    Then Select Checkbox for Check-out and click on Continue button
    Then Check-in Window should be displayed and Select driver
    Then Click on Drop down at top right corner
    Then Select Check-in & Complete
    Then Fill out Mandatory fields Driver "OUV Calendar,AG,2" mileage "OUV Calendar,AE,2" keys checked "OUV Calendar,AC,2" Return Location "OUV Calendar,AD,2" Vehicle Condition "OUV Calendar,AF,2"
    Then Verify the check-in & complete record in OUV Gatehouse Logs
    Then Logout from OUV
    
    
    #User tries to fill Mileage at close booking as lesser than Vehicle Mileage in Vehicle details page
  @OUVnewRun @OUV_173 @OUVBatch @SOUV-425 @batch151to200 @Shifa
  Scenario: User tries to fill Mileage at close booking as lesser than Vehicle Mileage in Vehicle details page
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,B,10"
    And select the vehicle
    Then click on drop down and select CheckIn and complete
    Then fill fields like driver logistics "OUV vehicles,P,2", milage at close booking with "OUV vehicles,W,2", return location "OUV vehicles,W,2" and vehicle condition "OUV vehicles,D,2"
    And Click on save
    Then Update sales channel, rate and date Sales channel "OUV vehicles,Q,2"  Sales Rate "OUV vehicles,S,2" Sales date "OUV vehicles,R,3"
    Then Click on Save
    Then Logout from the OUV
    
     #User tries to do Checkout/Check-in to OUV vehicle which is pre-live stage
  @OUVnewRun @OUV_174 @OUVBatch @SOUV-426 @batch151to200 @Shifa
  Scenario: User tries to do Checkout/Check-in to OUV vehicle which is pre-live stage
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,B,8"
    And select the vehicle
    When User click on CheckOut CheckIn button
    Then checkbox checkout and click on continue
    And Verify the error message displayed
    Then Logout from the OUV
    
      #User can perform Checkout/Check-in when OUV vehicle in Handed in stage which is having OUV booking as 'Repair' type
  @OUVnewRun @OUV_175 @OUVBatch  @batch151to200 @Shifa
  Scenario: User can perform Checkout/Check-in when OUV vehicle in Handed in stage which is having OUV booking as 'Repair' type
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User searches for live vehicle "OUV Calendar,AC,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,70"
    Then User saves an OUV Booking including other location and Booking Quick Reference "Calendarcell,A,70",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User click on CheckOut CheckIn
    Then checkbox the checkout and click on continue
    And User selects driver or logistics and save it
    When Navigate to OUV Gatehouse Logs tab
    Then Logout from OUV
    
    #User tries to do Checkout/checkin to OUV vehicle with closed reservation
  @OUVnewRun @OUVBooking @OUV_176  @batch151to200 @Shifa
  Scenario: User tries to do Checkout/checkin to OUV vehicle with closed reservation
    Given User access OUV Portal with UserName "Users,D,2" Password "Users,E,2"
	And User Navigates to OUV Booking menu
   Then User search an OUV Booking with status "OUV Bookings,A,7"
    And select the OUV Booking
    Then Navigate to Reservation
    Then click on vehicle
    When click on checkout button and select checkout option
    And Validate the error message
    Then Logout from OUV
    
    #User is able to search for the Booking ID in the OUV Booking Tab
  @OUVnewRun @OUVCal @OUV_177 @batch151to200 @Shifa
  Scenario: User is able to search for the Booking ID in the OUV Booking Tab
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User Navigates to OUV Booking menu
    Then Search for an OUV Booking with Approved status Search List "OUV Bookings,E,2"
    Then Logout from OUV

  #User is able to create a new Booking on OUV Booking  window
  @OUVnewRun @OUVCal @OUV_178 @batch151to200 @Shifa
  Scenario: User is able to create a new Booking on OUV Booking  window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User Navigates to OUV Booking menu
    Then Click on New
    Then Select the OUV Booking Editable radiobutton and click Next
    Then Enter all the mandatory details and click on Save Booking Quick Reference "OUV Bookings,F,2" Reason For Booking "OUV Bookings,G,2" Booking Type "OUV Bookings,H,2" Journey Type "OUV Bookings,I,2" Booking request type "OUV Bookings,J,2" Booking Justification "OUV Bookings,K,2" Passout Type "OUV Bookings,L,2" Start Date "OUV Bookings,Q,2" End Date "OUV Bookings,R,2" Other Location "OUV Bookings,N,2" Internal JLR "OUV Bookings,M,2" Email Id "OUV Bookings,O,2" Driverone "OUV Bookings,P,2"
    Then Logout from OUV

  #User is able to change the owner name for the Booking Id on OUV Booking Search Window
  @OUVnewRun @OUVCal @OUV_179 @batch151to200 @Shifa
  Scenario: User is able to change the owner name for the Booking Id on OUV Booking Search Window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User Navigates to OUV Booking menu
    Then Search for an OUV Booking with Approved status Search List "OUV Bookings,E,2"
    Then Select any record and click on the Change Owner option on OUV Booking Search Window
    Then Search for the owner name who is member of the fleet "Users,E,2" and click on submit button
    Then Logout from OUV

  #User is not able to change the owner name for the Booking Id on OUV Booking Search Window
  @OUVnewRun @OUVCal @OUV_180 @batch151to200 @Shifa
  Scenario: User is not able to change the owner name for the Booking Id on OUV Booking Search Window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Bookings
    And User searches for the OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then Click on Change owner icon and select a new owner "OUV Bookings,D,2"
    And Validate that user cannot change the owner for the booking
    Then Logout from OUV
    
     #User cannot edit the New Requested Hand In Date for a different fleet on OUV Vehicle window
  @OUVnewRun @OUVCal @OUV_181 @batch151to200 @Shifa
  Scenario: User cannot edit the New Requested Hand In Date for a different fleet on OUV Vehicle window
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV Calendar,M,4"
    And select the vehicle
    Then User edits the New Requested Hand In Date on OUV Vehicle window
    Then Verify the error message for date
    Then Logout from OUV
    
     #User tries to edit the New Requested Hand In Date on OUV Vehicle window
  @OUVnewRun @OUV_182 @OUVBatch @batch151to200 @Shifa
  Scenario: User tries to edit the New Requested Hand In Date on OUV Vehicle window
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,4"
    Then User selects the vehicle to open the vehicle related pages
    Then Validate that New Requested Hand in Approval status should be approved / New
    Then User tries to edit the New Requested Hand In Date on OUV Vehicle window HandinDate
    Then Enter any free text in justification on OUV Vehicle window and click on Save Justification "OUV vehicles,P,2"
    Then Validate that New Requested Hand in Approval status is changed to Pending Approval status
    Then Logout from the OUV

  #User tries to edit New Requested Hand In date when New Requested Hand In Approval status is Pending Approval
  @OUVnewRun @OUV_183 @OUVBatch @batch151to200 @Shifa
  Scenario: User tries to edit New Requested Hand In date when New Requested Hand In Approval status is Pending Approval
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,4"
    Then User selects the vehicle to open the vehicle related pagess
    Then Validate that New Requested Hand in Approval status should be pending approval / New
    Then User tries to edit the New Requested Hand In Date on OUV Vehicle window HandinDate
    Then Verify that User cannot edit New Requested Hand In date if the New Requested Hand In Apporval status is Pending Approval
    Then Logout from the OUV

  #User can update Master status for an OUV Vehicle which is in Handed in status
  @OUVnewRun @OUV_184 @OUVBatch @batch151to200 @Shifa
  Scenario: User can update Master status for an OUV Vehicle which is in Handed in status
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle related pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,2"
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Click on Save
    Then Logout from the OUV
    
    #User cannot update Master status for an OUV vehicle which is in Handed in status if justification field is blank
  @OUVnewRun @OUVVehicle @OUV_185  @batch151to200 @Shifa
  Scenario: User cannot update Master status for an OUV vehicle which is in Handed in status if justification field is blank
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,5"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,3"
    Then User edits sub status to "OUV vehicles,C,6"
    Then Provide no details on Justification window
    And save the changes performed
    And Verify the displayed error message
    Then Logout from the OUV

  #User can change master status if the selected sub status is sold and fields under Remarketing section are provided with details
  @OUVnewRun @OUVVehicle @OUV_186  @batch151to200 @Shifa
  Scenario: User can change master status if the selected sub status is sold and fields under Remarketing section are provided with details
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,5"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,3"
    Then User edits sub status to "OUV vehicles,C,10"
    Then User adds Justification
    Then Enters details for Remarketing Section with vehicle condition "OUV vehicles,D,2", CustomerIdentifier "OUV vehicles,E,2",SalesChannel "OUV vehicles,F,2",FinalPrice "OUV vehicles,G,2"
    And save the changes performed
    Then Logout from the OUV

  #User cannot change master status if the selected sub status is sold, if fields are blank under Remarketing section
  @OUVnewRun @OUVVehicle @OUV_187 @batch151to200 @Shifa
  Scenario: User cannot change master status if the selected sub status is sold, if fields are blank under Remarketing section
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,5"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,3"
    Then User edits sub status to "OUV vehicles,C,10"
    And save the changes performed
    And Verify the displayed error message
    Then Logout from the OUV
    
    #User can update Master status for an OUV vehicle which is in Handed in status with past date
  @OUVnewRun @OUV_188 @OUVBatch @batch151to200 @Shifa
  Scenario: User can update Master status for an OUV vehicle which is in Handed in status with past date
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle related pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,3"
    Then Update New Extended Hand In date in past HandIndate
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Update sales channel, rate and date Sales channel "OUV vehicles,Q,2"  Sales Rate "OUV vehicles,S,2" Sales date "OUV vehicles,R,3"
    Then Click on Save
    Then Logout from the OUV

  #User cannot update Master status for an OUV vehicle which is in Handed in status with future date
  @OUVnewRun @OUV_189 @OUVBatch @batch151to200 @Shifa
  Scenario: User cannot update Master status for an OUV vehicle which is in Handed in status with future date
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle related pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,3"
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Update New Extended Hand In date in future HandIndate
    Then Click on Save
    Then Verify the error message for date
    Then Logout from the OUV
    
     #User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
  @OUVnewRun @OUV_190 @OUVBatch @batch151to200 @Shifa
  Scenario: User can update Master status for an OUV vehicle which is in Handed in status where extended hand in status should be approved
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,5"
    Then User selects the vehicle to open the vehicle pagesss
    Then Change Master status to Dispose and Sub Status to Write-Off Master Status "OUV vehicles,N,2" Subststus "OUV vehicles,O,3"
    Then Verify that New Requested Handed In Approval Status should be approved before saving the record
    Then Provide details on Justification window Justification "OUV vehicles,P,2"
    Then Click on Save
    Then Logout from the OUV
    
      #User cannot update Master status for an OUV vehicle which is in Handed in status where extended hand in status is not approved
  @OUVnewRun @OUV_191 @OUVVehicle @batch151to200 @Shifa
  Scenario: User cannot update Master status for an OUV vehicle which is in Handed in status where extended hand in status is not approved
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,5"
    And select the vehicle
    Then User validates that extended Handed In status is not approved
    Then User edits master status to "OUV vehicles,B,6"
    Then User edits sub status to "OUV vehicles,C,10"
    And save the changes performed
    Then Verify the displayed error message	for Handed In status
    Then Logout from the OUV

  #User cannot update Master status for an OUV vehicle which is in Disposed Vehicles status
  @OUVnewRun @OUV_192 @OUVVehicle @batch151to200 @Shifa
  Scenario: User cannot update Master status for an OUV vehicle which is in Disposed Vehicles status
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,12"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,9"
    Then User edits sub status to "OUV vehicles,C,12"
    And save the changes performed
    Then Verify the displayed error message	for Handed In status
    Then Logout from the OUV
    
    #User cannot add Multiple Line Items for ATO Request Type as Single Vehicle-New
  @OUVnewRun @OUVATO @OUV_193 @batch151to200 @Shifa
  Scenario: User cannot add Multiple Line Items for ATO Request Type as Single Vehicle-New
    Given Access an OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to ATO Request
    Then search for the ATO Request with status "ATORequests,A,3"
    And select the ATO Request
    Then User clicks on Create Line Items
    And User enters number of line items and click on Save
    Then Validate the error message displayed for creating line items
    Then Logout from OUV Portal

#User can add Multiple Line Items for ATO Request Type as Multiple Vehicle and ATO Request Status as New
  @OUVnewRun @OUVATO @OUV_194 @batch151to200 @Shifa
  Scenario: User User can add Multiple Line Items for ATO Request Type as Multiple Vehicle and ATO Request Status as New
    Given Access an OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as "ATORequests,T,4"
    Then At the top of right hand side in ATO Request window, select the dropdown for create line items "ATORequests,AA,3"
    Then Enter the Value  and click on save "ATORequests,AB,2"
    Then Logout from OUV Portal

  #User cannot add Multiple Line Items with ATO Request Type as Single Vehicle and ATO Request Status as Approved
  @OUVnewRun @OUVATO @OUV_195 @batch151to200 @Shifa
  Scenario: User User cannot add Multiple Line Items with ATO Request Type as Single Vehicle and ATO Request Status as Approved
    Given Access an OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as "ATORequests,T,5"
    Then User is not able to view select the dropdown for create line items
    Then Logout from OUV Portal

  #User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle and ATO Request Status as Pending Approval
  @OUVnewRun @OUVATO @OUV_196 @batch151to200 @Shefali
  Scenario: User User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle and ATO Request Status as Pending Approval
    Given Access an OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as "ATORequests,T,6"
    Then At the top of right hand side in ATO Request window, select the dropdown for create line items "ATORequests,AA,3"
    Then Enter the Value  and click on save "ATORequests,AB,2"
    Then Verify the Error Message
    Then Logout from OUV Portal

  #User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle and ATO Request Status as New
  @OUVnewRun @OUVATO @OUV_197 @batch151to200 @Shefali
  Scenario: User User cannot add Multiple Line Items with ATO Request Type as Multiple Vehicle and ATO Request Status as New
    Given Access an OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to ATO Request
    Then Click on any ATO Request having ATO Request status as "ATORequests,T,4"
    Then At the top of right hand side in ATO Request window, select the dropdown for create line items "ATORequests,AA,3"
    Then Enter the Value  and click on save "ATORequests,AB,2"
    Then Verifying the Error Message
    Then Logout from OUV Portal
    
     #User verifies that the newly created OUV Booking status changed to Pending Approval if Booking Justification is selected as Event/Sales.
  @OUVnewRun @OUV_198 @OUVBatch @batch151to200 @Shefali
  Scenario: User verifies that the newly created OUV Booking status changed to Pending Approval if Booking Justification is selected as Event/Sales.
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,71"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,71",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    Then Verify that Approval status is set to Booking Pending Approval
    Then Logout from OUV

  #User is not able to edit the fields for a newly created OUV Booking if Booking Justification is selected as Event/Sales activity and Request Type as Single Vehicle Booking
  @OUVnewRun @OUV_199 @OUVBatch @batch151to200 @Shefali
  Scenario: User is not able to edit the fields for a newly created OUV Booking if Booking Justification is selected as Event/Sales activity and Request Type as Single Vehicle Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,72"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,72",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    Then Verify that Approval status is set to Booking Pending Approval
    When User edit the Booking Quick reference "OUV Calendar,F,2" and save it
    Then Verify the error message displayed to edit
    Then Logout from OUV
    
      #User is able to edit the fields for a newly created OUV Booking for Event/Sales activity and Request Type as Multiple Vehicle Booking
  @OUVnewRun @OUVCal @OUV_200  @batch151to200 @Shefali
  Scenario: User is able to edit the fields for a newly created OUV Booking for Event/Sales activity and Request Type as Multiple Vehicle Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,73"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,73",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,5",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then View that Booking Approval Status is set to New
    Then Edit the Fields under Booking Request section on OUV Booking ID window and click on save Quickreff "OUV Calendar,F,8" bookin reason "OUV Calendar,AB,3" booking req type "OUV Calendar,H,2" journey type "OUV Calendar,J,2"
    Then Logout from OUV
    
#Batch of 201 to 250 scripts
    
 #User edits the Master status to Pre-Live from Live
  @OUVnewRun @OUV_201 @OUVVehicle @batch201to250 @Shefali 
  Scenario: User edits the Master status to Pre-Live from Live
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,15"
    And select the vehicle
    Then User edits master status to "OUV vehicles,B,15"
    Then User edits sub status to "OUV vehicles,C,15"
    And save the changes performed
    Then Verify the displayed status error message
    Then Logout from the OUV

  #User is able to edit the Vehicle Name for an OUV Vehicle
  @OUVnewRun @OUV_202 @OUVVehicle @batch201to250 @Shefali
  Scenario: User is able to edit the Vehicle Name for an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,13"
    Then select and edit the vehicle	name
    And Enter the Vehicle Name as "OUV vehicles,T,2"
    Then Validate that user should be able to save the changes to the record
    Then Logout from the OUV

  #User is able to edit the Vehicle Name for an OUV Vehicle
  @OUVnewRun @OUV_203 @OUVVehicle @batch201to250 @Shefali
  Scenario: User is not able to edit the Vehicle Name for an OUV Vehicle of a different fleet
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,14"
    Then select and edit the vehicle	name
    And Enter the Vehicle Name as "OUV vehicles,T,3"
    Then Validate that user should not be able to save the changes to the record
    Then Logout from the OUV
    
 #User tries to change fleet for an OUV Vehicle without updating reporting levels and without keys transferred selected
  @OUVnewRun @OUV_204 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to change fleet for an OUV Vehicle without updating reporting levels and without keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,2"
    Then User select the vehicle to open the vehicle related page
    Then Select any other fleet from the Fleet "OUV vehicles,H,3"
    Then User click on save button
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle without updating reporting levels with keys transferred selected
  @OUVnewRun @OUV_205 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to change fleet for an OUV Vehicle without updating reporting levels with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,2"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,H,3"
    Then User clicks on save button
    Then Logout from the OUV

  #User tries to change fleet for an OUV Vehicle with updating reporting levels with keys transferred selected
  @OUVnewRun @OUV_206 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to change fleet for an OUV Vehicle with updating reporting levels with keys transferred selected
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,3"
    Then User selects the vehicle to open the vehicle related page
    Then Validate that the fields Transfer - No of Keys Transferred & Transfer - Keys transferred are checked
    Then Select any other fleet from the search Fleet "OUV vehicles,H,2"
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then clicks on save button
    Then Logout from the OUV

  #User tries to edit the OUV Reporting Level 1,2,3 in OUV Vehicle Details Page
  @OUVnewRun @OUV_207 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to edit the OUV Reporting Level 1,2,3 in OUV Vehicle Details Page
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then User searches for an OUV Vehicle on their fleet Search "OUV vehicles,H,3"
    Then User select the vehicle to open the vehicle related page
    Then User edits the OUV Reporting Levelone
    Then Update OUV reporting Levels to categories that do not match the new fleet LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    Then User click the save button
    Then Logout from the OUV
    
     #User tries to update the OUV Reporting Category for an OUV Fleet
  @OUVnewRun @OUV_208 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to update the OUV Reporting Category for an OUV Fleet
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,17"
    And select the vehicle
    Then Update OUV reporting Levels to LevelOne "OUV vehicles,J,2" LevelTwo "OUV vehicles,K,2" LevelThree "OUV vehicles,L,2"
    And save the changes performed
    And Verify the displayed error message
    Then Click on Fleet field
    Then Validate that user cannot edit the OUV Reporting Category field
    Then Logout from the OUV

  #User tries to select the Pre-Sold Admin option in OUV Vehicle Details Page
  @OUVnewRun @OUV_209 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to select the Pre-Sold Admin option in OUV Vehicle Details Page
    Given Access to the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,A,17"
    And select the vehicle
    Then Click on pre-sold admin button and validate the error message
    Then click on cancel
    Then Logout from the OUV
    
     #User tries to enter New Requested Hand In Date for OUV Reporting Level as 'Employee Cars'
  @OUVnewRun @OUV_210 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to enter New Requested Hand In Date for OUV Reporting Level as 'Employee Cars'
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,B,13"
    And select the vehicle
    And Validate that new requested hand in approval status as "OUV vehicles,U,2"
    Then enter the new requested hand in date and enter free text in justification as "OUV vehicles,P,2"
    Then Click on save and validate that new requested hand in status as approved
    Then Logout from the OUV

  #User tries to enter New Requested Hand In Date for OUV Reporting Level as 'Buybacks'
  @OUVnewRun @OUV_211 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to enter New Requested Hand In Date for OUV Reporting Level as 'Buybacks'
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle in the list with status "OUV vehicles,B,12"
    And select the vehicle
    And Validate that new requested hand in approval status as "OUV vehicles,U,2"
    Then enter the new requested hand in date and enter free text in justification as "OUV vehicles,P,2"
    Then Click on save and validate that new requested hand in status as approved
    Then Logout from the OUV
   
    @OUVnewRun @OUV_212 @OUVVehicle @batch201to250 @Shefali
  Scenario: User verifies that the Latest Planned Hand In Date and Actual Hand In Date gets updated after entering  New Requested Hand In Date for OUV Reporting Level as 'Buybacks'
    Given Access to the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,16"
    And select the vehicle
    Then User enters New Requested Hand In Date and verifies that the Latest Planned Hand In Date and Actual Hand In Date gets updated
    Then Logout from the OUV
    
#User submits and approves an ATO Request
  @OUVnewRun @OUVATO @OUV_213 @batch201to250 @Shefali
  Scenario: User submits and approves an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    And User Fills all mandatory Fields like RepoLOne "ATORequests,B,2" RepoLTwo "ATORequests,C,2" RepoLThree "ATORequests,D,2" Market "ATORequests,E,2" disposalRoute "ATORequests,H,2"
    Then user clicks on ATO request line
    And clicks on new ATO request line with vehicle "ATORequests,P,2"
    And Navigate back to ATO Request
    And user submit an ATO request for approval
    And Validate the Over Budget Status as Yes/No
    Then Navigate to Approval History and validate the Approver it is pending to as "Users,C,3"
    Then Logout from OUV Portal
    And Login to OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then Approve the ATO Request
    And Navigate back to ATO Request
    Then Navigate to Approval History and validate the Approver it is pending to as "Users,C,4"
    Then Logout from OUV Portal
    And Login to OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then Approve the ATO Request
    And Navigate back to ATO Request
    Then Navigate to Approval History and validate the Approver it is pending to as "Users,C,5"
    Then Logout from OUV Portal
    And Login to OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then Approve the ATO Request
    And Navigate back to ATO Request
    Then Navigate to Approval History and validate the Approver it is pending to as "Users,C,6"
    Then Logout from OUV Portal
    And Login to OUV Portal with user "Users,D,6" and password as "Users,E,6"
    Then Approve the ATO Request
    Then Navigate to Approval History and validate the status as approved,if not login as user "Users,D,6" and password as "Users,E,6" ,and user "Users,D,6" and password as "Users,E,6"
    Then Logout from OUV Portal
    
    #User is able to view the Approval History for an ATO Request
  @OUVnewRun @OUVATO @OUV_214 @batch201to250 @Shefali
  Scenario: User is able to view the Approval History for an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for the ATO Request with status "ATORequests,I,2"
    And select the ATO Request
    Then Navigate to Approval History
    And Validate that user is able to view the Approval History for an ATO Request
    Then Logout from OUV Portal

  #User tries to update the Proposed Hand In Date less than Proposed Add to Fleet Date for an ATO Request
  @OUVnewRun @OUVATO @OUV_215 @batch201to250 @Shefali
  Scenario: User tries to update the Proposed Hand In Date less than Proposed Add to Fleet Date for an ATO Request
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then search for the ATO Request with status "ATORequests,A,2"
    And select the ATO Request
    Then User tries to update the Proposed Hand In Date less than Proposed Add to Fleet Date
    Then Validate the error message displayed for Proposed Hand In Date
    Then Logout from OUV Portal
    
 #User tries to associate another OUV booking in an existing OUV Booking
  @OUVnewRun @OUVCal @OUV_216 @batch201to250 @Shefali
  Scenario: User tries to associate another OUV booking in an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then click on edit associate booking
    When booking information is entered click save
    And verify the error message for associate booking
    Then Logout from OUV
        
 #User tries to add a Replacement Vehicle which is in Pre-Live status
  @OUVnewRun @OUVATO @OUV_217 @batch201to250 @Shefali
  Scenario: User tries to add a Replacement Vehicle which is in Pre-Live status
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then verify the ATO Request
    Then click on edit Replacement Vehicle
    When Replacement Vehicle information VIN "ATO Requests,Q,4" and Vehicle Name "ATO Requests,AC,4" is entered click save
    And verify the error message for Replacement Vehicle
    Then Logout from OUV Portal
    
    #User tries to add a Replacement Vehicle which is in 'Live' status
  @OUVnewRun @OUVATO @OUV_218 @batch201to250 @Shefali
  Scenario: User tries to add a Replacement Vehicle which is in 'Live' status
    Given Access an OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to ATO Request
    Then Click on New button
    Then fill all mandatory fields like QuickReference "ATO Requests,K,2" ATORequestType "ATO Requests,A,2" OUVReportinglevelone "ATO Requests,B,2" OUVReportingleveltwo "ATO Requests,C,2" OUVReportinglevelthree "ATO Requests,D,2" Fleet "ATO Requests,L,2" Market "ATO Requests,E,2" InternalJLRContact "ATO Requests,M,2" Fleetdate "ATO Requests,F,2" Handindate "ATO Requests,G,2" DisposalRoute "ATO Requests,H,2" ReworkRoute "ATO Requests,N,2"
    Then verify the ATO Request
    Then click on edit Replacement Vehicle
    When Replacement Vehicle information VIN "ATO Requests,Q,3" and Vehicle Name "ATO Requests,AC,3" is entered click save
    Then remove the added vehicle
    Then Logout from OUV Portal
    
    #User tries to collect all the driver information for an OUV Booking
  @OUVnewRun @OUVCal @OUV_219 @batch201to250 @Shefali
  Scenario: User tries to collect all the driver information for an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,74"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,74",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    And User clicks on VLA and verify details are autopopulated like Driver "OUV Bookings,S,2" First Name "OUV Bookings,T,2" Last Name "OUV Bookings,U,2" Phone "OUV Bookings,V,2"
    Then Logout from OUV
    
     #User cannot add more than one Vehicle Loan Agreements in an OUV Booking
  @OUVnewRun @OUVCal @OUV_220 @batch201to250 @Shefali
  Scenario: User cannot add more than one Vehicle Loan Agreements in an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,75"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,75",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User clicks on Save and New VLA
    And User adds another VLA by entering all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,3" Last Name "OUV Calendar,O,3" and Email "OUV Calendar,P,4"
    And User saves the VLA
    Then Validate the error message displayed for creating a New VLA
    Then Logout from OUV
    
     #User is not be able to do CheckOut after doing CheckIn and Complete
  @OUVnewRun @OUV_221 @OUVBatch @batch201to250 @Shefali
  Scenario: User is not be able to do CheckOut after doing CheckIn & Complete
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,6"
    Then Open any live OUV Vehicle for which CheckIn & complete is completed
    Then Verify that the reservation is completed for which CheckIn & complete is completed
    Then Verify that the OUV Booking status is closed which is associated with reservation
    Then User navigate to OUV vehicles
    Then Open any live OUV Vehicle for which CheckIn & complete is completed
    Then Click CheckOut/CheckIn button
    Then Select CheckOut from the checkbox
    Then Click Continue button
    Then Verify that an error occurred when user tries to do CheckOut for Closed OUV Booking
    Then Logout from the OUV
    
    #User is able to view utilisation logs after CheckOut & CheckIn
  @OUVnewRun @OUVCal @OUV_222 @batch201to250 @Shefali
  Scenario: User is able to do Check-in for the OUV vehicle
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,76"
    Then User save an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,76",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notification Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then Verify the check-out record in OUV Gatehouse Logs
    Then Click on Check-out/Check-in button
    Then Select Checkbox for Check-out and click on Continue button
    Then Check-in Window should be displayed and Select driver
    Then Verify that CheckOut entry is presented in the OUV Gatehouse Logs
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then Verify the check-out record in OUV Gatehouse Logs
    Then Click on Check-out/Check-in button
    Then Select Checkbox for Check-in and click on Continue button
    Then Check-in Window should be displayed and Select driver
    Then Navigate to OUV Vehicle Utilisation
    Then Verify that User can be able to view utilisation logs after doing CheckOut & CheckIn
    Then Logout from OUV
    
     #User tries to select both CheckIn and CheckOut checkbox for an OUV Vehicle
  @OUVnewRun @OUV_223 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to select both CheckIn and CheckOut checkbox for an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,17"
    And select the vehicle
    When User click on CheckOut CheckIn button
    Then checkbox checkout and checkin at a time to proceed
    And Verify the error message displayed to select one option
    Then Logout from the OUV
    
    #User is able to complete the reservation after doing CheckIn & Close
  @OUVnewRun @OUV_224 @OUVBatch @batch201to250 @Shefali
  Scenario: User is able to complete the reservation after doing CheckIn & Close
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User searches for live vehicle "OUV Calendar,AC,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,77"
    #And select an availble slot for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "Calendarcell,A,77",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User click on CheckOut CheckIn
    Then checkbox the checkout and click on continue
    And User selects driver or logistics and save it
    When User click on CheckOut CheckIn
    Then checkbox checkin and close then click on continue
    Then Logout from OUV
    

  #User tries to do CheckOut after doing CheckIn & Close
  @OUVnewRun @OUV_225 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to do CheckOut after doing CheckIn & Close
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,18"
    And select the vehicle
    When User navigate to OUVGatehouseLogs tab
    Then Verify the ActionType which is in Checkin and close
    And Navigate back to selected OUV Vehicle
    When User click on CheckOut CheckIn button
    Then checkbox checkout and click on continue
    And Verify the error message displayed
    Then Logout from the OUV
    
     #User is able to verify the Status of the reservation should be confirmed after the Checkin & close
  @OUVnewRun @OUV_226 @OUVBatch @batch201to250 @Shefali
  Scenario: User is able to verify the Status of the reservation should be confirmed after the Checkin & close
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User searches for live vehicle "OUV Calendar,AC,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,78"
    #And select an availble slot for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "Calendarcell,A,78",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User click on CheckOut CheckIn
    Then checkbox the checkout and click on continue
    And User selects driver or logistics and save it
    When User click on CheckOut CheckIn
    Then checkbox checkin and close then click on continue
    And Navigate to Reservations Tab
    Then Verify that status of Reservation is set to confirmed
    Then Logout from OUV
    
     #User is able to complete the reservation after doing CheckIn & Close
  @OUVnewRun @OUV_227 @OUVBatch @batch201to250 @Shefali
    Scenario: User is able to complete the reservation after doing CheckIn & Close
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User searches for live vehicle "OUV Calendar,AC,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,79"
    #And select an availble slot for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "Calendarcell,A,79",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User click on CheckOut CheckIn
    Then checkbox the checkout and click on continue
    And User selects driver or logistics and save it
    When User click on CheckOut CheckIn
    Then checkbox checkin and close then click on continue
    Then Logout from OUV
    
    
     #User cannot do CheckIn/CheckOut if the End Time of the Booking is completed as per local time zone
  @OUVnewRun @OUV_228 @OUVBatch @batch201to250 @Shefali
  Scenario: User cannot do CheckIn/CheckOut if the End Time of the Booking is completed as per local time zone
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,80"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,80",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User click on CheckOut CheckIn
    Then checkbox the checkout and click on continue
    And Verify the error message displayed to checkout
    Then Logout from OUV
    
    #User tries to do CheckOut/CheckIn procces for the OUV vehicle which is having a closed OUV booking
  @OUVnewRun @OUV_229 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to do CheckOut/CheckIn procces for the OUV vehicle which is having a closed OUV booking
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,6"
    Then Open any live OUV Vehicle for which CheckIn & Close is completed
    Then Verify that the reservation is completed for which CheckIn & Close is completed
    Then Verify that the OUV Booking status is closed which is associated with reservation
    Then User navigate to OUV vehicles
    Then Open any live OUV Vehicle for which CheckIn & Close is completed
    Then Click CheckOut/CheckIn button
    Then Select CheckOut from the checkbox
    Then Click Continue button
    Then Verify that an error occurred when user tries to do CheckOut for Closed OUV Booking
    Then Logout from the OUV
    
     #User tries to give keys returned date earlier than OUV booking start date at the time of completing reservation
  @OUVnewRun @OUV_230 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to give keys returned date earlier than OUV booking start date at the time of completing reservation
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then User searches for OUV Vehicle on their fleet Search "OUV vehicles,H,6"
    Then Open any live OUV Vehicle for which CheckIn & Close is completed
    Then Verify that reservation is completed for which CheckIn & Close is completed
    Then Click on Complete button
    Then Enter Keys Returned Date earlier than OUV Booking Start Date Start date
    Then Fill No. Keys CheckedIn, Return Location, Mileage fields Keys CheckedIn "OUV vehicles,W,2" Return Location "OUV vehicles,X,2" Mileage "OUV vehicles,Y,2"
    Then Verify that an error occurred when user tries to enter Keys Returned date in past
    Then Logout from the OUV
    
    #User tries to do consecutive CheckOut for 2 OUV Booking for the same OUV Vehicle
  @OUVnewRun @OUVCal @OUV_231 @batch201to250 @Shefali
  Scenario: User tries to do consecutive CheckOut for 2 OUV Booking for the same OUV Vehicle
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,81"
    Then User save an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,81",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notification Menu
    Then User approves the Booking
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,82"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,82",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notification Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Bookings
    Then Navigate to Reservations from Related Quick Links
    Then Click on OUV Vehicle by clicking on Blue link
    Then Click on Check-out/Check-in button
    Then Select Checkbox for Check-out and click on Continue button
    Then Check-in Window should be displayed and Select driver
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then Navigate to Reservations from Related Quick Links
    Then Click on OUV Vehicle by clicking on Blue link
    Then Click on Check-out/Check-in button
    Then Select Checkbox for Check-out and click on Continue button
    Then Check-in Window should be displayed and Select driver
    Then Logout from OUV
    
     #User can clone an OUV Booking at any stage
  @OUVnewRun @OUVBooking @OUV_232 @batch201to250 @Shefali
  Scenario: User can clone an OUV Booking at any stage
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,83"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,83",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User tries to Clone an OUV Booking
    Then Navigate to Reservation
    Then Validate that no Reservation is created for the cloned Booking
    Then Logout from OUV

  #User can edit OUV Booking details of cloned OUV Booking
  @OUVnewRun @OUVBooking @OUV_233 @batch201to250 @Shefali
  Scenario: User can edit OUV Booking details of cloned OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,84"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,84",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User tries to Clone an OUV Booking
    Then User edit the details of the cloned Booking
    And User Save the changes
    Then Logout from OUV

  #User cannot submit cloned OUV Booking for an approval
  @OUVnewRun @OUVBooking @OUV_234 @batch201to250 @Shefali
  Scenario: User cannot submit cloned OUV Booking for an approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,85"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,85",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User tries to Clone an OUV Booking
    Then User tries to submit the booking for Approval
    Then Logout from OUV
    
     #User cannot submit cloned OUV Booking for an approval
  @OUVnewRun @OUVCal @OUV_235 @batch201to250 @Shefali
  Scenario: User cannot submit cloned OUV Booking for an approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,86"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,86",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notification Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then Click on Clone button
    Then Enter mandatory fields in the window open driver one "OUV Calendar,Z,3"
    Then navigate to calender
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then save booking
    Then Click on Submit for Approval button
    Then Logout from OUV
    
     #User can create new OUV Fleet
  @OUVnewRun @OUVF @OUV_236 @batch201to250 @Shefali
  Scenario: User can create new OUV Fleet
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    Then Click New button
    Then User enters all the mandatory fields available on the form OUV Fleet Name "Fleet,A,4" Market "Fleet,B,2" OUV Category One "Fleet,D,2" Fleet Manager "Fleet,E,2" Fleet Administrator One "Fleet,F,2" Cost Center "Fleet,G,2" Account "Fleet,H,2" Day Pass Approve "Fleet,I,2" Overnight Pass Approver "Fleet,J,2" Weekend Pass Approver "Fleet,K,2" Weekend Pass Approver Alternative "Fleet,L,2" Approval Route "Fleet,M,2" Registration Approver "Fleet,N,2" Disposal Approver "Fleet,O,2"
    Then Click on Save button
    Then Logout user from OUV Portal

  #User can create new OUV Appoval Route
  @OUVnewRun @OUVF @OUV_237 @batch201to250 @Shefali
  Scenario: User can create new OUV Appoval Route
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then Navigate to OUV Approval Routes
    Then Click New button
    Then User enter all the mandatory fields available on the form OUV Approval Route Name "Fleet,P,2" Remarketing Manager "Fleet,Q,2" Finance Approver "Fleet,R,2" Operations Approver "Fleet,S,2" VLA Record Keeper "Fleet,T,2" Regional Finance Director "Fleet,U,2" Regional Managing Director "Fleet,V,2"
    Then Click on Save button
    Then Logout user from OUV Portal

  #User can edit Fleet Name, Fleet Code and Fleet Administrators, if user is member of selected fleet
  @OUVnewRun @OUVF @OUV_238 @batch201to250 @Shefali
  Scenario: User can edit Fleet Name, Fleet Code and Fleet Administrators, if user is member of selected fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    Then Click New button
    Then User enters all the mandatory fields available on the form OUV Fleet Name "Fleet,A,4" Market "Fleet,B,2" OUV Category One "Fleet,D,2" Fleet Manager "Fleet,E,2" Fleet Administrator One "Fleet,F,2" Cost Center "Fleet,G,2" Account "Fleet,H,2" Day Pass Approve "Fleet,I,2" Overnight Pass Approver "Fleet,J,2" Weekend Pass Approver "Fleet,K,2" Weekend Pass Approver Alternative "Fleet,L,2" Approval Route "Fleet,M,2" Registration Approver "Fleet,N,2" Disposal Approver "Fleet,O,2"
    Then Click on Save button
    Then Verify that the logged in user is member of the selected fleet Add User "Fleet,W,2"
    Then Edit details for Fleet Name, Fleet Code and Fleet Administrators fields Fleet Name "Fleet,A,5" Fleet Code "Fleet,X,2" Fleet administrators "Fleet,Y,2"
    Then Logout user from OUV Portal
    
     #User cannot remove any member from the fleet, if user is not member of selected fleet
  @OUVnewRun @OUVF @OUV_240 @batch201to250 @Shefali
  Scenario: User cannot remove any member from the fleet, if user is not member of selected fleet
    Given User access OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Fleet members
    Then user click on delete option
    Then Verify the error message shown
    Then Logout user from OUV Portal
    
     #User cannot edit any fields, if user is not member of selected fleet
  @OUVnewRun @OUVF @OUV_239 @batch201to250 @Shefali
  Scenario: User cannot edit any fields, if user is not member of selected fleet
    Given User access OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    Then User edits the fleet administrator with "Fleet,F,2"
    And user saves the changes
    And validates the message displayes
    Then Logout user from OUV Portal
    
     #User can add and remove any member from the fleet, if user is member of selected fleet
  @OUVnewRun @OUVF @OUV_241 @batch201to250 @Shefali
  Scenario: User can add and remove any member from the fleet, if user is member of selected fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Fleet members
    When click on new add new member "Users,C,12"
    Then delete the added fleet member
    Then Logout user from OUV Portal

  #User cannot change owner of the selected fleet, if user is member of the selected fleet
  @OUVnewRun @OUVF @OUV_242 @batch201to250 @Shefali
  Scenario: User cannot change owner of the selected fleet, if user is member of the selected fleet
    Given User access OUV Portal with user "Users,D,4" and password as "Users,E,4"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And user tries to change owner with "Fleet,F,2"
    Then Validate the error message
    And close the window
    Then Logout user from OUV Portal
    
     #User can edit each and every field of selected fleet, if user is owner of the fleet
  @OUVnewRun @OUVF @OUV_243 @batch201to250 @Shefali
  Scenario: User can edit each and every field of selected fleet, if user is owner of the fleet
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,3" and market to "Fleet,B,3"
    Then Verify the changes in fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,2" and market to "Fleet,B,2"
    Then Logout user from OUV Portal
    
    #User can add and remove any member from the fleet, if user is owner of the fleet
  @OUVnewRun @OUVF @OUV_244 @batch201to250 @Shefali
  Scenario: User can add and remove any member from the fleet, if user is owner of the fleet
    Given User access OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Fleet members
    When click on new add new member "Users,C,12"
    Then delete the added fleet member
    Then Logout user from OUV Portal

  #User can view OUV Approval Route Details of selected fleet
  @OUVnewRun @OUVF @OUV_245 @batch201to250 @Shefali
  Scenario: User can view OUV Approval Route Details of selected fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Approval Route
    Then verify that user is able to view Details of Approval Route
    Then Logout user from OUV Portal

  #User can edit any field of an OUV Approval Route, if the user is owner of an OUV Approval Route
  @OUVnewRun @OUVF @OUV_246 @batch201to250 @Shefali
  Scenario: User can edit any field of an OUV Approval Route, if the user is owner of an OUV Approval Route
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Approval Route
    Then verify that user is able to view Details of Approval Route
    When user clicks on edit Approval Route fleet
    And User changes the Approval Route fleet name to "Fleet,C,3"
    Then Verify the changes in fleet
    When user clicks on edit Approval Route fleet
    And User changes the Approval Route fleet name to "Fleet,C,3"
    Then Logout user from OUV Portal

  #User cannot edit any field of an OUV Approval Route, if the user  is not a member of the Finance Team
  @OUVnewRun @OUVF @OUV_247 @SOUV-499 @batch201to250 @Shefali
  Scenario: User cannot edit any field of an OUV Approval Route, if the user  is not a member of the Finance Team
    Given User access OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Approval Route
    Then verify that user is able to view Detail of Approval Route
    Then Verify the edit options are not available
    Then Logout user from OUV Portal
    
    #User sends S-Sign Envelop for E-sign for any approved OUV Booking
  @OUVnewRun @OUV_248 @batch201to250 @Shefali
  Scenario: User sends S-Sign Envelop for E-sign for any approved OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,87"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,87",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    And User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User saves the VLA
    Then User view details of newly created OUV Vehicle Loan Agreement
    And User enters all vehicle user information like Company "OUV VLA,E,2" Phone "OUV VLA,G,2" Date of Birth "OUV VLA,H,2" Occupation "OUV VLA,I,2" Address "OUV VLA,K,2" Postcode "OUV VLA,N,2" Passport Number "OUV VLA,O,2" Driver License Number "OUV VLA,P,2" Country of issue "OUV VLA,L,2" License Start Date "OUV VLA,I,2" License End Date "OUV VLA,J,2" Insured By "OUV VLA,Q.2" Insurance company "OUV VLA,M,2"
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Verify that OUV Booking status is Approved if Booking Justification "OUV Calendar,G,6"
    And Verify that the document in S-Sign section is available for E-Sign
    Then User sends the Sign Request
    Then Logout from OUV

  #User can view Encrypted Company & Encrypted Full Name field under Driver/Vehicles User section for an approved OUV Booking
  @OUVnewRun @OUV_249 @batch201to250 @Shefali
  Scenario: User can view Encrypted Company & Encrypted Full Name field under Driver/Vehicle User section for an approved OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,88"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,88",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    And User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User saves the VLA
    Then User view details of newly created OUV Vehicle Loan Agreement
    And User enters all vehicle user information like Company "OUV VLA,E,2" Phone "OUV VLA,G,2" Date of Birth "OUV VLA,H,2" Occupation "OUV VLA,I,2" Address "OUV VLA,K,2" Postcode "OUV VLA,N,2" Passport Number "OUV VLA,O,2" Driver License Number "OUV VLA,P,2" Country of issue "OUV VLA,L,2" License Start Date "OUV VLA,I,2" License End Date "OUV VLA,J,2" Insured By "OUV VLA,Q.2" Insurance company "OUV VLA,M,2"
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Verify that OUV Booking status is Approved if Booking Justification "OUV Calendar,G,6"
    And Verify that the document in S-Sign section is available for E-Sign
    Then User sends the Sign Request
    Then Logout from OUV
    
     #User tries to do CheckIn after completing CheckIn for an OUV Vehicle
  @OUVnewRun @OUV_250 @OUVBatch @batch201to250 @Shefali
  Scenario: User tries to do CheckIn after completing CheckIn for an OUV Vehicle
    Given Access to the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV vehicles
    Then search for the vehicle with status "OUV vehicles,A,19"
    And select the vehicle
    When User navigate to OUVGatehouseLogs tab
    Then Verify the ActionType which is in Checkin
    And Navigate back to selected OUV Vehicle
    When User click on CheckOut CheckIn button
    Then Select Check-In checkbox and click on continue
    Then Logout of OUV Portal
    
    
   #Demo for reservation 1
  @Demoforcal1 @democalbatch
  Scenario: User is able to see VLA Status as 'Created' under Vehicle Loan Agreements for an Approved Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,2"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,2",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    Then Logout from the OUV
    
    #Demo for reservation 2
  @Demoforcal2 @democalbatch
  Scenario: User is able to see VLA Status as 'Created' under Vehicle Loan Agreements for an Approved Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle "Calendarcell,B,3"
    Then User saves an OUV Booking including other location with Booking Quick Reference "Calendarcell,A,3",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    Then Logout from the OUV
