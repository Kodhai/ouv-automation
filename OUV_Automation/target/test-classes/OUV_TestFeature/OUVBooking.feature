
Feature: Validate OUV Booking

#In this scenario user tries to cancel booking which is in Pending Approval
@OUV @OUVBooking @OUV_137
Scenario: User is not able to cancel the booking for a Booking Approval Status as 'Booked Pending Approval'
	Given User access OUV Portal with UserName "Users,D,3" Password "Users,E,3"
	And User Navigates to OUV Booking menu
	Then User search an OUV Booking with status "OUV Bookings,A,3"
	And User tries to cancel an OUV Booking which is pending for approval
	Then User do logout from OUV Portal
		
#In this scenario user tries to cancel booking which is in Approved
@OUV @OUVBooking @OUV_138
Scenario: User is able to cancel the booking for a Booking Approval Status as 'Approved'
	Given User access OUV Portal with UserName "Users,D,3" Password "Users,E,3"
	And User Navigates to OUV Booking menu
	Then User search an OUV Booking with status "OUV Bookings,A,4"
	And User tries to cancel an OUV Booking which is approved
	Then User do logout from OUV Portal	
	
#User is able to search for the Booking ID in the OUV Booking Tab  
@OUV @OUVCal @OUV_177
Scenario: User is able to search for the Booking ID in the OUV Booking Tab
	Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
	And User Navigates to OUV Booking menu
  Then Search for an OUV Booking with Approved status Search List "OUV Bookings,E,2" 
	Then Logout from OUV
		
#User is able to create a new Booking on OUV Booking  window
@OUV @OUVCal @OUV_178
Scenario: User is able to create a new Booking on OUV Booking  window
	Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
	And User Navigates to OUV Booking menu
	Then Click on New	
	Then Select the OUV Booking Editable radiobutton and click Next
	Then Enter all the mandatory details and click on Save Booking Quick Reference "OUV Bookings,F,2" Reason For Booking "OUV Bookings,G,2" Booking Type "OUV Bookings,H,2" Journey Type "OUV Bookings,I,2" Booking request type "OUV Bookings,J,2" Booking Justification "OUV Bookings,K,2" Passout Type "OUV Bookings,L,2" Start Date "OUV Bookings,Q,2" End Date "OUV Bookings,R,2" Other Location "OUV Bookings,N,2" Internal JLR "OUV Bookings,M,2" Email Id "OUV Bookings,O,2" Driverone "OUV Bookings,P,2" 
	Then Logout from OUV
	
#User is able to change the owner name for the Booking Id on OUV Booking Search Window
@OUV @OUVCal @OUV_179
Scenario: User is able to change the owner name for the Booking Id on OUV Booking Search Window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User Navigates to OUV Booking menu
    Then Search for an OUV Booking with Approved status Search List "OUV Bookings,E,2"
    Then Select any record and click on the Change Owner option on OUV Booking Search Window
    Then Search for the owner name who is member of the fleet "Users,C,2" and click on submit button
    Then Logout from OUV
		
 #User is not able to change the owner name for the Booking Id on OUV Booking Search Window
  @OUV @OUVCal @OUV_180
  Scenario: User is not able to change the owner name for the Booking Id on OUV Booking Search Window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Bookings
    And User searches for the OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then Click on Change owner icon and select a new owner "OUV Bookings,D,2"
    And Validate that user cannot change the owner for the booking
    Then Logout from OUV

    #User is able to view the PDF document in an OUV Vehicle Agreement for an Approved OUV Booking with Signature
    @OUV @OUV_Phase2_062
    Scenario: User is able to view the PDF document in an OUV Vehicle Agreement for an Approved OUV Booking with Signature
    Given User access OUV Portal with UserName "Users,D,7" Password "Users,E,7"
    And User Navigates to OUV Booking menu
    Then User search an OUV Booking with status "OUV Bookings,A,7"
    And User navigate to Vehicle Loan Agreements
    And User view details of available VLA
    Then User navigates to Notes & Attachments section
    And User is able to view PDF Document in OUV VLA
    
    #User is not able to view the PDF document in an OUV Vehicle Agreement for an Approved OUV Booking with Signature
   	@OUV @OUV_Phase2_063
    Scenario: User is not able to view the PDF document in an OUV Vehicle Agreement for an Approved OUV Booking with Signature
    Given User access OUV Portal with UserName "Users,D,4" Password "Users,E,4"
    And User Navigates to OUV Booking menu
    Then User search an OUV Booking with status "OUV Bookings,A,7"
    And User navigate to Vehicle Loan Agreements
    Then Verify that user is not able to view PDF Document in an OUV VLA
    Then User do logout from OUV Portal

    #User is not able to view Vehicle user details in an OUV Booking
    @OUV @OUVBooking @OUV_Phase2_058
    Scenario: User is not able to view Vehicle user details in an OUV Booking
	Given User access OUV Portal with UserName "Users,D,7" Password "Users,E,7" 
	And User Navigates to OUV Booking menu
	Then User search an OUV Booking with status "OUV Bookings,A,4" 
	And Verify that the Vehicle user details are not present in an OUV Booking
	Then User do logout from OUV Portal	
    
 