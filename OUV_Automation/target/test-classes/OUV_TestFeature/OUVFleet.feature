Feature: Validate OUV Fleet

  #User can edit each and every field of selected fleet, if user is owner of the fleet
  @OUV @OUVF @OUV_243 @batch200
  Scenario: User can edit each and every field of selected fleet, if user is owner of the fleet
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,3" and market to "Fleet,B,3"
    Then Verify the changes in fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,2" and market to "Fleet,B,2"
    Then Logout user from OUV Portal

  #User can create new OUV Fleet
  @OUV @OUVF @OUV_236 @batch200
  Scenario: User can create new OUV Fleet
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    Then Click New button
    Then User enters all the mandatory fields available on the form OUV Fleet Name "Fleet,A,4" Market "Fleet,B,2" OUV Category One "Fleet,D,2" Fleet Manager "Fleet,E,2" Fleet Administrator One "Fleet,F,2" Cost Center "Fleet,G,2" Account "Fleet,H,2" Day Pass Approve "Fleet,I,2" Overnight Pass Approver "Fleet,J,2" Weekend Pass Approver "Fleet,K,2" Weekend Pass Approver Alternative "Fleet,L,2" Approval Route "Fleet,M,2" Registration Approver "Fleet,N,2" Disposal Approver "Fleet,O,2"
    Then Click on Save button
    Then Logout user from OUV Portal

  #User can create new OUV Appoval Route
  @OUV @OUVF @OUV_237 @batch200
  Scenario: User can create new OUV Appoval Route
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then Navigate to OUV Approval Routes
    Then Click New button
    Then User enter all the mandatory fields available on the form OUV Approval Route Name "Fleet,P,2" Remarketing Manager "Fleet,Q,2" Finance Approver "Fleet,R,2" Operations Approver "Fleet,S,2" VLA Record Keeper "Fleet,T,2" Regional Finance Director "Fleet,U,2" Regional Managing Director "Fleet,V,2"
    Then Click on Save button
    Then Logout user from OUV Portal

  #User can edit Fleet Name, Fleet Code and Fleet Administrators, if user is member of selected fleet
  @OUV @OUVF @OUV_238 @batch200
  Scenario: User can edit Fleet Name, Fleet Code and Fleet Administrators, if user is member of selected fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    Then Click New button
    Then User enters all the mandatory fields available on the form OUV Fleet Name "Fleet,A,4" Market "Fleet,B,2" OUV Category One "Fleet,D,2" Fleet Manager "Fleet,E,2" Fleet Administrator One "Fleet,F,2" Cost Center "Fleet,G,2" Account "Fleet,H,2" Day Pass Approve "Fleet,I,2" Overnight Pass Approver "Fleet,J,2" Weekend Pass Approver "Fleet,K,2" Weekend Pass Approver Alternative "Fleet,L,2" Approval Route "Fleet,M,2" Registration Approver "Fleet,N,2" Disposal Approver "Fleet,O,2"
    Then Click on Save button
    Then Verify that the logged in user is member of the selected fleet Add User "Fleet,W,2"
    Then Edit details for Fleet Name, Fleet Code and Fleet Administrators fields Fleet Name "Fleet,A,5" Fleet Code "Fleet,X,2" Fleet administrators "Fleet,Y,2"
    Then Logout user from OUV Portal

  #User can add and remove any member from the fleet, if user is owner of the fleet
  @OUV @OUVF @OUV_244 @batch200
  Scenario: User can add and remove any member from the fleet, if user is owner of the fleet
    Given User access OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Fleet members
    When click on new add new member "Users,C,12"
    Then delete the added fleet member
    Then Logout user from OUV Portal

  #User can view OUV Approval Route Details of selected fleet
  @OUV @OUVF @OUV_245 @batch200
  Scenario: User can view OUV Approval Route Details of selected fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Approval Route
    Then verify that user is able to view Details of Approval Route
    Then Logout user from OUV Portal

  #User can edit any field of an OUV Approval Route, if the user is owner of an OUV Approval Route
  @OUV @OUVF @OUV_246 @batch200
  Scenario: User can edit any field of an OUV Approval Route, if the user is owner of an OUV Approval Route
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Approval Route
    Then verify that user is able to view Details of Approval Route
    When user clicks on edit Approval Route fleet
    And User changes the Approval Route fleet name to "Fleet,C,3"
    Then Verify the changes in fleet
    When user clicks on edit Approval Route fleet
    And User changes the Approval Route fleet name to "Fleet,C,3"
    Then Logout user from OUV Portal

  #User cannot edit any field of an OUV Approval Route, if the user  is not a member of the Finance Team
  @OUV @OUVF @OUV_247 @SOUV-499 @batch200
  Scenario: User cannot edit any field of an OUV Approval Route, if the user  is not a member of the Finance Team
    Given User access OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,2"
    Then Select the fleet
    And Navigate to OUV Approval Route
    Then verify that user is able to view Detail of Approval Route
    Then Verify the edit options are not available
    Then Logout user from OUV Portal

  #User is not able to edit the Fleet name for an OUV Fleet
  @OUV @OUVF @OUV_phase2_045
  Scenario: User is not able to edit the Fleet name for an OUV Fleet
    Given User access OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,3"
    Then validate the error message displayed
    Then Logout user from OUV Portal

  #User is not able to edit the Fleet Administrator 2 for an OUV Fleet
  @OUV @OUVF @OUV_phase2_046
  Scenario: User is not able to edit the Fleet Administrator 2 for an OUV Fleet
    Given User access OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When User edit fleet administrator and enter "Users,C,9"
    Then validate the error message displayed
    Then Logout user from OUV Portal

  #User is able to edit the Fleet name for an OUV Fleet
  @OUV @OUVF @OUV_phase2_039
  Scenario: User is able to edit the Fleet name for an OUV Fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,6"
    Then Verify the changes in fleet name
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,5"
    Then Logout user from OUV Portal

  #User is able to verify the updated Fleet name in an OUV Vehicle
  @OUV @OUVF @OUV_phase2_040
  Scenario: User is able to verify the updated Fleet name in an OUV Vehicle
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,6"
    Then Verify the changes in fleet name
    And User navigate to OUV Vehicles
    Then User select the first vehicle
    And User click on Fleet name link
    When user clicks on edit fleet
    And User changes the fleet name to "Fleet,A,5"
    Then Verify the changes in fleet name
    Then Logout user from OUV Portal

  #User is able to add and delete Fleet Members in an OUV fleet
  @OUV @OUVF @OUV_phase2_043
  Scenario: User is able to add and delete Fleet Members in an OUV fleet
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And Navigate to OUV Fleet members
    When click on new add new member "Users,C,12"
    Then delete the added fleet member
    Then Logout user from OUV Portal

  #User is not able to edit the fields in an OUV Fleet record
  @OUV @OUVF @OUV_phase2_044
  Scenario: User is not able to edit the fields in an OUV Fleet record
    Given User access OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And Verify that fleet manager details cannot be edited
    Then Verify that Day pass approver one field cannot be edited
    And Verify that Day pass approver two field cannot be edited
    Then Verify that Day pass approver three field cannot be edited
    And Verify that Approval Route cannnot be edited
    Then Verify that Registration Approver field cannot be edited
    And Verify that Disposal Approver filed cannot be edited
    Then Logout user from OUV Portal

  #User is able to update the Approval Routes for an OUV Fleet Record
  @OUV @OUVF @OUV_phase2_049
  Scenario: User is able to update the Approval Routes for an OUV Fleet Record
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    When User changes the Approval Route to "Fleet,C,4"
    And Verify that Approval Route is updated
    When User changes the Approval Route to "Fleet,C,3"
    Then Logout user from OUV Portal

  #User is able to update the Fleet Record and validate the details in the run report ATO Request Updates_Check
  @OUV @OUVF @OUV_phase2_050
  Scenario: User is able to update the Fleet Record and validate the details in the run report ATO Request Updates_Check
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And User changes the fleet name to "Fleet,A,6"
    When User edit fleet administrator and enter "Users,C,4"
    Then User changed the Fleet manager to "Users,C,3"
    When User changes the Approval Route to "Fleet,C,4"
    And Navigate to Reports
    Then Select All folders under folders section
    And Select the folder "Fleet,X,2"
    Then Navigate to the folder "Fleet,X,3"
    And Select Report for check "Fleet,Y,2"
    Then Verify that Fleet name is updated to "Fleet,A,6"
    And Verify that Fleet Administrator is updated to "Users,C,4"
    Then Verify that Remarketing Manager is updated to "Users,C,6"
    And Verify that Finance Approver is updated to "Users,C,18"
    Then Verify that Operations Manager is updated to "Users,C,3"
    And Verify that Regional Finance Director is updated to "Users,C,19"
    Then Verify that Regional Managing Director is updated to "Users,C,20"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,6"
    Then Select the fleet
    And User changes the fleet name to "Fleet,A,5"
    When User edit fleet administrator and enter "Users,C,11"
    Then User changed the Fleet manager to "Users,C,21"
    When User changes the Approval Route to "Fleet,C,3"
    Then Logout user from OUV Portal

  #User is able to update the Fleet Record and validate the details in the run report OUV Vehicle  Updates_Check
  @OUV @OUVF @OUV_phase2_051
  Scenario: User is able to update the Fleet Record and validate the details in the run report OUV Vehicle  Updates_Check
    Given User access OUV Portal with user "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And User changes the fleet name to "Fleet,A,6"
    When User edit fleet administrator and enter "Users,C,4"
    Then User changed the Fleet manager to "Users,C,3"
    And User changes the Registration Approver to "Users,C,18"
    When User changes the Approval Route to "Fleet,C,4"
    And Navigate to Reports
    Then Select All folders under folders section
    And Select the folder "Fleet,X,2"
    Then Navigate to the folder "Fleet,X,3"
    And Select Report for check "Fleet,Y,3"
    Then Verify that Fleet name is updated to "Fleet,A,6"
    And Verify that Fleet Administrator is updated to "Users,C,4"
    Then Verify that Remarketing Fleet Manager is updated to "Users,C,6"
    And Verify that Finance Approver is updated to "Users,C,18"
    Then Verify that Operations Approver is updated to "Users,C,3"
    And Verify that Registration Approver is updated to "Users,C,18"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,6"
    Then Select the fleet
    And User changes the fleet name to "Fleet,A,5"
    When User edit fleet administrator and enter "Users,C,11"
    Then User changed the Fleet manager to "Users,C,21"
    And User changes the Registration Approver to "Users,C,11"
    When User changes the Approval Route to "Fleet,C,3"
    Then Logout user from OUV Portal
    
  #User is able to update the Fleet Record and validate the details in the run report OUV Booking Updates_Check
  @OUV @OUVCal @OUV_phase2_052
  Scenario: User is able to update the Fleet Record and validate the details in the run report OUV Booking Updates_Check
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,5" and Brand "OUV Calendar,AM,2"
    And select buyback checkbox
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,2",ReasonOfBooking "OUV Calendar,F,2",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,2",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation to calendars "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,2",ReasonOfBooking "OUV Calendar,F,2",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,2",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation to calendarss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation to calendarsss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation to calendarssss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    Then Login with Fleet Manager as "Users,D,5" and password as "Users,E,5"
    Then User navigates to OUV Fleets
    And search for the Fleet with "Fleet,A,5"
    Then Select the fleets
    And User changes fleet name to "Fleet,A,6"
    When User edit fleet administrators and enter "Users,C,4"
    Then User changed the Fleet manager to "Users,C,3"
    And User changes the llfive approver to "Users,C,5"
    And Navigates to Reports "OUV Calendar,AN,2"
    Then Selects All folders under folders section
    And Select the folders "Fleet,X,2"
    Then Navigates to the folder "Fleet,X,3"
    And Selects Report for check "Fleet,Y,3"
    Then Verifys that Fleet name is updated to "Fleet,A,6"
    And Verifys that Fleet Administrator is updated to "Users,C,4"
    Then Verifys that approver is updated to "User,C,5"
    Then User navigates to OUV Fleets
    And search for the Fleet "Fleet,A,6"
    Then Select the fleets
    And User changes fleet name to "Fleet,A,5"
    When User edit fleet administrators and enter "Users,C,11"
    Then User changed the Fleet manager to "Users,C,21"
    And User changes the llfive approver to "User,C,22"
    Then Logout from OUV
    
    #User is not able to add and delete Fleet Members in an OUV fleet
 @OUV @OUVF @OUV_Phase2_047
  Scenario: User is not able to add and delete Fleet Members in an OUV fleet
    Given User access OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And Navigate to OUV Fleet members
    Then User tries to add new member "Users,C,12"
    Then verify the add new error message
    Then Logout user from OUV Portal
    
#User is not able to edit the fields in an OUV Fleet record
 @OUV @OUVF @OUV_Phase2_048
  Scenario: User is not able to edit the fields in an OUV Fleet record
    Given User access OUV Portal with user "Users,D,7" and password as "Users,E,7"
    Then User navigates to OUV Fleet
    And search for the Fleet "Fleet,A,5"
    Then Select the fleet
    And Verify that user is not able to edit the fields in an OUV Fleet
    Then Logout user from OUV Portal
