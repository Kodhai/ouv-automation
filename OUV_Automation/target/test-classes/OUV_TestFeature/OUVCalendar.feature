Feature: Validate OUV Calendar

  #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) for Brand - Jaguar
  @OUV @OUV_001 @OUVBatch @SOUV-253
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL for Brand - Jaguar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles for Brand "Jaguar"
    And Search for Vehicles
    Then Logout from OUV

  #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) for Brand - Land Rover
  @OUV @OUV_002 @OUVBatch
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL for Brand - LandRover
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles for Brand "Land Rover"
    And Search for Vehicles
    Then Logout from OUV

  #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) without selecting any Brand
  @OUV @OUV_003 @OUVBatch
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL without selecting any Brand
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search for Vehicles
    Then Logout from OUV

  #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) by selecting Booking Start Date and End Date
  @OUV @OUV_004 @OUVBatch
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL by selecting Future Booking Start Date and End Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles by selecting Future Booking Start Date and End Date
    And Search for Vehicles
    Then Logout from OUV

  #Below is the scenario for Searching a Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) by selecting Prior Booking Start Date
  @OUV @OUV_005 @OUVBatch
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL by selecting Prior Booking Start Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles by selecting Prior Booking Start Date
    And Search for Vehicles
    Then Verify the displayed Booking Start Date error message
    Then Logout from OUV

  #User searches for Vehicle for available market (Netherlands) and Fleet (Netherlands - PR NL) by selecting Prior Booking End Date
  @OUV @OUV_006
  Scenario: User searches for Vehicle for available market Netherlands and Fleet Netherlands - PR NL by selecting Prior Booking End Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search Vehicles by selecting Prior Booking End Date
    And Search for Vehicles
    Then Verify the error message displayed on selecting prior booking end date
    Then Logout from OUV

  #Verification code needed
  #User searches for Vehicle by selecting only the mandatory fields
  @OUV @OUV_007
  Scenario: User searches for Vehicle by selecting only the mandatory fields
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by selecting Future Booking Start Date and End Date
    And Search for Vehicles
    Then Verify the error message displayed for No records found
    Then Logout from OUV

  #User should be able to see unavailable vehicles which is greyed out in Reservation Calendar
  @OUV @OUV_008
  Scenario: User should be able to see unavailable vehicles greyed out in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search for Vehicles
    Then Verify that unavailable vehicles are greyed out
    Then Logout from OUV

  #User should be able to highlight available vehicles in Reservation Calendar
  @OUV @OUV_009
  Scenario: User should be able to highlight available vehicles in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search for Vehicles
    Then Verify that user is able to select an available Vehicle
    Then Logout from OUV

  #User should be able to see an entry for Vehicle in Reservation Calendar in days, weeks and months
  @OUV @OUV_010
  Scenario: User should be able to see an entry for Vehicle in Reservation Calendar in days, weeks and months
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search Vehicles by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And Search for Vehicles
    Then Select Day, Week and Month Tab in the Reservation Calendar
    Then Logout from OUV

  #User is able to view the entry of a Vehicle in time format under Day Tab in the Reservation Calendar
  @OUV @OUV_0011 @OUVBatch
  Scenario: User is able to view the entry of a Vehicle in time format under Day Tab in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    Then select the day tab in calendar and verify its format
    Then Logout from OUV

  #User is able to view the entry of a Vehicle in Days format under Week Tab in the Reservation Calendar
  @OUV @OUV_0012 @OUVBatch
  Scenario: User is able to view the entry of a Vehicle in Days format under Week Tab in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    Then select the Week tab in calendar and verify its format
    Then Logout from OUV

  #User is able to view the entry of a Vehicle in Months format under Month Tab in the Reservation Calendar
  @OUV @OUV_0013 @OUVBatch
  Scenario: User is able to view the entry of a Vehicle in Months format under Month Tab in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    Then select the Month tab in calendar and verify its format
    Then Logout from OUV

  #User is able to view all the searched vehicles in the Reservation Calendar
  @OUV @OUV_0014 @OUVBatch
  Scenario: User is able to view all the searched vehicles in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    Then verify the View list of searched vehicles in the Reservation Calendar
    Then Logout from OUV

  #User should be able to see an OUV Booking Pop Up after selecting the available vehicle for Reservation in the Reservation Calendar
  @OUV @OUV_015 @OUVBatch
  Scenario: User should be able to see an OUV Booking Pop Up after selecting the available vehicle for Reservation in the Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Search for Vehicle by giving Market as "OUV Calendar,A,2" and Fleet as "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User should be able to select any option [Reservation(Passout)] from dropdown from OUV Booking window
  @OUV @OUVCal @OUV_016 @OUVBatch
  Scenario: User should be able to select any option ReservationPassout from dropdown from OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User should be able to edit Start Date and Start Time while doing OUV Booking
  @OUV @OUVCal @OUV_017 @OUVBatch
  Scenario: User should be able to edit Start Date and Start Time while doing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    And User edit Start Date "OUV Calendar,B,2" and Start Time "OUV Calendar,C,2" on OUV Booking window
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User should be able to edit End Date and End Time while doing OUV Booking
  @OUV @OUVCal @OUV_018 @OUVBatch
  Scenario: User should be able to edit End Date and End Time while doing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    And User edit End Date "OUV Calendar,D,2" and End Time "OUV Calendar,E,2" on OUV Booking window
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User saves an OUV Booking by giving prior Start Date
  @OUV @OUVCal @OUV_019 @OUVBatch
  Scenario: User saves an OUV Booking by giving prior Start Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    And User saves an OUV Booking with Booking Quick Reference "OUV Calendar,F,3",Start Date "OUV Calendar,B,3",Booking Justification "OUV Calendar,G,3",Booking Request Type "OUV Calendar,H,3",Passout Type "OUV Calendar,I,3",Journey Type "OUV Calendar,J,3"
    Then An error occurred when user saves an OUV Booking with prior Start Date
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User saves an OUV Booking by giving prior End Date
  @OUV @OUVCal @OUV_020 @OUVBatch
  Scenario: User saves an OUV Booking by giving prior End Date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select available vehicle
    Then Verify that user can select Reservation from the dropdown on OUV Booking window
    And User saves an OUV Booking with Booking Quick Reference "OUV Calendar,F,4",Start Date "OUV Calendar,B,4",Booking Justification "OUV Calendar,G,4",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then An error occurred when user saves an OUV Booking with prior End Date
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User saves an OUV Booking by selecting only the mandatory fields
  @OUV @OUV_021
  Scenario: User saves an OUV Booking by selecting only the mandatory fields
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    And An error occurred when user saves an OUV Booking
    Then Close the OUV Booking Pop Up
    Then Logout from OUV

  #User saves an OUV Booking by selecting all the mandatory fields and selecting other Location field
  @OUV @OUV_022
  Scenario: User saves an OUV Booking by selecting all the mandatory fields and selecting other Location field
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Logout from OUV

  #User is able to view the details of an OUV Booking in Reservation Calendar
  @OUV @OUV_023
  Scenario: User is able to view the details of an OUV Booking in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    And Validate the details in the Reservation Calender
    Then Logout from OUV

  #User saves an OUV Booking by Including Preparation/Transportation field
  @OUV @OUV_024
  Scenario: User saves an OUV Booking by Including Preparation/Transportation field
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including Preparation/Transportation field with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Logout from OUV

  #User is able to edit an existing OUV Booking
  @OUV @OUVCal @OUV_025
  Scenario: User is able to edit an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User opens any existing OUV Booking which is not yet approved from OUV Calendar
    Then User edits details of OUV Booking
    Then Logout from OUV

  #User is able to delete an existing OUV Booking
  @OUV @OUVCal @OUV_026
  Scenario: User is able to delete an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User opens any existing OUV Booking which is not yet approved from OUV Calendar
    Then User deletes created OUV Booking
    And Verify that User cannot delete OUV Booking
    Then Logout from OUV

  #User is able to view in Salesforce for an existing OUV Booking
  @OUV @OUVCal @OUV_027
  Scenario: User is able to view in Salesforce for an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User opens any existing OUV Booking which is not yet approved from OUV Calendar
    Then User clicks on View In Salesforce button and verify all details are displayed
    Then Logout from OUV

  #User is able to cancel any changes made in an existing OUV Booking
  @OUV @OUVCal @OUV_028
  Scenario: User is able to cancel any changes made in an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And User opens any existing OUV Booking which is not yet approved from OUV Calendar
    Then User cancels any changes made in an existing OUV Booking
    Then Logout from OUV

  #User is able to validate that OUV Vehicle is pre-populated in the OUV Booking
  @OUV @OUVCal @OUV_029
  Scenario: User is able to validate that OUV Vehicle is pre-populated in the OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User validate that OUV Vehicle is pre-populated in the OUV Booking window
    Then Logout from OUV

  #(Need to add approved scenario)
  #User is able to view the Approved OUV Booking for Reservation(Passout)�highlighted in green color in Reservation Calendar
  @OUV @OUVCal @OUV_030
  Scenario: User is able to view the Approved OUV Booking highlighted in green color in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And Validate that an approved OUV Booking is highlighted in green color
    Then Logout from OUV

  #User is able to view the Non-Approved OUV Booking for Reservation(Passout)�highlighted in blue color in Reservation Calendar
  @OUV @OUVCal @OUV_031
  Scenario: User is able to view the Non-Approved OUV Booking highlighted in blue color in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And Validate that a Non Approved OUV Booking is highlighted in blue color
    Then Logout from OUV

  #User creates an OUV Booking and Submit for Approval
  @OUV @OUVCal @OUV_032
  Scenario: User creates an OUV Booking and Submit for Approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    Then Logout from OUV

  #User is able to see Booking Start Date and Booking end Date pre-populated while searching a vehicle
  @OUV @OUVCal @OUV_033
  Scenario: User is able to see Booking Start Date and Booking end Date pre-populated while searching a vehicle
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    And Verify that Booking Start Date and Booking End date is pre-populated
    Then Logout from OUV

  #User is able to see Start Date and Start Time pre-populated in OUV Booking window
  @OUV @OUVCal @OUV_034
  Scenario: User is able to see Start Date and Start Time pre-populated in OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    And Verify that User can see Start date and Start Time are Pre-populated
    Then Logout from OUV

  #User is able to see End Date and End Time pre-populated in OUV Booking window
  @OUV @OUVCal @OUV_035
  Scenario: User is able to see End Date and End Time pre-populated in OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    And Verify that User can see End date and End Time are Pre-populated
    Then Logout from OUV

  #User does not enter Preparation/Transportation End Date while including  Preparation/Transportation checkbox in OUV Booking window
  @OUV @OUVCal @OUV_036
  Scenario: User does not enter Preparation/Transportation End Date while including  Preparation/Transportation checkbox in OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    And User saves an OUV Booking without entering Preparation/Transportation End date Field with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then An error occurred while saving new OUV Booking without entering Preparation/Trasportation End Date
    Then Logout from OUV

  #User is able to see an OUV Booking ID after saving an OUV Booking
  @OUV @OUVCal @OUV_037
  Scenario: User is able to see an OUV Booking ID after saving an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Validate that user is able to see the Booking ID generated
    Then Logout from OUV

  #User is able to see a confirmation message for Reservation successfully saved after saving an OUV Booking
  @OUV @OUVCal @OUV_038
  Scenario: User is able to see a confirmation message for Reservation successfully saved after saving an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    And Validate that user sees a confirmation message for Reservation successfully saved
    Then Logout from OUV

  #User is able to navigate to the OUV Booking ID after saving an OUV Booking
  @OUV @OUVCal @OUV_039
  Scenario: User is able to navigate to the OUV Booking ID after saving an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then Logout from OUV

  #User verifies all the details in the OUV Booking ID Page are auto-populated after saving an OUV Booking
  @OUV @OUVCal @OUV_040
  Scenario: User verifies all the details in the OUV Booking ID Page are auto-populated after saving an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User is able to verify all the details are auto-populated with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Logout from OUV

  #User is able to edit the details in the OUV Booking ID Page
  @OUV @OUVCal @OUV_041
  Scenario: User is able to edit the details in the OUV Booking ID Page
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User edits the details in the OUV Booking ID Page
    Then Logout from OUV

  #User is able to see VLA Market pre-populated under Vehicle Loan Agreement in OUV Booking ID Page
  @OUV @OUVCal @OUV_042
  Scenario: User is able to see VLA Market pre-populated under Vehicle Loan Agreement in OUV Booking ID Page
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User verify that VLA Market field is Auto populated by verifying Market "OUV Calendar,A,2"
    Then Logout from OUV

  #User enters an invalid Internal JLR contact Email in OUV Booking window
  @OUV @OUVCal @OUV_043
  Scenario: User enters an invalid Internal JLR contact Email in OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location and invalid emial with Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And verify the error message
    Then Logout from OUV

  #User should be able to verify the owner name from OUV Booking ID window
  @OUV @OUVCal @OUV_044
  Scenario: User should be able to verify the owner name from OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User verifies the owner name "Users,D,2" from OUV Booking ID Window
    Then Logout from OUV

  #User selects Auto Generate VLA in the OUV Booking ID window
  @OUV @OUVCal @OUV_045
  Scenario: User selects Auto Generate VLA in the OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA
    Then Logout from OUV

  #User selects Auto Generate VLA and saves the record in the OUV Booking ID window
  @OUV @OUVCal @OUV_046
  Scenario: User selects Auto Generate VLA and saves the record in the OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA
    And User saves the record and validate that an error message is displayed
    Then Logout from OUV

  #User selects Auto Generate VLA and enters Market,Template and Language for the Loan Agreement and saves the record in the OUV Booking ID window
  @OUV @OUVCal @OUV_047
  Scenario: User selects Auto Generate VLA and enters Market,Template and Language for the Loan Agreement and saves the record in the OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then Logout from OUV

  #User is able to add an OUV Vehicle Loan Agreement Name under Loan Agreements listed under Related Quick links
  @OUV @OUVCal @OUV_048
  Scenario: User is able to add an OUV Vehicle Loan Agreement Name under Loan Agreements
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then Logout from OUV

  #User cannot search for Frequent Drivers in New OUV Vehicle Loan Agreement
  @OUV @OUVCal @OUV_049
  Scenario: User cannot search for Frequent Drivers in New OUV Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User Navigate to OUV Vehicle Loan Agreements
    Then Verify that User cannot see Frequent Drivers in New OUV Vehicle Loan Agreement window
    Then Logout from OUV

  #User searches for Frequent Drivers in New OUV Vehicle Loan Agreement and saves the Agreement
  @OUV @OUVCal @OUV_050
  Scenario: User searches for Frequent Drivers in New OUV Vehicle Loan Agreement and saves the Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User Navigate to OUV Vehicle Loan Agreements
    #Funtional Error, no dropdown displays for Frequent Driver
    And User enters only Frequent Driver "OUV Calendar,N,2" and verify the error upon saves the Agreement
    Then Logout from OUV

  #User searches for Frequent Drivers and enters the driver information in New OUV Vehicle Loan Agreement and saves the Agreement
  @OUV @OUVCal @OUV_051
  Scenario: User searches for Frequent Drivers and enters the driver information in New OUV Vehicle Loan Agreement and saves the Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User Navigate to OUV Vehicle Loan Agreements
    And User enters only Frequent Driver "OUV Calendar,N,2" and Type of Driver "OUV Calendar,O,2" and saves VLA
    Then Logout from OUV

  #User creates an OUV Booking and approve it
  @OUV @OUVCal @OUV_052
  Scenario: User creates an OUV Booking and approve it
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Verify that OUV Booking status is Approved if Booking Justification "OUV Calendar,G,6"
    Then Logout from OUV

  #User verifies that after Submitting for Approval, the first approval is assigned to the 'Fleet Manager'
  @OUV @OUVCal @OUV_055
  Scenario: User verifies that after Submitting for Approval, the first approval is assigned to the 'Fleet Manager'
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    Then User verifies First level of approval is Fleet Manager from Booking Approvers List
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Logout from OUV

  #Need to approve
  @OUV @OUVCal @OUV_057
  Scenario: User verifies that when selecting Passout Type as Day the second approval is assigned to the Day Pass Approver (LL6)
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,5",Journey Type "OUV Calendar,J,5"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Validate that first approval is assigned to the Fleet Manager
    And Verify that the Day Pass Approver information as "Users,C,3"
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then verify that the Pending Approval is assigned to the Day Pass Approver "Users,C,3"
    Then Logout from OUV

  #User verifies that when selecting Passout Type as 'Weekend', the second approval is assigned to the 'Weekend Pass Approver (LL4)'
  @OUV @OUVCal @OUV_058
  Scenario: User verifies that after Submitting for Approval, the first approval is assigned to the 'Fleet Manager'
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Validate that first approval is assigned to the Fleet Manager
    Then Logout from OUV

  #User is able to Recall an approval from Approval History
  @OUV @OUVCal @OUV_059
  Scenario: User is able to Recall an approval from Approval History
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    Then User recalls an Approval
    Then Logout from OUV

  #User is able to validate the Booking Approval status changes from 'Pending' to 'Recalled' in the Approval History Tab after recalling an approval
  @OUV @OUVCal @OUV_060
  Scenario: User is able to validate the Booking Approval status changes from Pending to Recalled in the Approval History Tab after recalling an approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    Then User recalls an Approval
    And User validates that the Approval status changes to Recalled
    Then Logout from OUV

  #User is able to validate that a Recall Request was sent to the Approver
  @OUV @OUVCal @OUV_061
  Scenario: User is able to validate that a Recall Request was sent to the Approver
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then User recalls an Approval
    And User validates that the Approval status changes to Recalled
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    And User verifies OUV Booking status is Recalled
    Then Logout from OUV

  #User is able to validate that the Booking Approval Status changes from 'Booked Pending Approval' to 'New' after the Approval is recalled
  @OUV @OUVCal @OUV_062
  Scenario: User is able to validate that the Booking Approval Status changes from 'Booked Pending Approval' to 'New' after the Approval is recalled
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then User recalls an Approval
    And User validates that the Approval status changes to Recalled
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then User verify that the OUV Booking status moves to New from Booked Pending Approval
    Then Logout from OUV

  #User is able to view the Submit for Approval Button after recalling an approval
  @OUV @OUVCal @OUV_063
  Scenario: User is able to view the Submit for Approval Button after recalling an approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then User recalls an Approval
    And User validates that the Approval status changes to Recalled
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    Then User verify that the OUV Booking status moves to New from Booked Pending Approval
    And User submit an OUV Booking for an Approval after recalling
    Then Logout from OUV

  #"User is able to Reject an approval	"
  @OUV @OUVCal @OUV_064
  Scenario: User is able to Reject an approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User rejects the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently rejected OUV Booking
    And User navigates to Approval History
    Then Verify that OUV Booking status is Rejected if Booking Justification "OUV Calendar,G,6"
    Then Logout from OUV

  #User should be able to verify the owner name from OUV Booking ID window
  @OUV @OUVCal @OUV_129
  Scenario: User should be able to verify the owner name from OUV Booking ID window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User tries to change the owner as "Users,C,3"
    Then Logout from OUV

  #User should be able to see an error message if no VLA record is found when user submits the booking (Auto Generate VLA checkbox is selected) for approval
  @OUV @OUVCal @OUV_139
  Scenario: User should be able to see an error message if no VLA record is found when user submits the booking (Auto Generate VLA checkbox is selected) for approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then User submits the booking for Approval
    And verify the error message for no VLA record
    Then Logout from OUV

  #User should be able to reassign an approval request
  @OUV @OUVCal @OUV_140
  Scenario: User should be able to reassign an approval request
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    Then User reassign the approval request to "Users,C,4"
    Then Logout from OUV

  #User cannot generate document for any new OUV Booking, if document template section is blank
  @OUV @OUVCal @OUV_132
  Scenario: User cannot generate document for any new OUV Booking, if document template section is blank
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then User validates that document template section is blank
    And User clicks on generate document
    Then Validate that an error message is displayed
    Then Logout from OUV

  #User can cancel an OUV Booking from OUV Booking window
  @OUV @OUVCal @OUV_133
  Scenario: User can cancel an OUV Booking from OUV Booking window
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then User clicks on Cancel Booking
    And Validate that the Booking status is moved to cancelled
    Then Logout from OUV

  #User is unable to edit the details of OUV bookings when OUV booking status is in Booked Pending Approval status
  @OUV @OUVCal @OUV_134
  Scenario: User is unable to edit the details of OUV bookings when OUV booking status is at Booked Pending Approval
    Given Access the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,3"
    And select the OUV Booking
    Then User clicks on Edit Option
    And User tries to change the Booking Request Type
    Then Validate that user is not able to save the record
    Then Logout from OUV

  #User is able to see VLA number auto-populated in OUV Booking ID Page, after creating a new Vehicle Loan Agreement
  @OUV @OUVCal @OUV_141
  Scenario: User is able to see VLA number auto-populated in OUV Booking ID Page, after creating a new Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then Verify the VLA NUmber should be auto-populated in OUV booking ID page
    Then Logout from OUV

  #User is able to see VLA Status as 'Created' under Vehicle Loan Agreements for an Approved Booking
  @OUV @OUVCal @OUV_142
  Scenario: User is able to see VLA Status as 'Created' under Vehicle Loan Agreements for an Approved Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Verify the VLA status should be updated as created
    Then Logout from OUV

  #User is able to add permanent Accessories to reservation in New stage OUV booking
  @OUV @OUVCal @OUV_156
  Scenario: User is able to add permanent Accessories to reservation in New stage OUV booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then User navigate to reservations and select a reservation
    Then User navigate to Permanent Accessories and click on New
    And Fill Mandatory fields like Accessory with "OUV Bookings,C,2" and click on save
    Then Verify the accessory created
    Then Logout from OUV

  #User is able to delete the permanent Accessories in Reservation which is In New stage OUV booking
  @OUV @OUVCal @OUV_157
  Scenario: User is able to delete the permanent Accessories in Reservation which is In New stage OUV booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then User navigate to reservations and select a reservation
    Then User navigate to Permanent Accessories and click on New
    And Fill Mandatory fields like Accessory with "OUV Bookings,C,2" and click on save
    Then Click on drop down and select delete option
    Then Logout from OUV

  #User is not able to Submit a booking for Approval with a Booking Approval Status as 'Approved'
  @OUV @OUVCal @OUV_143
  Scenario: User is not able to Submit a booking for Approval with a Booking Approval Status as 'Approved'
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,4"
    And select the OUV Booking
    Then User selects submit for approval button
    And Validate that user is not able to submit the booking for approval
    Then Logout from OUV

  #User is able to edit the details for an OUV Vehicle Loan Agreement
  @OUV @OUVCal @OUV_145
  Scenario: User is able to edit the details for an OUV Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User Navigate to OUV Vehicle Loan Agreements
    And User edits the details for an OUV Vehicle Loan Agreement
    Then Logout from OUV

  #User is able to add an OUV Vehicle Loan Agreement Name under Loan Agreements by providing Drivers Information
  @OUV @OUVCal @OUV_144
  Scenario: User is able to add an OUV Vehicle Loan Agreement Name under Loan Agreements by providing Drivers Information
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User saves the VLA
    Then Verify all the details are autopopulated in the VLA Details Page with Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    Then Logout from OUV

  #User is able to add Insurance details to an OUV Vehicle Loan Agreement
  @OUV @OUVCal @OUV_146
  Scenario: User is able to add Insurance details to an OUV Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User enters all the Insurance details like Insurance Company "OUV Calendar,R,2" Insurance Policy Number "OUV Calendar,S,2" Vehicle Storage "OUV Calendar,T,2" and Driving Experience "OUV Calendar,U,2"
    And User saves the VLA
    Then Verify all the details are autopopulated in the VLA Details Page with Insurance Company "OUV Calendar,R,2" Insurance Policy Number "OUV Calendar,S,2" Vehicle Storage "OUV Calendar,T,2" and Driving Experience "OUV Calendar,U,2"
    Then Logout from OUV

  #User tries to add issue Keys to Reservation in Approved OUV booking
  @OUV @OUVCal @OUV_151
  Scenario: User tries to add issue Keys to Reservation in Approved OUV booking
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,4"
    And select the OUV Booking
    Then select Reservation quick link
    And select the Reservation name
    Then click on the Issue keys and enter number of keys as "OUV Bookings,B,2"
    When click on the save button
    And verify the displayed Reservation error message
    Then Logout from OUV

  #User tries to mark  Reservation as complete in Approved OUV booking
  @OUV @OUVCal @OUV_152
  Scenario: User tries to mark  Reservation as complete in Approved OUV booking
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,4"
    And select the OUV Booking
    Then select Reservation quick link
    And select the Reservation name
    Then click on the complete and enter number of keys as "OUV Bookings,B,2"
    When click on the save button
    And verify the displayed Reservation error message
    Then Logout from OUV

  #User tries to Add Temporary Accessories to reservations in 'Booked pending Approval' OUV booking
  @OUV @OUVCal @OUV_153
  Scenario: User tries to Add Temporary Accessories to reservations in Booked pending Approval OUV booking
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,3"
    And select the OUV Booking
    Then select Reservation quick link
    And select the Reservation name
    Then select Temporary Accessories quick link
    And click on New and fill the mandatory details like accessory "OUV Bookings,C,2"
    When save the new Temporary Accessories
    And verify the displayed Temporary Accessories error message
    Then Logout from OUV

  #User is able delete the OUV vehicle loan Agreement
  @OUV @OUVCal @OUV_158
  Scenario: User is able delete the OUV vehicle loan Agreement
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User Navigate to OUV Vehicle Loan Agreements
    And delete the OUV vehicle loan Agreement
    Then verify that user is able to delete vehicle loan Agreement
    Then Logout from OUV

  #User is able to add temporary Accessories to reservation in New stage OUV booking
  @OUV @OUVCal @OUV_159
  Scenario: User is able to add temporary Accessories to reservation in New stage OUV booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then navigate to the reservation and select Temporary Accessories quick link
    And click on New button and fill the mandatory details like accessory "OUV Bookings,C,2"
    When save the new Temporary Accessories
    Then verify that user is able to add new Temporary Accessories
    Then Logout from OUV

  #User is able to Clone the  temporary Accessories to reservation in New stage OUV booking
  @OUV @OUVCal @OUV_160
  Scenario: User is able to Clone the  temporary Accessories to reservation in New stage OUV booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then navigate to the reservation and select Temporary Accessories quick link
    And click on New button and fill the mandatory details like accessory "OUV Bookings,C,2"
    When save the new Temporary Accessories
    Then click on the Temporary Accessories and clone it
    Then navigate to the Temporary Accessories
    Then verify that user is able to add new Temporary Accessories
    Then Logout from OUV

  #User is able to add Multiple reservations to OUV booking with request type as Multiple Vehicle Booking
  @OUV @OUVCal @OUV_165
  Scenario: User is able to add Multiple reservations to OUV booking with request type as Multiple Vehicle Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,5",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then User navigates to OUV Calendar and searches for Vehicle
    And view the searched vehicles and select another vehicle which is available
    Then User saves the booking for Multiple reservations
    Then Logout from OUV

  #User tries to Clone the Reservation for OUV booking which Is having Booking request type as Single Vehicle Booking
  @OUV @OUVCal @OUV_161
  Scenario: User tries to Clone the Reservation for OUV booking which Is having Booking request type as Single Vehicle Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    Then navigate to the reservation and clone it
    And Verify the reservation clone error
    Then Logout from OUV

  #User tries to create a new reservation for same vehicle in between the reservation period
  @OUV @OUVCal @OUV_166
  Scenario: User tries to create a new reservation for same vehicle in between the reservation period
    Given Access the OUV Portal with user "Users,D,4" and password as "Users,E,4"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    And Select the time slot next to previous booking
    Then User saves an OUV Booking including other location with Booking start time Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,5",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Verify the error message displayed and click on close and cancel
    Then Logout from OUV

  #User tries to associate another OUV booking in an existing OUV Booking
  @OUV @OUVCal @OUV_216 @batch200
  Scenario: User tries to associate another OUV booking in an existing OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Bookings
    And User searches for an OUV Booking with status "OUV Bookings,A,2"
    And select the OUV Booking
    Then click on edit associate booking
    When booking information is entered click save
    And verify the error message for associate booking
    Then Logout from OUV

  #User is able to Add a new Booking with repair type
  @OUV @OUVCal @OUV_154
  Scenario: User is able to Add a new Booking with repair type
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then Select Repair option as booking type from drop down Booking Type "OUV Calendar,W,2"
    Then Fill out all Mandatory fields in Booking Window Booking Quick Reference "OUV Calendar,F,7" Internal JLR "OUV Calendar,X,2" Other Location "OUV Calendar,Y,2" Email Id "OUV Calendar,P,3" Driverone "OUV Calendar,Z,2" Drivertwo "OUV Calendar,AA,2"
    Then Click on Save, and user will be navigated back to Calendar
    Then Click on newly created OUV Booking record in the Reservation Calendar for the pop up to appear again
    Then User navigates to OUV Booking ID
    Then Logout from OUV

  #User tries to collect all the driver information for an OUV Booking
  @OUV @OUVCal @OUV_219 @batch200
  Scenario: User tries to collect all the driver information for an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    And User clicks on VLA and verify details are autopopulated like Driver "OUV Bookings,S,2" First Name "OUV Bookings,T,2" Last Name "OUV Bookings,U,2" Phone "OUV Bookings,V,2"
    Then Logout from OUV

  #User tries to give Preparation/Transportation End date as greater than Reservation End date
  @OUV @OUVCal @OUV_155
  Scenario: User tries to give Preparation/Transportation End date as greater than Reservation End date
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    Then Select a booking Slot in Calendar
    Then Click on Check box under includes Preparation/Transportation
    Then Select date which is greater than Reservation end date for OUV booking End Date
    Then Fill out all mandatory details Booking Quick Reference "OUV Calendar,F,7" Reason For Booking "OUV Calendar,AB,2" Booking Justification "OUV Calendar,G,2" Booking request type "OUV Calendar,H,2" Passout Type "OUV Calendar,I,5" Journey Type "OUV Calendar,J,5" Internal JLR "OUV Calendar,X,2" Other Location "OUV Calendar,Y,2" Email Id "OUV Calendar,P,3" Driverone "OUV Calendar,Z,2"
    Then Click on save button
    Then Verifying error message
    Then Logout from OUV

  #User cannot add more than one Vehicle Loan Agreements in an OUV Booking
  @OUV @OUVCal @OUV_220 @batch200
  Scenario: User cannot add more than one Vehicle Loan Agreements in an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    Then User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User clicks on Save and New VLA
    And User adds another VLA by entering all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,3" Last Name "OUV Calendar,O,3" and Email "OUV Calendar,P,4"
    And User saves the VLA
    Then Validate the error message displayed for creating a New VLA
    Then Logout from OUV

  #User can clone an OUV Booking at any stage
  @OUV @OUVBooking @OUV_232 @batch200
  Scenario: User can clone an OUV Booking at any stage
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User tries to Clone an OUV Booking
    Then Navigate to Reservation
    Then Validate that no Reservation is created for the cloned Booking
    Then Logout from OUV

  #User can edit OUV Booking details of cloned OUV Booking
  @OUV @OUVBooking @OUV_233 @batch200
  Scenario: User can edit OUV Booking details of cloned OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User tries to Clone an OUV Booking
    Then User edit the details of the cloned Booking
    And User Save the changes
    Then Logout from OUV

  #User cannot submit cloned OUV Booking for an approval
  @OUV @OUVBooking @OUV_234 @batch200
  Scenario: User cannot submit cloned OUV Booking for an approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User tries to Clone an OUV Booking
    Then User tries to submit the booking for Approval
    Then Logout from OUV

  #User need to verify the start date and time before performing check out
  @OUV @OUV_168 @OUVBatch
  Scenario: User need to verify the start date and time before performing check out
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then Verify that start date and time is before end date and time
    Then Logout from OUV

  #User cannot do CheckIn/CheckOut if the End Time of the Booking is completed as per local time zone
  @OUV @OUV_228 @OUVBatch
  Scenario: User cannot do CheckIn/CheckOut if the End Time of the Booking is completed as per local time zone
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User click on CheckOut CheckIn
    Then checkbox the checkout and click on continue
    And Verify the error message displayed to checkout
    Then Logout from OUV

  #User verifies that the newly created OUV Booking status changed to Pending Approval if Booking Justification is selected as Event/Sales.
  @OUV @OUV_198 @OUVBatch
  Scenario: User verifies that the newly created OUV Booking status changed to Pending Approval if Booking Justification is selected as Event/Sales.
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    Then Verify that Approval status is set to Booking Pending Approval
    Then Logout from OUV

  #User is not able to edit the fields for a newly created OUV Booking if Booking Justification is selected as Event/Sales activity and Request Type as Single Vehicle Booking
  @OUV @OUV_199 @OUVBatch
  Scenario: User is not able to edit the fields for a newly created OUV Booking if Booking Justification is selected as Event/Sales activity and Request Type as Single Vehicle Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    Then Verify that Approval status is set to Booking Pending Approval
    When User edit the Booking Quick reference "OUV Calendar,F,2" and save it
    Then Verify the error message displayed to edit
    Then Logout from OUV

  #User sends S-Sign Envelop for E-sign for any approved OUV Booking
  @OUV @OUV_248 @batch200
  Scenario: User sends S-Sign Envelop for E-sign for any approved OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    And User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User saves the VLA
    Then User view details of newly created OUV Vehicle Loan Agreement
    And User enters all vehicle user information like Company "OUV VLA,E,2" Phone "OUV VLA,G,2" Date of Birth "OUV VLA,H,2" Occupation "OUV VLA,I,2" Address "OUV VLA,K,2" Postcode "OUV VLA,N,2" Passport Number "OUV VLA,O,2" Driver License Number "OUV VLA,P,2" Country of issue "OUV VLA,L,2" License Start Date "OUV VLA,I,2" License End Date "OUV VLA,J,2" Insured By "OUV VLA,Q.2" Insurance company "OUV VLA,M,2"
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Verify that OUV Booking status is Approved if Booking Justification "OUV Calendar,G,6"
    And Verify that the document in S-Sign section is available for E-Sign
    Then User sends the Sign Request

  #User can view Encrypted Company & Encrypted Full Name field under Driver/Vehicles User section for an approved OUV Booking
  @OUV @OUV_249 @batch200
  Scenario: User can view Encrypted Company & Encrypted Full Name field under Driver/Vehicle User section for an approved OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    And User Navigate to OUV Vehicle Loan Agreements
    And User enter all the driver information like Type of driver "OUV Calendar,Q,2" First Name "OUV Calendar,N,2" Last Name "OUV Calendar,O,2" and Email "OUV Calendar,P,2"
    And User saves the VLA
    Then User view details of newly created OUV Vehicle Loan Agreement
    And User enters all vehicle user information like Company "OUV VLA,E,2" Phone "OUV VLA,G,2" Date of Birth "OUV VLA,H,2" Occupation "OUV VLA,I,2" Address "OUV VLA,K,2" Postcode "OUV VLA,N,2" Passport Number "OUV VLA,O,2" Driver License Number "OUV VLA,P,2" Country of issue "OUV VLA,L,2" License Start Date "OUV VLA,I,2" License End Date "OUV VLA,J,2" Insured By "OUV VLA,Q.2" Insurance company "OUV VLA,M,2"
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    Then Verify that OUV Booking status is Approved if Booking Justification "OUV Calendar,G,6"
    And Verify that the document in S-Sign section is available for E-Sign
    Then User sends the Sign Request

  #User is able to view the OUV Booking for Preparation/Transportation type highlighted in two different colours
  @OUV @OUV_Phase2_027 @OUVBatch
  Scenario: User is able to view the OUV Booking for Preparation/Transportation type highlighted in two different colours
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User Navigate to calender tab
    And Verify that Transportation reservation is highlighted in purple color
    And the second reservation is highlighted in green color
    Then Logout from OUV

  #User is able to create a new OUV Booking with Temporary Accessories
  @OUV @OUVBooking @OUV_Phase2_028
  Scenario: User is able to create a new OUV Booking with Temporary Accessories
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,9" reason Of Booking "OUV Calendar,AB,4" Booking Justification "OUV Calendar,G,4" Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4" Internal JLR Contact "OUV Calendar,V,2" email "OUV Calendar,P,4" Location "OUV Calendar,K,2" and Driver "OUV Calendar,AG,2"
    Then enter the Temporary Accessories as "OUV Bookings,C,2"  in OUV Booking
    Then enter the Temporary Accessories as "OUV Bookings,C,3"  in OUV Booking
    And click on save booking option
    Then Logout from OUV

  #User is able to verify the Temporary Accessories listed under Reservation for an OUV Booking
  @OUV @OUVBooking @OUV_Phase2_029
  Scenario: User is able to verify the Temporary Accessories listed under Reservation for an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And view the searched vehicles and select a vehicle which is available
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,9" reason Of Booking "OUV Calendar,AB,4" Booking Justification "OUV Calendar,G,4" Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4" Internal JLR Contact "OUV Calendar,V,2" email "OUV Calendar,P,4" Location "OUV Calendar,K,2" and Driver "OUV Calendar,AG,2"
    Then enter the Temporary Accessories as "OUV Bookings,C,2"  in OUV Booking
    Then enter the Temporary Accessories as "OUV Bookings,C,3"  in OUV Booking
    And click on save booking option
    Then User clicks on Booking Quick Reference "OUV Calendar,F,9" and navigates to OUV Booking ID
    Then navigate to the reservation and select Temporary Accessories quick link
    Then verify that user is able to add two new Temporary Accessories
    Then Logout from OUV

  #User is able to create and approve a booking with Transportation time
  @OUV @OUVCal @OUV_Phase2_023
  Scenario: User is able to create and approve a booking with Transportation time
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And verify the approved status
    Then Logout from OUV

  #User is able to verify the Permanent Accessories listed under Reservation for an OUV Booking
  @OUV @OUVCal @OUV_Phase2_033
  Scenario: User is able to verify the Permanent Accessories listed under Reservation for an OUV Booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    And fill permanent accessories with "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    Then User navigate to reservations and select a reservation
    Then User navigate to Permanent Accessories
    Then Verify the accessory created
    Then Logout from OUV

  #User is able to view the Accessories in an OUV Vehicle for an Approved Booking with Permanent Accessories
  @OUV @OUVCal @OUV_Phase2_034
  Scenario: User is able to view the Accessories in an OUV Vehicle for an Approved Booking with Permanent Accessories
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    And fill permanent accessories with "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And verify the approved status
    Then Navigate to booking window
    Then User navigate to reservations and select a reservation
    Then User navigate to Permanent Accessories
    Then Verify the accessory created
    Then Logout from OUV

  #User is able to create a Multiple Vehicle Booking with all the Accessories
  @OUV @OUV_Phase2_035
  Scenario: User is able to create a Multiple Vehicle Booking with all the Accessories
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And User select two temporary Accessories "OUV Calendar,AK,2" and "OUV Calendar,AK,3"
    Then User select three Service Reservations "OUV Calendar,AJ,2" and "OUV Calendar,AJ,3" and "OUV Calendar,AJ,4"
    And User select two Permanent accessories "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User set local system date and time for the reservation to calendar "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    Then Logout from OUV

  #User is able to view the list for all OUV Frequent Drivers in OUV Frequent Drivers tab
  @OUV @OUV_Phase2_061
  Scenario: User is able to view the list for all OUV Frequent Drivers in OUV Frequent Drivers tab
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    Then Verify that User can view All OUV Frequent drivers
    Then Logout from OUV

  #User is able to view the OUV Frequent Driver list of Benelux Market if user is a member of Benelux Market Team
  @OUV @OUV_Phase2_069
  Scenario: User is able to view the OUV Frequent Driver list of Benelux Market if user is a member of Benelux Market Team
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    Then Verify that User can view All OUV Frequent drivers
    And User opens any Frequent Driver from the list
    Then Verify that User is able to view OUV Frequent Driver of "OUV_Frequent_Driver,A,2" Market Team
    Then Logout from OUV

  #User is not able to view the OUV Frequent Driver list of Italian Market if user is a member of Benelux Market Team
  @OUV @OUV_Phase2_070
  Scenario: User is not able to view the OUV Frequent Driver list of Italian Market if user is a member of Benelux Market Team
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    Then Verify that User can view All OUV Frequent drivers
    And User searches "OUV_Frequent_Driver,B,2" Frequent Driver from the list
    Then Verify that No result found for searched Frequent Driver
    Then Logout from OUV

  #User is able to verify OUV Frequent Driver owner and validate the details in the run report Benelux Users Report
  @OUV @OUV_Phase2_071
  Scenario: User is able to verify OUV Frequent Driver owner and validate the details in the run report Benelux Users Report
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    And Verify that User can view OUV Frequent drivers Owner
    Then User verifies the Owner Name from the list
    And User Navigates to Reports
    Then User opens OUV Benelux Reports folder
    And User opens Benelux Users Report
    Then Verify Owner Name from Benelux Users Report
    Then Logout from OUV

  #User is able to approve a Registration Request
  @OUV @OUV_Phase2_010
  Scenario: User is able to approve a Registration Request
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    Then User Navigate to OUV Vehicle
    And User search the Vehicle "OUV vehicles,A,7"
    When User selects vehicle
    And User checkbox the Registration Required
    Then Verify the Registration Approval Status is changed to Pending Approval
    And User navigates to Approval History
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Verify the Registration Approval Status is changed to Approved
    Then Logout from OUV

  #User is able to validate the status as Confirmed for the two Reservations for an approved Booking with Transportation time
  @OUV @OUV_Phase2_024
  Scenario: User is able to validate the status as Confirmed for the two Reservations for an approved Booking with Transportation time
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then Verify that two Reservations are created on the same vehicle
    And Verify that status  is Confirmed for both the Reservations
    Then Logout from OUV

  #User is able to view the booking details of an OUV Booking with transportation time in Reservation Calendar
  @OUV @OUV_Phase2_025
  Scenario: User is able to view the booking details of an OUV Booking with transportation time in Reservation Calendar
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User Navigate to calender tab
    Then Verify that two Reservations are created in the Calendar for the booking "OUV Calendar,F,7"
    Then Logout from OUV

  #User is able to verify the start time and end time for the two reservations created for an OUV Booking with Transportation time
  @OUV @OUV_Phase2_026
  Scenario: User is able to verify the start time and end time for the two reservations created for an OUV Booking with Transportation time
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot for selected vehicle
    Then User Includes Preparation or transportation
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,7",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,7"
    Then User navigates to OUV Booking ID
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And select the first booking from the notification menu
    Then User approves the Booking
    Then Login with Approver as "Users,D,7" and password as "Users,E,7"
    And select the first booking from the notification menu
    Then User approves the Booking
    And User navigate back to booking number
    And Navigate to Reservations Tab
    Then User select the vehicle
    When User Navigate to calender tab
    And Navigate to Reservations Tab
    And Navigate to first reservation
    Then verify the start time of booking and end time of transportation
    And Navigate to Second reservation
    Then verify the start time of booking and end time of transportation
    Then Logout from OUV

  #User is able to create another booking in a multiple vehicle booking type and add the accessories to the booking
  @OUV @OUV_Phase2_036
  Scenario: User is able to create another booking in a multiple vehicle booking type and add the accessories to the booking
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And User select two temporary Accessories "OUV Calendar,AK,2" and "OUV Calendar,AK,3"
    Then User select three Service Reservations "OUV Calendar,AJ,2" and "OUV Calendar,AJ,3" and "OUV Calendar,AJ,4"
    And User select two Permanent accessories "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User set local system date and time for the reservation "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    Then User navigates to OUV Booking ID
    When User Navigate to calender tab
    And Search vehicles after Check boxing the Reserved Vehicles
    And select a slot of week for selected vehicle
    Then User select three Service Reservations "OUV Calendar,AJ,4" and "OUV Calendar,AJ,5" and "OUV Calendar,AJ,2"
    And User select two Permanent accessories "OUV Calendar,AL,4" and "OUV Calendar,AL,5"
    Then User save the Reservation
    Then Logout from OUV

  #User is not able to search for Frequent Drivers field in an OUV Vehicle Loan Agreement
  @OUV @OUV_Phase2_059
  Scenario: User is not able to search for Frequent Drivers field in an OUV Vehicle Loan Agreement
    Given Access the OUV Portal with user "Users,D,7" and password as "Users,E,7"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    And User Navigate to OUV Vehicle Loan Agreements
    Then Verify that User cannot see Frequent Drivers in New OUV Vehicle Loan Agreement window
    Then Logout from OUV

  #User is not able to add items for OUV Frequent Drivers in OUV App Navigation Items
  @OUV @OUV_Phase2_060
  Scenario: User is not able to add items for OUV Frequent Drivers in OUV App Navigation Items
    Given Access the OUV Portal with user "Users,D,7" and password as "Users,E,7"
    And User clicks on OUV App navigation items
    Then User clicks on Add more items and searches for "OUV Frequent Drivers" under All section
    Then Verify the OUV Frequent Drivers is not available in OUV App navigation items
    Then Logout from OUV

  #User is only able to view the Annual Market Summary for the markets in the Benelux Market Team
  @OUV @OUV_Phase2_072
  Scenario: User is only able to view the Annual Market Summary for the markets in the Benelux Market Team
    Given Access the OUV Portal with user "Users,D,5" and password as "Users,E,5"
    And User navigates to Annual Market Summary
    Then User selects All from list view and verify AMS for markets Netherands and Belgium
    Then Logout from OUV

  #User is able to edit the Annual Budget Threshold for an Annual Market Summary
  @OUV @OUV_Phase2_073
  Scenario: User is able to edit the Annual Budget Threshold for an Annual Market Summary
    Given Access the OUV Portal with user "Users,D,17" and password as "Users,E,17"
    And User navigates to Annual Market Summary
    Then User selects Benelux from list view
    Then Select an AMS for fiscal year "2021/2022"
    Then Edit the field for Annual budget threshold and save
    Then Logout from OUV

  #User is not able to edit the Annual Budget Threshold for an Annual Market Summary
  @OUV @OUV_Phase2_074
  Scenario: User is not able to edit the Annual Budget Threshold for an Annual Market Summary
    Given Access the OUV Portal with user "Users,D,5" and password as "Users,E,5"
    And User navigates to Annual Market Summary
    Then User selects All from list view and verify AMS for markets Netherands and Belgium
    Then Select an AMS for fiscal year "2021/2022"
    Then Verify user cannot edit the Annual Budget Threshold field
    Then Logout from OUV
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to Annual Market Summary
    Then User selects All from list view and verify AMS for markets Netherands and Belgium
    Then Select an AMS for fiscal year "2021/2022"
    Then Verify Annual Budget Threshold field is not present under Details section
    Then Logout from OUV

  #User verifies the accessories for the two Reservations created for Multiple Vehicle Booking type
  @OUV @OUV_Phase2_037
  Scenario: User verifies the accessories for the two Reservations created for Multiple Vehicle Booking type
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And User select two temporary Accessories "OUV Calendar,AK,2" and "OUV Calendar,AK,3"
    Then User select three Service Reservations "OUV Calendar,AJ,2" and "OUV Calendar,AJ,3" and "OUV Calendar,AJ,4"
    And User select two Permanent accessories "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User set local system date and time for the reservation to calendarss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    Then User navigates to OUV Booking ID
    When User Navigate to calender tab
    And Search vehicles after Check boxing the Reserved Vehicles
    And select a slot of week for selected vehicle
    Then User select three Service Reservations "OUV Calendar,AJ,4" and "OUV Calendar,AJ,5" and "OUV Calendar,AJ,2"
    And User select two Permanent accessories "OUV Calendar,AL,4" and "OUV Calendar,AL,5"
    Then User save the Reservation
    Then Navigate to Reservation
    And Select the first Reservation
    Then User navigate to Permanent Accessories
    And Verify the accessories created
    Then User navigate to Temporary Accessories
    And Verify the accessories created
    Then User navigate to Service Reservations
    And Verify the accessories created
    Then Navigate back to Booking
    Then Navigate to Reservation
    And Select the second Reservation
    Then User navigate to Permanent Accessories
    And Verify the accessories created
    Then Logout from OUV

  #User is able to verify that only Permanent Accessories gets copied to the second reservation created for Multiple Vehicle Booking Type
  @OUV @OUV_Phase2_038
  Scenario: User is able to verify that only Permanent Accessories gets copied to the second reservation created for Multiple Vehicle Booking Type
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And select an availble slot of week for selected vehicle
    Then User saves an OUV Booking including other location and Booking Quick Reference "OUV Calendar,F,8",ReasonOfBooking "OUV Calendar,F,8",Booking Justification "OUV Calendar,G,2",Booking Request Type "OUV Calendar,H,5",internalJLRCnt "OUV Calendar,V,2",internalJLREmail "OUV Calendar,P,4",driver "OUV Calendar,AG,2", Other Location "OUV Calendar,K,2", Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    And User select two temporary Accessories "OUV Calendar,AK,2" and "OUV Calendar,AK,3"
    Then User select three Service Reservations "OUV Calendar,AJ,2" and "OUV Calendar,AJ,3" and "OUV Calendar,AJ,4"
    And User select two Permanent accessories "OUV Calendar,AL,2" and "OUV Calendar,AL,3"
    Then User set local system date and time for the reservation to calendarss "OUV Calendar,AH,2" Booking "OUV Calendar,F,8"
    Then User navigates to OUV Booking ID
    When User Navigate to calender tab
    And Search vehicles after Check boxing the Reserved Vehicles
    And select a slot of week for selected vehicle
    Then User select three Service Reservations "OUV Calendar,AJ,4" and "OUV Calendar,AJ,5" and "OUV Calendar,AJ,2"
    And User select two Permanent accessories "OUV Calendar,AL,4" and "OUV Calendar,AL,5"
    Then User save the Reservation
    Then Navigate to Reservation
    And Select the first Reservation
    Then User navigate to Permanent Accessories
    And Verify the accessories created
    Then User navigate to Temporary Accessories
    And Verify the accessories created
    Then User navigate to Service Reservations
    And Verify the accessories created
    Then Navigate back to Booking
    Then Navigate to Reservation
    And Select the second Reservation
    Then User navigate to Permanent Accessories
    And Verify the accessories created
    Then User navigate to Temporary Accessories
    And Verify no accessory is created
    Then User navigate to Service Reservations
    And Verify no accessory is created
    Then Logout from OUV

  #User is able to update the status from 'In Progress' to 'Submitted'  for an OUV Forecast Snapshot for an Annual Market Summary
  @OUV @OUV_Phase2_075
  Scenario: User is able to update the status from 'In Progress' to 'Submitted'  for an OUV Forecast Snapshot for an Annual Market Summary
    Given Access the OUV Portal with user "Users,D,5" and password as "Users,E,5"
    And User navigate to Annual Market Summary
    Then User selects an AMS with Fiscal Year "AMS,A,2"
    And Navigate to OUV Forecast Snapshot
    Then User selects an OUV Forecast Snapshot with status "AMS,B,2"
    And User update the status to "AMS,B,3" and saves the record
    And Verify that user cannot edit the status again
    Then Logout from OUV
    Given Access the OUV Portal with user "Users,D,17" and password as "Users,E,17"
    And User navigate to Annual Market Summary
    Then User selects an AMS with Fiscal Year "AMS,A,2"
    And Navigate to OUV Forecast Snapshots
    Then User selects an OUV Forecast Snapshot with status "AMS,B,3"
    And User update the status to "AMS,B,2" and saves the record

  #User is able to view the booking ID requested for approval listed under Items to Approve tab for an Approver
  @OUV @OUV_135
  Scenario: User is able to view the booking ID requested for approval listed under Items to Approve tab for an Approver
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,5",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,4",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,4"
    Then User navigates to OUV Booking ID
    And User selects Auto Generate VLA and enters details for Loan Agreement and saves the record
    And User Navigate to OUV Vehicle Loan Agreements
    And User enters Frequent Driver as "OUV Calendar,N,4" and Type of Driver as "OUV Calendar,Q,2" and saves VLA
    Then User submits the booking for Approval
    Then Logout from OUV
    Given Access the OUV Portal with user "Users,D,3" and password as "Users,E,3"
    Then Validate that the Requested Approval will be listed on the Home Page under Items to Approve Tab
    Then Logout from OUV

  #User is able to view the list for all OUV Frequent Drivers in OUV Frequent Drivers tab
  @OUV @OUV_Phase2_061
  Scenario: User is able to view the list for all OUV Frequent Drivers in OUV Frequent Drivers tab
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    Then Verify that User can view All OUV Frequent drivers
    Then Logout from OUV

  #User is able to view the OUV Frequent Driver list of Benelux Market if user is a member of Benelux Market Team
  @OUV @OUV_Phase2_069
  Scenario: User is able to view the OUV Frequent Driver list of Benelux Market if user is a member of Benelux Market Team
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    Then Verify that User can view All OUV Frequent drivers
    And User opens any Frequent Driver from the list
    Then Verify that User is able to view OUV Frequent Driver of "OUV_Frequent_Driver,A,2" Market Team
    Then Logout from OUV

  #User is not able to view the OUV Frequent Driver list of Italian Market if user is a member of Benelux Market Team
  @OUV @OUV_Phase2_070
  Scenario: User is not able to view the OUV Frequent Driver list of Italian Market if user is a member of Benelux Market Team
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    Then Verify that User can view All OUV Frequent drivers
    And User searches "OUV_Frequent_Driver,B,2" Frequent Driver from the list
    Then Verify that No result found for searched Frequent Driver
    Then Logout from OUV

  #User is able to verify OUV Frequent Driver owner and validate the details in the run report Benelux Users Report
  @OUV @OUV_Phase2_071
  Scenario: User is able to verify OUV Frequent Driver owner and validate the details in the run report Benelux Users Report
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Frequent Driver tab
    And Verify that User can view OUV Frequent drivers Owner
    Then User verifies the Owner Name from the list
    And User Navigates to Reports
    Then User opens OUV Benelux Reports folder
    And User opens Benelux Users Report
    Then Verify Owner Name from Benelux Users Report
    Then Logout from OUV

  #User is able to complete the reservation after doing CheckIn & Close
  @OUV @OUV_056 @OUVBatch
  Scenario: User verifies that when selecting Passout Type as 'Overnight', the second approval is assigned to the 'Overnight Pass Approver (LL5)'
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,6",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,4",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And Verify that the status of OUV Booking based on Booking Justification "OUV Calendar,G,6" FrequentDriver "OUV Calendar,N,2" DriverType "OUV Calendar,O,2" EmailId "OUV Calendar,P,2"
    And User navigates to Approval History
    And Validate that first approval is assigned to the Fleet Manager
    Then Login with Fleet Manager as "Users,D,3" and password as "Users,E,3"
    And Select the Booking from the Notifications Menu
    Then User approves the Booking
    Then Login with OUV Booking Owner with user "Users,D,2" and password as "Users,E,2"
    And User navigates to OUV Booking menu
    And Open recently approved OUV Booking
    And User navigates to Approval History
    And Verify that Second Level of Approval is Pass Manager for PassoutType "OUV Calendar,I.4"
    Then Logout from OUV

  #User verifies the Booking Approval Status as 'Booked Pending Approval' in the OUV Booking ID window after submitting for Approval
  @OUV @OUVCal @OUV_053
  Scenario: User verifies the Booking Approval Status as 'Booked Pending Approval' in the OUV Booking ID window after submitting for Approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Verify OUV Booking status is Booked Pending Approval
    Then Logout from OUV

  #User verifies the status as 'Submitted' in Approval History Tab under Related Quick Links after submitting it for approval
  @OUV @OUVCal @OUV_054
  Scenario: User verifies the status as 'Submitted' in Approval History Tab under Related Quick Links after submitting it for approval
    Given Access the OUV Portal with user "Users,D,2" and password as "Users,E,2"
    And User navigate to OUV Calendar
    Then User searches Vehicle by selecting Market "OUV Calendar,A,2" and Fleet "OUV Calendar,M,2"
    And View all searched Vehicle and select an available vehicle
    Then User saves an OUV Booking including other location with Booking Quick Reference "OUV Calendar,F,6",Booking Justification "OUV Calendar,G,5",Booking Request Type "OUV Calendar,H,6",Passout Type "OUV Calendar,I,6",Journey Type "OUV Calendar,J,6"
    Then User navigates to OUV Booking ID
    And User adds an OUV Vehicle Loan Agreement
    Then User submits the booking for Approval
    And Verify OUV Booking status is Booked Pending Approval
    And User navigates to Approval History
    Then Verify that status of OUV Booking as submitted
    Then Logout from OUV
