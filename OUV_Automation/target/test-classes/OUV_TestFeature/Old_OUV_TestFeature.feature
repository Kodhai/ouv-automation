Feature: Validate OUV Scenarios

  @OUVBooking @OUV
  Scenario: Make Reservation from OUV Calender
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search vehicle for fleet
    And Search Vehicle by giving Fleet "OUV Calendar,A,2"
    And The entry shows in Reservation Calendar in Days,Weeks and Months
    When Select vehicle and week for Reservation
    Then Make Reservation with required details like Start Date "OUV Calendar,B,2" End Date "OUV Calendar,C,2" Booking Quick Reference "OUV Calendar,D,2", Booking Justification "OUV Calendar,E,2", Booking Request Type "OUV Calendar,F,2", Passout Type "OUV Calendar,G,2", Journey Type "OUV Calendar,H,2", Location "OUV Calendar,I,2"
    And Save the Reservation for "OUV Calendar,D,2"
    Then Navigate to Repair menu and save the reservation
    And Validate the entry in the OUV Calendar for "OUV Calendar,D,2"
    Then Open Booking details using OUV Booking link
    Then Logout from OUV Portal

  #Below scenario is used to Search vehicle for Reservation from OUV Calendar with end date prior start date
  @OUVNegative_1
  Scenario: Search vehicle for Reservation from OUV Calendar with end date prior start date
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search Vehicle by giving Fleet "OUV Calendar,A,3" along with Start Date as tomorrow and End Date as today
    Then verify the displayed booking end date error message
    Then Logout from OUV Portal

  #Below scenario is used to Search vehicle for Reservation from OUV Calendar with start date prior to todays date
  @OUVNegative_2
  Scenario: Search vehicle for Reservation from OUV Calendar with start date prior to todays date
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search Vehicle by giving Fleet "OUV Calendar,A,3" along with Start Date prior to todays date
    Then verify the displayed booking start date error message
    Then Logout from OUV Portal

  #Below scenario is used to Search vehicle for Reservation from OUV Calendar with Market and Fleet being of different locations
  @OUVNegative_3
  Scenario: Search vehicle for Reservation from OUV Calendar with Market and Fleet being of different locations
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search Vehicle by giving Market as "OUV Calendar,K,3" and Fleet as "OUV Calendar,A,3"
    Then verify the displayed record error message
    Then Logout from OUV Portal

  #Below scenario is used to Search vehicle for Reservation from OUV Calendar while excluding Market information
  @OUVNegative_4
  Scenario: Search vehicle for Reservation from OUV Calendar while excluding Market information
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search Vehicle by giving Fleet as "OUV Calendar,A,3" while excluding Market information
    Then verify the displayed record error message
    Then Logout from OUV Portal

  #Below scenario is used to enter email in invalid format when user creates a Reservation from OUV Calendar
  @OUVNegative_6
  Scenario: User enters email in invalid format when user creates a Reservation from OUV Calendar
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search Vehicle by giving Fleet "OUV Calendar,A,3"
    And The entry shows in Reservation Calendar in Days,Weeks and Months
    When Select vehicle and week for Reservation
    Then Make Reservation with invalid email format
    And verify the displayed format error message
    Then Logout from OUV Portal

  #Below scenario is used to creates a Reservation from OUV Calendar without entering any driver information
  @OUVNegative_7
  Scenario: User do not enter driver information when user creates a Reservation from OUV Calendar
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search Vehicle by giving Fleet "OUV Calendar,A,3"
    And The entry shows in Reservation Calendar in Days,Weeks and Months
    When Select vehicle and week for Reservation
    Then Make Reservation with required details like Start Date "OUV Calendar,B,2" End Date "OUV Calendar,C,2" Booking Quick Reference "OUV Calendar,D,2", Booking Justification "OUV Calendar,E,2", Booking Request Type "OUV Calendar,F,2", Passout Type "OUV Calendar,G,2", Journey Type "OUV Calendar,H,2", Location "OUV Calendar,I,2"
    And clear the entered driver information
    Then verify the displayed driver error
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender using Incorrect VIN
  @OUV @OUV_Negative1
  Scenario: Searching a vehicle from OUV Calender	using Incorrect VIN
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect VIN "SALEA7BU6L2000938" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender using Incorrect Primary Location
  @OUV @OUV_Negative2
  Scenario: Searching a vehicle from OUV Calender	using Incorrect Primary Location
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect Primary Location as "Australia" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender using Incorrect Model Description
  @OUV @OUV_Negative3
  Scenario: Searching a vehicle from OUV Calender	using Incorrect Model Description
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect Model Description as "Freelander" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender using Incorrect Register Plate
  @OUV @OUV_Negative4
  Scenario: Searching a vehicle from OUV Calender	using Incorrect Register Plate
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect Register Plate Number as "BJ67UMP" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender	for all Vehicle Categories
  @OUV @OUV_Negative5
  Scenario: Searching a vehicle from OUV Calender	for all Vehicle Categories
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Selecting All Vehicle Categories to display
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the End to End scenario for Searching a vehicle from OUV Calender	for all Vehicle Categories
  @OUV @OUV_NegativeBatch
  Scenario: Searching a vehicle from OUV Calender	using Incorrect Data
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect VIN "SALEA7BU6L2000938" for searching vehicle in OUV Calendar
    And Enter Incorrect Primary Location as "Australia" for searching vehicle in OUV Calendar
    And Enter Incorrect Model Description as "Freelander" for searching vehicle in OUV Calendar
    And Enter Incorrect Register Plate Number as "BJ67UMP" for searching vehicle in OUV Calendar
    And Selecting All Vehicle Categories to display
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender	using Incorrect Model Year Description
  @OUV @OUV_Negative6
  Scenario: Searching a vehicle from OUV Calender	for Incorrect Model Year Description
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Future Model Description as "2021" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  @OUVNegative_5
  Scenario: User saves a Reservation from OUV Calender while excluding all the mandatory information
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search Vehicle by giving Fleet "OUV Calendar,A,3"
    And The entry shows in Reservation Calendar in Days,Weeks and Months
    When Select vehicle and week for Reservation
    And Save the Reservation
    Then verify the displayed Complete this field error message
    Then Logout from OUV Portal

  @OUVNegative_E2E
  Scenario: Negative end-to-end scenarios
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search Vehicle by giving Fleet "OUV Calendar,A,3" along with Start Date as tomorrow and End Date as today
    Then verify the displayed booking end date error message
    And Search Vehicle by giving Fleet "OUV Calendar,A,3" along with Start Date prior to todays date
    Then verify the displayed booking start date error message
    And Search Vehicle by giving Market as "OUV Calendar,K,3" and Fleet as "OUV Calendar,A,3"
    Then verify the displayed record error message
    And Search Vehicle by giving Fleet as "OUV Calendar,A,3" while excluding Market information
    Then verify the displayed record error message
    And Search Vehicle by giving Fleet "OUV Calendar,A,3"
    And The entry shows in Reservation Calendar in Days,Weeks and Months
    When Select vehicle and week for Reservation
    And Save the Reservation
    Then verify the displayed Complete this field error message
    Then Logout from OUV Portal

  #User enters email in invalid formate when user creates a Reservation from OUV Calender
  @OUVNegative_6
  Scenario: User enters email in invalid formate when user creates a Reservation from OUV Calender
    Given Access OUV Portal
    And Search Vehicle by giving Fleet "OUV Calendar,A,3"
    And The entry shows in Reservation Calendar in Days,Weeks and Months
    When Select vehicle and week for Reservation
    Then Make Reservation with invalid email format
    And verify the displayed format error message
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender using Incorrect Derivative Pack Description
  @OUV @OUV_Negative7
  Scenario: Searching a vehicle from OUV Calender	for Incorrect Derivative Pack Description
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect Derivative Pack Description as "Station Wagon XE" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender using Incorrect Paint Description
  @OUV @OUV_Negative8
  Scenario: Searching a vehicle from OUV Calender	for Incorrect Paint Description
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect Paint Description as "Golden" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender using Incorrect Trim Description
  @OUV @OUV_Negative9
  Scenario: Searching a vehicle from OUV Calender	for Incorrect Trim Description
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect Trim Description as "Ebony fabric seats with Ebony/Ebony exterior" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender using Incorrect Transmission Description
  @OUV @OUV_Negative10
  Scenario: Searching a vehicle from OUV Calender	for Incorrect Transmission Description
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect Transmission Description as "Auto 10 Speed Trans ZF 8H70" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal

  #Below is the scenario for Searching a vehicle from OUV Calender using Incorrect Option
  @OUV @OUV_Negative11
  Scenario: Searching a vehicle from OUV Calender	for Incorrect Option
    Given Access OUV Portal
    Then Navigate to OUV Calendar
    And Search a Vehicle by giving Fleet "OUV Calendar,A,3"
    And Enter Incorrect Option as "ABC" for searching vehicle in OUV Calendar
    And Search a Vehicle by Incorrect Data
    Then Logout from OUV Portal
